# Installation environnement de développement windows 10

## Serveur local

### 1. WampServer

  - Téléchargement : <http://www.wampserver.com/>.

  - **Attention** il faut installer des packages Visual C++ pour Visual Studio avant wampserver, vous trouverez les liens sur le forum de wampserver <http://forum.wampserver.com/read.php?1,137154>.
    + Chez moi les packages VC11, VC13 et VC15 ont suffis.
    + Si vous voulez être sûr de ne pas avoir de problème, il conseille d'installer tous les packages VC et dans les versions x86 et x64.
    + Si vous avez installés WampServer avant, il faudra le désinstaller.

  - Installation : il faut bien vérifier qu'il s'installe à la racine de `c:` .

  - Une fois wanpserver lancé vous devriez voir une nouvelle icône dans la zone de notification à droite de la barre des tâches.

  - Le compte de mysql et "phpMyAdmin" par défaut est `root` avec aucun mot de passe.

  - Pour changer la version de php il faut faire un clic gauche sur l'icone de wampserver dans la zone de notification puis allez dans php/version.

  - Le php.ini ce trouve dans `C:\wamp64\bin\php\php...`.

  - Les fichiers de configurations de phpMyAdmin se trouve dans `C:\wamp64\apps\phpMyAdmin...`.

  - Il va falloir ajouter les variables d'environnement pour php et mysql afin de pouvoir les utiliser en ligne de commande et ça sera aussi utile pour des packages VS Code.
    + Suivre ce tutoriel <https://helpdeskgeek.com/windows-10/add-windows-path-environment-variable/> et une fois que vous êtes sur la fenêtre de modification de `path`, il faudra faire nouveau et insérer le chemin d'accès au répertoire où se trouve le lanceur de php `C:\wamp64\bin\php\php7.2.4` et mysql `C:\wamp64\bin\mysql\mysql5.7.21\bin`.
    + Pour vérifier que c'est bon ouvrez votre shell et tapez ces deux commandes `php -v` puis `mysql -V`, sa doit vous afficher les versions de php et mysql.

## GIT

  - Téléchargement : https://git-scm.com/downloads
  
  - Installation :
    + `Ajusting your PATH environment` option -> `Use Git from Bash only`

    + `Choosing HTTPPS transport backend` option -> `Use the OpenSSL library`

    + `Configuring the line ending conversions` option -> `Checkout Windows-style, commit unix-style line ending`

    + `Configuring the terminal emulator tu use with Git Bash` option -> `Use MinTTY`

  - Avec le terminal Git Bash vous pouvez utiliser les mêmes commandes que sur le téléporteur `mkdir cd ls ...`, je vous conseille d'ailleurs d'utiliser celui-ci.

  - Raccourci pour copier `ctrl + inser` et pour coller `shift + inser`

  - Configuration de git & GitHub <https://github.com/O-clock-Alumni/fiches-recap/blob/master/ldc/git.md>
    + Vous trouverez votre clé SSH dans le dossier `C:\Users\nom-utilisateur\.ssh`

## Node.JS

  - Téléchargez la version LTS : https://nodejs.org/en/

  - Installation : rien à préciser.

## VS Code

  - Téléchargement : https://code.visualstudio.com/

  - Installation : 
    + `Tâches supplémentaires` vérifiez que l'option `Ajouter à PATH` est bien coché, ça vous permettra de lancer VS Code depuis le terminal avec la commande `code`(il faudra redémarrer votre PC pour que ça soit pris en compte).

  - Configuration :
    + User settings que vous trouverez dans `File->Preferences->Settings`
    + Voici mes user settings en exemple : 
    ```
    {
      "editor.tabSize": 2,
      "editor.renderWhitespace": "all",
      "editor.wordWrap": "wordWrapColumn",
      "editor.wordWrapColumn": 120,
      "[php]": {
          "editor.tabSize": 4  
      },
    }
    ```

### Extensions

#### ESLint 

  - Voir fiche récap <https://github.com/O-clock-Alumni/fiches-recap/blob/master/js/eslint.md#int%C3%A9gration-aux-%C3%A9diteurs-de-texte>
    + Pas de sudo sur Windows et il faut faire les 2 lignes de commandes qui installent les plugins.
    + Il faut créer le fichier `.eslintrc` dans `C:\Users\nom-utilisateur` via VS Code que sinon Windows risque de vous mettre un .txt en extension.

#### PHP IntelliSense

- Configuration :
    + Il faut ajouter `"php.suggest.basic": false,` dans les user settings pour ne pas avoir les suggestions en double.
