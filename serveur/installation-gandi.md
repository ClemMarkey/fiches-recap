# Exemple d'installation complète depuis un environnement Gandi

> La fiche récap suivante, part d'un serveur Gandi, installé avec une image Gandi d'un Ubuntu 18.04 LTS.
> Dans le cas d'un setup autre, quelques différences peuvent apparaître _(mariadb au lieu de mysql par exemple)_

> La fiche récap suivante, est sous une forme de tutoriel à suivre pas-à-pas.


## Connexion au serveur

### Gandi

<details>

Dans l'interface Gandi, repérer l'info bulle contenant l'adresse IP du serveur afin de s'y connecter en SSH.

![](img/installation-gandi.png)
</details>

### SSH

<details>

Ouvrir un terminal et entrer :

`ssh nomUtilisateur@adresseIP`

- `nomUtilisateur` correspond au nom d'administrateur _(admin par défaut)_ saisi lors de la création du serveur sur Gandi.
- `adresseIP` correspond à l'adresse IP précédemment trouvée sur l'interface de Gandi.

A la première connexion, il est possible d'avoir ce type de message :

![](img/installation-ssh.png)

Notre machine nous avertit simplement qu'elle ne connaît pas la machine distante (notre serveur) et nous demande si nous souhaitons bien nous y connecter.

Taper `yes`

Taper ensuite le mot de passe saisi lors de la création du serveur sur Gandi.

**:warning: sous linux lors d'une saisi d'un mot de passe, les caractères ne s'affichent pas. De plus le temps de saisie du mot de passe est limité par défaut à 60 secondes.**

Appuyer sur `Entrer` pour valider la saisie.

A la connexion un message d'accueil apparaît, puis laisse la main, nous sommes connectés:

`nomUtilisateur@nomServeur:~ $`

- `nomUtilisateur` correspond au nom d'administrateur _(admin par défaut)_ saisi lors de la création du serveur sur Gandi.
- `nomServeur` correspond au nom du serveur saisi lors de la création du serveur sur Gandi.
- `~` signifie que l'on se trouve dans le dossier `home` de l'utilisateur courant.

**:warning: toujours s'assurer que les commandes entrées le sont bien sur le serveur et non pas la machine locale.**

* Quelques commandes
  * Connexion: `ssh nomUtilisateur@adresseIP`
  * Déconnexion: `exit`
  * Devenir root _(super administrateur)_: `su`
  * Changer de dossier: `cd cheminDuDossier`
  * Editeur de texte en ligne de commande: `nano nomDuFichierAOuvrir`
    * Enregistrer: `ctrl` + `o`
    * Quitter: `ctrl` + `x`
  * [Fiche Récap - Terminal](https://github.com/O-clock-Alumnis/fiches-recap/blob/master/ldc/terminal.md)
</details>

## Mise à jour

<details>

En **root**, taper `apt update`

(si necessaire `su`)

_L'option update met à jour la liste des fichiers disponibles dans les dépôts APT présents dans le fichier de configuration /etc/apt/sources.list. L'exécuter régulièrement est une bonne pratique, afin de maintenir à jour votre liste de paquets disponibles._ [Source Ubuntu-fr](https://doc.ubuntu-fr.org/apt-get#mises_a_jour)

En **root**, taper `apt upgrade`

_L'option upgrade met à jour tous les paquets installés sur le système vers les dernières versions (couramment utilisé)._ [Source Ubuntu-fr](https://doc.ubuntu-fr.org/apt-get#mises_a_jour)

Taper `Y` pour valider la mise à jours des paquets listés.

Lors d'une première installation du serveur, il n'y a aucun risque de perte de donnée ou de disponibilité. Il est donc conseillé d'en profiter pour mettre à jour le serveur.

En cas de messages demandant une validation, valider le premier choix avec la touche `Entrer`

![](img/installation-utf8.png)
![](img/installation-grub.png)
> Exemple de messages demandant une validation

:warning: Si des messages concernant des locales apparaissent, regarder l'annexe [locale-settings](#locale-settings).

</details>

## Installations & configurations

### Apache2

#### Installation

<details>

En **root**, taper `apt install apache2`

Valider avec `Y`

Le `DocumentRoot` _(dossier racine du serveur web)_ se trouve dans `/var/www/html`

Il est possible de tester le bon fonctionnement en se rendant dans le navigateur Web à l'adresse IP du serveur.

![](img/installation-apache2.png)
</details>

#### Configuration

<details>

En **root**, taper `nano /etc/apache2/sites-available/000-default.conf`

> C'est le fichier de paramétrage par défaut de apache2

![](img/installation-apache2-config.png)

Après `DocumentRoot`, sauter une ligne et ajouter (en essayant de respecter l'indentation)

```
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```

- `Options Indexes FollowSymLinks` autorise l'indexation des fichiers & les liens symboliques
- `AllowOverride All` autorise l'utilisation du fichier `.htaccess`
- `Require all granted` aucune authentification n'est demandée

Enregistrer (`ctrl + O` puis `Entrer`) & quitter (`ctrl + x`).

En **root**, taper `a2enmod rewrite` pour activer le mod "rewrite"

En **root**, taper `service apache2 restart` pour redémarrer apache. Si aucun message n'apparaît, apache a correctement été relancé.
</details>

### MySQL

#### Installation

<details>

En **root**, taper `apt install mysql-server`

Valider avec `Y`

:warning: Pour info, le compte root n'est plus accessible qu'en ligne de commande `mysql` depuis un compte lui-même root ! Donc si on passe par phpMyAdmin, il faudra un autre compte.
</details>

#### Configuration

> Désormais le compte root MySQL n'étant disponible qu'en ligne de commande, nous allons devoir créer un nouveau "super-utilisateur"

<details>

- En **root** se connecter en ligne de commande à MySQL : `mysql`
- Créer un nouvel utilisateur :
  - personnaliser cette requête `CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';`
  - `newuser` = nom de l'utilisateur "superuser", par exemple, le même nom que le compte "admin" du serveur
  - `password` = à remplacer par le mot de passe complexe souhaité
  - coller la requête et l'exécuter
- Donner tous les droits sur toutes les bases de données pour cet utilisateur :
  - personnaliser cette requête `GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost' WITH GRANT OPTION;`
  - `newuser` = nom de l'utilisateur "superuser", celui de la requête précédente
  - coller la requête et l'exécuter
- Taper `exit` pour quitter
- :ok_hand:

</details>

### PHP7

#### Installation

> PHP7.2 va être installé avec une série de dépendances nécessaires au bon fonctionnement de la plupart des installations. Cette liste n'est en aucun cas exhaustive.

<details>

En **root**, taper `apt install php7.2 libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-gd php7.2-mbstring php7.2-intl`

Valider avec `Y`
</details>

#### Configuration

<details>

Afin de s'assurer que PHP est bien installé, taper `php -v`.

Afin de s'assurer que PHP est bien activé auprès de apache, en **root** taper `a2enmod php7.2`

En **root**, taper `service apache2 restart` pour redémarrer apache.
</details>

### phpMyAdmin

<details>

En **root**, taper `apt install phpmyadmin`

Valider avec `Y`

A l'apparition de la boite de dialogue, cocher `apache2` avec la touche `espace` du clavier, puis appuyer sur la touche `TAB` du clavier pour aller sur `<Ok>`
Valider avec `Entrer`

![](img/installation-phpmyadmin.png)

Aux boites de dialogue suivantes appuyer simplement sur `Entrer` (`<Ok>`)

Pour tester le bon fonctionnement, dans le navigateur Web entrer l'adresse IP du serveur suivi de `/phpmyadmin/`
Utiliser le compte crée dans la partie **MySQL** pour se connecter.

</details>

## Annexes

### Outils pratiques

#### zip / unzip

<details>

En **root**, taper `apt install zip unzip`

Pour zipper: `zip nomFichier.zip fichierAZipper`
Pour dézipper: `unzip nomFichier.zip`
</details>

#### git

<details>

En **root**, taper `apt install git`

[Fiche récap - Git & GitHub](https://github.com/O-clock-Alumnis/fiches-recap/blob/master/ldc/git.md)
</details>

#### curl

<details>

En **root**, taper `apt install curl`
</details>

### Configurer une BDD partagée

#### Autoriser MySQL

<details>

En **root**, taper `nano /etc/mysql/mysql.conf.d/mysqld.cnf`
ou `nano /etc/mysql/mariadb.conf.d/50-server.cnf` (MariaDB)

Rechercher la ligne `bind-address            = 127.0.0.1` et la commenter `#bind-address            = 127.0.0.1`

Enregistrer & quitter.

En **root**, taper `service mysql restart` pour relancer MySQL.
</details>

#### Si serveur AWS, autoriser le port 3306

<details>

Sur l'instance EC2, dans l'onglet `Description`, cliquer sur le groupe de sécurité activé. Dans le groupe de sécurité, sur l'onglet `Entrant`cliquer sur `Modifier`.
Ajouter une règle ayant pour type `MYSQL/AURORA` et pour source `n'importe où`.

</details>

#### Autoriser l'utilisateur

<details>

Dans phpMyAdmin, lors de la création de l'utilisateur, pour le paramètre `Nom d'hôte :` sélectionner `Tout client`

![](img/installation-bddpartage.png)
</details>

### Téléverser des fichiers

<details>

En **root**, taper `chown -R nomUtilisateur:www-data /var/www/html`

  - `chown` commande permettant de changer les propriétaires des fichiers/dossiers
  - `-R` argument permettant de spécifier la récursivité _(dossier/sous-dossier etc.)_
  - `nomUtilisateur` correspond au nom d'administrateur _(admin par défaut)_ saisi lors de la création du serveur sur Gandi.
  - `www-data` groupe du serveur apache. Cela permet de donner des droits à Apache sur les fichiers/dossiers.
  - `/var/www/html` DocumentRoot

**:warning: Sur la machine où sont présents les fichiers à envoyer _(le téléporteur par exemple)_**

Installer [FileZilla](https://filezilla-project.org/), sur le téléporteur: `sudo apt install filezilla`.

Dans FileZilla, cliquer sur le bouton permettant d'ouvrir le `Gestionnaire de Sites`

![](img/installation-filezilla-1.png)

Créer un `Nouveau Site`.
  * Dans `Hôte`, renseigner l'adresse IP du serveur _(juste l'IP **pas** de `http://`)_.
  * Dans `Protocole` sélectionner `SFTP`.
  * Dans `Type d'authentification` sélectionner `Normale`
    * Dans `Identifiant` renseigner le nom d'administrateur _(admin par défaut)_ saisi lors de la création du serveur sur Gandi.
    * Dans `Mot de passe` renseigner le mot de passe saisi lors de la création du serveur sur Gandi.

![](img/installation-filezilla-2.png)

A la première connexion, un message nous avertit que nous nous connectons pour la première fois sur cette machine. Cocher `Toujours faire confience à cet hôte, ajouter cette clé au cache`.

Il est ensuite possible de réaliser des glisser/déposer entre la machine locale (à gauche) et le serveur (à droite).

**:warning: Bien penser à se rendre dans le `DocumentRoot` _(/var/www/html)_**
</details>

### Problèmes "classiques"

#### Locale settings

En cas de message d'erreur suivant:

```
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LANG = "en_US.UTF-8"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
```

<details>

  - En **root**, taper: `export LANGUAGE=en_US.UTF-8`
  - En **root**, taper: `export LANG=en_US.UTF-8`
  - En **root**, taper: `export LC_ALL=en_US.UTF-8`
  - En **root**, taper: `locale-gen en_US.UTF-8`
  - En **root**, taper: `dpkg-reconfigure locales`

La commande `dpkg-reconfigure locales` va ouvrir une boite de dialogue. Sélectionner `en_US.UTF-8` et valider.

Pour finir, en **root**, taper `service apache2 restart`
</details>

#### No space left on device

Par exemple au redémarrage de MySQL le message suivant apparaît:

```
Failed to add /run/systemd/ask-password to directory watch: No space left on device
```

<details>

Bien vérifier qu'il reste de la place sur le disque avec un :

`df -h`

* Si il reste bien de la place

  * Pour corriger immédiatement :
    ```
    sudo -i
    echo 1048576 > /proc/sys/fs/inotify/max_user_watches
    exit
    ```

  * Puis solutionner de manière durable:
    ```
    fs.inotify.max_user_watches=1048576
    ```
* Si il n'y a plus de place
  * Supprimer des fichiers lourds :-)

</details>

#### phpMyAdmin non accessible

<details>

Bien vérifier que phpMyAdmin est installé:

Entrer la commande suivante : `whereis phpmyadmin`.

Si `whereis` retourne quelque chose comme :

```
phpmyadmin: /etc/phpmyadmin /usr/share/phpmyadmin
```

c'est que phpMyAdmin est bien installé.

Dans le cas contraire, retourner à l'installation de [phpMyAdmin](#phpmyadmin).

Comme phpMyAdmin est bien installé, il va falloir reconfigurer phpMyAdmin avec la commande suivante:

En **root**, taper `dpkg-reconfigure phpmyadmin`

Puis bien "cocher" avec la touche `espace` "apache2" comme sur l'image:

![](img/installation-phpmyadmin.png)

Puis continuer la reconfiguration en appuyant sur `entrer` (`<Ok>`).

Sauf besoin particulier, à la question concernant la création des tables, ne pas demander à phpMyAdmin de recréer ces propres tables.

</details>

### Erreurs dans phpmyadmin

- Le message d'erreur suivant apparait:
```“Warning in ./libraries/sql.lib.php#613 count(): Parameter must be an array or an object that implements Countable”```

<details>

Première étape, sauvegarder le fichier incriminé

En **root** taper la commande suivante:
`cp /usr/share/phpmyadmin/libraries/sql.lib.php /usr/share/phpmyadmin/libraries/sql.lib.php.bak`

Ensuite ouvrir le fichier avec nano:

En **root**, éditer le fichier:
`nano /usr/share/phpmyadmin/libraries/sql.lib.php`

Chercher `CTRL + W` dans le fichier le code suivant:
`count($analyzed_sql_results['select_expr'] == 1)`

Le remplacer par:
`((count($analyzed_sql_results['select_expr']) == 1)`

Enregistrer et quitter
`CTRL + X` et ensuite appuyer sur `Y` pour valider

</details>

- Le message d'erreur suivant apparait:
```Warning in ./libraries/plugin_interface.lib.php#551```

<details>

Première étape, sauvegarder le fichier incriminé

En **root** taper la commande suivante:
`cp /usr/share/phpmyadmin/libraries/plugin_interface.lib.php /usr/share/phpmyadmin/libraries/plugin_interface.lib.php.bak`

Seconde étape corriger le problème

En **root**, éditer le fichier
`nano /usr/share/phpmyadmin/libraries/plugin_interface.lib.php`

Chercher `CTRL + W` dans le fichier le code suivant:
`if (! is_null($options) && count($options) > 0) {`
ou bien (en fonction de la version de PMA)
`if ($options != null && count($options) > 0) {`

Le remplacer par:
`if (!empty($options)) {`

Enregistrer et quitter
`CTRL + X` et ensuite appuyer sur `Y` pour valider

</details>

- Une erreur apparait dans le concepteur

<details>

Première étape, sauvegarder le fichier incriminé

En **root** taper la commande suivante:
`cp /usr/share/phpmyadmin/libraries/pmd_common.php /usr/share/phpmyadmin/libraries/pmd_common.php.bak`

Seconde étape corriger le problème

En **root**, éditer le fichier
`nano /usr/share/phpmyadmin/libraries/pmd_common.php`

Chercher `CTRL + W` dans le fichier le code suivant:
`if (count($min_page_no[0])) {`

Le remplacer par:
`if (!empty($min_page_no[0])) {`

Enregistrer et quitter
`CTRL + X` et ensuite appuyer sur `Y` pour valider

</details>

### Mysql - Table 'mysql.user' doesn't exist:ERROR

<details>

Dans le terminal executer la commande suivante:
`mysql_upgrade -u root`

</details>

