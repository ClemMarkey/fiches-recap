## Pourquoi UML ?

**Constat** : l'informatique, c'est compliqué >_< et personne n'est d'accord sur ce qu'il convient de faire…

**UML** (_Unified Modeling Language_) : un outil visuel de modélisation, pour nous aider à spécifier, concevoir (_design_), construire, comprendre et valider un projet informatique. Et être sûr que tout le monde est raccord & d'accord…

> Analogie BTP : **UML == les plans d'un bâtiment, en théorie _VS_ le code == le bâtiment construit, en fonctionnement.**

## Ressources

Rien de mieux que le livre officiel : [_The Unified Modeling Language, Reference Manual, Second Edition_](https://www.utdallas.edu/~chung/Fujitsu/UML_2.0/Rumbaugh--UML_2.0_Reference_CD.pdf) de James Rumbaugh, Ivar Jacobson et Grady Booch.

Il y a énormément de ressources sur UML, de qualité variable. À jauger à l'aune du livre officiel.

## Généralités

Modéliser, c'est tenter de (re)présenter le réel de manière simplifiée mais utile et cohérente. On peut modéliser bien des choses, et de bien des manières : modélisation 3D (utile dans le BTP, les jeux-vidéos…), modélisation mathématique (avec des équations approchant le réel, utile en physique théorique et appliquée), modélisation par des post-its (utile pour les réunions au boulot, pour réfléchir à sa vie), modélisation avec des cailloux et des branches (bien pour… euh… faire un plan d'évasion d'une prison, un plan de bataille, etc.)

UML, en tant qu'outil de modélisation, s'intéresse plus particulièrement aux projets informatiques, et utilise comme _medium_ des schémas 2D, appelés « diagrammes UML » — il en existe différents types, car il y a toujours plusieurs points de vue à (re)présenter pour un même projet.

UML est un…

- _langage_ : UML définit un langage visuel 2D, avec un vocabulaire et des règles « grammaticales » précises ;
- de _modélisation_ : un diagramme UML (re)présente le plus clairement possibles les aspects importants d'un système à l'étude, selon un certain point de vue, et simplifie (voire ignore) le reste. Un même système est souvent (re)présenté par différents diagrammes, complémentaires !
- _unifié_ : UML est une boîte à outil généraliste (donc générique), unifiant différentes approches spécialisées plus anciennes et plus limitées. C'est un outil de conception moderne, on peut même mettre des couleurs :see_no_evil: !

UML est utile, car les diagrammes permettent entre autre :

- d'expérimenter (ils sont simples et peu coûteux à produire / modifier) ;
- d'anticiper des problèmes (grâce à la simplification du réel) ;
- de travailler à différents niveaux d'abstraction (plusieurs types de diagrammes, complémentaires) ;
- de formaliser une décision prise collectivement, ou de présenter une piste de réflexion ;
- de penser et contrôler la complexité (cas des systèmes rapides, chaotiques, dangereux, etc.) ;
- de contrôler la bonne implémentation et le bon fonctionnement du système modélisé.

En modélisant, on documente afin d'aider les différents intervenants du projet à réaliser leur travail et à comprendre celui des autres : client (Maîtrise d'œuvre), chef de projet, développeur (maîtrise d'ouvrage), etc. Comme chaque rôle a des besoins différents, il existe différents types de diagrammes UML. Chaque type de diagramme met l'accent sur une facette particulière de la modélisation, et c'est la complémentarité de toutes ces visions et approches qui permet d'avoir une vue d'ensemble cohérente et opérationnelle du projet.

Quand on réalise une modélisation UML, le résultat concret est avant tout… des idées (sous la forme de diagramme, à lire et interpréter). Au début de la vie d'un projet, les diagrammes sont _de facto_ perfectibles, car ils servent de point de départ pour amorcer la réflexion, ils comportent des erreurs ou des approximations (plus ou moins importantes). L'approche UML est donc empirique et itérative, il faut retravailler les diagrammes jusqu'à en être satisfait. Avec de l'expérience, l'équipe travaillant sur la phase d'avant-projet arrivent à cadrer de mieux en mieux les demandes client, ce qui facilite et accélère le travail de modélisation.

Les premiers modèles, dits de haut-niveau, offrent peu de détails sur le système, mais permettent d'en valider les grandes lignes architecturales et comportementales (le Quoi). Ces modèles sont ensuite complétés par des « zooms » successifs, jusqu'à arriver à une modélisation dite « d'implémentation » qui documente les algorithmes, structures de données, mécanismes de communication, etc. requis par les équipes de développement (le Comment).

Le concepteur-développeur peut donc utiliser UML pour couvrir tous les aspects de son travail, de l'analyse jusqu'à l'implémentation et la maintenance, aussi bien coté client que serveur.

> **D'après le lexique du titre pro CDA** :
> _Unified Modeling Language_. Formalisme basé sur les concepts de développement objet, qui permet de modéliser graphiquement une application informatique à toutes les étapes de son développement.

## UML est un outil, pas une méthode

On peut faire, informatiquement parlant, n'importe quoi avec UML ; de la même manière qu'on peut faire n'importe quoi avec un marteau. Il faut donc apprendre les « règles d'utilisation » d'UML. Ces règles sont entièrement spécifiées dans un corpus de documents créés par le _Object Management Group_ (_OMG_). On parle de méta-modèle UML. En tant que concepteur-développeur, ce corpus théorique n'est pas utile, mais c'est intéressant de savoir qu'il existe et qu'il y a une réflexion conceptuelle derrière UML, l'outil.

> Pour une grande part, UML s'inspire et synthétise des travaux théoriques et d'autres outils antérieurs (par exemple, la méthode [Merise](https://fr.wikipedia.org/wiki/Merise_(informatique))), et les outils de programmation orientée objet. UML est en effet historiquement issue de la mouvance POO : en 1995, formalisation & unification des différentes variantes de POO dans un document intitulé *Unified Modeling Language*, une spécification pour standardiser les langages de programmations orientés objet concrets, puis version finale d'UML 1.0 en 1997 et version 2.0 en 2005 avec l'intégration de formalisme de conception type Merise à chaque fois. UML est un outil de conception libre de droits et gratuit. Il existe par ailleurs des outils propriétaires et/ou payants pour _faciliter_ l'écriture et la gestion d'une modélisation UML, mais ils sont optionnels.

UML est un outil utilisable :
- avec n'importe quelle méthodologie de modélisation de données (ex. Merise) ;
- avec n'importe quelle méthode de gestion de projet. Peu importe qu'on travaille en mode _Waterfall_ ou _Agile_, à un moment donnée, souvent très tôt dans le cycle de développement, il faut modéliser et (re)présenter les données et leurs traitements pour arriver à y voir clair et prendre des décisions informées. Plusieurs outils sont efficaces à cette fin : use-cases, user stories, wireframes… et bien sûr, diagrammes UML.

## Vues statique et dynamique

Une des forces d'UML est de mettre l'accent sur deux types de représentation, chacunes déclinées en plusieurs types de diagrammes :
- l'architecture statique du projet/système (représentation des acteurs et des données) ;
- les relations dynamiques entre ses constituants (représentation des flux mobilisant les acteurs et des traitements sur les données).

**Structure statique (architecture / vue statique / les acteurs & données)** : les objets / constituants du système, leur implémentation statique, leurs relations de dépendance / contrôle.

**Relations dynamiques (comportements / vue dynamique / les flux & traitements)** : les états (et l'évolution de ces états) des objets au cours du temps, en fonction d'événements, et sous la contrainte de la structure statique.

## Les différents types de diagrammes

**UML est polyvalent** : la conception d'UML a été influencée par la programmation orientée objet, mais aussi par le _database design_ (ex. modèle Entity-Relationship / [MCD Merise](https://fr.wikipedia.org/wiki/Mod%C3%A8le_entit%C3%A9-association)), la modélisation d'état continu à transitions discrètes (ex. [*state machine*](https://fr.wikipedia.org/wiki/Automate_fini)), etc. Il existe donc de nombreux types de modélisation UML :

- diagramme _Use-case_
- diagramme de classe
- diagramme de séquence
- diagramme d'activité
- etc.

Toutes ces approches sont _unifiées_ par UML dans sa version 2.0, qui propose une boîte à outil standardisée. En UML, le vocabulaire est précis, les représentations grapniques sont normalisées. Pour autant, même si UML est centré sur l'univers informatique, ce vocabulaire et ces représentations graphiques peuvent être appliquées à différents domaines très facilement, même au-delà de l'informatique (auquel cas on s'aidera souvent d'une méthodologie de conception généraliste, type Merise).

> Si UML est au contraire trop générique pour modéliser un système informatique précis, on utilisera un _DSL_ (_Domain Specific Language_). Par exemple : le formalisme Gerkhin pour les user stories ; une syntaxe type `describe`/`it` pour les tests unitaires ; etc.

**UML n'est pas nécessairement utilisé pour représenter l'implémentation** : on s'intéresse souvent au _quoi_ (les acteurs & données) et au _comment_ (les flux & traitements), mais sans aller dans le détail du code. C'est-à-dire qu'on peut utiliser UML pour documenter un projet, peu importe son implémentation (en JS, PHP, Ruby, Python, etc.) Certains diagrammes (ex. de classe, d'objet) sont toutefois liés à l'implémentation et permettent d'avoir un niveau d'abstraction assez bas pour concrétiser les choses, au besoin.

## UML en pratique

Un projet informatique se conçoit sous plusieurs contraintes :

- les classiques _temps_ et _argent_ (budgétisation) ;
- les besoins clients (exprimés et analysés lors du cadrage).
- les technologies disponibles (coût, connaissances, praticité) ;

Ces contraintes jouent parfois dans le même sens, ou bien peuvent être contradictoires. Il faut donc faire des compromis, et documenter ces compromis (et les critères de validation qui vont avec).

### Architecture (_Static Views_)

On cherche ici à définir le « terrain de jeu », aussi appelé le domaine :

- quel est le but du système décrit par UML, à quoi et à qui sert t-il ?
- quels sont ses constituants (acteurs, objets) ?
- à quoi et à qui servent-ils respectivement ?
- quel constituant va devoir parler à quel autre, et pour lui dire quoi ?

L'architecture d'un système est une _vision statique_ de ce système (_Static view_). Ça n'empêche pas d'anticiper le coté dynamique de ce système, qui va probablement subir des événements et évoluer en réaction ; mais fondamentalement, l'architecture d'un système est un _invariant_, elle reste stable.

> Considérons par exemple une maison. Elle peut évoluer dans son usage, être entretenue (ou pas), habitée par différentes personnes au cours du temps, le contenu et la destination de ses pièces peut changer, etc. Mais en tant que _système_, elle reste _la même_ maison, avec son identité et son adresse postale spécifique.
> Éventuellement, l'architecture (càd la structure) de la maison peut également évoluer. Par exemple, une véranda peut être ajoutée, le toit peut être surélevé pour accepter un étage supplémentaire, etc. Un système dont l'architecture évolue trop souvent et/ou de façon non-contrôlé est peu fiable, coûteux et inadapté.

UML aide à anticiper les contraintes structurelles qui vont s'appliquer à un système, pour décider quelle architecture serait la plus adaptée et la plus fiable dans le temps pour modéliser puis construire ce système, tout en mimisant risques et coûts.

Dans le cas d'un projet informatique, comme une architecture équilibrée est souvent compliquée à imaginer d'emblée, on s'aide de représentations et de schémas (vues et diagrammes UML). Ces aides visuelles, cumulées et confrontées entre elles, permettent de documenter la structure du système à l'étude sous tous ses angles. Il s'agit de diagrammes de différents types, regroupés dans la catégorie _Static view_.

* [Diagramme de classe](./Diagramme-de-classe.md) (_Class diagram_)
* Diagramme de déploiement (_Deployment view & diagram_)

### Comportement (_Dynamic Views_)

Un projet informatique est mouvant, fait circuler de l'information qui subit des traitements (transformation, stockage…) en mobilisant des acteurs (internes et externes).

* [Diagramme de cas d'usage](./Diagramme-use-case.md) (_Use-case diagram_)
* [Diagramme d'état](./Diagramme-etat.md) (_State machine diagram_)
* Diagramme de séquence (_Sequence diagram_)
* Diagramme de communication (_Communication diagram_)
* Diagramme d'activité (_Activity diagram_)
