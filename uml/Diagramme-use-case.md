# Diagramme _Use-case_

Un diagramme de « cas d'usage » (_use-case_) représente le comportement d'un système, d'une classe, d'un composant… du point de vue de son utilisateur.

Il met en avant les interactions intéressantes pour l'utilisateur (et les acteurs du _use-case_ au sens large), souvent à un haut niveau d'abstraction (c'est-à-dire sans rentrer dans les détails). Pour un système donné, chaque fonctionnalité ou mode d'interactivité entre l'utilisateur et le système est un _use-case_, qu'il est possible de décrire par un diagramme.

Une interaction au cœur d'un _use-case_ tourne toujours autour d'une action ou d'un message initié par l'utilisateur principal du _use-case_, à destination du système lui-même ou à minima d'un acteur tiers (par exemple dans le cas où l'interaction avec le système n'est pas directe, mais passe par un intermédiaire, humain ou informatique).

Dans ce type de diagramme, tout est plus ou moins simplifié et idéalisé. Par exemple, les acteurs sont quasiment équivalents à des rôles, qui peuvent être assumés par des humains, des processus informatiques ou logistiques, des « trucs », etc. Les interactions entre acteurs, quant à elles, sont limitées à ce qui permet de modifier l'état du système au global et d'atteindre un objectif (la fonctionnalité décrite étant un moyen et pas une fin). C'est donc une vision relativement statique du système. Pour aller dans le détail, on précisera le fonctionnement dynamique et interne avec des diagrammes de séquence, d'activité, etc.

En terme de formalisme UML, un diagramme _use-case_ utilise les mêmes codes qu'un diagramme de classe : association, héritage, inclusion, généralisation, etc. En effet, on peut considérer qu'un _use-case_ est un peu comme une classe qui ne possèderait qu'une seule opération (méthode), invocable par l'acteur principal, avec cette particularité que les acteurs du _use-case_ peuvent continuer à intéragir avec l'opération une fois celle-ci lancée (ce qui n'est pas le cas pour une méthode d'une classe).

## Exemple

* Objectif : manger un repas à l'extérieur
* Fonctionnalité (_use-case_) : manger au restaurant
* Acteur principal : le client
* Acteur secondaire : le serveur
* Acteur tiers : les cuisines, le restaurant (lieu)…

![Use-case restaurant](./use-case/use-case-restaurant.png)