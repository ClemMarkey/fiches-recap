# Diagramme d'état (_state machine_)

Il permet de décrire le comportement dynamique d'un objet ou d'un ensemble d'objets, c'est-à-dire leur évolution au cours du temps. On s'intéresse ici au cycle de vie d'un objet, sous le contrôle d'évènements.

Les évènements représentent des messages externes que l'objet est capable de détecter (soit par observation active, soit par réception passive). En réaction à un évènement, l'objet va faire évoluer son état interne grâce à une action (ou activité) qui, en général, dépend au minimum de l'état interne courant.

Ces évolutions d'état sont prédictibles, c'est-à-dire que si plusieurs objets sont similaires (ex. des instances d'une classe) et dans le même état, alors ils réagiront de la même manière à un évènement.

L'intérêt des diagrammes d'état est de donner une vue dynamique d'entités normalement décrites de façon statique, par exemple des classes ou des _use-cases_. Concrètement, un diagramme d'état est un graphe dont les nœuds sont des états et les arrêtes des évènements.

![Exemple basique](./state/state-basic.png)

Un bon diagramme d'état explicite tous les scénarios possibles, c'est-à-dire tous les états atteignables au cours du cycle de vie, et toutes les transitions possibles entre ces états. Les évènements et le cycle de vie sont complètement décrits par le diagramme, du point de vue de l'entité dont on décrit l'évolution.

## États

Tous les états sont représentés de la même manière, sans détails particuliers, sous la forme d'un rectangle aux coins arrondis.

![](./state/state-state.png)

Il est possible de préciser le fonctionnement interne d'un état, en explicitant des activités (c'est-à-dire ce qu'il se passe dans le système, lorsqu'il est dans cet état).

![Exemple basique](./state/state-detailled.png)

Il existe deux états particuliers :

* l'état de départ ![](./state/state-start.png)
* l'état de fin ![](./state/state-final.png)

## Évènements

On fait la distinction entre quatre types d'évènements :

* évènement synchrone (_call event_)
* évènement asynchrone (_signal event_)
* évènement sous contrainte (_when event_, condition booléenne)
* évènement de fin (_time event_, timeout)

Pour être plus précis, un évènement est représenté sous la forme d'une transition d'état, qui est caractérisée par :

* un déclencheur
* une éventuelle condition booléen (_guard_)
* un effet (activité)
* un état cible

Il y a quatre types d'effets représentables en UML :

* effet d'entrée, déclenché lorsqu'un état devient actif suite à une transition vers cet état
* effet de sortie, déclenché lorsqu'un état devient inactif suite à une transition vers un autre état
* effet externe
* effet interne

Comme dans un diagramme d'état UML, les évènements / transitions d'état sont représentés par des flèches, des annotations sur ces flèches permettent de préciser tous ces effets et leurs contraintes.

![](./state/state-exemple.png)

## Exemples

![Exemple State machine Keyboard capslock](examples/examples-state-keyboard-capslock.png)

---

![Exemple State machine React Component Lifecycle](examples/examples-state-react-component-lifecycle.png)