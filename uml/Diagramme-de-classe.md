# Diagramme de classe

Dans la plupart des langages orientés-objet, le modèle objet est basé sur les classes. UML permet de représenter ces classes dans une vue statique (le comportement _at runtime_ des classes à travers leurs instances/objets n'est pas explicité).

> Pour les langages à modèle prototypal (ex. JavaScript), on considèrera simplement que les fonctions-constructeurs sont des classes, le formalisme UML restant le même.

## Entités

* Un **objet** est une entité discrète/précise ayant une identité persistante, un état (attributs/propriétés) et des comportements invocables (méthodes).
* Une **classe** représente un concept discret/précis d'une application, modélisé en UML sous forme d'une boîte rectangulaire décrivant l'API de la classe en terme de structure, de comportements et de relations. Les instances d'une classe (objets) partagent tous cette API.
* Un **diagramme de classe** se focalise sur les classes, pas les instances/objets. Néanmoins, il y a toujours l'idée sous-jacente que les classes seront mobilisées _at runtime_ sous la forme d'objets, et un certain nombre d'informations à propos de ces objets peuvent être déduites du diagramme de classe statique.

UML représente une classe sous la forme d'une rectangle en trois parties :

* nom de la classe (en italique si classe abstraite)
* (optionel) section des attributs/propriétés d'instance
* (optionel) section des opérations (implémentées sous la forme de méthodes)

Exemple de boîtes représentant une entité de type classe :

![Diagramme de classe (entité)](./entities/entities-Class-simple.png)
![Diagramme de classe (entité)](./entities/entities-Class-detailled.png)

Les attributs et opérations faisant partie de l'API publique sont préfixés d'un `+`, les éléments privés d'un `-`, les éléments protégés d'un `~`. Les informations de typage sont optionnelles, mais si présentes, doivent référencer soit des types natifs du langage d'implémentation, soit d'autres entités (classes, interfaces, etc.)

> Dans un modèle prototypal, les diagrammes de classes deviennent des diagrammes de prototypes/constructeurs. Le formalisme UML reste identique.

Pour expliciter le fonctionnement d'un programme, un diagramme de classe peut représenter deux types d'interactions entre des classes : leurs _relations_ et leurs _associations_.

## Relations

Un diagramme de classe met le plus souvent en œuvre plusieurs classes, qui interagissent entre elles. Lorsque l'interaction est basée sur une dépendance mutelle, on parle de relation. Ces relations sont qualifiées et représentées par des traits et des flèches connectant les boîtes.

> Dans le formalisme UML, les annotations textes sur les flèches sont optionnelles.

### Association simple

_Un simple trait._

![Relation d'association](./relationships/relationships-association.png)

Décrit le fait que des _instances_ de classes (objets) sont en relation de travail dans l'application, sans plus de détails. La relation est basée sur l'identité des objets, sauf si spécifié autrement. Il n'y a pas de notion de hiérarchie, de dépendance, etc. (même si elles peuvent exister, auquel cas c'est simplement que le choix a été fait de ne pas le représenter dans le diagramme).

### Généralisation (héritage & polymorphisme)

_Une flèche à trait plein et tête triangulaire._

![Relation de généralisation](./relationships/relationships-generalization.png)

Permet de décrire la relation entre une classe parente (aussi dite super-classe) et une ou plusieurs classes enfants (aussi dites sous-classes). De façon générale, une sous-classe est plus précise/concrète qu'une classe parente, qui constitue donc une généralisation (elle se situe à un niveau d'abstraction supérieur).

Une sous-classe est complètement décrite par la fusion de sa spécification propre et celle de sa classe parente. On évite ainsi de se répéter, en mettant en commun des attributs, opérations et relations.

UML permet de décrire, à l'aide de la relation de généralisation, aussi bien l'héritage (généralisation verticale, par lien hiérarchique) que la composition (généralisation horizontale, par fusion de _mixins_/traits). Le polymorphisme (design pattern `State` ou `Strategy`) est représenté sous cette forme.

### Dépendance

_Une flèche à trait pointillé court et tête évidée._

![Relation de dépendance](./relationships/relationships-dependency.png)

Permet de décrire le fait qu'une classe expose une API utilisée (toute ou partie) par une autre classe.

Cette relation de dépendance est similaire au modèle client/serveur. Si l'API du serveur (classe requise) change, alors l'implémentation du client (classe utilisant la classe requise) est impactée.

Il existe différentes formes de dépendances, qui peuvent être précisées sur le diagramme par des annotations ou des commentaires :

* injection de dépendance (récupération d'objets en paramètres)
* dépendance vers une spécification (implémentation d'une interface)
* _binding_ (association _at runtime_ de valeurs spécifiques dans un _template_)
* …

#### Cas particulier : injection de dépendance (usage)

_Une flèche à trait pointillé moyen, à tête évidé, annoté._

![Relation d'injection de dépendance (usage)](./relationships/relationships-usage.png)

Dans le cas d'une injection de dépendance (par exemple via le constructeur de la classe), il est possible d'annoter la relation pour préciser le nom de l'injection.

#### Cas particulier : réalisation (implémentation d'une interface)

_Une flèche à trait pointillé moyen et tête pleine._

![Relation de réalisation](./relationships/relationships-realization.png)

La relation dite de « réalisation » permet en toute généralité de décrire le fait qu'une entité est reliée à une spécification. Dans le cas particulier d'une classe, elle permet de spécifier qu'une classe implémente une interface.

> Rappel : une interface est la description d'une API (attributes, opérations), sans la partie implémentation.

### Aggrégation

_Un trait plein avec un diamant plein coté agrégat._

![Relation d'aggrégation](./relationships/relationships-aggregation.png)

Permet de décrire une relation de type composition, où une entité (agrégat) est composée de sous-partie (composants).

> Une relation binaire (entre deux composants, organisés « hiérarchiquement ») peut être représentée comme une relation d'aggrégation, par exemple pour insister sur la structure interne du système.

Il est possible de décrire des compositions libres, où les composants peuvent être partagés entre plusieurs aggrégats, et des compositions strictes, où un composant ne peut appartenir qu'à un seul aggrégat. Les arbres de composants (ex. DOM du navigateur) sont des compositions strictes. Il est également possible de décrire des compositions récursives.

Sémantiquement, une _aggrégation_ de composants est une association générique, sans plus de précisions quant à la gestion des composants ; tandis qu'une _composition_ est une association tout-partie dans laquelle l'aggrégat possède seul la responsablité de gérer ses composants internes, sur l'ensemble de leurs cycles de vie. Pour distinguer les deux, l'aggrégation simple est représentée par un diamant évidé, tandis que l'association est représentée par un diamant plein.

## Associations cardinales

Une association cardinale concrétise une interaction entre deux classes, en se focalisant sur la _cardinalité_ d'une association entre ces deux classes, à travers leurs instances.

Concrètement, UML permet de préciser, à partir d'une relation de type _association simple_, si deux classes sont en relation 1-to-1, 1-to-many, etc. Ces associations se manifesteront _at runtime_ par des liens concrets entre des objets concrets, ces liens étant créés et détruits sur la base de l'identité des objets.

> À bien distinguer d'une _relation_, qui modèlise une interaction entre deux classes à un niveau d'abstraction supérieur, à savoir les classes elle-mêmes, par leurs instances.

Formalisme : _un trait plein avec éventuellement des annotations aux extrémités._

### Association asymétrique

Instance de la classe A reliée à 0, 1 ou n instance(s) de la classe B.

![Association cardinale](./relationships/relationships-cardinality-any.png)

---

Instance de la classe A reliée à exactement 1 instance de la classe B.

![Association cardinale](./relationships/relationships-cardinality-exactly-1.png)

---

Instance de la classe A reliée à exactement 0 ou 1 instance de la classe B.

![Association cardinale](./relationships/relationships-cardinality-0-or-1.png)

---

Instance de la classe A reliée à 1 ou n instance(s) de la classe B.

![Association cardinale](./relationships/relationships-cardinality-at-least-1.png)

---

Instance de la classe A reliée à exactement 10 à 50 (exclus) instances de la classe B.

![Association cardinale](./relationships/relationships-cardinality-ten-to-fifty.png)

### Association symétriques (type MCD)

Instance de la classe A reliée à 1 instance de la classe B, et inversement ([one-to-one](https://en.wikipedia.org/wiki/One-to-one_(data_model))).

![Association cardinale](./relationships/relationships-cardinality-1-to-1.png)

---

Instance de la classe A reliée à des instances de la classe B, et chaque instance de B reliée à 1 instance de A ([one-to-many](https://en.wikipedia.org/wiki/One-to-many_(data_model))).

![Association cardinale](./relationships/relationships-cardinality-1-to-many.png)

---

Instance de la classe A reliée à des instances de la classe B, et inversement ([many-to-many](https://en.wikipedia.org/wiki/Many-to-many_(data_model))).

![Association cardinale](./relationships/relationships-cardinality-many-to-many.png)

## Exemples de diagrammes de classe

Une classe `Window` implémentant deux interfaces :

![Exemple réalisation](./examples/examples-realization.png)

---

Un pays possède une, et une seule, capitale :

![Exemple réalisation](./examples/examples-cardinality-1-to-1.png)

---

Un livre possède au moins un chapitre, potentiellement plusieurs :

![Exemple réalisation](./examples/examples-cardinality-1-to-many.png)

---

Un livre peut être associé à zéro (anonyme), un ou plusieurs auteurs ; un auteur peut avoir écrit zéro (pas encore publié/actif), un ou plusieurs livres :

![Exemple réalisation](./examples/examples-cardinality-many-to-many.png)

---

![Exemple aggrégation](./examples/examples-aggregation.png)

---

Exemple d'aggrégation récursive : dans ce système, un élément de type bloc peut contenir un autre élément de type, qui lui-même peut… Par contre, un élément de type _inline_ ne peut pas contenir d'autres éléments, ce qui stoppe la récursion.

![Exemple aggrégation](./examples/examples-aggregation-recursive.png)

---

Dans ce système, les _donations_ sont modélisées comme l'association entre un _donneur_ et ses _dons_. En terme de formalisme UML, l'association entre les classes `Donor` et `Grant` est qualifiée en tant que `Donations`, avec des rôles respectifs `Donor` et `Donation`, dans une relation 1-to-many.

![Exemple réalisation](./examples/examples-relationship.png)

---

Un diagramme de classe complet (design pattern _Strategy_), avec annotations (en bleu et gris) et commentaires (en orange). Dans cet exemple, la classe `Order` implémente l'interface `OrderI`, mais potentiellement pas seulement car il y a un lien d'aggrégation entre les deux (donc `Order` est composite). `OrderI` est relié à l'interface `ShippingI`, sans plus de précision, mais on déduit des opérations visibles dans l'API de `OrderI` que `ShippingI` fournit au moins la spécification de `getShippingCost`. Par ailleurs, deux classes concrètes (`ChronopostExpress` et `Normal`) réalise une implémentation par polymorphisme de cette interface.

![Exemple complet](./examples/examples-class-diagram.png)
