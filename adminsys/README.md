# Administration Système

Pour gérer les serveurs de production (installés dans des Datacenters), on va passer par une connexion à distance sécurisée en ligne de commande : _connexion SSH_

Ensuite, il faudra que les logiciels nécessaires au fonctionnement d'un site Web soient installés sur le serveur.

Dans le cas d'un projet PHP, ce sera :
- Serveur Web : Apache
- Langage de programmation côté serveur : PHP
- Serveur de base de données : MySQL ou MariaDB ou PostGreSQL
- Interface de gestion de la base de données : PHPMyAdmin ou Adminer
- Logiciel de versioning : Git

Dans le cas d'un projet NodeJS, ce sera plutôt : 
- Serveur Web : NodeJS
- Langage de programmation côté serveur : Javascript
- Serveur de base de données : MySQL ou MariaDB ou PostGreSQL
- Interface de gestion de la base de données : PGAdmin ou Adminer
- Logiciel de versioning : Git

## Amazon Web Services

On peut facilement se créer des instances de serveurs virtuels avec AWS.  
Pour cela, on doit disposer d'un compte AWS Educate Student.

- [Création d'un compte Amazon Educate Student](aws/create_account.md)
- [Création d'un serveur Cloud AWS EC2](aws/create_server.md)
- [Connexion et installation des logiciels pour un projet PHP](aws/install.md)
- [Connexion et installation des logiciels pour un projet NodeJS](aws/install_node.md)

