# Connexion & Installation

## Connexion SSH

On va avoir besoin du fichier `.pem` téléchargé lors de la création de l'instance EC2.

- ouvrir un terminal sur notre ordinateur, dans le dossier contenant le fichier `.pem`
- exécuter la commande suivante : `ssh -i mon-fichier.pem ubuntu@dns-public.amazonaws.com`
  - `mon-fichier.pem` correspond au fichier `.pem` téléchargé
  - `ubuntu@` car on a crée un serveur Ubuntu, et le username par défaut est `ubuntu`
  - `dns-public.amazonaws.com` à remplacer par la _DNS public_ de ton instance EC2
- pas besoin de mot de passe, c'est le fichier `.pem` qui fait office d'authentification
- une fois exécutée, si c'est la première connexion au serveur, on vous demandera de confirmer la connexion car ce serveur ne fait pas partie des _know hosts_ => `yes`, on confirme :wink:
- là, on est refusé car le fichier `.pem` autorise l'accès au _group_ et _others_
- on exécute cette commande permettant de restreindre l'accès au _user_ ou _owner_ uniquement : `chmod 600 mon-fichier.pem`
  - `chmod` permet de changer les droits d'accès / permissions
  - `600` accès en lecture et écriture pour le propriétaire, rien pour les autres
  - `mon-fichier.pem` correspond au fichier `.pem` téléchargé
- on réexécute la commande `ssh -i etc.` (utilise les flèches haut et bas, c'est génial !)

### Option : Déplacement du fichier de clé à un endroit plus adapté

Pour pouvoir nous connecter en ssh depuis n'importe quel dossier, on va déplacer la clé dans un dossier prévu à cet effet : `/home/etudiant/.ssh`, (on part du principe que la clé est dans le dossier _Téléchargements_ de votre répertoire personnel).

```
mv /home/etudiant/Téléchargements/votre_cle.pem /home/etudiant/.ssh
```
Maintenant nous pouvons accéder au serveur depuis n'importe quel emplacement via :

```
ssh -i /home/etudiant/.ssh/votre_cle.pem ubuntu@dns-public.amazonaws.com
```

## Installation de logiciels

L'accès au serveur se fait uniquement par ligne de commande.  
On va donc installer les logiciels avec les commandes suivantes.

### Apache

```
sudo apt-get update
sudo apt-get install apache2
```

### PHP 7.4

```
# sudo add-apt-repository ppa:ondrej/php - n'est plus nécessaire depuis Ubuntu 20.04
sudo apt-get update
sudo apt-get install php7.4 php7.4-common php7.4-cli php7.4-mysql libapache2-mod-php7.4 php7.4-mbstring php7.4-json php7.4-xml
```

### MySQL

```
sudo apt-get install mysql-server
```

Si vous souhaitez permettre un accès à la base de donnée depuis l'exterieur (par exemple pour une base de donnée partagée d'apothéose :wink:) :

Modification de la configuration de mysql
```
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
```
puis remplacer `bind-address = 127.0.0.1` par `bind-address = 0.0.0.0`

On redémarre mysql : `sudo service mysql restart` 

Pour permettre l'accès depuis l'exterieur avec un utilisateur : `'toto'@'%'`

### Création d'un super-utilisateur

Nous allons créer un super-utlisateur autre que `root` afin de gérer notre serveur MySQL.

On se connecte à la console de mysql via `sudo mysql` puis on va entrer les requêtes SQL ci-dessous.

Pour un utilisateur ayant l'identifiant `toto` et le mot de passe `password` :

> Ne pas mettre `toto` et `password` bien sûr :wink:

```sql
CREATE USER 'toto'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'toto'@'localhost' WITH GRANT OPTION;
```

Vous pourrez utiliser ce compte pour gérer vos bases depuis PMA.

### PhpMyAdmin

```
sudo apt-get install phpmyadmin
```

A la première question on répondra `Ok` ou `Yes` (installation d'un package supplémentaire et création d'un mot de passe automatique pour PMA).

:hand: Quand l'install vous demande quel serveur web vous utilisez, cochez `apache2` avec la barre d'`Espace` ! Un astérisque `*` valide le choix, ensuite faites `Entrée` ou `Ok`.

> Si vous vous êtes trompé suite à ce choix, après l'install, vous pouvez relancer la configuration via `sudo dpkg-reconfigure phpmyadmin`

Connectez-vous à PMA depuis l'URL de notre serveur `http://votre_serveur_aws/phpmyadmin` et le compte créé juste au-dessus.

### Git (normalement déjà installé)

```
sudo apt-get install git
```
### Zip

```
sudo apt-get install zip
```

### Composer

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```

## Configurations

### DocumentRoot Apache

`DocumentRoot`, c'est le dossier contenant les fichiers du site livré par Apache.  
Par exemple, dans la VM (comme dans ce serveur), c'est `/var/www/html`

Suppression du fichier `index.html` par défaut d'Apache 2  
Et modification des droits d'accès du `DocumentRoot`

```
sudo rm -f /var/www/html/index.html
sudo chown -Rf ubuntu:www-data /var/www/html
```

### URL Rewriting

```
sudo a2enmod rewrite
```

Dans /etc/apache2/apache2.conf

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```
Puis

```
sudo systemctl restart apache2
```

### Git

On va créer une clé SSH pour notre compte _GitHub_ afin que le serveur se connecte à nos dépôts _GitHub_ facilement.  
Ainsi, on pourra cloner nos dépôts et mettre en production nos sites internet :nerd_face:

[Fiche récap](https://github.com/O-clock-Alumni/fiches-recap/blob/master/ldc/git.md#cr%C3%A9er-une-cl%C3%A9-ssh)

```
ssh-keygen -t rsa
```

La commande est interactive, voici un exemple :  
(on a laissé volontairement toutes les réponses vides)

```

Generating public/private rsa key pair.
Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ubuntu/.ssh/id_rsa.
Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:NW6UvsvzP9s6hRPRRL8ZE+MlFU0Xub/k299qPL+ETUM ubuntu@ip-172-31-36-116
The key's randomart image is:
+---[RSA 2048]----+
|              .@%|
|           .  ooO|
|          =    Eo|
|         = .  o.=|
|        S +    B.|
|         . .  *.+|
|          .  oo=.|
|         ...  B+o|
|          oo.o=@X|
+----[SHA256]-----+

```

**Vérifier la création de la clé publique et copier le contenu du fichier qui va être affiché**

```
cat ~/.ssh/id_rsa.pub
```

**Ajouter la clé SSH à son compte _GitHub_**

- aller à l'URL https://github.com/settings/profile
- `SSH & GPG keys` dans le menu de gauche
- bouton `New SSH Key` à droite en haut
- nommer la clé "AWS ec2"
- coller le contenu du fichier dans le grand "textarea"
- "ok"

**Configurer Git sur le serveur**

[Fiche récap](https://github.com/O-clock-Alumni/fiches-recap/blob/master/ldc/git.md#configuration-locale-de-git)

Les deux premières commandes sont à personnaliser avec le pseudo et l'email de son compte _GitHub_

```
git config --global user.name "github_username"
git config --global user.email "github_email@my.com"
git config --global push.default simple
git config --global color.ui true
```

Et pour vérifier (chaque configuration doit apparaitre) :

```
git config -l
```

## It's alive :tada:

Le serveur est prêt.  
Tu peux y accéder dans le navigateur sur le domaine note dans "DNS public" dans la console AWS.

:bulb: Pour tester, tu peux cloner dans `/var/www/html` un dépôt de la saison 1... par exemple _Hero Corp_ :wink:
