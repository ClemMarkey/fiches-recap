# Connexion & Installation

## Connexion SSH

### Si vous avez créé une nouvelle paire de clés

On va avoir besoin du fichier `.pem` téléchargé lors de la création de l'instance EC2.

- ouvrir un terminal sur notre ordinateur, dans le dossier contenant le fichier `.pem`
- exécuter la commande suivante : `ssh -i mon-fichier.pem ubuntu@dns-public.amazonaws.com`
  - `mon-fichier.pem` correspond au fichier `.pem` téléchargé
  - `ubuntu@` car on a crée un serveur Ubuntu, et le username par défaut est `ubuntu`
  - `dns-public.amazonaws.com` à remplacer par la _DNS public_ de ton instance EC2
- pas besoin de mot de passe, c'est le fichier `.pem` qui fait office d'authentification
- une fois exécutée, si c'est la première connexion au serveur, on vous demandera de confirmer la connexion car ce serveur ne fait pas partie des _know hosts_ => `yes`, on confirme :wink:
- là, on est refusé car le fichier `.pem` autorise l'accès au _group_ et _others_
- on exécute cette commande permettant de restreindre l'accès au _user_ ou _owner_ uniquement : `chmod 600 mon-fichier.pem`
  - `chmod` permet de changer les droits d'accès / permissions
  - `600` accès en lecture et écriture pour le propriétaire, rien pour les autres
  - `mon-fichier.pem` correspond au fichier `.pem` téléchargé
- on réexécute la commande `ssh -i etc.` (utilise les flèches haut et bas, c'est génial !)

### Si vous avez utilisé une clé existante

Rien à faire ! La clé SSH est déjà dans votre machine. 

Il vous suffit d'ouvrir un terminal et lancer `ssh ubuntu@<votre-dns-public.amazonaws.com>`


## Créer la DB
2 solutions : 
- Installer et configurer une BDD directement sur l'instance EC2 (solution recommandée pour débuter)
    - `sudo apt install postgresql postgresql-contrib`
    - puis [fiche récap !](https://github.com/O-clock-Alumni/fiches-recap/blob/master/bdd/confg-postgres.md)
- [Utiliser une instance RDS](./rds.md)

## Mise en place du serveur

- Mettre à jour les repolist de apt : `sudo apt update`,
- installer git: `sudo apt install git`,
- installer node : https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html,
- créer une clef ssh pour git : https://github.com/O-clock-Alumni/fiches-recap/blob/master/adminsys/aws/install.md#git-1.

## C'est tarpi !

Une fois toutes ces étapes réussies, il ne vous reste qu'à cloner votre code depuis GitHub, installer les dépendances avec npm, et lancer le tout avec `node` !

## Accèder à mon serveur sans spécifier de port

Lancer un serveur Node sur le port 80 (le port HTTP par défaut) n'est pas possible : il nous faut les droits "super utilisateur".

Or, lancer Node en "mode sudo" est considéré comme un très mauvaise pratique : Node sait lire/écrire/modifier n'importe quel ressource ! En mode Sudo, le moindre code "risqué" peut se transformer en bombe à retardement - voir pire, en faille de sécurité !

Une astuce répandue consiste à lancer le serveur Node de manière classique (sur un port à 4 chiffres - disons 5050), et à rediriger les requêtes qui arrivent sur le port 80 vers le port 5050. 

`sudo iptables -tnat -A PREROUTING -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 5050`

C'est un win-win : Node garde des droits limité (et ne peut donc pas effacer des fichiers importants), mais vous accédez quand même à une adresse simple, sans rajouter de port.
