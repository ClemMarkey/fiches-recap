# Utiliser une BDD RDS

## Étape 1 : créer l'instance RDS 

Accèdez au services RDS via `Services->RDS`

Cliquez sur "créer une nouvelle base de données"

![aws-rds-step1](https://user-images.githubusercontent.com/3660402/73179979-b3ce1400-4114-11ea-8ef2-8ce177554d92.png)

Sélectionnez le moteur de base de donnée PostgreSQL

![aws-rds-step2-moteur-select](https://user-images.githubusercontent.com/3660402/73179984-b6c90480-4114-11ea-8284-6826bcb52bf8.png)

Sélectionnez bien l'offre gratuite :

![aws-rds-step3-offre-gratuire](https://user-images.githubusercontent.com/3660402/73180002-baf52200-4114-11ea-99bd-4da26214a1ff.png)

Ensuite choisissez un nom explicite pour base de donnée, par exemple okanban.

![aws-rds-step4-okanban-named](https://user-images.githubusercontent.com/3660402/73251353-17aa1880-41b9-11ea-8473-b663b46bcf6f.png)

Ensuite cochez l'option `Laissez AWS générer un mot de passe`

![aws-rds-step5-credential](https://user-images.githubusercontent.com/3660402/73180023-bf213f80-4114-11ea-9b3f-b7c0e0da7ca1.png)

Vous pouvez garder le reste des options par défaut et valider.

Quand votre instance sera créée vous aurez une notification pour aller visualiser vos accès login/motdepasse, notez les bien dans en endroit sécurisé.

![aws-rds-step6-view-credential](https://user-images.githubusercontent.com/3660402/73180030-c21c3000-4114-11ea-8afc-9c657901df88.png)


On attends un peu, et on retourne sur l'accueil de RDS.

Dans le menu à gauche, cliquer sur "bases de données", cliquer sur "okanban", vérifier que tout va bien (infos: "disponible" ou "sauvegarde en cours")

Vous pouvez maintenant noter le point de terminaison de votre base de données, c'est en fait le nom d'hôte du serveur dédié qui va héberger votre serveur PostgreSQL, il faudra utiliser ce nom pour vous y connecter dans votre applicatif et depuis la ligne de commande. Dans notre cas c'est okanban.cnybqcavqa5t.us-east-1.rds.amazonaws.com

![aws-rds-step7-get-endpoint](https://user-images.githubusercontent.com/3660402/73181153-f8f34580-4116-11ea-9548-3907323d66cb.png)

## Étape 2, connection et mise en place de la DB

- Depuis votre instance EC2 (en SSH), installer le client postgres (pour la commande psql) : `sudo apt install postgresql-client`
- se connecter à l'instance RDS : `psql -U postgres -h votre.addresse.rds.amazonaws.com` (puis coller le mdp généré à l'étape précédente)
- creer un user : `CREATE ROLE okanban WITH LOGIN PASSWORD 'okanban';`
- creer une db : `CREATE DATABASE okanban OWNER okanban;`
    - si erreur (must be member of role "okanban"), on fait l'opération en 2 fois : 
    - `CREATE DATABASE okanban;`
    - `ALTER DATABASE okanban OWNER TO okanban;`
- Ctrl-D pour sortir de psql
- optionnel, pour vérifier, re-rentrer mais en tant que "okanban" :  `psql -U okanban -h votre.addresse.rds.amazonaws.com`