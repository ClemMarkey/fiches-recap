# Créer un compte AWS Educate

Un compte AWS Educate permet :

- d'accéder aux services AWS sans avoir à saisir de carte bancaire
- d'avoir un crédit en $ à dépenser dans la création de serveurs

## Invitation

- Tu as dû recevoir un email t'invitant à rejoindre _AWS Educate_.  
- Ajoute l'expéditeur (`support@awseducate.com`) à ta liste de contacts pour éviter que ses emails passent en spams ou autres filtres.  
- Clique sur le lien t'amenant au formulaire de création de compte.

<details>

![email](img/create_account/1-email_invitation.PNG)

</details>

## Formulaire d'inscription

- Nom de l'établissement : O'clock
- Pays : FR
- Prénom : ton prénom :wink:
- Nom : ton nom :astonished:
- Month & Year : mois et année d'obtention prévu du diplôme => mettre 2-3 mois après la fin de la formation
- Date de naissance : ta date de naissance :boom:
- Code promotionnel : laisser vide :disappointed:

<details>

![](img/create_account/1a-account_creation_form.png)

</details>

## Conditions d'utilisation

- Lire les conditions d'utilisation :joy:
- Les accepter et valider

<details>

![](img/create_account/1b-terms_and_conditions.png)

</details>

## Compte créé

- tout est ok
- tu vas recevoir un email de vérification de ton adresse email
- il faut désormais attendre que le compte soit approuvé

:warning: les emails d'AWS peuvent être classés dans l'onglet **Promotions** de gmail ou dans les **Spams**

<details>

![](img/create_account/1c-account_request_saved.png)
![](img/create_account/1d-email_checked.png)

</details>

## Compte approuvé

Une fois le compte créé, AWS Educate valide le compte et l'ajoute à la "classe".  
Clique sur lie lien permettant de définir le mot de passe.

<details>

![](img/create_account/2-account_creation.PNG)

</details>

## Mot de passe

Le formulaire est assez explicite.  
Si tu manque d'inspiration et/ou que tu as peur de perde le mot de passe, tu peux utiliser : `<oclock ID=2>`  
Ton compte est créé et approuvé :tada:

<details>

![](img/create_account/2a-password.png)

</details>

## AWS Classroom & Console

- accéder à ses _classrooms_
- accéder au _classroom_ créé par un prof O'clock
- valider les conditions d'utilisation
- accéder à la console AWS depuis laquelle on pourra créer des serveurs (il faudra autoriser la popup)

<details><summary>ses classrooms</summary>

![](img/create_account/3-educate_interface.PNG)

</details>

<details><summary>classroom O'clock</summary>

![](img/create_account/4-myclassroom.png)

</details>

<details><summary>conditions d'utilisation</summary>

![](img/create_account/4a-myclassroom-terms.png)

</details>

<details><summary>accès à la console AWS</summary>
  
:warning: il faudra autoriser l'ouverture d'une popup après le click sur le bouton :warning:

![](img/create_account/5-classroom_interface.png)
  
:warning: il faudra autoriser l'ouverture d'une popup après le click sur le bouton :warning:

</details>

<details><summary>console AWS</summary>

![](img/create_account/6-AWS-console.png)

</details>
