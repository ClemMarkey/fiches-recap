# Expression de besoin

Une bonne expression du besoin permet d'éviter les incompréhensions : 

![paf-le-projet](http://img.over-blog-kiwi.com/0/25/76/24/201211/ob_4c29c7_gestion-projet-web-humour.gif)

## Règles de base

L'expression de besoin :
- est la matière première du Cahier des Charges
- n'est pas contractuelle
- est orientée métier
- est concise
- laisse plusieurs "portes ouvertes" / ouverte au changement
- ne préjuge pas d'une éventuelle solution
- doit contenir un **glossaire** pour préciser les termes et sigles employés
- doit contenir des explications, si mentionnés, des :
  - procédés
  - démarches
  - spécificités

## Contenu idéal

- Positionnement stratégique
  - Montrer l'importance du projet
  - Quels sont ses bénéfices ?
  - Que se passera-t'il si le projet ne se fait pas ?
- Echeances
  - A partir de quand le projet peut-il commencer ?
  - Une date limite pour sa finalisation ? (incitative ou primordiale)
  - Quelles sont les disponibilités des personnes concernées ?
- Utilisateurs
  - Qui sont les destinataires du projet ? (type d'utilisateur)
  - Quels sont les profils types ?
  - Combien d'utilisateurs visés ? (En tout ? Simultanément ?)
  - Où sont les utilisateurs ? (même réseau interne ? Internet ?)
  - Quel est leur degré d'aisance avec les outils informatiques ?
- Besoin fonctionnels
  - À quoi servira l'application
  - Décomposition avec une granularité adéquate (cf. exemples)
  - Les besoins sont nommés / identifiés
  - Prioriser les besoins (2 ou 3 niveaux de priorité - MoSCoW)
- Evolutions à venir
  - Concernant le périmètre fonctionnel
  - Concernant le périmètre d'utilisation
  - Concernant la réutilisation
- Contexte technique
  - Support physique (Mobile, PC, ...)
  - Support logiciel (client léger - web, lourd - linux / windows, ...)
- Contraintes
  - Techniques
    - Plages horaires de fonctionnement
    - Tolérance d'interruption
    - Nombre d'utilisateurs simultanés
    - Temps maximal d'exécution
  - Humaines
    - Ressources
    - Prestataires externes
  - Réglementaires
    - Normes
    - Codes
  - Financières
  - ...



## Outils : [QQOQCCP et SMART](matrice-analytique.md)
