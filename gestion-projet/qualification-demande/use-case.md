Use-case
========

Un _Use-case_ (UC) ou « cas d'utilisation » (CU) en français (la traduction étant peu utilisée en pratique !) est une forme de documentation à propos d'un système. Un use-case décrit un usage particulier du système, généralement quelque chose de fondamental, à forte valeur ajoutée.

Présenté sous forme manuscrite et/ou visuelle (schéma, diagramme UML…), un use-case illustre un contrat d'usage entre (typiquement) un acteur et le système, dans le but de réaliser une action apportant de la plus-value à l'acteur.

## Rôle

> Comprendre et spécifier les besoins du client pour rédiger le cahier des charges fonctionnel

Trois questions principales

1. Définir les utilisations principales du système : à quoi sert-il ?
   - Tâches principales
   - Ses différents cas d'utilisation
   - ...
2. Définir l'environnement du système : qui va l'utiliser ou intéragir avec lui ?
   - des humains
   - autres systèmes
   - capteurs / bornes
   - ...
3. Définir les limites du système : où s'arrête sa responsabilité ?
   - Délégation
   - Récup de données à l'extérieur
   - ...

## Ressources
- [wiki](https://fr.wikipedia.org/wiki/Cas_d%27utilisation)
- [wiki en](https://en.wikipedia.org/wiki/Use_case) (plus d'exemples)
- [Expression des besoins avec UML](https://medium.com/@manurnx/les-cas-d-utilisation-64cc69b9a67f)
- [Slides de présentation des diagrammes use-case](https://www.slideshare.net/khmansouri/uml-part2-diagramme-des-uses-casesmansouri)
- [Video de présentation de diagrammes use-case](https://www.youtube.com/watch?v=zid-MVo7M-E) (bon ça reste une vidéo de promo d'un outil mais 13 minutes pour tout aborder, c'est pas mal :P)

## Définir un use-case

### Étape 1 : Scénario d'utilisation

Séquences d'étapes 
- Décrivant une interaction entre l'utilisateur et le système
- permettant à l'utilisateur de réaliser son objectif

#### Exemple

**Commander** est l'objectif de l'utilisateur

```
Système : Site de vente en ligne

Scénario : Commander

Le client s'authentifie dans le système puis choisit une adresse et un mode de livraison. Le système indique le montant de la commande au client. Le client donne ses informations de paiement. La transaction est effectuée et le système informe le client par e-mail.
...

```

#### Vocabulaire

Un cas d'utilisation est l'objectif que cherche à réaliser l'utilisateur qui effectue les différents scénarios 
- Ensemble des scénarios réalisant un objectif de l'utilisateur
- Fonctionnalités principales du système du point de vue extérieur (Fonctionnalités pour lesquelles le système est utilisé)

**Acteur** : Entité qui intéragit avec le système 
- Personne, objet, logiciel, ... extérieur au système
- Représente un rôle, une même entité peut remplir plusieurs rôles (internaute : Visiteur par défaut / Client si il est connecté)
- identifié par le nom du rôle

**Cas d'utilisation** : Fonctionnalité visible depuis l'extérieure
- Action déclenché par un acteur (Le système n'a pas de volonté, il ne fait que réagir)
- Identifié par une action


### Étape 2 : Diagramme use-case UML

#### Outils

Pour réaliser les diagrammes UML use-case il existe beaucoup d'outils disponible.
- [PlantUML](http://plantuml.com/) pour les amoureux du code (des diagrammes visuels avec du code :heart: )
- [yEd](https://www.yworks.com/products/yed) accessible mais assez contraignant
- [Violet UML Editor](http://alexdp.free.fr/violetumleditor/page.php) Elégant, très simple de prise en main mais quelques limitations sont à noter
- [draw.io](https://www.draw.io/) est gratuit, efficace, simple de prise en main et relativement complet

> Pour une première approche, [draw.io](https://www.draw.io/) est de loin le meilleur choix.

#### Éléments de base

**système** : le système et ses limites
**association** : relation entre acteurs et cas d'utilisation : Représente la possibilité pour l'acteur de déclencher le cas (liaison entre chaque acteur avec tous les cas qu'il peut declencher)

![vocabulaire diagramme](../img/uc-format.svg)

#### Acteurs

Plusieurs acteurs peuvent cohabiter

![vocabulaire diagramme](../img/uc-acteurs.svg)

#### Héritage acteurs

Si un acteur enrichi un autre acteur et afin de limiter la complexité du diagramme il est possible de marquer un héritage

![vocabulaire diagramme](../img/uc-acteurs2.svg)

Ici, Client peut faire tout ce que peut faire Visiteur.

#### Cas d'utilisations

Si un cas d'utilisation possède des variantes / cas particuliers, il est possible de les représenter comme des sous-parties.

![vocabulaire diagramme](../img/uc-sub.svg)

dans cet exemple, la "Commander" réunit 2 cas particuliers
- e-billet -> pas de livraison physique
- tee-shirt -> Livraison attendue (stock, adresse, transporteur, ...)

#### Cas facultatifs / optionnels

Il est possible de faire apparaître des cas d'utlisation optionnels

![vocabulaire diagramme](../img/uc-extends.svg)

Ici, pour commander un produit, il est possible / facultatif de souscrire à une extension de garantie

#### Cas inclus / induits

Pour plus de clarté, il est parfois nécessaire de faire apparître des cas inclus. 

![vocabulaire diagramme](../img/uc-includes.svg)

Dans cet exemple, l'objectif du client est de "Commander un objet" ou "Souscrire à un abonnement".

Afin d'expliciter les différentes étapes, le diagramme fait apparaître le cas induit "Payer". Il est plus simple de comprendre les étapes et les intéractions 

Un autre exemple : Un utilisateur veut "Modifier son profil" client (objectif recherché). Il est possible d'inclure un cas "S'identifier" (ce n'est pas l'objectif utilisateur - mais un mal nécessaire :) ).

### Étape 3 : Scénarios détaillés

Afin de préciser les choses il est nécessaire d'accompagner ces diagrammes avec un scénario détaillé. Cela permet de préciser les différents scénarios et leurs alternatives (en cas d'echec, en cas de traitement inattendu, etc.)


#### Exemple de scénario détaillé

UC-127 **Commander**

* **Résumé** : un achat est effectué par un client, ce qui donne lieu à livraison.
* **Description** : un client (anonyme ou possédant un compte utilisateur) doit être en capacité de réaliser un achat sur le site, la partie e-commerce représentant un des services principaux. Pour ce faire, le client doit au préalable avoir ajouté au moins un produit dans son panier. L'achat est réalisé au moyen d'un parcours d'achat en plusieurs étapes : vérification du panier (modifications possibles), confirmation du panier, paiement en ligne (plusieurs plateformes disponibles), confirmation d'achat. L'achat s'intègre dans un processus de commande, qui comprend à sa suite la livraison (+ suivi) et les éventuelles réclamations.
* **Acteur principal** : un client doté d'un panier
* **Acteurs** : client
* **Intervenants** : parcours d'achat, plateforme de paiement (tiers), mailer (tiers)
* **Pré-conditions** : panier d'achat non-vide
* **Déclencheurs** : CTA « Procéder à l'achat »
* **Post-conditions** : achat pris en compte (y compris paiement validé), confirmations sur site et par mail
* **Valeur ajoutée** : conversion, service
* **Déroulé** :
  1. Le client se rend sur son panier.
  2. Le client confirme son panier.
      - A2.1 : le client réalise un achat rapide (UC128)
  3. Le client paie.
  4. Le client obtient une confirmation d'achat.

#### Titre

Chaque use-case possède un titre, mettant au moins en avant un acteur et son but. Le titre est à la forme active du présent de l'indicatif, et doit permettre de comprendre intuitivement quelle est la valeur ajoutée pour le projet.

Exemples :

* _Le client procède à son achat._
  * sujet : le client
  * but : réaliser son achat
  * moyen : non-précisé
* _La newsletter est validée et envoyée._
  * sujet : la newsletter
  * but : re reçue par ses lecteurs
  * moyen : non-précisé
* _Le système de messagerie permet aux utilisateurs de communiquer en privé._
  * sujet : les utilisateurs
  * but : communiquer en privé
  * moyen : système de messagerie

#### Description

La description précise le but de l'acteur principal et présente succintement le(s) moyen(s) par le(s)quel(s) ce but sera atteint — ce qui correspond au cas d'usage dit *happy path* ou *sunny day* (== quand tout va bien). Elle peut également préciser certaines contraintes, certains cas d'erreurs notables (*sad path* ou *rainy day*), certaines alternatives (totales ou partielles) au *happy path* etc.

La description n'est *pas* un scénario détaillé ou pas-à-pas, même si elle a souvent un peu cette forme. Le but ici n'est pas tant de décrire *comment* faire quelque chose, que *pourquoi* (et *qui* est concerné).

#### Acteur principal

En règle générale, l'acteur principal est la personne ou l'entité qui déclenche le use-case (cf. *Déclencheur*), et obtiendra une plus-value à l'issue du *happy path*.

*Exemple* : un client réalisant un achat, un gagnant au Loto qui ouvre un compte bancaire, etc.

#### Acteurs

Il arrive qu'un use-case fasse interagir plusieurs acteurs, qui sont alors mentionnés.

Chaque acteur, y compris le principal, a un rôle spécifique à jouer dans le use-case.

*Exemple* : le support client dans le cas d'une réclamation par un client.

#### Intervenants

À la différence des acteurs qui sont, par définitions, actifs, les intervenants sont en général des systèmes passifs mais nécessaires pour le bon déroulé du use-case.

*Exemples* : le « parcours d'achat » ou la « plateforme de paiement » dans le cas d'un achat en ligne.

#### Contraintes

##### Pré-condition

Un prérequis au use-case. Si une pré-condition n'est pas remplie, le use-case _doit_ échouer.

##### Déclencheur

Aussi appelé _trigger_, c'est l'action concrète effectuée par l'acteur principal qui déclenche le use-case.

> Certains use-cases ont un déclencheur qui n'est pas associé à l'acteur principal, par exemple une tâche récurrente, une notification d'erreur…

##### Post-condition

Ce qui doit être absolument vérifié (vrai) au terme du déroulé du use-case.

#### Valeur ajoutée

Pourquoi le use-case existe t-il en premier lieu, qui et à quoi sert-il ? Quel intérêt pour le projet ?

#### Déroulé

##### Déroulé nominal

Il correspond au *happy path*. C'est une liste d'étapes _très schématique_ visant à la réalisation concrète du use-case. Il peut contenir des alternatives, notées `A`, qui font éventuellement référence à d'autres use-cases pour précisions (mais pas toujours).

> Dans les méthodes _Agile_, le déroulé est souvent utilisé comme base pour créer plusieurs [user stories](./user-stories.md). Là où le déroulé n'est pas un scénario à proprement parler, mais plutôt un déroulé de principe (*workflow*), les user-stories s'attèlent à documenter la réalisation concrète du use-case (comment faire les choses, en succès et en erreur !)

##### Déroulé d'extension

Il correspond à un cas alternatif global (remplace le déroulé nominal, mais amène à remplir l'objectif tout aussi bien) ou à un cas d'erreur. Une use-case bénéficie d'avoir des déroulés d'extension, mais ils sont chronophages à rédiger, et sont donc souvent remplacés par une liste voire par des mentions implicites dans la description…

---

### Dynamique interne d'un use-case

Un use-case fonctionne schématiquement sur le mode client-serveur :

- l'acteur principal est le client, il initie une requête (déclencheur + but)
- le ou les systèmes avec le(s)quel(s) l'acteur principal interagit est le serveur, qui répond à la requête.

Cela signifie que l'accent est mis sur la réalisation d'un objectif à travers une coopération : l'acteur principal a un *intérêt* à établir une relation dynamique avec d'autres acteurs et/ou des intervenants, et les autres acteurs et/ou intervenants ont également leurs propres intérêts. L'objectif n'est atteignable que si ces intérêts respectifs ne rentrent pas en conflit.

Le use-case existe donc pour documenter la coordination entre tous ces intérêts, et doit se limiter à ça. Tous les comportements nécessaires et suffisants dont les acteurs et intervenants doivent faire preuve pour satisfaire leurs intérêts, doivent être mentionnés. Ces comportements sont de trois types :

- une interaction (qui rapproche de l'objectif ; *exemple* : le client valide son panier d'achat)
- une validation (qui protège des intérêts ; *exemple* : l'adresse de livraison saisie par le client est vérifiée)
- un changement d'état (qui satisfait des intérêts ; *exemple* : une facture est générée en base de données)

> Dans une démarche qualité, la « réponse serveur » doit toujours se faire en veillant à protéger les intérêts de tous les intervenants (ex. conserver l'intégrité des données, vérifier les informations bancaires, afficher les erreurs, etc.) Les intervenants en question peuvent être soit directement impliqués dans le use-case, soit êtres implicites (ex. la base de donnée, la plateforme bancaire, l'UI). Il faut penser à tout !

### Astuces pour écrire un bon use-case

#### À faire

* Rester simple, ne pas aller dans les détails (mais être exhaustif, cf. _Contenu d'un use-case_).
* Grâce à une [matrice analytique](./matrice-analytique.md), identifier le périmètre fonctionnel, les acteurs principaux et leurs objectifs.
* Toujours se placer du point de vue de l'acteur principal, et garder son objectif en tête (mais écrire à la troisième personne).
* Commencer par le *happy path* (puis penser aux alternatives et aux cas d'erreur).
* Ne pas hésiter à commencer par un ou plusieurs schémas pour réfléchir.

#### À éviter

- Faire du pas-à-pas concret (les use-cases documentent des objectifs, des intentions et des intérêts).
- Mentionner l'UI (les use-cases sont découplés de l'ergonomie).
- Passer sous silence les cas d'échec (les use-cases sont holistiques).
- Écrire des user stories au lieu de use-cases (pas le même niveau d'abstraction, pas le même angle d'attaque, pas le même formalisme).
  - Exemple : _En tant que client enregistré, je veux pouvoir passer une commande rapide [afin de rapidement récupérer mon produit]._ => c'est un appel à spécifier _comment_ passer une commande, l'objectif (réaliser un achat) est secondaire et parfois omis, contrairement au use-case qui est centré sur cet objectif. Cette user story est de fait reliée au use-case UC-127 de l'exemple ci-avant, mais devra être précisée pour passer à l'implémentation : le use-case peut apporter des éléments de réponses _fonctionnels_, qu'il faudra compléter avec des éléments _opérationnels_ à définir sur le moment (critères d'acceptance, couplage à l'UI avec mention de pages, formulaires, boutons, CTA, etc.). [En savoir plus sur les différences entre use-cases et user stories](http://www.agile-ux.com/2009/01/23/use-cases-user-stories-so-precious-but-not-the-same/) (en) et [encore plus](https://manurenaux.wp.imt.fr/2013/09/25/user-story-vs-use-case/).

## Aller plus loin

- [Rédiger des cas d'utilisation efficaces](https://docplayer.fr/44855426-D-apres-alistair-cockburn-2001-rediger-des-cas-d-utilisation-efficaces-eyrolles-paris-290-pp.html)
