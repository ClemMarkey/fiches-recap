Les matrices analytiques
========================

_Fiche en cours de rédaction._

## QQOQCCP

- **Qui** : De qui, avec qui, pour le compte de qui...
  > Responsable, intervenants, acteurs, sujet, cible...
- **Quoi** : Quoi, avec quoi, en relation avec quoi...
  > Outil, moyen, objet, ressources, résultat, objectif...
- **Où** : *spatial* : Où, par où, vers où...
  > Localisation, lieu, trajet,...
- **Quand** : *temporel*  : À partir de quand, jusqu'à quand, dans quel délai...
  > Date, périodicité / récurence, durée ...
  - Quand, tous les quand...
- **Comment** : De quelle façon, dans quelles conditions, par quel procédé...
  > Procédure, technologie / technique, méthode, action, moyens matériel...
- **Combien** : Dans quelle mesure, valeurs en cause, à quelle dose...
  > Budget, décompte, quantités ...
- **Pourquoi** : Pour quelle(s) raison(s), cause, facteur déclenchant, motif, finalité...
  > Justification, raison d'être, résultat attendu ...

> Selon le contexte, les questions n'ont pas le même rôle
> - s'accorder sur un problème
> - définir le périmètre et objectif d'un projet à lancer
> - poser un diagnostic
> - établir des conditions de réalisation
> - ...

- https://fr.wikipedia.org/wiki/QQOQCCP
- https://www.qualiblog.fr/outils-et-methodes/methode-qqoqccp-outil-analyse-simple-et-performant/
- Le plus important est de réaliser une écoute active, pour _identifier_ dans les réponses du client les éléments d'intérêt pour passer faciliter la rédaction des use-cases : acteurs, intervenants, systèmes, buts respectifs, etc.
- Les questions _quoi, qui, où, quand_ sont plutôt factuelles et 1er degré, elles permettent de clarifier le besoin sous-jacent à la demande ; tandis que les questions _combien, comment, pourquoi_ sont plutôt contextuelles et 2d degré, elles permettent de clarifier les _contraintes_ associées au besoin.
- À l'issue de cette démarche analytique, il est de bon ton de produire un document récapitulatif, un _brief fonctionnel_, qu'on envoie au client pour validation. La forme importe peu, l'important c'est que le client et le prestataire soient d'accord sur l'analyse du besoin (sans aucune considération sur les fonctionnalités précises, l'ergonomie ou l'implémentation à ce stade)
- Contrôle des réponses aux questions avec [des indicateurs SMART](https://fr.wikipedia.org/wiki/Objectifs_et_indicateurs_SMART)
  - Spécifique, 
  - Mesurable, 
  - Acceptable, 
  - Réaliste, 
  - Temporellement défini

## 5 pourquoi

La voiture ne démarre pas
```
    - Pourquoi la voiture ne démarre pas : Pas de contact, je tourne la clé rien ne se passe
    - Pourquoi rien ne se passe : la batterie est en panne
    - Pourquoi la batterie ne fonctionne pas : l'alternateur est cassé
    - Pourquoi l'alternateur est cassé : La révision du véhicule n'a pas été faite
    - Pourquoi ...
```

- https://fr.wikipedia.org/wiki/Cinq_pourquoi
-  https://www.qualiblog.fr/outils-et-methodes/la-methode-des-5-pourquoi-pour-eradiquer-vos-problemes/

## _Story mapping_

Technique d'analyse consistant à organiser des user stories selon deux axes : temporalité et priorité.
