# Les bonnes pratiques

## Indenter son code

L'indentation, c'est le B à BA du développeur. Il s'agit de décaler son code
vers la droite à l'aide de la touche `TAB`, qui vient rajouter une tabulation
ou des espaces (selon les conventions des langages) afin de réaliser visuellement
une séparation logique de son code. L'indentation n'a en général pas d'incidence
sur le code, à part dans certains langages.

L'indentation de son code peut permettre :

- une lecture plus facile de son code

- un débug plus facile  
Par exemple : bien refermer une accolade ou une valise html

- de "replier" un bout de code dans Atom, pour mieux visualiser son code.

Même s'il est conseillé d'indenter son code petit à petit — et Atom nous
aide en indentant intelligemment lorsqu'on passe à la ligne — on peut aussi
indenter automatiquement son code avec le [package](packages.md) [Atom Beautify](https://atom.io/packages/atom-beautify).

## Variables

Le choix du nom des variables est capital pour comprendre son code et que d'autres développeurs puissent également le comprendre aisément.

```js
value = 'Julien';
//il est préférable de choisir un nom de variable représentatif
prenom = 'Julien';

```


## Fonctions

La même pratique que pour les variables doit être utilisée pour les paramètres : bien choisir leur nom

```js
function afficheCouleur(value) {
  // ...
}
// le paramètre est plus parlant
function afficheCouleur(couleur) {
  // ...
}
```

Il est recommandé de ne pas excéder les 20-25 lignes pour une fonction.

**argument ou paramètre ?**

La différence est subtile

```js
// lors de la déclaration, on parle de paramètre
function exemple(parametre) {
  // ...
}

// lors de l'appel, on parle d'argument (la valeur passée)
exemple(argument);
```

## Library ou Framework

**Library** ou bibliothèque : boîte à outils, multiples fonctions utilitaires

**Framework** : est une structure / organisation possédant également de multiples fonctions servant l'organisation
