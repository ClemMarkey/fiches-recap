# Raccourcis

Pour connaître les touches de différents clavier, [c'est par ici](clavier.md).

La liste ci-dessous est une liste des raccourcis les plus utiles.  
Pour la liste un peu plus complète, [c'est par là](https://github.com/nwinkler/atom-keyboard-shortcuts).

## Commandes fréquentes

| Command | Mac OS X | Windows | Linux | Description |
| ------- | -------- | ------- | ----- | ----------- |
| Enregistrer | `cmd-s` | `ctrl-s`  | `ctrl-s` | Enregistre le fichier en cours |
| Fermer l'onglet | `cmd-w` |  `ctrl-w` | `ctrl-w` | Fermer l'onglet du fichier en cours |
| Commenter/Décommenter | `cmd-/` | `ctrl-/`  | `ctrl-/` | Commente ou décommente le bout de texte sélectionné |
| Rechercher/Remplacer | `cmd-f` | `ctrl-f`  | `ctrl-f` | Ouvre la recherche sur le fichier en cours |
| Rechercher dans le projet | `shift-cmd-f` | `ctrl-shift-f`  | `ctrl-shift-f` | Ouvre la recherche sur le projet entier |

## Navigation

| Command | Mac OS X | Windows | Linux | Description |
| ------- | -------- | ------- | ----- | ----------- |
| Preferences/Settings | `cmd-,` | `ctrl-,`  | `ctrl-,` | Opens the Preferences/Settings view |
| Command Palette | `shift-cmd-p` | `shift-ctrl-p`  | `ctrl-shift-p` | Ouvre la palette de commande. On va pouvoir tapper "settings" ou "install package", ou même des inscructions que rajoute les plugins. Par exemple "beautify" si on a le plugin [Atom Beautify](https://atom.io/packages/atom-beautify) installé. |
| Rechercher fichier | `cmd-p` | `ctrl-p`  | `shift-p` | Pour ouvrir un fichier en tapant son nom, on peut utiliser `:` pour le numéro de ligne. Par exemple `:24`, pour aller à la ligne 24 du document en cours ou `contact.php:24` pour ouvrir le fichier contact.php et aller à la ligne 24 |

## Astuces

| Command | Mac OS X | Windows | Linux | Description |
| ------- | -------- | ------- | ----- | ----------- |
| Sélection multiple | `cmd-d` | `ctrl-d` | `ctrl-d` | Une fois un bout de texte sélectionné, cela va sélectionner les autres occurences de cette sélection. Utile pour modifier un nom de variable par exemple. |
| Go To Matching Bracket | `ctrl-m` | `ctrl-m`  | `ctrl-m` | The cursor goes to the matching top bracket that the cursor is ecapsulated in  |
| Select Line | `cmd-l` | `ctrl-l`  | `ctrl-l` | Selects the entire line the cursor's current position is in |
| Duplicate Lines | `shift-cmd-d` | `ctrl-shift-d` | `ctrl-shift-d` | Duplicates the line of the current cursor position and creates a new line under it with the same contents |

## Snippets

| Command | Mac OS X | Windows | Linux | Description |
| ------- | -------- | ------- | ----- | ----------- |
| Lorem ipsum | `lorem` + `tab` | `lorem` + `tab` | `lorem` + `tab` | Permet de générer un « faux texte » latin pour avoir rapidement du contenu texte dans une intégration. |
| Snippet HTML | `html` + `tab` | `html` + `tab` | `html` + `tab` | Permet de générer un [template html](../html/template-html.md). **Fonctionne uniquement sur les fichiers html.** |
