# Les packages Atom

Atom est un éditeur extensible par des "packages", c'est-à-dire des suppléments
qu'on peut télécharger pour venir enrichir l'éditeur ou rajouter des
fonctionnalités.

## Installer un package

Puisqu'on est intime avec la ligne de commande, il nous suffira d'une seule
ligne pour installer un package atom :

```shell
apm install nom-du-package
```

## Liste des packages utiles

- [Les linters](linters.md) : pour avoir une vérification d'erreur directement dans Atom.
Les linters, c'est la vie.

- [Atom Beautify](https://atom.io/packages/atom-beautify) : permet de réorganiser le code automatiquement.

- [File icons](https://atom.io/packages/file-icons) : affiche l'icone d'un fichier en fonction de son extension.

- [Pigments](https://atom.io/packages/pigments) : permet de colorer directement dans l'éditeur les couleurs codées en hexa, rgb ou en anglais.

- [Tabs-to-spaces](https://atom.io/packages/tabs-to-spaces) : permet de passer de tabulations à espaces et inversement, pour ne plus avoir des indentations incohérentes !

- [Emmet](https://atom.io/packages/emmet) : permet d'accélérer l'écriture du HTML avec des syntaxes simples (répétitions, enfants, descendants, attributs, ...)

- [Docblockr](https://atom.io/packages/docblockr) : permet d'accélérer et de rendre plus facile l'écriture de la documentation 
