# Touches de clavier

## Glossaire

`alt gr` = La touche ALT qui se trouve à droite de la barre d'espace  
`alt` = la touche qui comporte l'inscription "alt" ou "options"  
`shift` = la touche qui comporte l'inscription "shift" ou une flèche vers le haut  


## Configuration Linux

`{` = alt gr + 4  
`}` = alt gr + = (la touche juste avant la touche `Retour Arrière`)  
`[` = alt gr + 5  
`]` = alt gr + )  
`|` = alt gr + 6  
`` ` `` = alt gr + 7  
`’` = alt gr + shift + b  


## Configuration Mac

`{` = alt + (  
`}` = alt + }  
`[` = alt + shift + (  
`]` = alt + shift + )  
`|` = alt + shift + L  
`` ` `` = touche £  
`’` = alt + shift + 4  
