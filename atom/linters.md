# Linters

Pour avoir des informations sur notre code directement dans Atom, on peut utiliser
des **linters**, c'est-à-dire des petits programmes qui vont parser notre code
pour y dénicher les erreurs, un caractère manquant ou en trop par exemple.

Pour installer des linters, il faut avoir le package [linter](https://atom.io/packages/linter),
que l'on peut installer en ligne de commande :

```shell
apm install linter
```


## HTML

[linter-htmlhint](https://atom.io/packages/linter-htmlhint)

### Installation

```shell
apm install linter-htmlhint
```


## PHP

[linter-php](https://atom.io/packages/linter-php)

### Installation

```shell
apm install linter-php
```

## JS

[linter-js](https://atom.io/packages/linter-eslint)

### Installation

```shell
apm install linter-eslint
```

## CSS

[linter-stylelint](https://atom.io/packages/linter-stylelint)

### Installation

```shell
apm install linter-stylelint
```
