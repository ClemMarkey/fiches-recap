# Search Engine Optimization

Optimisation de notre site internet (code et contenu) pour améliorer son placement dans les résultats des moteurs de recherche.

https://fr.wikipedia.org/wiki/Optimisation_pour_les_moteurs_de_recherche

## Liens intéressants

### Audit de site

* https://moz.com/researchtools/ose/
* http://www.positeo.com/check-position/
* https://majestic.com/
* https://ahrefs.com/
* https://insight.yooda.com/
* https://www.woorank.com/fr
* https://gtmetrix.com/

### Blog sur le sujet

* http://blog.axe-net.fr/
* https://moz.com/blog
* https://blog.ahrefs.com/
* http://blog.majestic.com/fr/
* http://www.laurentbourrelly.com/blog/

### Articles sur le sujet

* http://blog.axe-net.fr/le-b-a-ba-du-referencement/
* https://refeo.com/netlinking/
* https://moz.com/blog/distance-from-perfect
