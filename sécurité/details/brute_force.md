# Attaque par force brute

## Le principe

Tester tous les mots de passe possibles jusqu'à trouver le bon. Tout simplement.

## L'attaque

1. Trouver le pseudo ou l'email de notre victime. (pas si compliqué)

2. Réaliser une requête vers le site qu'on attaque

`https://lesite.com/login?pseudo=victime&password=aaa`

3. Changer de pass et recommencer en boucle jusqu'à trouver le bon !

Alors oui, à la main, c'est long. Mais on peut rapidement coder une boucle qui teste tout très vite. Un petit exemple en 50 lignes se trouve [ici](./brute_force.php).

## Détails
* Les formulaires de connexion sont la plupart du temps en POST. Pas grave, on a les outils pour faire des requêtes POST. [La doc là](https://www.php.net/manual/fr/book.curl.php), et [un exemple ici](https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php).

* Pour trouver le nom des paramètres à envoyer au serveur, rien de plus simple, il suffit d'inspecter le contenu du formulaire dans un navigateur et de repérer les `name` des `input`.


## Comment se défendre

Avant toute chose, il faut dire que plus le mot de passe est long et compliqué, plus il sera difficile de le trouver. En tout cas, ça sera plus long.

La première chose à faire est donc d'obliger ses utilisateurs à choisir un mot de passe suffisament solide. Selon la [recommandation de la CNIL](https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires), il faut : 
- au moins 8 caractères
- au moins une lettre majuscule
- au moins une lettre minuscule
- au moins un chiffre
- au moins un caractère spécial 


Ensuite on peut compter le nomdre d'essais erronés pour se connecter, et verouiller le compte après X essais infructueux ou trop proches dans le temps. Mais c'est à double tranchant ! Si un pirate s'amuse à faire des requêtes volontairement éronnées avec tous les pseudos possibles et imaginables, il peut bloquer les comptes de tous les utilisateurs et rendre le site complètement inaccessible !

On peut imaginer alors de mettre en place la même chose, mais avec l'adresse IP. Mais c'est loin d'être évident à faire en PHP. On peut aussi utiliser des outils tel [fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page) qui scanne les logs du serveur et bannit les IP qui paraissent malicieuses.


Enfin, on peut multiplier les étapes de validation d'un login, en rajoutant par exemple un CAPTCHA (pour s'assurer que c'est bien un humain et non un script qui tente de se log), ou une double authentification par email, SMS, ou tout autre système (2FA).
