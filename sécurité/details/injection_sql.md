# Injections SQL

## Le principe

Exécuter toutes les requêtes qu'on veut dans la base de données de notre victime !

**Note**: le principe de l'injection fonctionne aussi dans les bases NoSQL !

## L'attaque

1. Trouvez un site où la soumission d'un formulaire déclenche selon vous une requête SQL. Pas trop difficile, c'est presque toujours le cas ! Par exemple, un formulaire de connexion va faire une requête SQL pour vous retrouver dans la bdd...

2. Au lieu de taper votre pseudo dans l'input, tapez plutôt une requête SQL. Ou un bout de requête SQL.

3. Si vous avez de la chance, votre requête sera exécutée ! :smiling_imp:

## L'attaque en détail

1. Imaginez la requête SQL qui se cache derrière le formulaire. Dans notre exemple, la requête devrait ressembler à :
```sql
SELECT * FROM users WHERE username = 'pseudo';
```
où la valeur `pseudo` vient directement du formulaire.

2. Maintenant, écrivez ceci dans le formulaire : 

`'; DROP TABLE users; --`

On se retrouve avec la requête suivante : 
```sql
SELECT * FROM users WHERE username = ''; DROP TABLE users; --
```

:boom: Catastrophe, les 2 parties de la requêtes vont s'exécuter, et la table `users` va être totalement supprimée !!

## Comment se défendre

Si on a réussi à effectuer cette attaque, c'est probablement parce que des développeurs ont fait l'erreur suivante :
```php
<?php
// récupérer la valeur passée dans le formulaire
$username = $_POST['login'];
// puis construire la requête SQL en concaténant simplement la valeur récupérée
$query = "SELECT * FROM users WHERE username= ' $username ' ";
// exécuter la requête (et pleurer...)
```

Mais comment faire autrement ?

#### À l'ancienne : échapper les chaines de caractères

En utilisant des fonctions d'échappement (comme [addslashes en PHP](https://www.php.net/manual/fr/function.addslashes.php)), on force le serveur à considérer la valeur comme une seule chaîne de caractères.
Ainsi, on peut faire : 
```php
// récupérer la valeur passée dans le formulaire
$username = $_POST['login'];
// échapper la valeur (= protéger les caractères spéciaux avec des \)
$username = addslashes($username);
// puis construire la requête SQL en concaténant simplement la valeur échappée
$query = "SELECT * FROM users WHERE username= ' $username ' ";

// l'exécution est un peu plus sécurisée
```

Si on essaye de passer notre valeur malicieuse ( `'; DROP TABLE users; --` ), elle sera considérée comme une chaine de caractères et non comme une instruction.

#### La vraie bonne solution : en utilisant les requêtes préparées

L'exemple suivant utilise PDO en PHP, mais le principe des requêtes préparées existe dans tous les langages !

```php
// récupérer la valeur passée dans le formulaire
$username = $_POST['login'];

// créer la requête avec un jeton (ou paramètre préparéé)
$query = "SELECT * FROM users WHERE username = :username ";

// on PREPARE la requête 
$statement = $pdo->prepare($query);

// Puis, avant d'exécuter la requête, on remplace les jetons par des valeurs qui seront automatiquement échappées
$statement->bindValue(":username", $username );

// Et enfin, on peut exécuter la requête en toute sécurité
$statement->execute();
```

REMARQUE : avec PDO, on peut se passer de `bindValue` et passer directement les valeurs dans un tableau associatif en paramètre de `execute`, comme ceci :
```php
$statement = $pdo->prepare($query);

$statement->execute([
  ":username" => $username
]);
```
