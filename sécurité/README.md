# Sécurité

La sécurité, c'est bien. :+1:

Mais la comprendre, c'est mieux ! :muscle:

Voici les failles les plus communes : 

- [Injection SQL](./details/injection_sql.md)
- [Attaque par force brute (ou Brute Force)](./details/brute_force.md)
- [Cross-Site Scripting (ou XSS)](./details/xss.md)
- [Cross-Site Request Forgery (ou CSRF)](./details/csrf.md)

Pour chacune de ces failles, on va d'abord se mettre dans la peau du pirate en tentant d'attaquer un site mal protégé, puis on verra comment se prémunir.

**Important**

Cette liste n'est pas exhaustive ! C'est un bon début, mais pour aller plus loin, vous pouvez : 
- [Lire les fiches avancées](../sécurité-avancée/README.md)
- S'interesser à OWASP :
  - OWASP est une fondation qui fait référence en matière de sécurité du web. [Site officiel - en anglais](https://www.owasp.org/index.php/Main_Page).
  - OWASP publie régulièrement un TOP 10 des failles les plus dangeureuses. [Version officielle - en anglais](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project)
  - Une traduction est disponible pour la dernière version en date (2017). [Repo Github](https://github.com/OWASP/Top10/tree/master/2017/fr).


**Encore plus important**

Ce n'est pas parce qu'on sait attaquer un site qu'il faut le faire !! Toutes les attaques décrites dans ces fiches sont bien évidemment illégales ! Le but de ces fiches récap est de vous apprendre à vous défendre, pas à attaquer le voisin !