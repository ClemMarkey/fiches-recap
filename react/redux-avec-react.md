# Redux avec React

* [React-Redux](https://github.com/reactjs/react-redux)
* [Redux: Usage With React](http://redux.js.org/docs/basics/UsageWithReact.html)


## Qu'est-ce que React-Redux ?

`react-redux` est une librairie qui permet de simplifier les liens entre Redux et React.  
Cette librairie nous permet de créer facilement des Containers, faisant automatiquement le lien entre le store (state et actions) et nos composants de présentation.


## Installation

Pour utiliser React-Redux, on installe tout d'abord le package [react-redux](https://github.com/reactjs/react-redux).

```shell
# Avec Yarn
yarn add react-redux

# Avec Npm
npm install react-redux
```

Ensuite, il va falloir :
* Utiliser le composant `Provider` afin de rendre le store disponible dans l'arborescence de composants.
* Créer des containers avec la fonction `connect` avec les composants à qui on souhaite donner de l'info.

## Provider

Ce composant est un « High Order Component », il va fournir de l'information à ses composants enfants.  
Pour ce faire, le Provider utilise le [`context`](https://reactjs.org/docs/context.html).

Pour l'installer, nous allons wrapper notre composant racine avec `<Provider>`.  
Par exemple :
```js
import React from 'react';
import { Provider } from 'react-redux';
import store from 'src/store';

document.addEventListener('DOMContentLoaded', () => {
  const provider = (
    <Provider store={store}>
        <App />
    </Provider>
  );
  render(provider, document.getElementById('root'));
});
```


## Containers

Nous avons défini nos composants à l'intérieur du dossier `src/components`.  
Pour nos containers, on peut se créer un dossier `src/containers`.

Pour chaque composant de présentation pour lequel on veut créer un container,
on crée un nouveau fichier dans lequel on utilise la fonction `connect`.

Pour cela, il faut définir les fonctions `mapStateToProps` et `mapDispatchToProps`.  
On peut ensuite utiliser la fonction `connect` de la manière suivante :
```js
const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
```

### mapStateToProps

La fonction `mapStateToProps` reçoit en argument le state et doit renvoit les propriétés à créer.  
Par exemple, si un composant a besoin de la propriété `user` présente dans le state :
```js
const mapStateToProps = state => ({
  user: state.user,
});
```

On peut également avoir accès aux propriétés que recoit mon container, via un deuxième paramètre.
Par exemple, si on a besoin de l'id pour sélectionner le state :
```js
const mapStateToProps = (state, ownProps) => ({
  user: state.users.byId[ownProps.id],
});
```

Le composant de présentation récupérera alors tous les propriétés renvoyées par `mapStateToProps`.  
Dans notre exemple, notre composant reçoit une propriété `user` :
```js
const User = ({ user }) => (
  <div className="user">
    <div className="user-name">{user.name}</div>
    <div className="user-role">{user.role}</div>
  </div>
);
```

Il peut arriver qu'on ait à faire des calculs pour sélectionner de l'info dans notre state.  
Par exemple, faire un `.filter` sur un array de données :
```js
const mapStateToProps = (state, ownProps) => ({
  user: state.users.filter(user => user.id === ownProps.id),
});
```

Dans ce cas, une bonne pratique est d'utiliser des fonctions qu'on appelle `selectors` afin d'exécuter ces calculs.  
On place les sélecteurs dans notre duck, auprès de nos types, reducer et action creators.  
Par exemple :
```js
const getUserById = (state, id) =>
  state.users.filter(user => user.id === ownProps.id);
```

```js
const mapStateToProps = (state, ownProps) => ({
  user: getUserById(state, ownProps.id),
});
```

Enfin, si nous ne souhaitons passer aucune propriété provenant du state, on définit notre `mapStateToProps` à `null`.

```js
const mapStateToProps = null;
```

Chaque fois que state va changer, le composant de présentation `User` pourra être rendu à nouveau pour mettre à jour l'UI.


### mapDispatchToProps

La fonction `mapDispatchToProps` reçoit en argument la fonction dispatch et doit renvoyer les propriétés à créer.  
Par exemple :
```js
const mapDispatchToProps = dispatch => ({
  sendMessage: () => {
    dispatch({ type: 'SEND_MESSAGE' });
  }
});
```

Comme `mapStateToProps`, la fonction reçoit également en deuxième paramètre les propriétés que reçoit le composant.

Le composant de présentation récupérera alors tous les propriétés renvoyées par `mapDispatchToProps`, en plus de celles créées par `mapStateToProps`.  
Dans notre exemple, notre composant reçoit une propriété `sendMessage` :
```js
const User = ({ user, sendMessage }) => (
  <div className="user">
    <div className="user-name">{user.name}</div>
    <div className="user-role">{user.role}</div>
    <button onClick={sendMessage}>Envoyer un message</button>
  </div>
);
```

Par souci de cohérence, on peut choisir de donner toutes les actions que l'on veut donner à nos composants via une propriété `actions`.  
Par exemple :
```js
const mapDispatchToProps = dispatch => ({
  actions: {
    sendMessage: () => {
      dispatch({ type: 'MESSAGE_SEND' });
    },
    clearMessage: () => {
      dispatch({ type: 'MESSAGE_CLEAR' });
    },
  },
});
```

Le composant de présentation récupérera une seule propriété `actions`, contentant toutes les actions définies.  
Par exemple :
```js
const User = ({ user, actions }) => (
  <div className="user">
    <div className="user-name">{user.name}</div>
    <div className="user-role">{user.role}</div>
    <button onClick={actions.sendMessage}>Envoyer un message</button>
    <button onClick={actions.clearMessage}>Effacer le message</button>
  </div>
);
```

Pour ne pas avoir à se rappeler chaque objet d'action à dispatcher, on préfère utiliser des action creators définis dans le duck.
Par exemple :
```js
const mapDispatchToProps = dispatch => ({
  actions: {
    sendMessage: () => {
      dispatch(sendMessage());
    },
    clearMessage: () => {
      dispatch(clearMessage());
    },
  },
});
```

Lorsqu'on utilise des action creators du même nom que la propriété à créer, on peut utiliser la fonction `bindActionCreators` de la librairie `redux` pour écrire nos `mapDispatchToProps` de manière plus concise.
Par exemple :
```js
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ sendMessage, clearMessage }, dispatch),
});
```


Si nous ne souhaitons passer aucune propriété utilisant dispatch, on définit notre `mapDispatchToProps` à `{}`.  
Si on définit `mapDispatchToProps` à `null`, une propriété `dispatch` sera créée, ce qui rarement ce que l'on souhaite.

```js
const mapDispatchToProps = {};
```


## Exemple complet

```js
/*
 * Npm import
 */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


/*
 * Local import
 */
// Component
import Task from 'src/components/Task';

// Actions and selectors
import { handleInput, addTask, getTaskById } from 'src/store/ducks/todo';


/*
 * Code
 */
// State
const mapStateToProps = (state, ownProps => ({
  task: getTaskById(state, ownProps.id),
});

// Actions
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ addTask, handleInput }, dispatch),
});


/*
 * Export
 */
export default connect(mapStateToProps, mapDispatchToProps)(Task);
```

---

`connect` peut recevoir un troisième argument `mergeProps`.  
C'est utile dans de très rare occasions, par exemple lorsqu'on doit dispatcher des actions qui dépendent du résultat de `mapStateToProps`.

→ [Voir la doc en détail](https://github.com/reactjs/react-redux/blob/master/docs/api.md)
