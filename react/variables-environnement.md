# Gérer des variables d'environnement

Durant les projets, il arrive qu'on ait besoin d'avoir des variables dont le contenu diffère selon notre environnement. 
Par exemple, on peut avoir besoin de stocker l'URL d'une API qui ne sera pas la même si on est en cours de développement
ou si notre application est déjà déployée en production.
Il existe un petit package qui va nous permettre de stocker dans des fichiers : `dotenv`.

## Installation

Voici comment ajouter `dotenv` à votre application en quelques étapes.

### 1 - Récupérer le package

On va utiliser la commande `yarn add dotenv` récupérer le package. Puis, en haut de notre fichier `wepback.config.js`, on l'appelle 
afin de pouvoir l'utiliser : 
```js
const DotenvPlugin = require('dotenv');
```

### 2 - Définir notre environnement

Par défaut, dans nos scripts du fichier `package.json`, on a une variable d'environnement appelée `NODE_ENV` qui a pour valeur
`production` ou `development` selon le script.
Elle est en revanche absente du script `start`. On va donc le modifier pour considérer que, lorsqu'on lance `yarn start`, on est en 
environnement de développement.

```js
"start": "NODE_ENV=development webpack-dev-server",
```

### 3 - Créer nos variables d'environnement

On peut maintenant créer les fichiers qui contiendront nos variables d'environnement. Vu qu'on a deux environnements différents dans nos
scripts, on va créer deux fichiers : `.env.development` et `.env.production`.
Dans ces fichiers, on va ajouter une variable par ligne, sous le format `VARIABLE=valeur`. 

Par exemple pour le fichier `.env.development` : 

```
URL_API=http://localhost:8001
```

Ou pour `.env.production` : 
```
URL_API=http://mon-api.com
```

Il peut aussi être judicieux de créer un fichier `.env` qui, contrairement aux deux autres, ne serait pas ignoré par git.
Ainsi on peut s'en servir comme gabarit pour savoir quelles variables créer dans nos fichiers de développement ou de production.

### 4 - Exploiter les fichiers `.env`

Afin de pouvoir utiliser le contenu de nos fichiers `.env`, il va falloir effectuer quelques modifications dans notre config webpack.
Dans `webpack.config.js`, on va devoir ajouter ces lignes de code, avant le `module.exports`. Elles servent à lire 
le fichier correspondant à notre environnement et à rendre son contenu exploitable.

```js 
// Récupération des variables stockées dans le fichier .env correspondant
// à l'environnement courant.
const env = DotenvPlugin.config({ path: '.env.' + process.env.NODE_ENV}).parsed;
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});

```

Enfin, on va passer le contenu du fichier `.env` à notre application grâce à la méthode `DefinePlugin` de `webpack`. Il faut
donc ajouter cette ligne dans la liste des plugins de webpack en bas du fichier : 

```js 
new webpack.DefinePlugin(envKeys),
```

Au passage, si ce n'est pas déjà fait, on importe `webpack` en haut du fichier avec `const webpack = require('webpack');`

### 5 - Utiliser les variables !

On peut maintenant utiliser nos variables dans notre application grâce à la notation `process.env.VARIABLE`. Par exemple, 
si je veux utiliser la variable précédemment créée, je vais utiliser : 
```js
process.env.URL_API
```
