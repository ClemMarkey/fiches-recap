# Gestion des événements en React

## Principe

Quand on crée un élément React à partir d'un composant, on peut fournir des props dont certaines sont interprétées par React comme des gestionnaires d'événement.

Par exemple, la prop `onClick` permet de déclarer une fonction qui devra être utilisé pour créer un écouteur d'événement dans le navigateur :

``` js
React.createElement(
  // composant à instancier :
  Composant,
  // props :
  {
    className: 'app-button',
    onClick: function(event) { … }
  },
  …
);
```

Dans cet exemple, lorsque l'élément React (DOM virtuel) sera transformé en élément HTML, un écouteur d'événement sera créé, comme si on avait écrit : `element.addEventListener('click', function(event) { … });`

En version JSX :

``` jsx
<Composant className="app-button" onClick={function(event) { … }} />
```

## Événements supportés par React

Ils sont listés dans la documentation :

- https://fr.reactjs.org/docs/events.html
- https://fr.reactjs.org/docs/handling-events.html

## Bonnes pratiques

### Séparation DOM / DOM virtuel

Certains événements requièrent que leurs gestionnaires interviennent sur le DOM. C'est par exemple le cas des événements `submit` :

``` jsx
<form
  onSubmit={
    (event) => {
      event.preventDefault(); // empêcher le rechargement de la page
      this.setState({ … });
    }
  }
/>
```

Dans ce cas de figure, il est recommandé de séparer le code utilisant l'API du DOM, du code utilisant l'API du DOM virtuel :

``` jsx
// Agit sur le DOM.
handleSubmit(event) {
  event.preventDefault();
  this.update();
}

// Agit sur le DOM virtuel.
update() {
  this.setState({ … });
}

<form
  onSubmit={handleSubmit}
/>
```
