# Redux avancé

## Preloaded State

Lorsqu'on crée le store, on peut injecter des données initiales dans notre state.  
C'est très utile quand on doit reprendre un state qui aurait été calculé côté serveur.  
Pour cela, on passe l'objet que l'on souhaite en deuxième argument de la fonction `createStore`.

Par exemple :
```html
<!doctype html>
<html>
  <body>
    <div id="root"></div>
    <script>
    var preloadedState = {
      user: 'Rouky',
      tasks: [
        { id: 1, label: 'Tourner un film', done: true },
        { id: 2, label: 'Signer des autographes', done: false }
      ]
    };
    </script>
    <script src="/js/app.js"></script>
  </body>
</html>
```

```js
import { createStore } from 'redux';
import reducer from 'src/store/reducer';

const store = createStore(reducer, window.preloadedState);
```


## Middleswares

Pour gérer les effets de bord séparément, on va aller placer toute notre logique qui n'est pas directement lié au state (ajax, websocket, setTimeout, etc.) au sein de middlewares.

![Fonctionnement du middlesware avec React](img/middleware.jpg)

### Définition

Un middleware se définit de la façon suivante :
```js
export default store => next => (action) => {
  // ...
}
```

Il s'agit d'une triple fléchée :scream:  
Pas de panique, son utilisation reste très simple, et ressemble à une fonction à 3 arguments :
* `store` : le store de redux, on pourra utiliser `store.getState()` et `store.dispatch()`.
* `next` : il s'agit du middleware suivant. Il faudra exécuter cette fonction en lui passant `action`, pour qu'il puisse lui aussi traiter l'action comme il le souhaite.
* `action` : l'action qui vient d'être dispatchée.

Un middleware se déclenche à chaque fois qu'une action survient.  
Dans son fonctionnement, cela ressemble beaucoup à un reducer, mais pour des actions externes.  
Par exemple :
```js
/*
 * Npm import
 */
import axios from 'axios';


/*
 * Local import
 */
import { TASK_DELETE } from 'src/store/ducks/todo';


/*
 * Code
 */
export default store => next => (action) => {
  // On écoute les actions qui nous intéressent
  switch (action.type) {
    case TASK_DELETE: {
      const { id } = action;
      axios.post('http://localhost:3000/task/delete', { id });
      break;
    }

    default:
  }

  // On passe à son voisin
  next(action);
};
```

Pour comprendre un peu mieux ce `next(action)`, voyez plutôt :

![Representation du fonctionnement de  next(action) ](img/next.jpg)


### Appliquer un middleware

Pour appliquer un middleware dans notre store, on utilise la fonction `applyMiddleware` de Redux.  
Puis, on l'ajoute en argument de la fonction `createStore`.

Par exemple :
```js
import { createStore, applyMiddleware } from 'redux';
import reducers from 'src/store/reducers';
import ajax from 'src/store/middlewares/ajax';

const ajaxEnhancer = applyMiddleware(ajax);
const store = createStore(reducers, ajaxEnhancer);

export default store;
```

Si on veut ajouter plusieurs middlewares ?  
Pas de problème, la fonction `applyMiddleware` peut prendre plusieurs arguments.

Par exemple :
```js
import { createStore, applyMiddleware } from 'redux';
import reducers from 'src/store/reducers';
import ajax from 'src/store/middlewares/ajax';
import notifications from 'src/store/middlewares/notifications';

const enhancers = applyMiddleware(ajax, notifications);
const store = createStore(reducers, enhancers);

export default store;
```

Et si on a déjà passé un `preloadedState` en 2e argument ?  
Pas de problème, les enhancers peuvent être passés en 3e argument.

Par exemple :
```js
import { createStore, applyMiddleware } from 'redux';
import reducers from 'src/store/reducers';
import ajax from 'src/store/middlewares/ajax';
import notifications from 'src/store/middlewares/notifications';

const enhancers = applyMiddleware(ajax, notifications);
const store = createStore(reducers, window.preloadedState, enhancers);

export default store;
```


## DevTools Extension

[Redux DevTools Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd) est une extension Chrome nous permettant d'inspecter très facilement notre store, le state, son évolution et chacun des actions qui se déclenchent.

Cette extension rajoute une variable globale `devToolsExtension` sur nos pages.  
Elle contient une fonction qui renvoie un enhancer que l'on peut passer à la création de notre store.

```js
import { createStore } from 'redux';
import reducers from 'src/store/reducers';

const store = createStore(reducers, window.devToolsExtension());

export default store;
```

Cependant, ce code va bugguer lorsque l'on va tester notre site sur un navigateur qui n'a pas l'extension.  
`window.devToolsExtension` sera `undefined` si l'extension n'est pas disponible, et la fonction `createStore` renverra une erreur.

Pour éviter de se retrouver dans cette situation, on peut utiliser la technique du spread (`...`) pour venir déverser le contenu de notre array en argument. Si l'array est vide, aucun argument ne sera ajouté. On remplit notre array avec notre enhancer seulement si la variable est définie :

```js
import { createStore } from 'redux';
import reducers from 'src/store/reducers';

// Redux DevTools Extension
const devTools = [];
if (window.devToolsExtension) {
  devTools.push(window.devToolsExtension());
}

// Store
const store = createStore(reducers, ...devTools);

export default store;
```

Et si nous avons déjà des enhancers, car nous avons rajouté des middlewares ?  
On doit alors utiliser la fonction `compose` de Redux pour coupler nos enhancers.

Par exemple :
```js
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from 'src/store/reducers';
import ajax from 'src/store/middlewares/ajax';
import notifications from 'src/store/middlewares/notifications';

// Middlewares
const middlewares = applyMiddleware(ajax, notifications);

// Redux DevTools Extension
const devTools = [];
if (window.devToolsExtension) {
  devTools.push(window.devToolsExtension());
}

// Compose enhancers
const enhancers = compose(middlewares, ...devTools);

// Store
const store = createStore(reducers, window.preloadedState, enhancers);

export default store;
```

Une fois installé, on peut découvrir plein d'informations croustillantes sur notre store.  
→ [Redux DevTools](https://github.com/gaearon/redux-devtools)


## Résumé

Pour résumer : une action est déclenchée via `dispatch`, les middlewares interceptent les actions pour déclencher des actions asynchrones comme des appels ajax à une API, puis les reducers appliquent ces actions sur le state actuel pour créer un nouveau state qui sera redonné à la vue pour modifier l'UI.

![store](img/store-with-middlewares.gif)
