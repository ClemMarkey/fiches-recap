
### S1 | Introduction

- Révisions JS
- Prog. fonctionnelle
- Prog. déclarative
- ES6

### S2 | React

- Les outils
- React
- Composants
- State - class
- Lifecycles - class
- Composants controlés
- Bonus : Sass

### S3 | React avancé

- React et AJAX
- Hooks - useState & useEffect
- Router

### S4 | Redux

- Gestion de state
- Redux
- react-redux
- Organisation
- Middleware

### S5 | Projet #1

- Middleware
- Auth
- Bonus : Socket.io

### S6 | Projet #2

- Router et Redux
- Auth persistant
- Tests et TDD

### S7 | Vers l'apothéose

- Ouvertures 
- Outil : Create-React-App
- FAQ
