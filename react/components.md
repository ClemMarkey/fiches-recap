# Les composants React

## Presentational / Container Component

Lorsque l'on conçoit une interface, on a deux choses à gérer :
* La manipulation des données
* La façon dont les données vont être représentées

Par exemple, pour une to-do list, on doit savoir :
* Les tâches qu'on a ajouté (leur label, si elles sont effectuées ou pas…)
* Les éléments du DOM qui représente graphiquement ces tâches, le style associé…

Afin de se simplifier la vie, on va **séparer ces concepts**. On va donc utiliser
les composants React de deux manières différentes :
* Les Container Components, qui manipulent les données.
* Les Presentational Components, qui s'occupent de l'UI.

Dans une application, les containers reçoivent de l'information (en important un fichier, en faisant un appel ajax, etc.) et utilisent des composants de présentation
pour le rendu.

## Stateless Component

### Juste une fonction…

C'est la méthode la plus simple pour créer un composant React.  
Ce sera notre méthode préférée pour les composants de présentation.

```js
const MonComponent = (props) => (
  <div>Hello {props.name}!</div>
);
```

On pourra alors utiliser le composant de cette façon :

```js
import MonComponent from 'src/components/MonComponent';

// ...

<MonComponent name="World" />
```

### …Pure

Les fonctions utilisées pour créer des composants doivent être **pures**.

Une *pure function* est une fonction qui, pour les mêmes arguments reçus en paramètres, renverra toujours le même résultat.

```js
// Pure function
const ajouteDix = x => x + 10;

// Impure function
const random = (min, max) => min + Math.random() * (max - min);
```

Pour un composant React, cela veut dire que pour les mêmes props, le JSX renvoyé ne doit pas changer. Ainsi, on s'assure que la vue générée par un composant de présentation dépend uniquement des datas qui proviennent du composant container.

### Avantages / Inconvénients

Les avantages :
* Facile à écrire et à comprendre. C'est juste une fonction !

* Rapidement éxécutée, donc assez léger.

Les inconvénients :
* Pas de `state`, obviously.

* Léger, mais qui s'éxécute à chaque fois qu'un render parent survient.  
A nuancer, car la réconciliation est parfois plus rapide qu'une comparaison
dans un `shouldComponentUpdate`.

* Pas de lifecycles.  
A nuancer, car on utilise en général les Stateless Components pour les composants
de présentation, qui n'ont de toute façon pas souvent besoin de lifecycles.


## State Component

### Une classe JS…

Pour créer un state component, il faut passer par une classe.  
ES2015 permet la syntaxe suivante :

```js
class User {
  constructor(name) {
    this.name = name;
    this.count = 0;
  }

  hello() {
    this.count++;
    console.log(`Hello ${this.name}!`);
  }
}
```

> Cf. fiche-récap [`this`](../js/this.md).

Avec le plugin babel `transform-class-properties`, on peut même écrire :

```js
class User {
  count = 0

  constructor(name) {
    this.name = name;
  }

  hello() {
    this.count++;
    console.log(`Hello ${this.name}!`);
  }
}
```

### …qui étend React

Grâce au mot-clef `extends`, on peut étendre les classes de la library React.  
L'extenson de la `React.Component` permet notamment l'utilisation du `state`.

```js
class Light extends React.Component {
  state = {
    on: false,
  }

  render() {
    const { on } = this.state;
    return (
      <div className="light">
        Light is {on ? 'on' : 'off'}
      </div>
    )
  }
}
```

On peut alors utiliser la méthode `setState` pour modifier le state :

```js
class Light extends React.Component {
  state = {
    on: false,
  }

  toggleLight = () => {
    const light = !this.state.light;
    this.setState({ light });
  }

  render() {
    const { on } = this.state;
    return (
      <div className="light">
        Light is {on ? 'on' : 'off'}
      </div>
    );
  }
}
```

En plus d'avoir accès au state, les State Component peuvent définir des méthodes
de lifecycles. Cela permet d'éxécuter du code lorsque le composant évolue.
Les State Component seront donc notre méthode préférée pour créer des Containers.

La doc de React sur le sujet est plutôt complète :  
→ [State and Lifecycles](https://reactjs.org/docs/state-and-lifecycle.html)

Pour visualiser, voici un diagrame des méthode de lifecycle disponible **à
partir de React 16.3**. Ceux qu'on utilisera le plus sont ceux de la phase
de commit, c'est-à-dire après que React ait fait le boulot : `componentDidMount`,
`componentDidUpdate` et `componentWillUnmount`.

![lifecycle](img/lifecycle.jpg)


### Avantages / Inconvénients

Les avantages :
* Accès au `state`, obviously.

* Permet de définir des lifecycles ! Sera donc souvent indispensable à toute
utilisation qui va plus loin que le simple affichage / évènements.

* En définissant la fonction `shouldComponentUpdate`, on peut obtenir
un composant plus optimisée qu'un stateless component.

Les inconvénients :
* Moins facile à écrire, mais aussi plus difficile à comprendre,
car ce n'est plus juste qu'une simple pure fonction.

* Un peu plus lourd lorsque l'on utilise pas de `shouldComponentUpdate`.


## PropTypes

Dans nos composants, on va pouvoir faire des vérifications d'existence
et de types sur les props que reçoivent nos composants.

→ [Fiche récap sur les `prop-types`](prop-types.md)
