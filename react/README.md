# React

Ces fiches-récap sont associées à la [spécialité React](https://oclock.io/specialites/react) chez O'clock.

## Fondamentaux

* [Styles de programmation utilisés en React](styles-de-programmation.md)
* [Nouveautés ES6+](ES2015.md)
* [Le JSX](JSX.md)
* [Les PropTypes](prop-types.md)
* [Les différents types de composants React](components.md)
* [Gestion d'événements](evenements.md)

## Gestion d'état

* [Redux](redux.md)
* [Redux avec React](redux-avec-react.md)
* [Redux avancé](redux-avance.md)

## Compléments

* [Relation entre back & front](back-to-front.md)
* [Webpack](webpack.md)
* [Modules npm utiles](modules.md)
* [Création d'un module npm](create-module-npm.md)
* [Utiliser des variables d'environnement](variables-environnement.md)
