# Les PropTypes

Il s'agit d'un module nous permettant d'effectuer de tests de types et d'existence sur les props de nos composants.  
Cela permet de s'assurer que les données s'écoulent de composants en composants sans erreur.

* [Lien Github](https://github.com/reactjs/prop-types)
* [Lien vers les différents PropTypes](http://facebook.github.io/react/docs/typechecking-with-proptypes.html)

## Installation

On installe le paquet :
```
yarn add prop-types
```

## Utilisation

On l'importe, et on peut ensuite l'utiliser en rajoutant une propriété `propTypes`
aux composants que l'on souhaite tester :
```js
import PropTypes from 'prop-types';

const HelloWorld = ({ username, children }) => (
  <div id="hello-world">
    <p>Je m’appelle {username}.</p>
    {children}
  </div>
);
HelloWorld.propTypes = {
  username: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
```

Attention :
* Par convention d'écriture, on écrit avec une majuscule le nom de la librarie : `PropTypes`.
* En revanche, la propriété qu'on rajoute au composant s'écrit : `Component.propTypes`.

## En pratique

**Qu'est-ce qu'il se passe si le test n'est pas bon ?**

* Si on se trompe de type (par exemple on met un `number` à la place d'une `string`) :
on a un message dans la console -> `Warning: Failed prop type: Invalid prop {...}`

* Si on ne met rien alors que la propriété est requise (`.isRequired`) :
on a un message dans la console -> `Warning: Failed prop type: The prop {...} is marked as required in {...}`

---

### Exemple de propTypes

```js
const HelloWorld = ({ language, children }) => (
  <div id="hello-world">
    <h1>Ce HTML a été généré par {language.type}</h1>
    <h2>Et affiché avec {language.method}</h2>
    {children}
  </div>
);

HelloWorld.propTypes = {
  language: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

// ...

<HelloWorld language={{ type: 'React', render: 'ReactDOM'}} />
```

### Exemple de shape

```js
HelloWorld.propTypes = {
  // Shape : C'est un objet qui prend une certaine forme.
  language: PropTypes.shape.({
    type: PropTypes.string.isRequired,
    render: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.node.isRequired,
};
```
