# Redux

* [Docs de Redux](http://redux.js.org/)
* [GitHub Redux](https://github.com/reactjs/redux)


## Qu'est-ce que Redux ?

Depuis que l'on développe avec React, force est de constater que de passer une prop de composant en composant pour arriver à un niveau plus bas peut vite devenir pénible... Passer le sel, c'est sympa deux minutes.

Sans compter que le code devient vite illisible pour le composant container racine qui se retrouve facilement à plus de 200 lignes tellement on lui passe de handlers ou autres méthodes.

L'idée de Redux, c'est de déporter l'état de notre application en dehors de nos composants React. Une image vaut plus que 1000 mots :

![Representation with/without Redux](img/redux.png)


## Installation

Pour utiliser Redux, on installe tout d'abord le package [redux](https://github.com/reactjs/redux).

```shell
# Avec Yarn
yarn add redux

# Avec Npm
npm install redux
```

On peut ensuite se créer un store avec la fonction `createStore`.

```js
import { createStore } from 'redux';
import reducer from './reducer';

const store = createStore(reducer);
```


## Utilisation

Les méthodes les plus utiles de notre store sont les suivantes :
* `store.getState()` : pour venir récupérer notre `state`
* `store.dispatch(action)` : pour dispatcher une action = générer un nouveau state en donnant l'action à nos reducers.
* `store.subscribe(func)` : pour s'abonner aux changements du state = les callbacks seront exécutés après chaque dispatch.


## Reducers

Il faut passer à la fonction `createStore` une fonction appelée `reducer`.  
Comme son nom l'indique, c'est une fonction sous la forme `(cumul, item) => result`.

Ou plus exactement `(state, action) => newState`  
= Cette fonction est responsable de créer le nouveau state à partir de l'ancien et d'une action à traiter.

Par exemple :
```js
const state = { light: true };           // La lumière est éteinte
const action = { type: 'SWITCH_OFF' };   // Action pour éteindre la lumière
const newState = reducer(state, action); // => le nouveau state { light: false }
```

Une manière d'écrire nos reducers est d'utiliser un `switch` pour modifier le state en fonction du type d'action.  
Par exemple :
```js
const initialState = { light: true };
const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case 'SWITCH_OFF':
      return {
        ...state,
        light: false,
      };
    default:
      return state;
  }
};
```

De cette manière, on respecte plusieurs règles importantes :
* Notre fonction est pure : elle ne modifie pas directement l'ancien state, car on utiliser `{ ...state }` pour faire une copie.
* Lorsque notre reducer ne reconnaît pas l'action, on renvoie le state qui n'a pas été modifié.
* Le state par défaut est l'état initial, l'action par défaut est un objet vide.

:warning: Lorsqu'on utilise un `switch`, les variables sont locales au `switch` entier, et non à chaque `case`.  
On peut ajouter une paire d'accolade `{ }` pour venir créer une portée de variable pour chaque `case`.  
Par exemple :
```js
switch (action.type) {
  case TASK_ADD: {
    // La variable `task` ne sera définie que dans le case
    const { task } = action;
    return {
      ...state,
      list: [...state.list, task],
    };
  }
  default:
    return state;
}
```


## Action Creators

Pour ne pas avoir à se rappeler du format de toutes nos actions, on va plutôt se créer et exporter des fonctions retourner un objet d'action. On appelle cela des `action creators`.

Par exemple :
```js
const switchOff = () => ({
  type: 'SWITCH_OFF',
});

const changeName = name => ({
  type: 'CHANGE_NAME',
  name,
});
```


## Types

On a également tendance à créer des constantes pour nos types.
Avec une variable, ESLint nous alerte en cas de faute de frappe.
On repère donc plus facilement nos erreurs de cette façon.

Par exemple :
```js
/*
 * Types
 */
export const SWITCH_OFF = 'SWITCH_OFF';

export default (state = {}, action = {}) => {
  switch (action.type) {
    case SWITCH_OFF:
      return {
        ...state,
        light: false,
      }
    default:
      return state;
  }
}

export const switchOff = () => ({
  type: SWITCH_OFF,
});
```

Il est alors agréable de regrouper types, reducer et action creators au sein d'un même fichier.  
On appelle ça des [`ducks`](https://github.com/erikras/ducks-modular-redux).


## Plusieurs reducers

Si notre application grossit, on pourra découper nos applications en plusieurs ducks.  
On pourra aller placer nos types, reducer et action creators dans un nouveau fichier chaque fois que l'on identifie une partie de notre application cohérente et séparée du reste.  
Par exemple : `user.js`, `settings.js`, `chat.js`…

Cependant, la fonction `createStore` ne prend qu'un seul reducer. On doit donc utiliser la fonction `combineReducers` afin de fusionner nos différents reducers.  
Par exemple :
```js
import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import user from 'src/store/user';
import settings from 'src/store/settings';
import chat from 'src/store/chat';
import userMiddleware from './userMiddleware';

// DevTools
const devTools = [];
if (window.devToolsExtension) {
  devTools.push(window.devToolsExtension());
}

// Middleware vers Enhancers
const userEnhancer = applyMiddleware(userMiddleware);
const enhancers = compose(userEnhancer, ...devTools);
const rootReducer = combineReducers({
  user,
  settings,
  chat,
});

// createStore
const store = createStore(rootReducer, enhancers);
export default store;
```

Il faudra également faire attention lors de vos appels au state. En effet, il vous faudra définir le reducer que vous souhaitez utiliser pour tel ou tel valeur. Par exemple dans un container :
```js
const mapStateToProps = state => ({
  userConnected: state.user.userConnected,
  chatConnected: state.chat.chatConnected,
});
```

Ou dans un Midlleware : `store.getState().user.userConnected`, `store.getState().settings.stateValue`


## Résumé

Pour résumer : une action est déclenchée via `dispatch`, les reducers appliquent cette action sur le state actuel pour créer un nouveau state qui sera redonné à la vue pour modifier l'UI.

![store](img/store.gif)


## Utilisation avec React

Plutôt qu'utiliser `store.subscribe()` au sein de chacun des composants intéressés par le state, nous pouvons utiliser la librairie `react-redux`.  
→ [Fiche récap React-Redux](redux-avec-react.md)


## Utilisation avancée

Pour exécuter des actions asynchrones ou activer les DevTools permettant de visualiser les actions et les modifications du state, on peut passer d'autres arguments à la fonction `createStore` de Redux.  
→ [Fiche récap Redux avancée](redux-avance.md)  
→ [Glossaire Redux](https://github.com/reactjs/redux/blob/master/docs/Glossary.md)
