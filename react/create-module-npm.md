# Création d'un module public sur npm

## Pré-requis

* Avoir installé [**Node.js**](https://nodejs.org/en/) et **npm** (installé avec Node.js)
* Avoir un compte [npm](https://www.npmjs.com) et avoir confirmé votre adresse e-mail


## Introduction

### Dire à npm, qui vous êtes.

Écrire les lignes suivantes dans un terminal :

* `npm set init.author.name "VOTRE NOM"`
* `npm set init.author.email "VOTRE EMAIL"`
* `npm set init.author.url "VOTRE SITE"`


### Se connecter à npm

* `npm login` et suivre les instructions


## :books: Package.json

Toujours dans un terminal, pour initialiser un package.json, écrire :

* `npm init`, et suivre les instructions.

> :bulb: Pour le numéro de version, si le projet n'est pas encore développé, vous pouvez mettre quelque chose comme `0.0.0-dev`.

Quelques explications concernant le `package.json` :

  * `main` : vous pouvez ajouter dans votre package.json une propriété `main`, qui va alors indiquer le fichier qui va servir à **initialiser** votre module (_comme `src/index.js` pour React_)
  * `files` : vous pouvez également ajouter une propriété `files`, un **Array** où vous indiquerez ce qui va être envoyé à npm lors de la publication (_par exemple, le dossier `public` ou le dossier `dist`_)
  * `keywords` / `description` : vous pouvez indiquer des mot-clés qui pourront servir à retrouver votre projet sur [npmjs](https://www.npmjs.com) ; même principe avec la description


## :construction: Tester votre module

Si vous souhaitez tester votre module dans un projet de `test`, vous avez la possibilité de créer un « lien symbolique » en utilisant la commande [link](https://docs.npmjs.com/cli/link).

Pour ce faire :

* Dans le dossier du module que vous êtes entrain de développer, créer le lien en utilisant la commande `npm link` (ou `yarn link`)
* Positionnez-vous ensuite dans le dossier de votre projet de test, et tapez la commande `npm link "NOM_DE_VOTRE_PACKAGE"`, ce nom est celui que vous avez défini précédemment dans votre fichier `package.json`

Pour tester vos modifications dans votre projet :
  
* Vous devez `build` votre module à chaque modification
* Vous devez compiler à nouveau (`yarn start`) du côté de votre projet afin de prendre en compte les modifications 


Une fois que vous avez fini de tester, vous pouvez « casser » le lien symbolique :

* `npm unlink "votre_module"` (ou `yarn unlink "votre_module"`) dans votre projet de test
* Ainsi que la commande `npm unlink` (ou `yarn unlink`) dans le dossier de votre module


## :rocket: Build

En ce qui concerne le Build de module, vous ne pouvez -- _pour l'instant_ -- pas vous servir de **Brunch** pour la compilation de vos fichiers.

Il vous faut alors utiliser à la place :

* Soit un autre outil de build, comme **webpack**
* Soit utiliser Babel via `babel-cli` pour la compilation. On devra alors utiliser la commande suivante dans le terminal `babel app -d build`. (Je lui demande de me compiler le dossier source **app**, dans un nouveau dossier nommé **build**)


## :checkered_flag: Publication

Après avoir vérifié dans votre `package.json` que votre **version:** est à jour, écrire dans un terminal `npm publish` (ou `yarn publish`) et suivez les instructions.

* Si tout se passe bien, félicitation votre package est en ligne :rocket:
* Dans le cas contraire, npm vous indique pourquoi la procédure de publication à échoué


### :bulb: Diverses commandes à connaître

* `npm unpublish --force` : permet de retirer un package publié de npm

> Toutes les autres commandes sont disponibles [ici](https://docs.npmjs.com)
