# Brunch

## 1. Structurer son application

### 1.1 Dossier et fichiers
```
project/
|
|-- app/
|    |-- assets/
|    |-- src/
|    └-- styles/
|
|-- node_modules/
|    └-- // npm packages...
|
|-- public/
|    └-- // Brunch's compiled files...
|
|-- .gitignore
└-- brunch-config.js
```

Si vous voulez créer tous les dossier et fichiers :smiley:

```
mkdir app app/assets app/src app/styles public && touch .gitignore brunch-config.js
```

### 1.2 Le `.gitignore` basique

```
.DS_Store     # Fichier système sous mac OS
Thumbs.db     # Fichier système sous windows
/node_modules # Dossier contenant les packages installé via `NPM` ou `Yarn`
/public       # Dossier contenant les fichiers compilés par brunch
```


## 2. Installer brunch et ses dépendances

On peut le faire de deux manière possible : avec le gestionnaire de paquet `yarn` ou avec `npm`.  
D'une manière générale, Yarn est plus rapide et plus sécurisé.

### 2.1. Initialisation

Avec `yarn`
```shell
yarn init
```

Avec `npm`
```shell
npm init
```

### 2.2. Installation des packages nécéssaires

Liste des packages :
- `brunch`
- `babel-brunch` => Permet à brunch d'utiliser babel pour transpiler (parser + transformer le code en ES5) notre code
- `auto-reload-brunch` => actualise automatiquement le navigateur lorsqu'on sauvegarde des modifications
- `babel-preset-env` => plugin de babel permettant de transpiler les dernières versions de ES201x
- `babel-preset-react` => plugin de babel permettant de transpiler le JSX

Avec `Yarn`

```shell
yarn add --dev brunch babel-brunch auto-reload-brunch babel-preset-env babel-preset-react
```

Avec `npm`

```shell
npm install --save-dev brunch babel-brunch auto-reload-brunch babel-preset-env babel-preset-react
```

> Warning : En VM Server, si erreurs, exécuter `npm install --no-bin-links` pour ne pas utiliser de liens symboliques, inexistants sur le dossier de partage machine hôte/VM.

Si tout se passe bien, deux nouveaux fichiers vont être créés : `package.json` (et `yarn.lock` ou `package.lock`) ainsi qu'un dossier : `node_modules`.


## 3. Paramétrer brunch

Vous pouvez le faire dans un fichier `.coffee` ou `.js`, comme vous préférez.

### 3.1. Exemples de config

#### `brunch-config.coffee`
```coffee
exports.config =
  files:
    stylesheets:
      joinTo: 'css/app.css'
      order:
        before: 'app/styles/reset.css'

    javascripts:
      joinTo: 'js/app.js'
```

#### `brunch-config.js`
```js
exports.config = {
  files: {
    stylesheets: {
      joinTo: 'css/app.css',
      order: {
        before: 'app/styles/reset.css'
      }
    },
    javascripts: {
      joinTo: 'js/app.js'
    }
  }
}
```

### 3.2 Npm scripts

Et voilà, vous avez fini d'installer brunch. Pour vérifiez si ça marche, vous pouvez lancer un `brunch build` (qui devrait se dérouler sans erreurs), si vous avez installé brunch en global (`yarn add --global brunch`).

Vous pouvez aussi créer des **npm scripts**.  
Il s'agit d'une convention lorsqu'on travaille avec NodeJS : on lance l'application en faisant `npm start` ou `yarn start`.

On va créer un script "start" qui va nous permettre de lancer notre application.
D'autres développeurs pourront lancer l'application sans savoir qu'elle utilise brunch.
Ou si un jour, on souhaite remplacer brunch par un autre outil de build, on pourra toujours lancer l'application avec un `yarn start`.

Pour cela, on peut rajouter à notre `package.json` :
```
  "scripts": {
    "start": "brunch watch --server",
    "build": "brunch build --production"
  }
```

On peut aussi se créer d'autres scripts.
Ci-dessus, on a également créer un script nous permettant de compiler brunch en faisant `yarn build`.

### 3.2. Tester si ça fonctionne

Pour tester notre application Brunch, on se rajoute quelques autres fichiers :

`app/assets/index.html`
```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Modèle</title>
		<link rel="stylesheet" href="css/app.css">
	</head>
	<body>
		<div id="root"></div>
		<script src="js/app.js"></script>
		<script>require('src/index');</script>
	</body>
</html>
```
---

`app/src/index.js`
```js
import coucou, { sum } from 'src/utils/maths';

coucou();
console.log(sum(2, 3));
```
---

`app/src/utils/maths.js`
```js
export const sum = (a, b) => a + b;
export default () => console.log('coucou');
```

Comment tester ?
- Ajoutez tous ces fichiers
- Lancez la commande à la racine de votre projet : `yarn start`
- Ouvez un navigateur et allez à http://localhost:3333/
- Ouvrez la console
- Si vous voyez `coucou` et `5` dans la console, c'est que vous avez correctement configuré brunch ! :tada:

## 4. Installer eslint

Avec `Yarn`
```shell
yarn add --dev eslint babel-eslint eslint-config-airbnb eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import eslint-import-resolver-node
```

Avec `npm`
```shell
npm install --save-dev eslint babel-eslint eslint-config-airbnb eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import eslint-import-resolver-node
```

Qu'est-ce que ça installe ?
- `eslint` => Le linter
- `babel-eslint ` => le parser qui permet à ESLint de lire le ES2015
- `eslint-config-airbnb ` => pour prendre les [règles d'Airbnb](https://github.com/airbnb/javascript)
- `eslint-plugin-react` => permet d'ajouter des règles spécifiques à React
- `eslint-plugin-jsx-a11y` => permet d'ajouter des règles d'accessibilité pour React
- `eslint-plugin-import` => permet à ESLint de vérifier les chemins d'import de fichiers
- `eslint-import-resolver-node` => permet d'ajouter une config à ESLint pour qu'ils vérifient les chemins d'import comme brunch le fait


## 5. Une fois que tout est installé ?

Les commandes à se rappeler une fois que tout est en place :
- `brunch build` / `brunch b` : compiler les fichiers
- `brunch build --production` / `brunch b -p` : compiler les fichiers, et appliquer les optimisations. Aussi accessible par `yarn build` si on a ajouté le npm script dans le `package.json`.
- `brunch watch` / `brunch w` : lancer le watch, pour compiler à chaque sauvegarde
- `brunch watch --server` / `brunch w -s` : lancer le watch, avec un mini-serveur bien pratique (localhost:3333). Aussi accessible par `yarn start` si on a ajouté le npm script dans le `package.json`.

> Warning : En VM Server, si erreurs, exécuter `brunch watch -s -n` pour lancer l'appli et y accéder hors de la VM.

## 6. Récap pour créer un projet

[Créer un projet React avec Brunch](brunch-projet.md)
