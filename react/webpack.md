# Webpack

## 1. Structurer son application

### 1.1 Dossier et fichiers
```
project/
|
|-- dist/
|    └-- // Fichiers pour la production (distribution)
|
|-- src/
|    |-- assets/
|    |-- components/
|    |-- styles/
|    |-- index.html
|    └-- index.js
|
|-- node_modules/
|    └-- // npm packages...
|
|-- .babelrc
|-- .eslintignore
|-- .eslintrc
|-- .gitignore
|-- package.json
└-- webpack.config.js
```


### 1.2 Le `.gitignore` basique

```
.DS_Store     # Fichier système sous mac OS
Thumbs.db     # Fichier système sous windows
/node_modules # Dossier contenant les packages installé via `npm` ou `yarn`
/dist         # Dossier contenant les fichiers compilés
```


## 2. Installer webpack et ses dépendances

On peut le faire de deux manière possible : avec le gestionnaire de paquet `yarn` ou avec `npm`.  
D'une manière générale, Yarn est plus rapide et plus sécurisé.

### 2.1. Initialisation

Avec `yarn`
```sh
yarn init
```

Avec `npm`
```sh
npm init
```

### 2.2. Dépendaces de développement

#### Webpack (Bundler de fichiers)

- `webpack`
> Le bundler (empaqueteur de fichiers)

- `webpack-cli`
> Les commandes pour webpack en ligne de commandes

- `webpack-dev-server` 
> Le serveur de développement, prévisualisation du projet (rechargement automatique, compilation à la volée, ...)

- `html-webpack-plugin`
> Plugin permettant de construire le fichier HTML de production à partir d'un template (présent à la racine de `src/`)

- `mini-css-extract-plugin`
> Plugin permettant de construire le(s) fichier(s) CSS de production à partir des fichiers du projet `src/...`

- `optimize-css-assets-webpack-plugin`
> Plugin permettant de minifier (alléger) le(s) fichier(s) CSS de production

- `uglifyjs-webpack-plugin`
> Plugin permettant de minifier (alléger) le(s) fichier(s) JS de production

``` sh
# Webpack
yarn add --dev webpack webpack-cli
# serveur de developpement
yarn add --dev webpack-dev-server
# Plugins
yarn add --dev html-webpack-plugin
yarn add --dev mini-css-extract-plugin
yarn add --dev optimize-css-assets-webpack-plugin
yarn add --dev uglifyjs-webpack-plugin
```

webpack.config.js

#### Babel (Transpilation / transformation de ES6/JSX vers ES5)

- `babel-core`
> babel, le transpileur

- `babel-loader`
> La configuration pour que webpack puisse utiliser babel

- `babel-preset-env`
> Le vocabulaire ES6 pour babel

- `babel-preset-react`
> Le vocabulaire React/JSX pour babel

- `babel-plugin-transform-class-properties`
> Plugin babel pour prendre en compte les propriétés de classe

- `babel-plugin-transform-object-rest-spread`
> Plugin babel pour comprendre les opérateurs "rest" et "spread"

``` sh
# Babel
yarn add --dev babel-core
# Babel pour webpack
yarn add --dev babel-loader
# vocabulaire ES6 -> ES5 de base
yarn add --dev babel-preset-env
# vocabulaire React
yarn add --dev babel-preset-react
# Plugin : propriétés de classes
yarn add --dev babel-plugin-transform-class-properties
# Plugin : rest et spread operator pour les objets
yarn add --dev babel-plugin-transform-object-rest-spread
```

- .babelrc

#### ESLint (Linter JavaScript)

- `eslint`
> ESLint, le linter

- `babel-eslint`
> Le parser qui permet à ESLint de lire le ES2015

- `eslint-config-airbnb`
> La configuration de ESLint de l'entreprise [Airbnb](https://github.com/airbnb/javascript)

- `eslint-plugin-import`
> Plugin permettant à ESLint de vérifier les chemins d'import de fichiers

- `eslint-import-resolver-webpack`
> Utilitaire permettant à ESLint de vérifier les imports en fonction de la configuration de webpack

- `eslint-plugin-react`
> Plugin ESLint permettant d'ajouter des règles spécifiques à React

- `eslint-plugin-jsx-a11y` 
> Plugin ESLint permettant d'ajouter des règles d'accessibilité pour React

``` sh
# ESLint
yarn add --dev eslint
# Config ESLint
yarn add --dev eslint-config-airbnb babel-eslint
# ESLint résolution des imports
yarn add --dev eslint-plugin-import 
yarn add --dev eslint-import-resolver-webpack
# ESLint pour React
yarn add --dev eslint-plugin-jsx-a11y eslint-plugin-react
```

eslintrc
.eslintignore

### Préprocesseur CSS et utilitaires pour les assets


- `style-loader`
> Loader permettant à webpack d'injecter des styles CSS dans la page

- `css-loader`
> Loader permettant à webpack de parcourir et traiter les styles CSS

- `file-loader`
> Loader permettant à webpack de parcourir les fichiers d'assets (images notament)

- `file-loader`
> Loader permettant à webpack de parcourir les fichiers d'assets (images notament)

- `postcss`
> Processeur CSS permettant des opérations d'optimisation

- `autoprefixer`
> Utilitaire CSS basé sur PostCSS pour préfixer le code CSS (en fonction des compatibilités navigateurs)

- `postcss-loader`
> Loader permettant à webpack d'utiliser PostCSS et autoprefixer

- `node-sass`
> Préprocesseur CSS compatible avec les syntaxes SCSS et SASS

- `sass-loader`
> Loader permettant à webpack d'utiliser les syntaxes SCSS et SASS


``` sh
# Traitement des styles et assets
yarn add --dev style-loader css-loader file-loader
# PostCSS et autoprefixer
yarn add --dev postcss autoprefixer postcss-loader
# SASS
yarn add --dev node-sass sass-loader
```

- .postcssrc
- .browserslistrc


### 2.3 Dépendances de projet

#### Utilitaires

- `babel-polyfill`
> Rend disponible plusieurs méthodes / syntaxes JavaScript pour des navigateurs ne possédant pas les implémentations récentes des ces utilitaires

``` sh
yarn add babel-polyfill
```

#### React

- `react`
> La librairie React

- `react-dom`
> La librairie react-dom permet de convertir les composants React vers des éléments du DOM et de les y insérer 

- `prop-types`
> Librairie de vérifications des "props" (données transmises entre les composants)


```sh
yarn add react react-dom prop-types
```

#### Autres librairies

- `classnames`
> Utilitaire de gestion facilitant le travail avec des classes CSS au sein du JavaScript

- `react-icons`
> Librairie proposant une implémentation des bibliothèques d'icones les plus répandues

- `axios`
> Librairie facilitant les échanges en AJAX

- `redux`
> Librairie facilitant la gestion de state

- `react-redux`
> Utilitaires pour connecter React et Redux

- `react-router-dom`
> Système de routes pour React

```sh
yarn add classnames
yarn add react-icons
yarn add axios
yarn add redux react-redux
yarn add react-router-dom
```

### 2.4 Erreurs fréquentes

#### bin

> Warning : En VM Server, si erreurs, exécuter `npm install --no-bin-links` pour ne pas utiliser de liens symboliques, inexistants sur le dossier de partage machine hôte/VM.

Si tout se passe bien, deux nouveaux fichiers vont être créés : `package.json` (et `yarn.lock` ou `package.lock`) ainsi qu'un dossier : `node_modules`.

#### The engine "node" is incompatible with this module. Expected version ...

Certains modules nécessite des versions spécifiques de Node pour assurer un bon fonctionnement

Si la version de Node `node --version` ne correspond pas aux besoins des modules, une erreur du type `The engine "node" is incompatible with this module. Expected version >= x.x.x || = x.x.x ...` apparait alors.

Cette liste de commandes permet d'installer la dernière version stable de Node

```sh
sudo npm install -g n
sudo n stable
```

Si une alerte de problème de cache NPM survient, la commande `sudo npm cache clean -f` permet de supprimer le cache de NPM **attention** à ne pas utiliser n'importe comment ;)


## 3. Configurer le projet

## 3.1. babel

Dans un fichier `.babelrc` à la racine du projet afin d'indiquer à babel comment fonctionner

```json
{
  "presets": ["env", "react"],
  "plugins": [
    "transform-class-properties",
    "transform-object-rest-spread"
  ]
}
```

### 3.2. webpack

Dans un fichier `webpack.config.js` à la racine du projet


#### Anatomie de la config

```js
// import Node et plugins
// ...

// Variables de configuration
// ...

module.exports = {
  mode: '', // ... mode de webpack (development | production | test)
  resolve: {
    // ... les règles pour les imports
  },
  entry: {
    // ... les fichiers en entrée
  },
  output: {
    // ... les fichier de sortie (nommage, emplacement, ...)
  },
  optimization: {
    // ... les optimisations de production / distribution
  },
  module: {
    rules: [
      // ... les différentes règles de fonctionnement
      // - JS et babel
      // - CSS / SASS / SCSS
      // - Images
      // - etc.
    ],
  },
  devServer: {
    // ... les options du serveur de prévisualisation
  },
  plugins: [
    // ... les plugins de webpack
  ],
};

```

#### Exemple de config

<details>

```js
// Import de Node
const path = require('path');

// Plugins de traitement pour dist/
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebPackPlugin = require('html-webpack-plugin');

// Config pour le devServer
const host = 'localhost';
const port = 8080;

const devMode = process.env.NODE_ENV !== 'production';

// Config de Webpack
module.exports = {
  // Passe le build par défaut en développement 
  mode: 'development',
  // Expose le dossier src/ pour les imports
  resolve: {
    alias: {
      src: path.resolve(__dirname, 'src/'),
    },
  },
  // Points d'entrée pour le travail de Webpack
  entry: {
    app: [
      // Styles
      './src/styles/index.sass',
      // JS
      './src/index.js',
    ],
  },
  // Sortie
  output: {
    // Nom du bundle
    filename: 'app.js',
    // Nom du bundle vendors si l'option d'optimisation / splitChunks est activée
    chunkFilename: 'vendors.js',
    // Cible des bundles
    path: path.resolve(__dirname, 'dist'),
  },
  // Optimisation pour le build
  optimization: {
    // Code spliting
    splitChunks: {
      chunks: 'all',
    },
    // Minification
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false // passer à true pour JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  // Modules
  module: {
    rules: [
      // JS
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          // babel avec une option de cache
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
            },
          },
        ],
      },
      // CSS / SASS / SCSS
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          // style-loader ou fichier
          devMode ? 'style-loader' :
            MiniCssExtractPlugin.loader,
          // Chargement du CSS
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')],
              sourceMap: true,
            },
          },
          // SASS
          'sass-loader',
        ],
      },
      // Inages
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/',
            },
          },
        ],
      },
    ],
  },
  devServer: {
    overlay: true, // Overlay navigateur si erreurs de build
    stats: 'minimal', // Infos en console limitées
    progress: true, // progression du build en console
    inline: true, // Rechargement du navigateur en cas de changement
    open: true, // on ouvre le navigateur
    host: host,
    port: port,
  },
  plugins: [
    // Permet de prendre le index.html de src comme base pour le fichier de dist/
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
    // Permet d'exporter les styles CSS dans un fichier css de dist/
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  ],
};
```

</details>

### 3.3 Npm scripts

Vous pouvez aussi créer des **npm scripts**.  
Il s'agit d'une convention lorsqu'on travaille avec NodeJS : on lance l'application en faisant `npm start` ou `yarn start`.

On va créer un script "start" qui va nous permettre de lancer notre application.

Pour cela, on peut rajouter à notre `package.json` :

```json
  "scripts": {
    "start": "webpack-dev-server",
    "build:dev": "NODE_ENV=development webpack --mode development",
    "build:prod": "NODE_ENV=production webpack --mode production",
  }
```

On peut aussi se créer d'autres scripts.
Ci-dessus, on a également créer un script nous permettant de compiler le projet (dans le dossier `dist/`) en faisant `yarn build:prod` pour la production ou bien `yarn build:dev` pour un build de développement.


### 3.4. Tester si ça fonctionne

Pour tester notre application Brunch, on se rajoute quelques autres fichiers :

`src/index.html`
```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modèle</title>
</head>
<body>
    <div id="root"></div>
</body>
</html>
```
---

`src/index.js`
```js
import coucou, { sum } from 'src/maths';

coucou();
console.log(sum(2, 3));
```
---

`src/maths.js`
```js
export const sum = (a, b) => a + b;
export default () => console.log('coucou');
```

Comment tester ?
- Ajoutez tous ces fichiers
- Lancez la commande à la racine de votre projet : `yarn start`
- Ouvez un navigateur et allez à http://localhost:8080/
- Ouvrez la console
- Si vous voyez `coucou` et `5` dans la console, c'est que vous avez correctement configuré votre projet ! :tada:


## 4. Configurer ESLint

### `.eslintrc`

Fichier contenant l'ensemble des règles que va respecter ESLint

**Exemple de configuration**
``` json
{
  "extends": "airbnb",
  "parser": "babel-eslint",
  "env": {
    "browser": true
  },
  "rules": {
    "brace-style": ["error", "stroustrup"],
    "no-param-reassign": ["error", { "props": false }],
    "no-mixed-operators": ["error", { "allowSamePrecedence": true }],
    "jsx-a11y/no-static-element-interactions": "off",
    "jsx-a11y/href-no-hash": "off",
    "jsx-a11y/no-noninteractive-element-interactions": "off",
    "react/jsx-filename-extension": "off",
    "react/forbid-prop-types": "off",
    "react/no-access-state-in-setstate": "warn",
    "react/jsx-one-expression-per-line": "off",
    "react/destructuring-assignment": "warn",
    "react/no-unescaped-entities": "off"
  },
  "settings": {
    "import/resolver": "webpack"
  }
}
```

### `.eslintignore`

Fichier indiquant à ESLint où ne pas regarder ;)

```text
node_modules/
dist/
webpack.config.js
```

## TLDR

### Projet

- le projet contient un dossier `dist/` avec les fichiers compilés
- le projet contient un dossier `src/` avec les sources de l'application
- `src/index.js` est le point d'entrée de l'application

### Outils

- `webpack` => bundler de fichiers
- `webpack-dev-serveur` => serveur de développement
- `babel` => transforme le code ES6/JSX vers ES5
- `ESLint` => linter permettant d'assurer le respect de conventions de structure et syntaxiques
- `SASS` => préprocesseur CSS utilisé

### Commandes utiles

- `yarn` installe les dépendances d'un projet existant
- `yarn start` lance le serveur de développement
- `yarn build:prod` lance la compilation du projet pour la production
