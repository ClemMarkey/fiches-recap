# Styles de programmation utilisés avec React

## Programmation orientée objet

React est codé en JavaScript. JavaScript est avant tout un langage de programmation orienté objet. Programmer une application React nécessite donc d'être à l'aise avec ce concept :ok_hand:

> En savoir plus : [fiche-récap _Programmation orientée objet_… en PHP !](https://github.com/O-clock-Alumni/fiches-recap/blob/f37d3e1705803c46b7523a80eac738674915c015/php/programmation-objet.md) (et oui, les concepts sont les mêmes en JS :heavy_check_mark:)

## Programmation fonctionnelle

React est (toujours) codé en JavaScript. JavaScript utilise beaucoup les fonctions. Programmer une application React nécessite donc de se familiariser avec la programmation fonctionnelle :ok_hand:

> En savoir plus : [fiche-récap _Programmation fonctionnelle_](https://github.com/O-clock-Alumni/fiches-recap/blob/master/js/programmation-fonctionnelle.md)

## Programmation déclarative

La [programmation déclarative](https://fr.wikipedia.org/wiki/Programmation_d%C3%A9clarative) est un [paradigme de programmation](https://fr.wikipedia.org/wiki/Paradigme_%28programmation%29) qui vise à séparer strictement données et _traitement_ de ces données. Le tout en mettant l'accent, dans le code, sur le résulat attendu de ces traitement, plutôt que sur les étapes nécessaires à leur réalisation (notion d'abstraction).
