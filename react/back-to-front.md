# Comment passer des datas du back au front ?

Il y a deux stratégies pour envoyer des données à notre application React.  
De deux choses l'une :
* Soit le front demande les données au back
* Soit le back envoie directement les données au rendu de la page

## Front : A tout moment, via AJAX

Pour cela, il faut :
* Définir une route sur le serveur
* Le JS envoie une requete AJAX sur cette route
* Le back renvoie un json contenant les données

```js
axios
  .get('/ma-route')
  .then(result => console.log(result.data));
```

Dans une application Redux, on pourrait utiliser un middleware pour effectuer
cette action :
* On envoie une action au chargement de la page
* Cette action est récupérée par le middleware
* La requête AJAX est envoyée
* Les données sont dispatchées via une autre action redux

Par exemple :

```js
const createMiddleware = store => next => (action) => {
  if (action.type === 'APP_LOADED') { // Le type de l'action lancée au chargement de la page
    axios
      .get('/ma-route')
      .then((result) => {
        store.dispatch(setDatas(result.data)); // setDatas est un action creator
      });
  }
  next(action);
};
```

On utilise souvent cette stratégie quand les données n'ont pas besoin
d'être utilisées tout de suite, pour le tout premier rendu.

On utilise par exemple cette stratégie dans le challenge Recipes,
pour récupérer le json des recettes de cuisine.

On peut également utiliser d'autres types d'envoi que de l'Ajax.  
Par exemple, du websoket, comme dans le challenge Chatroom.


## Back : Au chargement de la page

On peut aussi utiliser une stratégie différente, plus performante :
quand le back envoie le retour HTML, on y ajoute directement des données.
Pour ce faire, on ajoute au rendu html un `<script>`
qui contient des variables qu'on passe au JS.

Par exemple, dans une page PHP :

```php
<script>
var userId = <?= $user_id ?>;
var myObject = <?= json_encode($my_object) ?>;
</script>
```

Par exemple, dans un template Twig :

```twig
<script>
var userId = {{ app.user.id }};
var myObject = {{ my_object|json_encode()|raw }};
</script>
```

Cette technique permet d'avoir les données directement accessibles par le JS,
sans avoir à effectuer de requêtes supplémentaires. On utilise souvent cette
stratégie quand on a besoin des données pour effectuer le premier rendu
de notre application, et qu'on ne veut pas en retarder l'affichage.

On pourra utiliser cette variable où l'on veut dans notre javascript.  
On pourra par exemple l'utiliser pour le `preloadedState` de redux :
```js
const store = createStore(reducers, preloadedState, enhancers);
```
