# Modules

## Quelques modules utiles

- `axios` : pour faire des requêtes (ajax ou http)
- `classnames` : pour faire des classes conditionnelles
- `react-html-parser` : pour transformer des éléments HTML en composants
- `validate-email` : pour vérifier la validité d'un email
- `sanitize-html` : pour nettoyer du HTML (supprimer / remplacer des éléments)
- `sanitize-html-react` : clone de sanitize-html pour React.

## Listes

- https://github.com/brillout/awesome-react-components
- https://blog.bitsrc.io/11-react-component-libraries-you-should-know-178eb1dd6aa4
