# Le JSX

Lorsque l'on crée des composants React, on utilise du JSX pour définir nos vues.  
Il s'agit d'un **sucre syntaxique** permettant de décrire la création de nos éléments
plus facilement.

> Dans [VSCode](/vscode), l'extension recommandée pour la coloration syntaxique du JSX est [Sublime Babel](https://marketplace.visualstudio.com/items?itemName=joshpeng.sublime-babel-vscode). VSCode intègre également nativement des [fonctionnalités IntelliSense pour JSX](https://code.visualstudio.com/Docs/languages/javascript).

## Principe

En JavaScript, dans le navigateur, on décrit (== modélise) des éléments de la page HTML par des objets JS. L'ensemble de ces objets JS, organisés hiérarchiquement, forment le DOM. Par abus de language, on parle d'ailleurs « d'éléments du DOM », ce qui revient à mélanger les concepts de HTML et JS respectivement !

On peut créer des objets JS pour représenter des balises HTML (dans le but de les injecter dans la page HTML plus tard, avec `appendChild`). C'est une approche ultra-souple, mais verbeuse, car très explicite. Et c'est long à écrire !

```js
var link = document.createElement('a'); // link est un objet JS
link.id = 'home'; // objet qu'on configure à loisir…
link.href = 'https://oclock.io';
link.textContent = 'Accueil';
```

Les éléments du DOM correspondent à des balises HTML. Or, la syntaxe HTML est, certes moins souple, mais beaucoup plus concise :

```html
<!-- Un fichier HTML -->
<a id="home" href="https://oclock.io">Accueil</a>
```

L'idée de JSX, c'est de concilier le meilleur des deux mondes :

* on code du JS, donc on continue à créer/manipuler des objets ;
* mais on utilise la syntaxe HTML !

Concrètement, la syntaxe JSX est transpilé en JS version ES5 :

``` js
// Un fichier .js contient :
<a id="home" href="https://oclock.io">Accueil</a>
// Babel va transpiler ce code JSX en :
React.createElement('a', {id: 'home', href: 'https://oclock.io'}, 'Accueil'); 
```

Voilà pourquoi le JSX nécessite de `import React from 'react'` !

:warning: Le JSX, ça ***ressemble*** à du HTML, mais ça reste du JavaScript !

* Attention au nom des « attributs », ils correspondent aux propriétés JS des éléments du DOM. Par exemple : `<div className="ma-classe" />` => en JS, on a la propriété `className` pour définir la valeur de l'attribut HTML `class`.
* Les propriétés d'évènement (ex. `<button onClick={func} />`) … une mauvaise pratique en HTML, mais on fait bien du JS ! Donc bonne pratique en JSX.

## Créer un élément

Pour créer un élément React avec JSX, par exemple un objet modélisant un lien, on peut taper le code suivant :

```js
const link = <a href="https://oclock.io">Accueil</a>;
```

> Quand on parle d'*élément React*, on désigne bien un objet JS, leque peut être généré avec `React.createElement`. La syntaxe JSX est totalement optionnelle.
> 
> Babel transpile le JSX en une version ES5 rétro-compatible avec tous les navigateurs :
>
> ```js
> // la version transpilée du JSX ci-dessus :
> var link = React.createElement('a', { href: 'https://oclock.io' }, 'Accueil');
> ```

## Créer un composant

Quand on parle de composant React, on désigne une usine à objet JS (React), donc typiquement une fonction qui aura comme valeur de retour un objet JS (React) — objet générable par `React.createElement` donc par du JSX.

### Créer un composant simple

Avec React, nous pouvons donc créer des composants. Il s'agit en fait d'une fonction ou d'une classe retournant du JSX (lequel JSX retourne un objet JS).

Classe ou fonction, peu importe : on va pouvoir, qui exécuter la fonction, qui instancier la classe, et récupérer dans tous les cas une « instance du composant React » :

```js
import React from 'react';
import { render } from 'react-dom';

// Définition d'un composant React :
const Hello = () => (
  <div>Hello World!</div>
);

// On peut désormais instancier Hello avec la syntaxe <Hello /> :
const App = () => (
  <div id="app">
    <Hello /> // transpilé en React.createElement('Hello', null, null), équivalent à faire Hello();
  </div>
);

// De même pour le composant App ; on en crée une <instance /> qu'on injecte dans la page HTML :
render(<App />, document.getElementById('root'));
```

:warning: Attention, les composants que l'on crée (dits composants *customs*) ***doivent*** être écrit avec une majuscule.  
Sans majuscule, Babel croiera qu'il s'agit d'un composant « natif » correspondant à une balise HTML standard : `<div>` est autorisé, pas `<hello>` !

> **Attention** : par abus de language, on aura tendance à appeler aussi bien `Hello` que `<Hello />` un « composant React » — mais seul `Hello` peut véritablement être qualifié de composant React. `<Hello />` est une instance (parmi d'autres) de ce composant. À vous de faire le tri quand vous lirez « composant React » !

### Créer un composant acceptant des props

Un composant React est donc fondamentalement l'exécution d'une fonction (eh oui, en JS, même les `class` sont en fait des fonctions). Une fonction à qui on peut passer des arguments !

Dans la version JSX, on dit qu'on passe des `props` au composant.

Ainsi, on donne une prop JSX à un composant React, comme on donnerait un attribut à une balise HTML :

```js
<Hello data="World" />
```

On peut alors récupérer l'information en paramètre du composant :

```js
// Un composant React reçoit toujours en premier paramètre, un objet de props :
const Hello = (props) => (
  <div>Hello {props.data}</div>
);
```

On peut également donner implicitement une prop JSX `children`, comme on donnerait un enfant à une balise HTML :

```js
<Hello>
  World
</Hello>
```

On peut alors récupérer l'information en paramètre, via la prop `children` :

```js
const Hello = (props) => (
  <div>Hello {props.children}</div>
);
```

On peut également passer un lot de propriétés, avec le [spread operator](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Op%C3%A9rateur_de_d%C3%A9composition) : `...` :

```js
const props = {
  username: 'michelle',
  age: 40,
};
<User {...props} />;

// Cela correspond à :
<User username="michelle" age={40} />;
```

Enfin, on peut piocher les props qui nous intéressent avec la destructuration d'objet :

``` js
const UsernameDisplayer = ({ username }) => (<div>Username: {username}</div>);

const props = {
  username: 'michelle',
  age: 40,
};

<UsernameDisplayer {...props} />;
```

Comme, du point de vue d'un composant, les `props` sont porteuses d'information venant de l'extérieur, une bonne pratique consiste à valider ces `props`, en utilisant par exemple [PropTypes](prop-types.md).

## Insérer du JS dans le JSX

### Insertion d'une variable

Pour insérer du JS dans notre code JSX, on peut utiliser `{}` : une faille JavaScript, pour faire de l'interpolation de code / données. Que ce soit pour une prop, pour du texte, ou pour réaliser un calcul ou même écrire 30 lignes de JS bien horribles, peu importe :

```js
const page = 'Accueil';
const url = 'https://oclock.io';
const link = <a href={url}>{page}</a>;

const Hello = () => (
  <div id="app">
    {link}
  </div>
);
```

C'est donc la porte ouverte aux `.map`, `.filter`, et… au n'importe quoi. Attention à ne pas abuser de ces failles !

### Insertion d'un bloc

#### False

On pourra également se servir de `{}` pour « ouvrir » le JSX afin d'insérer un bloc de façon conditionnelle. JSX sait en effet gérer la valeur `false` : elle a pour effet de ne rien afficher dans la vue, et cela ne génère pas d'erreur.

Cela permet par exemple de faire des conditions directement au sein du JSX
avec des [évaluations court-circuit](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Op%C3%A9rateurs_logiques#.C3.89valuation_court-circuit) :

```js
/*
 * Si `page` contient 'Accueil', la faille renvoie <div>Bienvenue !</div>.
 * Sinon, la faille renvoie false, et le JSX n'ajoute rien à la vue.
 */
const Hello = () => (
  <div id="app">
    {page === 'Accueil' &&
      <div>Bienvenue !</div>
    }
  </div>
)
```

Cela permet donc de répliquer l'instruction `if` en JSX.

On peut également avoir recours à l'opérateur ternaire, pour répliquer `if` / `else` :

```js
/*
 * Si `page` contient 'Accueil', le bloc {} renvoie la <div>Bienvenue !</div>
 * Sinon, le bloc {} renvoie l'élément <a>Retour à l’accueil</a>
 */
const Hello = () => (
  <div id="app">
    {page === 'Accueil'
      ? <div>Bienvenue !</div>
      : <a>Retour à l’accueil</a>
    }
  </div>
)
```

### Array

On peut également donner un tableau d'éléments JSX, au JSX lui-même. Un `array` sera rendu à l'écran comme une suite d'éléments frères (*siblings*) :

```js
// Array d'éléments
const list = [
  <li>Farine</li>,
  <li>Lait</li>,
  <li>Oeufs</li>,
];

// Component
const Crepes = () => (
  <ul id="crepes">
    {list}
  </ul>
);
```

Le cas d'utilisation le plus courant, c'est de transformer un tableau de données brutes en un tableau d'éléments JSX :

```js
const ingredients = [
  'Farine',
  'Lait',
  'Oeufs',
];

const elements = ingredients.map(str => <li>{str}</li>);

const Ingredients = () => (
  <ul id="recipe">
    {elements}
  </ul>
);
```

De façon similaire, mais directement au beau milieu du JSX :smiley: (très utile, surtout quand le tableau provient des props) :

```js
const Ingredients = ({ ingredients }) => (
  <ul id="recipe">
    {ingredients.map(ingredient => (
      <li>{ingredient}</li>
    ))}
  </ul>
);
```

Afin de permettre à React d'optimiser la mise-à-jour lors d'un nouveau `render` (ReactDOM), il est conseillé de rajouter des clés uniques sur les éléments JSX générés par un `.map` (ou autre itération). Cela permettra de faire comprendre à React comment l'UI doit être mise à jour de façon optimale :

```js
const Ingredients = ({ ingredients }) => (
  <ul id="recipe">
    {ingredients.map(ingredient => (
      // Notez la prop key :
      <li key={ingredient}>{ingredient}</li>
    ))}
  </ul>
);
```

Ces clés `key` doivent être uniques, c'est-à-dire unique pour chaque élément généré dans l'itération, et doivent si possible (pour la clareté du code) représenter une information liée à l'élément. Si on n'a vraiment rien d'autre sous la main,
on peut utiliser l'index du tableau comme clé, mais ce n'est pas idéal et ne permettra pas d'optimiser la [réconciliation](https://reactjs.org/docs/reconciliation.html) :

```js
const Numbers = () => (
  <div id="numbers">
    {[1, 2, 3, 2, 1].map((number, index) => (
      <div key={index}>{number}</div>
    ))}
  </div>
);
```

:warning: Si deux éléments d'un array ont la même `key`, seul le premier élément sera affiché, et cela produira une erreur en console.

:warning: Si aucune clé n'est ajoutée, cela produira également une erreur en console. Ces erreurs ne sont pas affichées en production.

---

Fiches récap liées :

* [Les différents types de composants](./components.md)
* [PropTypes](./prop-types.md)

En savoir plus, sur la doc de React :
* [_Introducing JSX_](https://reactjs.org/docs/introducing-jsx.html) ([fr](https://fr.reactjs.org/docs/introducing-jsx.html))
* [_Lists and Keys_](https://reactjs.org/docs/lists-and-keys.html) ([fr](https://fr.reactjs.org/docs/lists-and-keys.html))
* [_JSX in Depth_](https://reactjs.org/docs/jsx-in-depth.html) ([fr](https://fr.reactjs.org/docs/jsx-in-depth.html))
* [_React Without JSX_](https://reactjs.org/docs/react-without-jsx.html) ([fr](https://fr.reactjs.org/docs/react-without-jsx.html))
