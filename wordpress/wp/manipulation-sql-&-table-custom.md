# Manipulation SQL & table custom

> WordPress est un CMS (Content Management System). Il a donc pour rôle de gérer du contenu quelque soit le type de contenu (articles de blog, pages statiques, utilisateurs, recettes de cuisine, etc.).

L'outil est donc pensé de manière à nous (les dev) simplifier au maximum le travail concernant la manipulation des données. Pas besoin de SQL, de PDO & co.

Cependant il peut parfois être nécessaire d'accéder en direct à notre base de donnée (besoin très spécifique, titre pro à passer :smirk:, ...) et encore une fois WordPress a tout prévu :tada:

## La classe `wp-db`

> https://core.trac.wordpress.org/browser/trunk/src/wp-includes/wp-db.php

La classe `wp-db` mise à disposition par WordPress, automatiquement instanciée au chargement, permet de manipuler la données sans faire de "raw SQL"

### L'instance `wpdb`

> https://codex.wordpress.org/Class_Reference/wpdb#Talking_to_the_Database:_The_wpdb_Class

Pour accéder à l'instance de la classe `wp-db` il est nécessaire d'utiliser le mot clé `global` [doc php](https://www.php.net/manual/fr/language.variables.scope.php).

Ainsi dans notre code: 

```php
global $wpdb;

$wpdb->...
```

### Les méthodes de manipulation

Tout est là dans la doc:

- https://codex.wordpress.org/Class_Reference/wpdb#SELECT_a_Variable
- https://codex.wordpress.org/Class_Reference/wpdb#SELECT_a_Row
- https://codex.wordpress.org/Class_Reference/wpdb#INSERT_row
- https://codex.wordpress.org/Class_Reference/wpdb#UPDATE_rows
- https://codex.wordpress.org/Class_Reference/wpdb#DELETE_Rows
- https://codex.wordpress.org/Class_Reference/wpdb#Running_General_Queries

Et la sécurité dans tout ça ?

La classe met à disposition une méthode `prepare` [lien vers la doc](https://codex.wordpress.org/Class_Reference/wpdb#Protect_Queries_Against_SQL_Injection_Attacks) qui permet de sécuriser les données en provenance d'inputs.  
Il est indispensable de l'utiliser sur une `query` qui contient des données non sécurisées mais les méthodes
`insert`, `update` ont déjà un système intégré qui va sécuriser la requete [lien vers le détail](https://codex.wordpress.org/Data_Validation#Database).


## Une table custom ?

Comme évoqué précédement, il peut parfois être nécessaire d'utiliser une table supplémentaire (dite custom), afin
de stocker des données qui ne rentrent pas dans l'utilisation proposée par wordpress.

Prenons l'exemple d'un plugin de newsletter qui automatiquement affiche une popin sur le site pour proposer au visiteur de s'inscrire à la newsletter.  
Pour cela nous allons créer une table `newsletter` qui ne va contenir que les adresses email des inscrits, ainsi que si la personne a accepté de s'inscrire aux newsletter des partenaires du site (les fameux optin) _(attention ceci est un exemple, il serait également possible d'exploiter les CPT & metadata pour l'ensemble)_.

### Création de la table

```php
  $sql = "CREATE TABLE {$this->table} (
            `id` INT NOT NULL AUTO_INCREMENT,
            `email` VARCHAR(255) NOT NULL,
            `optin_general` TINYINT NOT NULL DEFAULT '0',
            `optin_partners` TINYINT NOT NULL DEFAULT '0',
            `fk_user_id` INT NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            INDEX (`optin_general`),
            INDEX (`optin_partners`),
            UNIQUE (`email`)
          );";

  $this->wpdb->query($sql);
```

### Insertion de données dans la table

> Ici, on suppose que le formulaire renvoie un `POST` avec pour clé `email` contenant l'adresse email de l'inscrit.

```php
$this->wpdb->insert(
  $this->table,
  [
    'email' => $_POST['email'],
    'optin_general' => 1,
    'optin_partners' => $optin,
    'fk_user_id' => $fk_user_id,
  ],
  [
    '%s',
    '%d',
    '%d',
    '%d',
  ]
);
```

### Récupération d'une donnée dans la table

> Ici, on suppose le besoin de vérifier l'existence d'une adresse email dans la base

```php
  $prepared = $this->wpdb->prepare(
    "
      SELECT id
      FROM {$this->table}
      WHERE email = %s;
    ",
    $email);

  return $this->wpdb->get_var($prepared);
```

### Pour info

Le code du plugin ayant servi d'exemple ici est disponible à cette adresse:
https://github.com/O-clock-Alumni/fiches-recap/tree/master/wordpress/plugins/onewsletter/
