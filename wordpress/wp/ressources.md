# WordPress : Ressources

## Le Codex

[Le codex](https://codex.wordpress.org/) est le "manuel en ligne"  pour WordPress. C'est une ressource inestimable !

## Le Code reference

Parcourir le ["code reference"](https://developer.wordpress.org/reference/) de WordPress permet de naviguer dans l'ensemble des APIs proposées par WordPress

## WordPress TV

Une excellente source d'informations [WordPress.tv](http://wordpress.tv/)

## WPFR

Les [forums](https://wpfr.net/support/) de WPFR sont très populaires et très fréquentés

## Blogs autour de WordPress

* [Rarst](https://www.rarst.net/wordpress/) Une référence
* [Delicious Brains](https://deliciousbrains.com/blog/)
* [WPchannel](https://wpchannel.com/) Le seul blog fr de cette liste
* [Smashing Magazine](https://www.smashingmagazine.com/) Lui, je ne le présente plus... ;)
* [WPShout](https://wpshout.com/) Une référence
* [WP Tavern](https://wptavern.com/) Généraliste
* [WinningWP](https://winningwp.com/) Généraliste
* [tutsplus WordPress](https://code.tutsplus.com/categories/wordpress) Beaucoup de petites mise en pratique
* [WPmuDev](https://premium.wpmudev.org/blog/) Quelques articles sympas et des cas pratiques
* [Elegant themes blog](https://www.elegantthemes.com/blog/) Très orienté mais des articles très souvent bien avisés
* [WPeka](https://www.wpeka.com/) très commercial mais certains articles sont intéressants
* [WPKUBE](https://www.wpkube.com/) Potentiellement utile mais très orienté
* [whatswp.com](https://whatswp.com)
* [wpexplorer](http://www.wpexplorer.com/blog/)
* [wpnewsify](https://wpnewsify.com/blog/)
* ...

## Cheatsheets

* [La meilleure Cheatsheet](https://codex.wordpress.org/Template_Tags)
* [webdesign cheatsheet](http://www.webdesign-cheatsheets.com/wordpress.html)
* [dbwebsite](https://www.dbswebsite.com/design/wordpress-reference/V4/)
* [startblogging cheatsheet](../medias/wp-cheatsheet-start.pdf)
* [Artist Relations cheatsheet](../medias/wp-cheatsheet-artistrelations.pdf)
* [Ekin Ertac cheatsheet](../medias/wp-cheatsheet-ertac.pdf)
* Et il y a celle-ci...

![makeawebsite cheatsheet](../medias/wp-cheatsheet-ultimate.jpeg)
