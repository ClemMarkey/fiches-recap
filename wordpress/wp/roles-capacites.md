# Rôles et capacités

[Codex - Rôles et capacités](https://codex.wordpress.org/fr:R%C3%B4les_et_Capacit%C3%A9s)

WordPress propose par défaut plusieurs rôles et des capacités associées :

* **Super Admin** Accède aux réglages d'administration multisite, ainsi qu'à toutes les autres fonctionalités. Voir l'article créer un réseau.
* **Administrateur** Accède à toutes les fonctionnalités d'administration dans un site unique.
* **Éditeur** Peut publier et gérer articles et pages, les siens ainsi que ceux des autres utilisateurs.
* **Auteur** Peut publier et gérer ses propres articles.
* **Contributeur** Peut écrire et gérer ses articles, mais ne peut les publier.
* **Abonné** Ne peut gérer que son profil.



## Capacités

<table>
<tbody><tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr>
<tr>
<td>manage_network</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_sites</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_network_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_network_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_network_options</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>unfiltered_html</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr>
<tr>
<td>activate_plugins</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>create_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_plugins</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_files</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_plugins</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_theme_options</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>export</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>import</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr>
<tr>
<td>install_plugins</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>install_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>list_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_options</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>promote_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>remove_users</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>switch_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>update_core</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>update_plugins</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>update_themes</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x (site unique) </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_dashboard</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td></td>
<td>
</td></tr>
<tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr>
<tr>
<td>moderate_comments</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_categories</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>manage_links</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_others_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_others_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_published_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>publish_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_others_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_published_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_others_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_private_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_private_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>read_private_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_private_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_private_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr>
<td>read_private_pages</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td></td>
<td>
</td></tr>
<tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr>
<tr>
<td>edit_published_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td>
</td></tr>
<tr>
<td>upload_files</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td>
</td></tr>
<tr>
<td>create_product</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td>
</td></tr>
<tr>
<td>publish_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td>
</td></tr>
<tr>
<td>delete_published_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td></td>
<td>
</td></tr>
<tr>
<td>edit_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td>
</td></tr>
<tr>
<td>delete_posts</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td>
</td></tr>
<tr>
<td>read</td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x </td>
<td bgcolor="#ABCDEF" style="text-align:center !important"> x
</td></tr>
<tr style="background:#464646; color:#d7d7d7;">
<th> Capacité</th>
<th>Super Admin</th>
<th>Administrateur</th>
<th>Editeur</th>
<th>Auteur</th>
<th>Contributeur</th>
<th>Abonné</th></tr></tbody></table>


## Ajouter un rôle

[`add_role()`](https://codex.wordpress.org/Function_Reference/add_role) permet de facilement ajouter un rôle.

```php
add_role('oclockien', 'oClockien', [
	'read' => true
]);
```

> Une fois un rôle ajouté, il n'est pas possible de modifier sa déclaration (le rôle est stocké en BDD). Il faudra le supprimer avec [`remove_role()`](https://codex.wordpress.org/Function_Reference/remove_role) : `remove_role('oclockien');` puis le recréer

## Ajouter des capacités

La méthode [`add_cap()`](https://codex.wordpress.org/Function_Reference/add_cap) de WP_Role permet d'enrichir un rôle avec de nouvelles capacités.

```php
function register_role_cap()
{
	add_role('oclockien', 'oClockien', [
		'read' => true,
	]);
	
	$role = get_role('oclockien');
	
	//Creer des articles
	$role->add_cap('create_posts');
	//Éditer ses articles
	$role->add_cap('edit_posts');
	//Capacité custom
	$role->add_cap('oclock_show_secret');
}

add_action('init', 'register_role_cap');
```

## Conditions

Vérifier si l'utilisateur courant possède une capacité

```php
if ( current_user_can( $capability ) ) {
	// ...
}
```

Par exemple : 

```php
if (current_user_can('edit_posts')) {
    edit_post_link('Edition', '<p>', '</p>');
}
```

récupérer le rôle d'un utilisateur 

```php
$user = wp_get_current_user();
if ( in_array( 'author', $user->roles ) ) {
    // si le user est un auteur
}
```

## User Role Editor

[User Role Editor](https://wordpress.org/plugins/user-role-editor/) est un plugin qui permet de facilement gérer les capacités et les rôles
