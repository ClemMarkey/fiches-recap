# WordPress : Installation custom

Un [article du Codex](https://codex.wordpress.org/Giving_WordPress_Its_Own_Directory) expliquant comment donner à WordPress son propre répertoire.

## Etape 1 : Modifier les URL dans le Back-office (Réglages -> Général)

* Adresse web de WordPress  : url/sous-dossier
* Adresse web du site  : url

Il est également possible de coder ce changement d'adresse directement dans `wp-config.php` en y ajoutant ces 2 lignes

```php
define('WP_HOME','http://...../site');
define('WP_SITEURL','http://...../site/sous-dossier');
```

## Etape 2 : Organisation

* Créer le sous dossier -> tout placer dedans
* Copier le .htaccess et le index.php à la racine
* Déplacer le fichier [wp-config.php](wp-config.md) à la racine
* Déplacer le dossier wp-content à la racine

## Etape 3 : blog-header

Modifier le fichier index.php

```php
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
```

vers

```php
require( dirname( __FILE__ ) . '/sous-dossier/wp-blog-header.php' );
```

## Etape 4 : Modifier l'emplacement de wp-content

Il est possible de renommer ce dossier `wp-content` en autre chose, dans cet exemple : `content`.

dans [wp-config.php](wp-config.md), ajouter ces 2 lignes

```php
define( 'WP_CONTENT_URL', 'http://monurl.local/content' );
define( 'WP_CONTENT_DIR', dirname( ABSPATH ) . '/content' );
```

## Etape 5 : .htaccess

Modifier le fichier .htaccess de la racine

```
RewriteEngine on
RewriteCond %{REQUEST_URI} !^sousdossier/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1
```

## Etape 6 : Droits de fichiers

A la racine du projet :

**sur mac**

```bash
sudo chgrp -R _www .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
sudo chmod 644 .htaccess
```

**sur linux**

```bash
sudo chgrp -R www-data .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
sudo chmod 644 .htaccess
```

## Etape 7 : Permaliens

Réactiver les permaliens
