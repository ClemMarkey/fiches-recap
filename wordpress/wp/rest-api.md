# WordPress REST API

## Principe

L'API REST de WordPress offre la possibilité d'exploiter tous les contenus gérés par WordPress permettant aux développeurs d'interagir avec des sites à distance en envoyant et recevant du JSON (la Notation d'Objet de Javascript). 

### REST

REST signifie *Representational state transfer*. Pour faire simple, l'API s'appuie sur les méthodes (verbes) HTTP pour agir sur les contenus.

**GET** Lecture (récupération)
**PUT** Mise à jour
**POST** Création
**DELETE** Suppression
...

**Exemple** : quand on souhaite récupérer un article, la méthode GET sera utilisée

### API

API signifie *Application Programming Interface (interface de programmation applicative)*. Pour faire court, une API est une façade recouvrant une application. Elle possède des fonctions permettant d'accéder aux données de manière simple, rapide et sécurisée.

**Exemple** : Récupérer les informations d'un utilisateur sur github `https://api.github.com/users/octocat`

### JSON

JSON est un format de données standard ouvert, léger et lisible et qui offre une formidable compatibilité avec Javascript; d'où le nom. Quand vous envoyez du contenu ou faites une requête à l'API, la réponse sera transmise en JSON. 

Ce support d'échange permet de créer, lire et mettre à jour le contenu de WordPress côté client via du Javascript ou d'applications externes développées dans n'importe quel langage.

**Exemple** : une réponse récupérée depuis l'API github

```json
{
  "login": "octocat",
  "id": 583231,
  "avatar_url": "https://avatars3.githubusercontent.com/u/583231?v=4",
  "gravatar_id": "",
  "url": "https://api.github.com/users/octocat",
  "html_url": "https://github.com/octocat",
  "followers_url": "https://api.github.com/users/octocat/followers",
  "following_url": "https://api.github.com/users/octocat/following{/other_user}",
  "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
  "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
  "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
  "organizations_url": "https://api.github.com/users/octocat/orgs",
  "repos_url": "https://api.github.com/users/octocat/repos",
  "events_url": "https://api.github.com/users/octocat/events{/privacy}",
  "received_events_url": "https://api.github.com/users/octocat/received_events",
  "type": "User",
  "site_admin": false,
  "name": "The Octocat",
  "company": "GitHub",
  "blog": "http://www.github.com/blog",
  "location": "San Francisco",
  "email": null,
  "hireable": null,
  "bio": null,
  "public_repos": 7,
  "public_gists": 8,
  "followers": 1976,
  "following": 5,
  "created_at": "2011-01-25T18:44:36Z",
  "updated_at": "2017-10-23T04:10:54Z"
}
```

## Outils

Étant donné que le format d'échange de l'API est le JSON, il est intéressant de se doter d'un outil permettant une lecture aisée de ce format.

### JSONView

[JSONView](https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc) permet d'ajouter une mise en forme sur les réponses JSON au travers du navigateur, ce qui simplifie grandement la lisibilité.

### Postman

[Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) est un plugin pour Chrome qui permet de facilement tester des API et de présenter efficacement les réponses.

## L'API REST de WordPress

### C'est une longue histoire

Le projet démarre en 2013, la version 1.0 voit le jour en 2014 sous forme d'un plugin.

En 2015 la version 2 de l'API est rendue disponible, ajoutant beaucoup de nouveautés et de nouvelles routes.

Fin 2015, le projet prend une autre tournure lorsqu'il est question d'intégrer cette API directement au sein de WordPress.

La version 4.4 de WordPress commence à intégrer l'API dans sa première phase de développement.

La version 4.7 de WordPress implante la totalité de l'API pour la lecture et l'écriture.

### Documentation

Historiquement, la documentation de l'API était (est pour le moment encore) disponible à l'adresse http://v2.wp-api.org. Cette documentation possède encore des exemples d'implémentation compatible avec les dernières version de l'API.

L'API faisant désormais partie de WordPress, une documentation complète est disponible sur le site wordpress.org https://developer.wordpress.org/rest-api/.

- [Référence de l'API](https://developer.wordpress.org/rest-api/reference/)
- [Utilisation de l'API](https://developer.wordpress.org/rest-api/using-the-rest-api/)
- [Étendre l'API](https://developer.wordpress.org/rest-api/extending-the-rest-api/)

### Accès

> Considérant un projet WordPress à l'adresse `http://localhost/oblog`
> _(la réécriture est activée et supprime `index.php` des urls)_

L'API REST de WordPress est accessible depuis `wp-json`. 

Le premier contact avec l'API peut donc se faire par cette URL `http://localhost/oblog/wp-json`

L'API propose grand nombre de routes permettant d'accéder aux différents types de contenus.

Il est également possible d'utiliser des arguments pour personnaliser la réponse.

#### Récupérer des posts

[WP doc - List posts](https://developer.wordpress.org/rest-api/reference/posts/#list-posts)

**GET `/wp/v2/posts`**

Lister les 10 derniers posts (articles)

`http://localhost/oblog/wp-json/wp/v2/posts`

Lister les 3 derniers posts

`http://localhost/oblog/wp-json/wp/v2/posts?per_page=3`

Lister les 3 derniers posts par auteur

`http://localhost/oblog/wp-json/wp/v2/posts?per_page=3&orderby=author`

#### Récupérer un post

[WP doc - Retrieve a post](https://developer.wordpress.org/rest-api/reference/posts/#retrieve-a-post)

**GET `/wp/v2/posts/<id>`**

Récupérer un post par son ID

`http://localhost/oblog/wp-json/wp/v2/posts/2`

#### Récupérer des pages

[WP doc - List pages](https://developer.wordpress.org/rest-api/reference/pages/#list-pages)

**GET `/wp/v2/pages`**

Lister les 10 dernières pages

`http://localhost/oblog/wp-json/wp/v2/pages`

#### Récupérer une page

[WP doc - Retrieve a page](https://developer.wordpress.org/rest-api/reference/pages/#retrieve-a-page)

**GET `/wp/v2/pages/<id>`**

Récupérer une page par son ID

`http://localhost/oblog/wp-json/wp/v2/pages/1`

#### Récupérer des Custom Post Type

**GET `/wp/v2/<post-type>`**

Récupérer les 10 derniers posts (ici, de type `project`)

`http://localhost/oblog/wp-json/wp/v2/project`

#### Récupérer un Custom Post Type

**GET `/wp/v2/<post-type>/<id>`**

Récupérer un post par son type et son ID

`http://localhost/oblog/wp-json/wp/v2/project/18`

#### ... 

Beaucoup d'autres exemples sont disponible par l'intermédiaire de la [référence](https://developer.wordpress.org/rest-api/reference/)

### Modifier la réponse de l'API

WordPress propose d'adapter librement la réponse JSON de l'API. Par exemple, dans le retour par défaut, l'URL d'un thumbnail de post n'est pas disponible.

Il faut demander à WordPress d'ajouter des données à la réponse par l'intermédiaire de la fonction [`register_rest_field()`](https://developer.wordpress.org/reference/functions/register_rest_field/).

Le principe sera d'ajouter une nouvelle donnée par l'intermédiaire de l'exécution du hook : `rest_api_init`

#### Ajouter l'URL du `post-thumbnail`

```php
// On se greffe au hook `rest_api_init`
add_action( 'rest_api_init', 'oblog_register_rest_fields' );

// Fonction exécutée lorsque le hook `rest_api_init` sera exécuté
function oblog_register_rest_fields() {
	// Enregistrement d'une nouvelle donnée dans la réponse
    register_rest_field( 
        'post', // champ associé aux articles (type de post = post)
        'thumbnail_url',  // la clé qui sera ajoutée
		[
	        // référence à la fonction appelée lors d'un GET (wp/v2/posts par exemple)
            'get_callback'    => 'oblog_get_thumbnail_url', 
	        // fonction appelée lors d'une mise à jour
            'update_callback' => null,
            // structure de la donnée
            'schema'          => null,
        ]
    );
}

/**
 * Callback appelé lors d'un GET (wp/v2/posts par exemple) pour récupérer l'URL du thumbnail
 *
 * @param array $object Le post courant (tableau avec toutes les données du post).
 * @param string $field_name Le nom du nouveau champ / clé, ici: thumbnail_url.
 * @param WP_REST_Request $request Requête courante
 *
 * @return mixed
 */
function oblog_get_thumbnail_url($object, $field_name, $request){
	// Le post courant possède une image mise en avant ?
    if(has_post_thumbnail($object['id'])){
        $thumbnail = wp_get_attachment_image_src( $object['id'], 'thumbnail' ); 
        return $thumbnail[0]; // récupération de l'URL dans le tableau (index 0)
    } else {
        return false;
    }
}

```

#### Ajouter les metas du post

```php
// On se greffe au hook `rest_api_init`
add_action( 'rest_api_init', 'oblog_register_rest_fields_meta' );

// Fonction exécutée lorsque le hook `rest_api_init` sera exécuté
function oblog_register_rest_fields_meta() {
	register_rest_field( 
		'post', // champ associé au type de post = post
		'post_meta_cf', // clé ajoutée
		[
			// callback lors d'un GET
           'get_callback' => 'oblog_get_post_meta_cf',
		]
    );
}

/**
 * Callback appelé lors d'un GET (wp/v2/posts par exemple) pour récupérer les meta
 *
 * @param array $object Le post courant (tableau avec toutes les données du post).
 * 
 * @return array
 */
function oblog_get_post_meta_cf( $object ) {
	// Récupère toutes les meta du post
	return get_post_meta( $object['id'] );
}
```

#### Permettre la récupération des CPT via REST API

Afin de s'assurer de pouvoir récupérer les CPTs via l'API, il faut penser à déclarer l'option `show_in_rest` lors de la déclaration des CPTs (`register_post_type()`)

```php
'show_in_rest' => true
```

## Cas Pratique avec jQuery

En se servant de jQuery pour manipuler le DOM et effectuer des requêtes AJAX, il est possible de concevoir un système rapatriant des posts au sein d'une page web


```html
<button id="btn-load">Charger des articles</button>
<div class="list">
	<!-- Future liste d'articles-->
</div>
``` 


```js
var app = {
  baseUrl: 'http://localhost/oblog',
  wpjson: '/wp-json',
  route: '/wp/v2/',
  init: function(){
    console.log('App initialized !');
    
    // stockage de l'élément du dom .list dans app
    app.$list = $('.list');
    
    //btn click -> chargement de posts
    $('#btn-load').on('click', app.getPosts);
  },
  getPosts: function(evt){
    // Si on recupere un event, on interompt son comportement par défaut
    if (evt.length) evt.preventDefault();
      
    // On vide la liste
    app.$list.empty();
    
	// On appelle l'url pour la recup des derniers articles
	// On compose l'URL : http://localhost/oblog + /wp-json + /wp/v2/ + posts
	var xhr = $.ajax(app.baseUrl + app.wpjson + app.route + 'posts');
  
    //en cas de succes
    xhr.done(function(data){
      for(var index in data) {
        var post = data[index];
        // post.title.rendered contient le titre du post
        app.$list.append('<div class="post">' + post.title.rendered + '</div>');
      }
    });
    
    //en cas d'erreur
    xhr.fail(function(jqxhr){
      console.error(jqxhr);
    });
    
    //dans tous les cas
    xhr.always(function(response){
      console.info(response);
    });
  }
};

// Lancement de app.init lorsque le DOM est prêt 
$(app.init);
``` 

