# WP CLI

[WP-CLI](https://wp-cli.org/) propose des outils pour faciliter la gestion de WordPress depuis la ligne de commande.

A titre d'exemple, plutôt que de passer par un plugin spécifique pour re-générer tous les médias suite à l'ajout de nouvelles tailles d'images (notamment lors de l'ajout de thmubnail). `wp media regenerate` se chargera de tout en quelques secondes.

Parmi les usages les plus courants, WP-CLI permet de :

* Installer Wordpress
* Exporter / Importer des données
* Remplacer des occurence en BDD 
* Gérer les Posts types
* Regénérer les médias
* Gérer les Taxonomies
* Gérer les sidebars
* Installer des thèmes
* Installer des plugins
* Modifier les options de WordPress
* ...

La liste des usages est très longue ;)

Ainsi que celle des [commandes](https://developer.wordpress.org/cli/commands/) 

* [`wp cache`](https://developer.wordpress.org/cli/commands/cache/) Manage the object cache.
* [`wp cap`](https://developer.wordpress.org/cli/commands/cap/) Manage user capabilities.
* [`wp checksum`](https://developer.wordpress.org/cli/commands/checksum/) Verify WordPress core checksums.
* [`wp cli`](https://developer.wordpress.org/cli/commands/cli/) Manage WP-CLI itself.
* [`wp comment`](https://developer.wordpress.org/cli/commands/comment/) Manage comments.
* [`wp config`](https://developer.wordpress.org/cli/commands/config/) Manage the wp-config.php file
* [`wp core`](https://developer.wordpress.org/cli/commands/core/) Download, install, update and manage a WordPress install.
* [`wp cron`](https://developer.wordpress.org/cli/commands/cron/) Manage WP-Cron events and schedules.
* [`wp db`](https://developer.wordpress.org/cli/commands/db/) Perform basic database operations using credentials stored in wp-config.php
* [`wp eval`](https://developer.wordpress.org/cli/commands/eval/) Execute arbitrary PHP code.
* [`wp eval-file`](https://developer.wordpress.org/cli/commands/eval-file/) Load and execute a PHP file.
* [`wp export`](https://developer.wordpress.org/cli/commands/export/) Export WordPress content to a WXR file.
* [`wp handbook`](https://developer.wordpress.org/cli/commands/handbook/) \[Commands](/commands/) » handbook
* [`wp help`](https://developer.wordpress.org/cli/commands/help/) Get help on WP-CLI, or on a specific command.
* [`wp import`](https://developer.wordpress.org/cli/commands/import/) Import content from a WXR file.
* [`wp language`](https://developer.wordpress.org/cli/commands/language/) 
* [`wp media`](https://developer.wordpress.org/cli/commands/media/) Manage attachments.
* [`wp menu`](https://developer.wordpress.org/cli/commands/menu/) List, create, assign, and delete menus.
* [`wp network`](https://developer.wordpress.org/cli/commands/network/) 
* [`wp option`](https://developer.wordpress.org/cli/commands/option/) Manage options.
* [`wp package`](https://developer.wordpress.org/cli/commands/package/) Manage WP-CLI packages.
* [`wp plugin`](https://developer.wordpress.org/cli/commands/plugin/) Manage plugins.
* [`wp post`](https://developer.wordpress.org/cli/commands/post/) Manage posts.
* [`wp post-type`](https://developer.wordpress.org/cli/commands/post-type/) Manage post types.
* [`wp rewrite`](https://developer.wordpress.org/cli/commands/rewrite/) Manage rewrite rules.
* [`wp role`](https://developer.wordpress.org/cli/commands/role/) Manage user roles.
* [`wp scaffold`](https://developer.wordpress.org/cli/commands/scaffold/) Generate code for post types, taxonomies, plugins, child themes, etc.
* [`wp search-replace`](https://developer.wordpress.org/cli/commands/search-replace/) Search/replace strings in the database.
* [`wp server`](https://developer.wordpress.org/cli/commands/server/) Launch PHP’s built-in web server for this specific WordPress installation.
* [`wp shell`](https://developer.wordpress.org/cli/commands/shell/) Interactive PHP console.
* [`wp sidebar`](https://developer.wordpress.org/cli/commands/sidebar/) Manage sidebars.
* [`wp site`](https://developer.wordpress.org/cli/commands/site/) Perform site-wide operations.
* [`wp super-admin`](https://developer.wordpress.org/cli/commands/super-admin/) Manage super admins on WordPress multisite.
* [`wp taxonomy`](https://developer.wordpress.org/cli/commands/taxonomy/) Manage taxonomies.
* [`wp term`](https://developer.wordpress.org/cli/commands/term/) Manage terms.
* [`wp theme`](https://developer.wordpress.org/cli/commands/theme/) Manage themes.
* [`wp transient`](https://developer.wordpress.org/cli/commands/transient/) Manage transients.
* [`wp user`](https://developer.wordpress.org/cli/commands/user/) Manage users.
* [`wp widget`](https://developer.wordpress.org/cli/commands/widget/) Manage sidebar widgets.


## Installation

Récupération

```
$ curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
```

Droits d'exécution

```
$ chmod +x wp-cli.phar
```

Déplacement

```
$ sudo mv wp-cli.phar /usr/local/bin/wp
```

Dorénavant, WP-CLI est disponible depuis le terminal via la commande `wp`

```
$ wp --info
```


## Exemple d'installation de WordPress

Cet exemple est basé sur ce [gist](https://gist.github.com/polevaultweb/a955d29d19dedfc7e979#file-wp-install-core-sub-dir-sh) qui propose un script pour automatiser l'installation de WordPress.

Détail du script pas à pas : 

### Configuration

```
# Dossier d'installation de WordPress
CORE_DIR=wp
# Nom de la base de données qui devra être créée
DB_NAME=wpclitest
# User de bdd
DB_USER=user
# pass de bdd
DB_PASS=pass

# URL du site 
SITE_URL=http://localhost/dossier/ 
# Titre du site 
SITE_TITLE='WordPress CLI Test Site'
# Futur user Admin de WordPress
SITE_USER=admin
# Pass Admin de WordPress
SITE_PASS=password
# Email Admin de WordPress
SITE_EMAIL=your@email.com
```

### Creation du répertoire pour l'install

```
mkdir $CORE_DIR
cd $CORE_DIR
```

### Téléchargement de WordPress

à noter, il est possible d'ajouter un paramètre `--locale` pour spécifier la langue souhaitée `wp core download --locale=fr_FR`

```
# Telechargement de WP
wp core download

# retour au dossier racine
cd ../
```

### Création du fichier de config

```
wp core config --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASS --path=$CORE_DIR
```

### Création de la BDD

```
# Creation de la BDD 
wp db create

# Creation des tables de WordPress
wp core install --url=$SITE_URL --title="$SITE_TITLE" --admin_user=$SITE_USER --admin_password=$SITE_PASS --admin_email=$SITE_EMAIL --path=$CORE_DIR
```

### Structuration des dossiers / fichiers

```
# Copie de index.php
cp "$CORE_DIR/index.php" ./index.php

# Édition du index.php avec le bon dossier cible
sed -i '' "s/\/wp-blog-header/\/$CORE_DIR\/wp-blog-header/g" index.php

# Mise à jour de l'URL du site WP (avec $CORE_DIR)
wp option update siteurl $(wp option get siteurl)/$CORE_DIR

# Copie de wp-config à la racine du projet
cp "$CORE_DIR/wp-config.php" ./wp-config.php

echo 'Install finished!'
```