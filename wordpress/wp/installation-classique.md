# WordPress : Installation classique

## Prérequis

LAMP/ WAMP / config serveur perso

* Apache
* PHP
* MySQL

## Récupérer WordPress

* [WordPress](https://wordpress.org/)
* [WordPress FR](https://fr.wordpress.org/)

## Préparer la BDD

phpMyAdmin / Adminer : Création d'un utilisateur avec les privilèges sur une base de données dédiée

## Suivez le guide

wp-admin > étapes d'installation
pas "admin", mot de passe sécurisé, accès

## Réglages WordPress

Pas d'indexation

## Permaliens

Activation des permaliens

### .htaccess

```
RewriteEngine on
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . index.php [L]
```
Si vous êtes sur un serveur tout neuf (donc pas le téléporteur), il faudra dire à Apache de prendre en compte le fichier .htaccess. Il faut donc modifier le fichier `/etc/apache2/sites-enabled/default.conf` et ajouter ces lignes **à l'intérieur** de la balise `VirtualHost`  : 

```
<Directory "/var/www/html">
    # AllowOverride All permet d'autoriser la surcharge de la configuration d'Apache via .htaccess
    # Question de sécurité : on ne fait pas n'imoprte quoi avec la conf du serveur
    AllowOverride All
    Order allow,deny
    Allow from All
</Directory>
```

## Droits de fichiers

Il est nécessaire de modifier le groupe propriétaire (ce doit être celui d'Apache) et de modifier les droits d'accès, sur les fichiers et les répertoires de votre projet Wordpress.

**A la racine du projet**, exécuter les commandes suivantes dans le _terminal_ :

**sur mac**

```bash
sudo chgrp -R _www .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
sudo chmod 644 .htaccess
```

> :bulb: Il est également possible que sous Mac, Apache soit executé par l'utilisateur et le groupe `daemon`. On peut s'en assurer avec affichant le nom du groupe Apache avec la commande : `sudo apachectl -D DUMP_RUN_CFG | grep "Group"`. Dans ce cas, remplacer `_www` par `daemon`.

**sur linux**

```bash
sudo chgrp -R www-data .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
sudo chmod 644 .htaccess
```
[Article du codex sur les droits de fichiers](https://wordpress.org/support/article/changing-file-permissions/)

## Traduction

* [Traduction FR Glotpress](https://translate.wordpress.org/locale/fr) - The Hard Way
* [WPcentral Traduction FR](https://wpcentral.io/internationalization/fr/) - C'est déjà mieux mais...
* [wp-config.php](wp-config.md) => FS_METHOD 


