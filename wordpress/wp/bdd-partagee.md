# Travailler avec une base de donnée partagée

## Configurer la base

La base doit être accessible pour tout le monde, tout le temps ! Du coup il est fortement conseillé de l'héberger sur un serveur fourni pendant le socle.

Les deux étapes de configuration de la base sont détaillées [ici](https://github.com/O-clock-Alumni/fiches-recap/blob/master/serveur/installation-gandi.md#configurer-une-bdd-partag%C3%A9e).

## Configurer WordPress

Désormais la base de données correspond à une installation de WordPress en particulier. En effet, nous n'avons pas tous le même chemin pour y acceder: les routes ne fonctionnerons pas chez tout le monde.

Les routes chez WordPress sont dynamiques et générées via 2 options présentes en base (`siteurl` et `home`) dans la table `wp_options`.

Il est possible de venir "écraser" ces valeurs via le fichier `wp-config.php` :

```php
define('WP_HOME','http://...../site');
define('WP_SITEURL','http://...../site/sous-dossier');
```

Article du [codex](https://codex.wordpress.org/Editing_wp-config.php#WP_SITEURL) détaillant le sujet.

Ainsi la configuration de ces deux constantes prévaudra sur la valeur contenue en base de donnée et les url seront correctement générées en local.

## Savoir s'organiser

Lorsque l'utilisateur **A** du WordPress avec une base de donnée partagée ajoute un plugin, l'information est enregistrée dans la base.

_Problème_:  
Si l'utilisateur **B** arrive sur la page d'administration des plugins, WordPress va remarquer qu'un plugin est activé alors que les fichiers ne sont pas présents dans l'installation de l'utilisateur **B**.
WordPress va donc considérer cela comme une erreur et désactiver le plugin en question... 

_Solutions_:  
- Décider dès le début du projet quels seront les plugins utilisés dans le projet. Ainsi dès la configuration du projet (grâce à notre fichier `composer.json`) tout le monde aura la même base de travail.
- Si je dois tester un plugin qui n'était pas prévu, je peux toujours exporter la bdd partagée, puis l'importer en local en prenant soin de changer les informations de connexion à la base de donnée dans le fichier `wp-config.php`.
  - [fiche-récap migration & déploiement](https://github.com/O-clock-Alumni/fiches-recap/blob/master/wordpress/wp/migration-deploiement.md#via-wp-cli-pour-la-bdd)
  - [Handbook export](https://developer.wordpress.org/cli/commands/db/export/)
  - [Handbook import](https://developer.wordpress.org/cli/commands/db/import/)
