# Installation

Il existe de nombreuses manière d'installer WordPress

De la plus simple, en téléchargement un fichier zip et en suivant les différentes étapes jusqu'à la plus complète en créant un script shell s'appuyant sur wp-cli.

## Pré-requis

Quelle que soit la méthode choisie, il existe un certain nombre de pré-requis pour profiter au mieux d'une installation de WordPress.

### Environnement

- apache (avec le mod rewrite)
- PHP > 5.4 (avec la librairie gd)
- MySQL

### Droits de fichiers

Afin de gérer au mieux une installation de WordPress il est bon de faire un petit recap des problématiques associées aux permissions et droits de fichiers.

#### Droits et utilisateurs

<details>

##### Droits

Sous linux, il existe 3 types d'utilisateurs 

- `u` **User** Le propriétaire du fichier
- `g` **Group** Le groupe du propriétaire du fichier
- `o` **Others** Le reste du monde

Il existe également 3 types de droits

- `r` **Read** Droit de lecture
- `w` **Write** Droit d'écriture
- `x` **eXecute** Droit d'exécution

| Binaire | Octet | Droits | Signification           |
| ------- | ----- | ------ | ----------------------- |
| 000     | 0     | - - -  | Aucun droit             |
| 001     | 1     | - - x  | Exécutable              |
| 010     | 2     | - w -  | Ecriture                |
| 011     | 3     | - w x  | Ecrire et exécuter      |
| 100     | 4     | r - -  | Lire                    |
| 101     | 5     | r - x  | Lire et exécuter        |
| 110     | 6     | r w -  | Lire et écrire          |
| 111     | 7     | r w x  | Lire écrire et exécuter |

Par exemple changer les droits d'un fichier pour que je sois (moi le propriétaire) le seul à pouvoir le modifier, que les personnes de mon groupe puissent le lire comme l'exécuter et que le reste du monde puisse uniquement l'exécuter :

|         | User  | Group | Others|
| ------- | ----- | ----- | ------|
| Droits  | r w x | r - x | - - x |
| Binaire | 111   | 101   | 001   |
| Octet   | 7     | 5     | 1     |

##### Représentation

En ligne de commande suite à l'instruction `ls -l` les fichiers sont présentés sous forme de liste avec une représentation des droits exprimée en 10 caractères comme par exemple `drwxr-xr-x`

le premier caractère représente la nature du fichier

- `-` Fichier
- `d` Dossier
- `l` Lien

suivi des droits pour le propriétaire, le groupe et les autres

Par exemple `drwxr-xr-x` signifie : 

- un dossier `d` 
- que le propriétaire `rwx` peut lire, écrire et exécuter
- que le groupe `r-x` peut lire et exécuter
- que les autres `r-x` peuvent lire et exécuter

En valeur octale : `755`

</details>

#### `chmod` : Change le droits

<details>

En partant de l'exemple plus haut 

|         | User  | Group | Others|
| ------- | ----- | ----- | ------|
| Droits  | r w x | r - x | - - x |
| Binaire | 111   | 101   | 001   |
| Octet   | 7     | 5     | 1     |

La commande `chmod` permet d'appliquer des droits à un fichier, par exemple `index.html`

##### La structure de la commande `chmod`

`chmod [options] [droits en octets] [cible]`

###### [options] 

Parmi les options les plus intéressante il y a `-R` qui permet d'appliquer les nouveaux droits de manière récursive, c'est à dire par exemple, le dossier `exemple` et tous les fichiers et sous-dossiers seront affectés par le changement de droits 

###### [droits en octets]

- Valeur octale pour l'utilisateur, par exemple `7`
- Valeur octale pour le groupe, par exemple `5`
- Valeur octale pour les autres, par exemple `1`

soit pour l'exemple `751`

###### [cible]

Le dossier ou le fichier qui sera la cible de la commande

##### Application

`chmod 751 index.hml`

signifie que le fichier `index.hml` se verra attribuer les droits `7` pour le propriétaire (lecture, écriture, exécution), `5` pour le groupe (lecture et exécution), `1` pour les autres (exécution).

Autre exemple : 

`chmod -R 755 factures/`

signifie que le dossier `factures/` et que tous les sous-fichiers et sous-dossiers se verront attribuer les droits `755`.

</details>

#### `chown` : Change l'utilisateur et le groupe

<details>

`chown` permet de changer le propriétaire et le groupe sur un ou plusieurs fichiers/dossiers

##### La structure de la commande `chown`

`chown [options] [propriétaire]:[groupe] [cible]`

###### [options] 

Parmi les options les plus intéressante il y a `-R` qui permet d'appliquer un nouveau propriétaire/groupe de manière récursive, c'est à dire par exemple, le dossier `exemple` et tous les fichiers et sous-dossiers seront affectés par le changement. 

###### [propriétaire]

Défini l'utilisateur du système qui sera le propriétaire du fichier/dossier

###### [groupe]

Défini le groupe du système qui aura des droits sur le du fichier/dossier

###### [cible]

Le dossier ou le fichier qui sera la cible de la commande

##### Application

`chown philippe index.hml`

signifie que le fichier `index.hml` se verra attribuer `philippe` comme nouveau propriétaire.

`chown -R philippe:www-data mon-site/`

signifie que le dossier `mon-site/` et que tous les sous-fichiers et sous-dossiers se verront attribuer `philippe` comme nouveau propriétaire, `www-data` comme groupe (`www-data` = apache).

</details>

#### Droits et  WordPress

<details>

Afin de travailler avec WordPress, des commandes complémentaires seront utilisées.

`<mon-utilisateur>` : Votre utilisateur courant, sur le téléporteur il s'agit de `mint`

```bash
sudo chown -R <mon-utilisateur>:www-data .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
```

Les commandes `sudo find ...` sont des raccourcis pour non seulement appliquer des droits mais appliquer ces droits en fonction de la nature des fichiers (`-type f` pour les fichiers et `-type d` pour les dossiers). `chmod` est toujours utilisé mais adaptée en fonction de `-type`

> **( ! )** Il est parfois nécessaire d'appliquer ces droits à plusieurs reprises, notamment après le téléchargement de thèmes ou l'upload de fichiers.

Pour en apprendre davantage : [Article du codex sur les droits de fichiers](https://codex.wordpress.org/Changing_File_Permissions)

</details>

## Différentes méthodes

### Installation guidée

En passant par le célèbre assistant d'installation de WordPress (_installation en 5 minutes_)

Quelques écrans s'enchainent, quelques champs à renseigner et hop !

<details>

#### Récupérer WordPress

- [WordPress](https://wordpress.org/)
- [WordPress FR](https://fr.wordpress.org/)

Extraire l'archive récupérée et par exemple la renommée `mon-projet-wordpress`

Placer ce dossier dans le répertoire accessible par `http://localhost/`

#### Droits de fichiers

À la racine du dossier `mon-projet-wordpress`

`<mon-utilisateur>` : L'utilisateur du système, sur le téléporteur il s'agit de `mint`

```bash
sudo chown -R <mon-utilisateur>:www-data .
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
```

#### Préparer la BDD

Dans phpMyAdmin : Création d'un utilisateur et d'une base de données associée.

#### Suivez le guide

Se rendre à l'url `http://localhost/mon-projet-wordpress/`

##### Écran de connexion à la base de données

- Nom de la base de données
- Utilisateur de la base de données
- Mot de passe de la base de données
- Hôte de la base de données (localhost le plus souvent)
- Préfixe pour les tables (laisser wp_)

##### Écran de fichier de configuration

Cet écran indique si la connexion à la base de données est possible et si WordPress a réussi à créer le fichier de configuration.

Si le fichier de configuration n'a pas été créé, WordPress vous proposera de créer ce fichier dans le projet en fournissant le code PHP qui devra inséré dans le fichier `wp-config.php`.

##### Écran d'installation

- Titre du site
- Login pour l'admin 
- Mot de passe pour l'admin 
- Email pour l'admin
- Indexation (désactiver l'indexation par défaut)

##### Écran de succès

Tout s'est bien passé, il ne reste plus qu'à se connecter !

#### Permaliens

Activation des permaliens au format `/%postname%/`

WordPress devrait créer un fichier `.htaccess` par lui-même. Si ce n'est pas le cas, il est possible de créer ce fichier nous-même avec le code suivant.

```
RewriteEngine on
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . index.php [L]
```

Ce fichier `.htaccess` doit être placé à la racine du projet `mon-projet-wordpress`

#### Modifier la configuration

En modifiant le fichier [`wp-config.php`](wp-config.md), il est possible de modifier le comportement de WordPress 

</details>

### Installation via `composer`

`composer` est le gestionnaire de dépendance de PHP. Le concept est de considérer WordPress lui-meme comme une dépendance du projet.

composer offre une grande flexibilité et possède un atout majeur : la gestion des thèmes et plugins également comme des dépendances du projet :ok_hand: 

<details>

#### Nouvelle structure

Beaucoup de raisons visent à modifier la structure de WordPress pour donner au coeur de WordPress sont propre dossier, en voici quelques unes :

- Isoler `wp-config.php` _fichier très sensible_
- Grandement faciliter le partage de projet via `git`
- Détacher le dossier `wp-content` du reste de WordPress
- Faciliter les mises à jour
- Utiliser des outils comme `composer` pour la gestion de dépendances
- ...

Cette restructuration est complètement supportée par WordPress qui explique même tout le fonctionnement dans cet article dédié : [Donner à WordPress son propre répertoire](https://codex.wordpress.org/Giving_WordPress_Its_Own_Directory).

##### Arborescence

Le principe suite à la restructuration est d'obtenir une arborescence comme celle ci

```
+ mon-projet
|  + .git
|  + wp
|  |  + wp-admin
|  |  + wp-includes
|  |  - wp-blog-header.php
|  |  - wp-settings.php
|  |  - ...
|  + content
|  |  + themes
|  |  + plugins
|  |  + uploads
|  |  - ...
|  - .gitignore
|  - .htaccess
|  - index.php
|  - wp-config.php
```

Cette structure va permettre d'efficacement utiliser `composer` pour rapatrier WordPress dans un dossier indépendant.

##### Étape par étape

###### Étape 1 : Déplacement / Copie

- Créer un sous-dossier `wp` -> tout placer dedans
- Copier `index.php` à la racine
- Déplacer le fichier `wp-config.php` à la racine
- Déplacer le dossier `wp-content` à la racine et le renommé `content`

###### Étape 2 : Modifier `index.php`

Il est impératif de dire à `index.php` où trouver le fichier de démarrage `wp-blog-header.php`

Il suffit de modifier :

```php
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
```

vers

```php
require( dirname( __FILE__ ) . '/wp/wp-blog-header.php' );
```

###### Étape 3 : Modifier le chargement de `wp-content` 

Dans le fichier [`wp-config.php`](wp-config.md), il suffit d'ajouter ces 2 constantes en précisant pour `WP_CONTENT_URL` l'url du projet + `/content`

```php
define( 'WP_CONTENT_URL', 'http://monurl.local/content' );
define( 'WP_CONTENT_DIR', dirname( ABSPATH ) . '/content' );
```

###### Étape 4 : Droits de fichiers

À la racine du dossier `mon-projet-wordpress`

`<mon-utilisateur>` : L'utilisateur du système, sur le téléporteur il s'agit de `mint`

```bash
sudo chown -R <user>:www-data content/
sudo find . -type f -exec chmod 664 {} +
sudo find . -type d -exec chmod 775 {} +
```

Afin de modifier les droits du dossier `content/`

###### Étape 5 : Installer WordPress

Installer WordPress normalement mais à partir de l'adresse `http://localhost/mon-projet/wp/wp-admin`

###### Étape 6 : Permaliens et URL

Activation des permaliens au format `/%postname%/`

Dans Réglages > Général > URL de la home : Retirer le `/wp`

###### Étape 7 : Ajouter un fichier `.htaccess` adapté

à la racine du projet ajouter un fichier `.htaccess` avec ce code

```
RewriteEngine on
RewriteCond %{REQUEST_URI} !^wp/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1
```

##### En savoir plus

Plusieurs articles abordent WordPress et Composer avec cette nouvelle structure :

- [Rarst.net](http://composer.rarst.net/)
- [Roots.io](https://roots.io/using-composer-with-wordpress/)
- [Gilbert Pellegrom - WordPress+Git+Composer](https://deliciousbrains.com/storing-wordpress-in-git/)

#### packagist.org à la rescousse

Sur packagist.org, [johnpbloch/wordpress](https://packagist.org/packages/johnpbloch/wordpress) est un repo perpétuellement  mit à jour avec les dernières sources de wordpress.org.

Il est possible de s'appuyer sur ce repo pour installer WordPress au sein d'un projet.

```json
{
  "require": {
    "php": ">=5.4",
    "johnpbloch/wordpress": "4.*"
  },
  "extra": {
    "wordpress-install-dir": "wp"
  }
}
```

Cette configuration permet de récupérer WordPress dans un sous dossier `wp/`

#### composer, plugins et thèmes

[wpackagist](https://wpackagist.org/), un repo de packages associés à WordPress. Les thèmes et plugins peuvent être définis dans la config de composer afin de les installer rapidement via `composer install`.

```json
{
  "repositories": [
    {
      "type": "composer",
      "url": "https://wpackagist.org"
    }
  ],
  "require": {
    "php": ">=5.4",
    "johnpbloch/wordpress": "4.*",
    "wpackagist-plugin/advanced-custom-fields": "*",
    "wpackagist-plugin/contact-form-7": "*",
    "wpackagist-theme/hueman":"*",
    "wpackagist-theme/twentyseventeen":"*",
    "wpackagist-theme/bento":"*"
  },
  "extra": {
    "wordpress-install-dir": "wp",
    "installer-paths": {
        "content/plugins/{$name}/": ["type:wordpress-plugin"],
        "content/themes/{$name}/": ["type:wordpress-theme"]
    }
  }
}
```

</details>

### Installation via `wp-cli`

Une installation de WordPress via la ligne de commande en passant par l'utilitaire `wp-cli`. 

Configuration avancée, agir en direct sur le comportement de l'installation, installer des composants annexes ne sont que quelques atouts de cette méthode.

:hand:**Attention**
Il est impératif d'avoir `wp-cli` d'installer et configurer avant de se lancer. 
_Cette méthode nécessite également d'être très à l'aise avec la ligne de commande !_

<details>

#### Téléchargement de WordPress

`wp core download --locale=fr_FR`

#### Création du fichier de configuration

`wp config create --dbname="nom_de_la_base" --dbuser="user_de_la_base" --dbpass="pass_de_la_base" --locale=fr_FR --skip-check --extra-php <<PHP
define('WP_DEBUG', true);
PHP`

#### Création de la base de données

`wp db create`

#### Installation

`wp core install --url="http://url/projet" --title="Titre du site" --admin_user="login administrateur" --admin_password="pass administrateur" --admin_email="email administrateur" --skip-email`


#### Modifications de différentes options

Changement de l'url de WordPress

`wp option update siteurl $(wp option get siteurl)/$core_dir`

Refuser l'indexation

`wp option update blog_public 0`

Limiter à 5 posts par page (au lieu de 10 par défaut)

`wp option update posts_per_page 5`

</details>