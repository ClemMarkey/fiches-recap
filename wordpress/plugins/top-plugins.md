# Les plugins à connaître

## Debug et développement

* [Debug Objects](https://wordpress.org/plugins/debug-objects/) Configurable et très complet
* [What the file](https://wordpress.org/plugins/what-the-file/) Quel template et fragments de templates sont utilisés pour rendre la page
* [Query Monitor](https://fr.wordpress.org/plugins/query-monitor/) offre une vision très complete sur les requêtes de toutes nature au sein de WordPress
* [Simply Show Hooks](https://fr.wordpress.org/plugins/simply-show-hooks/) permet de rapidement voir quels hooks sont appelés et les fonctions associées.


## Les indispensables

* [Advanced Custom Fields](https://fr.wordpress.org/plugins/advanced-custom-fields/) donne des ailes aux Custom Fields
* [Contact Form 7](https://fr.wordpress.org/plugins/contact-form-7/) LE gestionnaire de formulaires
* [ACF-CF7](https://github.com/taylormsj/acf-cf7) permet d'associer Contact Form 7 et ACF
* [Yoast SEO](https://fr.wordpress.org/plugins/wordpress-seo/) LE plugin de SEO
* [WP Super Cache](https://fr.wordpress.org/plugins/wp-super-cache/) gestion du cache aux petits oignons.
* ou [W3 Total Cache](https://fr.wordpress.org/plugins/w3-total-cache/) une alternative à WP Super Cache
* [Page Builder SiteOrigin](https://fr.wordpress.org/plugins/siteorigin-panels/) un page builder très simple et très efficace
 
 
## Multilingue

* [WPML](https://wpml.org/) LA référence de gestion des langues
* [Polylang](https://fr.wordpress.org/plugins/polylang/) gère la traduction des contenus
* [Loco Translate](https://fr.wordpress.org/plugins/loco-translate/) permet d'éditer les fichiers PO depuis WordPress

## Utilitaires

* [Regenerate Thumbnails](https://fr.wordpress.org/plugins/regenerate-thumbnails/) permet de régénérer les médias
* [Duplicate Post](https://fr.wordpress.org/plugins/duplicate-post/) permet de dupliquer un post en 1 clic
* [Disable Comments](https://fr.wordpress.org/plugins/disable-comments/)
* [TinyMCE Widget](https://fr.wordpress.org/plugins/black-studio-tinymce-widget/) offre un widget avec un champ visual editor
* [EWWW Image Optimizer](https://fr.wordpress.org/plugins/ewww-image-optimizer/) pour l'optimisation des images
* [CPT UI](https://wordpress.org/plugins/custom-post-type-ui/) une interface pour la création des Custom Post Types.
* [Duplicator](https://fr.wordpress.org/plugins/duplicator/) permet de faire un backup très rapidement d'un site WordPress.



