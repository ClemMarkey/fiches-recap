# Custom post type

@note - refresh des permalinks

## Principe

Ajouter des types posts personnalisés afin d'enrichir la diversité proposée par WordPress

## Mise en place

Tout d'abord il faut configurer un post type qui sera enregistré via [register_post_type](https://codex.wordpress.org/Function_Reference/register_post_type) - [Reference](https://developer.wordpress.org/reference/functions/register_post_type/). Puis déclarer une nouvelle taxonomie via [register_taxonomy](https://codex.wordpress.org/Function_Reference/register_taxonomy) - [Reference](https://developer.wordpress.org/reference/functions/register_taxonomy/)


```php
class Pluginname_cpt_recipe {

    function __construct() {
	    add_action('init',array($this,'create_post_type'));
	    add_action('init',array($this,'create_taxonomies'));
    }

    private function create_post_type() {
		$labels = array(
	        'name' => _x( 'Recipes', 'pluginname-recipe' ),
	        'singular_name' => _x( 'Recipe', 'pluginname-recipe' ),
	        'add_new' => _x( 'Add New', 'pluginname-recipe' ),
	        'add_new_item' => _x( 'Add New Recipe', 'pluginname-recipe' ),
	        'edit_item' => _x( 'Edit Recipe', 'pluginname-recipe' ),
	        'new_item' => _x( 'New Recipe', 'pluginname-recipe' ),
	        'view_item' => _x( 'View Recipe', 'pluginname-recipe' ),
	        'search_items' => _x( 'Search Recipes', 'pluginname-recipe' ),
	        'not_found' => _x( 'No recipe found', 'pluginname-recipe' ),
	        'not_found_in_trash' => _x( 'No recipe found in Trash', 'pluginname-recipe' ),
	        'parent_item_colon' => _x( 'Parent Recipe:', 'pluginname-recipe' ),
	        'menu_name' => _x( 'Recipes', 'pluginname-recipe' ),
	    );
	
	    $args = array(
	        'labels' => $labels,
	        'hierarchical' => true,
	        'description' => __('Recipe filterable by type'),
	        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes' ),
	        'taxonomies' => array( 'type' ),
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'menu_position' => 5,
	        'menu_icon' => 'dashicons-layout',
	        'show_in_nav_menus' => true,
	        'publicly_queryable' => true,
	        'exclude_from_search' => false,
	        'has_archive' => true,
	        'query_var' => true,
	        'can_export' => true,
	        'rewrite' => true,
	        'capability_type' => 'post'
	    );
	
	    register_post_type( 'recipe', $args );
    }

    private function create_taxonomies() {
    	register_taxonomy(
	        'type',
	        'recipe',
	        array(
	            'hierarchical' => true,
	            'label' => 'Types',
	            'query_var' => true,
	            'rewrite' => array(
	                'slug' => 'type',
	                'with_front' => false
	            )
	        )
	    );
    }
}

new Pluginname_cpt_recipe();
```

## Templates

### Template custom

La création d'un template perso

```php
<?php
/*
Template Name: Recipes List
*/

// ...

?>
```

Comportant une requête adaptée avec l'attribut `'post_type' => 'mon-posttype'`

```php
<?php
 $query = new WP_Query( array('post_type' => 'recipe', 'posts_per_page' => 5 ) );
 if ($query->have_posts()) :
 while ( $query->have_posts() ) : $query->the_post(); ?>
// the_title() - the_content() - ...
<?php endwhile; ?>
<?php endif; wp_reset_postdata(); ?>
```

### single-$posttype.php

La création de ce fichier suffit à gérer l'affichage d'un CPT

### archive-$posttype.php

La création de ce fichier suffit à gérer l'affichage d'une taxonomie de CPT

## Pour se simplifier la vie

* [Générer un Custom Post Type](https://generatewp.com/post-type/)
* [Générer une taxonomie](https://generatewp.com/taxonomy/)


