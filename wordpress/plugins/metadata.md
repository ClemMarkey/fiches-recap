# Plugins - Metadata

## Metadata

Les metadata d'un post sont des données sur la donnée post. Les Custom fields, par exemple, sont un exemple parfait

### Manipuler les metadata

Comme vu avec les CF, récupérer les metadata d'un post est très simple

```php
// Récupération des metadata
<?php $meta = get_post_meta( get_the_ID() ); ?>
// Récupération des valeurs de `ma_cle`
<?php $valeurs_ma_cle = get_post_meta( get_the_ID(), 'ma_cle' ); ?>
// Récupération de la première valeur de `ma_cle`
<?php $valeur_ma_cle = get_post_meta( get_the_ID(), 'ma_cle', true ); ?>
```

Il est également possible de 

* [get_post_meta()](http://codex.wordpress.org/Function_Reference/get_post_meta) Récupérer des post metadata.
* [add_post_meta()](http://codex.wordpress.org/Function_Reference/add_post_meta) Ajouter des post metadata.
* [update_post_meta()](http://codex.wordpress.org/Function_Reference/update_post_meta) Mettre à jour des post metadata.
* [delete_post_meta()](http://codex.wordpress.org/Function_Reference/delete_post_meta) Supprimer des post metadata.

## Meta boxes

Il est possible d'ajouter des zones éditable dans les posts dans l'interface d'administration de WordPress. Par exemple, pour préciser une classe css à ajouter au post, une option de layout, une taille maximum, etc... 

Ces données seront stockées comme metadata du post.

### Exemple de mise en place

```php
function exemple_register_meta_boxes() {
    add_meta_box( 'meta-box-id', __( 'Ma Meta Box', 'textdomain' ), 'exemple_display_callback', 'post' );
}
add_action( 'add_meta_boxes', 'exemple_register_meta_boxes' );

function exemple_display_callback( $post ) {
    // Affichage de la metabox
}

function exemple_save_meta_box( $post_id ) {
    // Logique d'enregistrement de la metabox
}
add_action( 'save_post', 'exemple_save_meta_box' );
```

* [`add_meta_box()` code référence](https://developer.wordpress.org/reference/functions/add_meta_box/), possède des exemples très simples
* [How To Create WordPress Meta Boxes](https://www.smashingmagazine.com/2011/10/create-custom-post-meta-boxes-wordpress/) sur le site Smashing Magazine couvre une mise en place complète
* [Mise en place complète de metabox, en français](https://wabeo.fr/jouons-avec-les-meta-boxes/)

## Purger les metadata inutilisées

Comme pour les CF, `DELETE FROM wp_postmeta WHERE meta_key = 'meta_key';` permet de rapidement supprimer les éléments inutilisés

## Génerateur de metabox

Ce [générateur](https://jeremyhixon.com/tool/wordpress-meta-box-generator-v2-beta/) permet de facilement ajouter une gestion de metabox via une configuration simple.