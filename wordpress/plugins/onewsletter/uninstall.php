<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

require __DIR__.'onewsletter.php';

$onewsletter = new oNewsletter();
$onewsletter->removeTable();