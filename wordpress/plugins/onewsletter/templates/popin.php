<div class="ppshader">
  <div class="popin">
    <p class="popin__title">Restons en contact !</p>
    <form action="" method="post">
      <div class="popin__input">
        <label for="email">Mon email</label> <input type="email" name="email" id="email">
      </div>
      <div class="popin__input">
        <label for="optin_partners">Je m'inscris aux newsletters de mes partenaires</label> <input type="checkbox" name="optin" id="optin">
      </div>
      <input type="hidden" name="onewsletter_popin_submit" value="1">
      <input type="submit" value="Je ne veux plus jamais en entendre parler">
      <input type="submit" name="optin_general" value="Je m'inscrit à la newsletter">
    </form>
  </div>
</div>
