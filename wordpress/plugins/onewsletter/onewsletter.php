<?php
/*
Plugin Name: oNewsletter
Description: Génération automatique d'une popin qui propose de s'abonner à la Newsletter du site
Author: O'clock
Version: 1.0
*/

// Sécuriser le plugin
if (!defined('WPINC')) {die();}

class oNewsletter
{
  private $wpdb;
  private $table;

  public function __construct()
  {
    // Récupération de l'instance wpdb
    // https://codex.wordpress.org/Class_Reference/wpdb
    global $wpdb;

    $this->wpdb = $wpdb;
    $this->table = $wpdb->prefix . 'newsletter';

    // Si on a le cookie, on ne souhaite plus afficher la popin
    if (!$this->haveCookie()) {

      add_action('wp_enqueue_scripts', [$this, 'stylePopin'], 20);
      add_action('wp_footer', [$this, 'printPopin'], 20);
      add_action('init', [$this, 'catchFormSubmit'], 20);
    }
  }

  public function createTable()
  {
    $sql = "CREATE TABLE {$this->table} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `email` VARCHAR(255) NOT NULL,
              `optin_general` TINYINT NOT NULL DEFAULT '0',
              `optin_partners` TINYINT NOT NULL DEFAULT '0',
              `fk_user_id` INT NULL DEFAULT NULL,
              PRIMARY KEY (`id`),
              INDEX (`optin_general`),
              INDEX (`optin_partners`),
              UNIQUE (`email`)
            );";

    $this->wpdb->query($sql);
  }

  public function removeTable()
  {
    $sql = "DROP TABLE {$this->table};";

    $this->wpdb->query($sql);
  }

  public function printPopin()
  {
    require __DIR__ . '/templates/popin.php';
  }

  public function stylePopin()
  {
    wp_enqueue_style(
      'popin-style',
      plugin_dir_url( __FILE__ ) . '/public/popin.css',
      [],
      '1.0.0'
    );
  }

  public function catchFormSubmit()
  {
    // Si le formulaire n'a pas été soumis: je passe
    if (empty($_POST['onewsletter_popin_submit'])) {
      return;
    }

    // Si je n'ai pas d'adresse email ou si il n'a pas souhaité s'abonner: je passe
    if (empty($_POST['email']) || empty($_POST['optin_general'])) {
      $this->setCookie();
      return;
    }

    // je regarde si il a coché l'optin partners
    $optin = !empty($_POST['optin']) ? 1 : 0;

    // J'essaie de faire correspondre l'adresse email avec un user en base
    $fk_user_id = null;

    // si il est connecté je l'associe
    if (get_current_user_id() > 0) {
      $fk_user_id = get_current_user_id();

    } else {

      // Sinon je cherche l'email déjà présent en base
      $user = get_user_by('email', $_POST['email']);

      // Si il y a bien un user avec cet email
      if ($user) {

        // je l'associe
        $fk_user_id = $user->ID;
      }
    }

    // pour finir si j'ai un user
    if ($fk_user_id) {

      // je lui ajoute une meta "has_onewsletter"
      add_user_meta($fk_user_id, 'has_onewsletter', 1, true);
    }

    // Si il existe déjà en table newsletter
    $exist = $this->alreadyExists($_POST['email']);

    if ($exist) {

      // Je le remplace
      $this->wpdb->replace(
        $this->table,
        [
          'id' => $exist,
          'email' => $_POST['email'],
          'optin_general' => 1,
          'optin_partners' => $optin,
          'fk_user_id' => $fk_user_id,
        ],
        [
          '%d',
          '%s',
          '%d',
          '%d',
          '%d',
        ]
      );

    } else {

      // Sinon je l'ajoute
      $this->wpdb->insert(
        $this->table,
        [
          'email' => $_POST['email'],
          'optin_general' => 1,
          'optin_partners' => $optin,
          'fk_user_id' => $fk_user_id,
        ],
        [
          '%s',
          '%d',
          '%d',
          '%d',
        ]
      );
    }

    $this->setCookie();
  }

  public function alreadyExists($email)
  {
    $prepared = $this->wpdb->prepare(
      "
        SELECT id
        FROM {$this->table}
        WHERE email = %s;
      ",
      $email);

    return $this->wpdb->get_var($prepared);
  }

  public function setCookie()
  {
    setcookie(
      'onewsletter_popin',
      '1',
      mktime(0, 0, 0, 12, 31, date('Y') + 5),
      '/',
      COOKIE_DOMAIN,
      is_ssl(),
      true
    );

    // Je recharge pour éviter de re-afficher la popin
    $this->reload();
  }

  public function reload()
  {
    global $wp;

    $current_url = home_url(add_query_arg(array(), $wp->request));

    if ( wp_redirect( $current_url ) ) {
      exit;
    }
  }

  public function haveCookie()
  {
    return isset($_COOKIE['onewsletter_popin']);
  }

  public function activate()
  {
    $this->createTable();
  }
}

// J'instancie ma classe
$onewsletter = new oNewsletter();

// A l'activation du plugin...
register_activation_hook(__FILE__, [$onewsletter, 'activate']);