# Plugins - Shortcodes


Les [shortcodes](https://codex.wordpress.org/Shortcode) peuvent être vus comme des macros, déposés dans le contenu des posts, ils sont interprétés et génèrent un code html associé.

Par défaut, il existe un certain nombre de shortcodes déjà présent dans WordPress :

* [`[audio]`](https://codex.wordpress.org/Audio_Shortcode)
* [`[caption]`](https://codex.wordpress.org/Caption_Shortcode)
* [`[embed]`](https://codex.wordpress.org/Embed_Shortcode)
* [`[gallery]`](https://codex.wordpress.org/Gallery_Shortcode)
* [`[playlist]`](https://codex.wordpress.org/Playlist_Shortcode)
* [`[video]`](https://codex.wordpress.org/Video_Shortcode)


Il est possible de créer ses propres shortcodes via la [Shortcode_API](https://codex.wordpress.org/Shortcode_API) de WordPress.

## Exemple simple

En plaçant ce code dans `functions.php`

```php
function sc_exemple_func( $atts ){
	return "Heyy, voici un exemple";
}

add_shortcode( 'exemple', 'sc_exemple_func' );
```

Puis en insérant `[exemple]` dans un contenu d'article, la phrase `Heyy, voici un exemple` apparaîtra dans le contenu.


## Paramètres

Les shortcodes peuvent recevoir des paramètres 

```
[exemple label="mon label" size="200"]
```

Afin d'exploiter les paramètres passés, [`shortcode_atts()`](https://developer.wordpress.org/reference/functions/shortcode_atts/), permet de déclarer les paramètres possible et leurs attribue une valeur par défaut si le paramètre n'est pas défini.

```php
function sc_exemple_func( $atts ) {
    $a = shortcode_atts( array(
        'label' => 'label par défaut',
        'size' => 'size par défaut',
    ), $atts );

    return "foo = {$a['label']}";
}

add_shortcode( 'exemple', 'sc_exemple_func' );
```


## 2 types

Il existe des shortcodes avec ou sans balise de fermeture

```
[exemple]

[exemple]Du contenu[/exemple]
```

Lorsqu'un contenu passé dans un shortcode avec une balise de fermeture, ce contenu pourra être réutilisé directement dans la fonction allouée au shortcode

```php
function caption_shortcode( $atts, $content = null ) {
	return '<span class="caption">' . $content . '</span>';
}

add_shortcode( 'caption', 'caption_shortcode' );
```

Ainsi le code précédent pourra être appelé via `[caption]Hello[/caption]`

```html
<span class="caption">Hello</span>
```

## Imbrication de Shortcodes

Partant de ces Shortcodes

```php
//caption
function caption_shortcode( $atts, $content = null ) {
	return '<span class="caption">' . $content . '</span>';
}

add_shortcode( 'caption', 'caption_shortcode' );


//hello_world
function hello_shortcode( $atts) {
	return '<strong>' . 'Hello World' . '</strong>';
}

add_shortcode( 'hello_world', 'hello_shortcode' );
```

On pourrait envisager cet appel

```
[caption]Caption: [hello_world][/caption]
```

Mais le résultat ne sera celui attendu

```html
<span class="caption">Caption: [hello_world]</span>
```

Il est possible d'imbriquer l'exécution des Shortcodes via [`do_shortcode()`](https://developer.wordpress.org/reference/functions/do_shortcode/). En modifiant la déclaration du shortcode `caption` vers :

```php
function caption_shortcode( $atts, $content = null ) {
	return '<span class="caption">' . do_shortcode($content) . '</span>';
}

add_shortcode( 'caption', 'caption_shortcode' );
```


## Lectures

* [Shortcode_API](https://codex.wordpress.org/Shortcode_API)
* [Shortcodes - Complete Guide](https://www.smashingmagazine.com/2012/05/wordpress-shortcodes-complete-guide/) sur Smashing Magazine