# Browser-sync

[Browser Sync](https://browsersync.io/) est un package NPM [browser-sync](https://www.npmjs.com/package/browser-sync) qui offre la possibilité de recharger automatiquement le navigateur dès qu'une modification intervient sur un fichier.

## Installation

```bash
$ npm install -g browser-sync
```

## Initialisation

Produit un fichier de config pour Browser-sync avec toutes les options modifiables (pratique pour découvrir les possibilités)

```bash
$ browser-sync init
```

## Démarrage rapide

Depuis un projet statique (index.html à la racine)

```bash
$ browser-sync start --server --files *
```

## `--server`

Sites statiques

## `--proxy "..."`

Sites dynamiques

`--proxy "localhost/monsitephp"`

## `--files "..."`

Les types de fichiers qui déclencheront un rechargement du navigateur

## `----logLevel "..."`

* silent
* info
* debug