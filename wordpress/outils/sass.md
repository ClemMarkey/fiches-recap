# SASS

[SASS](http://sass-lang.com/) est un pré-processeur CSS extrêmement populaire.

## Pré-processeur ?

Un pré-processeur (preprocessor) CSS propose des outils pour faciliter l'écriture et la maintenance des scripts CSS. Variables, fonctions, imbrications...

L'écriture passe par une syntaxe particulière adaptée au pré-processeur qui interprète le code écrit afin d'en générer du CSS classique.

Syntaxe spéciale => Pré-processeur (SASS, Stylus, Less, ...) => Code CSS

**Différents pré-processeurs**

* [SASS](http://sass-lang.com/)
* [Stylus](http://stylus-lang.com/)
* [LESS](http://lesscss.org/)
* ...

## Installation

Il est possible d'installer SASS directement dans notre projet:

### node-sass

<details>

Tout d'abord, il faut initialiser le `package.json`

```bash
$ npm init
```

node-sass s'installe via npm (option --save-dev pour définir une dépendance au projet)

```bash
$ npm install --save-dev node-sass
```

#### Exemple rapide

On utilise le bin directement

```bash
$ ./node_modules/.bin/node-sass style.scss style.css
```

Puis en Script simple dans NPM

```json
"scripts": {
  	"css": "node-sass style.scss style.css"
}
```

#### package.json

```json
{
  "name": "projet-sass",
  "version": "1.0.0",
  "description": "Un simple projet SASS",
  "scripts": {
  	"css": "node-sass style.scss style.css",
    "build-css-simple": "node-sass scss/style.scss css/style.css",
    "build-css": "node-sass --watch --output-style compressed scss/style.scss css/style.css"
  },
  "devDependencies": {
    "node-sass": "^4.5.2"
  }
}
```

##### 2 scripts sont disponibles avec `npm run`

- `npm run build-css-simple` : compile les fichiers `scss` vers `css` 1 seule fois
- `npm run build-css` : compile les fichiers `scss` vers `css` à chaque modification sur un fichier `scss`

</details>

Mais également en utilisation avec Brunch:

### sass-brunch

<details>

#### Dans notre package.json

Le package officiel **sass-brunch** étant en retard sur le module **node-sass** il est possible d'utiliser le fork ci-dessous maintenu à jour. Cela simplifie son installation (pas besoin de recompiler)
```json
"devDependencies": {
"sass-brunch": "git+ssh://git@github.com:christopheOclock/sass-brunch.git",
[...]
}
```

</details>

## Syntaxe

Le pré-processeur SASS propose 2 syntaxes bien distinctes.

###  Syntaxe SCSS

Syntaxe identique au CSS, les fichiers seront enregistrés avec l'extension `.scss`

```scss
ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
```

### Syntaxe SASS

Syntaxe indentée, les fichiers seront enregistrés avec l'extension `.sass`

```scss
ul
  margin: 0
  padding: 0
  list-style: none
```

## Fonctionnalités

### Variables

Il est possible d'utiliser des variables via `$` afin de gérer des occurrences de valeurs beaucoup plus simplement.

**SCSS**

```scss
$font-stack: Helvetica, sans-serif;
$primary-color: #123456;

body {
	font: 100% $font-stack;
	color: $primary-color;
}
```

**CSS généré**

```css
body {
	font: 100% Helvetica, sans-serif;
    color: #123456;
}
```

### Imbrication

L'imbrication permet de structurer plus facilement les déscendances.

**SCSS**

```scss
ul {
	margin: 0;
	li {
		color: #ccc;
  }
}
```

**CSS généré**

```css
ul {
  margin: 0;
}
ul li {
  color: #ccc;
}
```
### Référence au parent

Il est d'utiliser `&` pour cibler l'élément parent, c'est un opérateur très puissant.

**SCSS**

```scss
h3 {
	padding: 10px;
  &:hover {
  	color: #ccc;
  }
  .autre & {
  	font-weight: bold;
  }
}
```

**CSS généré**

```css
h3 {
	padding: 10px;
}

h3:hover {
  color: #ccc;
}

.autre h3 {
	font-weight: bold;
}
```

### Fragmentation & import

Le symbole `_` défini un fichier à importer comme `_vars.scss` ou `_reset.scss`

l'import se fait avec la syntaxe d'importation classique `@import`

```scss
@import 'vars';
@import 'reset';
```

### Mixins

`@include` permet d'exécuter un mixin déclaré avec `@mixin`. Un mixin peut prendre des paramètres

**SCSS**

```scss
@mixin border-radius($radius) {
  -webkit-border-radius: $radius;
     -moz-border-radius: $radius;
      -ms-border-radius: $radius;
          border-radius: $radius;
}

.box { @include border-radius(10px); }
```

**CSS généré**

```css
.box {
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
}
```

### Héritage

`@extend` permet de gérer l'héritage d'un autre élément

Il est possible d'utiliser les placeholders `%` mais ne pas en abuser ! [Cet article](https://csswizardry.com/2014/11/when-to-use-extend-when-to-use-a-mixin/) permet d'y voir plus clair.

**SCSS**

```scss
.message {
  border: 1px solid #ccc;
  padding: 10px;
  color: #333;
}

.success {
  @extend .message;
  border-color: green;
}

.error {
  @extend .message;
  border-color: red;
}
```

**CSS généré**

```css
.message, .success, .error {
  border: 1px solid #cccccc;
  padding: 10px;
  color: #333;
}

.success {
  border-color: green;
}

.error {
  border-color: red;
}
```

### Opérateurs arithmétiques

Il est possible de manipuler des valeurs (les unités peuvent être différentes) avec des opérateurs arithmétiques courants `+`, `-`, `*`, `/`

**SCSS**

```scss
.container { width: 100%; }

article[role="main"] {
  float: left;
  width: 600px / 960px * 100%;
}

aside[role="complementary"] {
  float: right;
  width: 300px / 960px * 100%;
}
```

**CSS généré**

```css
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 62.5%;
}

aside[role="complementary"] {
  float: right;
  width: 31.25%;
}
```


## Guide

Une formidable ressource est [ce guide](https://sass-guidelin.es/) de Hugo Giraudel. Architecture, choix de nommage, proposition d'organisation, tout y est !
