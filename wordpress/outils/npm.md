# NPM

[NPM](https://www.npmjs.com) Node Package Manager, est un gestionnaire de packages.

à noter que la plateforme [Libraries.io](https://libraries.io) permet de rechercher parmi toutes les plateformes de gestionnaire de packages

Une documentation très fournie et claire est disponible sur le site de [NPM](https://docs.npmjs.com/)

## Commande `npm init`

Initialisation du fichier `package.json`

> l'option `--yes` ou `-y` permet de passer les étapes de validation rapidement

**package.json par défaut** 

```json
{
  "name": "projet",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

## Commande `npm install`

> alias `i`

Installation de la configuration mentionnée dans le package.json

## Commande `npm install <package>`

> alias `i`

Installer un package trouvé sur la plateforme [NPM](https://www.npmjs.com)

Les packages installés seront placés dans le dossier `node_modules`

### globalement ou localement

Il est possible d'installer un package globalement avec l'option `--global` ou `-g` le package pourra être appelé directement en ligne de commande, par exemple `brunch` installé globalement pourra être appelé comme ceci

```bash
$ brunch new
```

### option `--save-dev` ou `-D`

**Dépendances de développement**

par exemple il est nécessaire, pour le développement, d'avoir un système de rechargement automatique du navigateur mais cette fonctionnalité ne jouera aucun rôle en production

### option `--save` ou `-S`

**Dépendances de projet**

par exemple, jQuery, bootstrap, normalize, ... sont des dépendances du projet

## Commande `npm run <command>`

> alias de `npm run-script <command>`

Permet de lancé un script défini dans le fichier package.json

```bash
$ npm run test
```

---

## Tâches courantes

### Exécution en parallèle

#### &

**Pour** : `&` ... c'est tout

**Contre** : Réservé aux systèmes UNIX - Incompatible avec Windows

#### Options cross-platform

- [npm-run-all](https://www.npmjs.com/package/npm-run-all)
- [concurrently](https://www.npmjs.com/package/concurrently)
- ...

**npm-run-all**

```bash
npm install npm-run-all --save-dev
```

### Suppression de dossiers

[`rimraf`](https://www.npmjs.com/package/rimraf)

### Live reload

[`browser-sync`](https://www.browsersync.io/)

### Restart

[`nodemon`](https://nodemon.io/)

