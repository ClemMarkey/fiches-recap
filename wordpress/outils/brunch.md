# Brunch

[Brunch](http://brunch.io/) est un builder apparu en Janvier 2011, dans ce domaine c'est donc le dinosaure. (Grunt Septembre 2011)

**Quelques concurrents**

* Grunt
* Gulp
* Broccoli
* Glou
* Webpack
* ...

Brunch est un builder d'assets en pipeline - Lire ["Pourquoi je préfère Brunch"](https://delicious-insights.com/fr/articles/brunch-mon-builder-prefere/)

Cet article a été enrichi et est utilisé comme la documentation fr de brunch : [brunch-guide fr](https://github.com/brunch/brunch-guide/tree/master/content/fr)

## Installation

```bash
$ npm install -g brunch
```
## Initialisation 

### Création d'un projet

Cette opération ajoutera tout le nécessaire pour un projet de base

```bash
$ brunch new mon-projet
```

### Génération des fichiers

Dans le dossier du projet, cette commande permet de générer les fichiers à partir du dossier app/ (par défaut) vers le dossier public/ (par défaut)

```bash
$ brunch build
```

Il est possible d'ajouter l'option `--production` pour optimiser les fichiers (minification, nettoyage, ...)

---

Lors de l'installation, brunch, met également un script npm à disposition pour exécuter `brunch build --production` il s'agit du script `build` 

```bash
$ npm run build
```

### Génération des fichiers à la volée

Il est possible de générer les fichiers dès qu'une modification est faite

```bash
$ brunch watch
```

En y ajoutant l'option `-s` ou `--server`, brunch lance un serveur local proposant notamment le rechargement automatique du navigateur (facilement remplaçable par browser-sync -> voir la section plugins).

```bash
$ brunch watch --server
```

---

Lors de l'installation, brunch, met également un script npm à disposition pour exécuter `brunch watch --server` il s'agit du script `start` 

```bash
$ npm run start
```

## Configuration

Le fichier `brunch-config.js` est le fichier de configuration de brunch.

[Configuration](http://brunch.io/docs/config)

## Squelettes

Brunch propose des squelettes de projets (avec React, jQuery, bootstrap, SASS, etc...)

[Skeletons](http://brunch.io/skeletons)

## Plugins

Brunch peut être enrichi avec de multiples plugins comme sass-brunch, browser-sync-brunch, etc...

[Plugins](http://brunch.io/plugins)

### Installation de `sass-brunch`

Dans le dossier du projet

```bash
$ npm install --save-dev sass-brunch
```

ou directement dans le `package.json`

Voir [la fiche récap de SASS](sass.md#dans-notre-packagejson)


### Installation de `browser-sync-brunch`

Dans le dossier du projet

```bash
$ npm install --save-dev browser-sync-brunch
```

Une fois `browser-sync-brunch` installé, il faut penser à supprimer `auto-reload-brunch`

#### Configuration de `browser-sync-brunch`

Dans le brunch-config:

```javascript
exports.plugins = {
  browserSync: {
    files: ['*']
  }
};
```

```javascript
exports.watcher = {
  usePolling: true,
  awaitWriteFinish: true
};
```