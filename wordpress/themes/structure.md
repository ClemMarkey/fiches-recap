# Thème : Structure

## Dossiers et fichiers

### Structure du Thème 2017

* assets/
	* css/
	* images/
	* js/
	* inc/
* template-parts/
	* footer/
	* header/
	* navigation/
	* page/
	* post/
* 404.php
* archive.php
* comments.php
* footer.php
* front-page.php
* functions.php
* header.php
* index.php
* page.php
* README.txt
* rtl.css
* screenshot.png
* search.php
* searchform.php
* sidebar.php
* single.php
* style.css

Pour la gestion des langues, il est nécessaire d'ajouter un dossier `languages` à la racine du thème

## En détail

### style.css

```css
/*
Theme Name: Twenty Seventeen
Theme URI: https://wordpress.org/themes/twentyseventeen/
Author: the WordPress team
Author URI: https://wordpress.org/
Description: ... 
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: twentyseventeen
Tags: ...
*/
```

La [liste](https://make.wordpress.org/themes/handbook/review/required/theme-tags/) de tags de theme

### index.php

LE point d'entrée et fichier de template principal

### front-page.php

Si il existe, ce template est utilisé pour afficher une page statique (admin > réglages > lecture)

### home.php

Si il existe, ce template est utilisé pour afficher les derniers posts (admin > réglages > lecture)

### header.php

le Template d'entête contient doctype, head, link et autre méta

### footer.php

le Template de pied de page

### singular.php

Le template singular est utilisé lorsque les template single ou page sont absents. Si singular est lui-même absent, index.php prend le relais.

### single.php

Template pour l'affichage d'un post

### single-{post-type}.php

Template pour l'affichage d'un post d'un type particulier (Custom Post Type)

### page.php

Template pour l'affichage d'une page

### page-{slug}.php

Template pour l'affichage d'une page en précisant un slug précis

### archive.php

Ce template prend en charge l'affichage des catégories, tags, auteurs ou date

### attachment.php

Le template propre aux types de post attachement (media)

### search.php

Template des résultats de recherche

### comments.php

Le template pour les commentaires

### 404.php

Ce template est appelé quand la requête utilisateur ne coïncide avec aucun résultat
