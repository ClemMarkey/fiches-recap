# Setup d'un thème

## Starter

* style.css 
* index.php 
* screenshot.png

**style.css**

```css
/*
Theme Name: oFirst
*/
```

**index.php**

```php
<?php
	echo 'Hello WordPress';
?>
```

**screenshot.png**

La preview du thème

## Fichiers minimums

### header.php

Il doit impérativement contenir l'appel à `wp_head()`

> get_header() appellera ce fichier

### footer.php

Il doit impérativement contenir l'appel à `wp_footer()`

> get_footer() appellera ce fichier

### sidebar.php

> get_sidebar() appellera ce fichier

---

## functions.php

Ce fichier va permettre d'ajouter des fonctionnalités au thème auquel il est associé. Les principaux objectifs de ce fichier seront :

* Utiliser les Hooks de WordPress pour ajouter ou modifier un comportement
* Activer des fonctionnalités de thèmes (Image à la une, format de post, ...)
* Proposer des fonctions utilitaires pour les templates
* Enrichir les chargements de styles et scripts par défaut (enqueue)
* Définir et configurer des options avancées comme le customizer

Typiquement, le fichier `functions.php` devrait contenir au minimum une fonction php pour initialiser la configuration du thème. Souvent appelée `themeexemple_setup()`

```php
if ( ! function_exists( 'themeexemple_setup' ) ) :

function themeexemple_setup() 
{
	// ...
}

endif;
// Ajout d'une action au Hook 'after_setup_theme'
add_action( 'after_setup_theme', 'themeexemple_setup' );
```

## Déclarations des CSS & JS

Les déclarations des scripts et styles doivent être placées dans le fichier `functions.php`, idéalement dans une fonction indépendante au setup. Typiquement `themeexemple_scripts()`.

```php
function themeexemple_scripts()
{
	// ...
}
add_action( 'wp_enqueue_scripts', 'themeexemple_scripts' );
```

### [`wp_enqueue_style`](https://developer.wordpress.org/reference/functions/wp_enqueue_style/)

```php
wp_enqueue_style( $handle, $src, $deps, $ver, $media );
```

* `$handle` Nom
* `$src` Url du fichier
* `$deps` Dépendances (tableau)
* `$vers` Version
* `$media` Media : 'all', 'screen', 'print', ...

**Exemple**

```php
wp_enqueue_style( 'exempletheme-css', get_template_directory_uri() . '/assets/css/app.css', [], '20170425', 'all');
```


### [`wp_enqueue_script`](https://developer.wordpress.org/reference/functions/wp_enqueue_script/)

```php
wp_enqueue_script( $handle, $src, $deps, $vers, $in_footer);
```

* `$handle` Nom
* `$src` Url du fichier
* `$deps` Dépendances (tableau)
* `$vers` Version
* `$in_footer` Le fichier doit apparaitre dans le header ou le footer

**Exemple**

```php
wp_enqueue_script( 'exempletheme-vendor', get_template_directory_uri() . '/assets/js/vendor.js', [], '20170425', true );
```

---

## Retirer des éléments chargés et injectés par défaut

Un petit regard sur le code source d'une page WordPress fera apparaître un certain nombre d'injections de code effectuées par `wp_head()`, certains scripts ou certaines balises peuvent aisément être retirées.

Cette liste n'est pas exhaustive mais couvre la plupart des besoins :

```php
// Supprime WP EMOJI
remove_action( 'wp_head', 'print_emoji_detection_script', 7);
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_action( 'wp_print_styles', 'print_emoji_styles');

// Supprime le lien vers Windows Live Editor Manifest
remove_action( 'wp_head', 'wlwmanifest_link' );

// Supprime le lien RSD + XML
remove_action( 'wp_head', 'rsd_link' ); 

// Supprime la meta generator
remove_action( 'wp_head', 'wp_generator' ); 

// Supprime les extra feed rss
remove_action( 'wp_head', 'feed_links_extra', 3 ); 

// Supprime les feeds des Posts et des Commentaires
remove_action( 'wp_head', 'feed_links', 2 ); 

```

---

## Fonctionnalités de thème

Les fonctionnalités de thème se déclarent via la fonction `add_theme_support()`.

**Les déclarations de fonctionnalités doivent être placées dans la fonction de setup du fichier `functions.php`**

* [Content Width](https://codex.wordpress.org/Content_Width "Content Width")
* [Automatic Feed Links](https://codex.wordpress.org/Automatic_Feed_Links "Automatic Feed Links")
* [Custom Backgrounds](https://codex.wordpress.org/Custom_Backgrounds "Custom Backgrounds")
* [Custom Headers](https://codex.wordpress.org/Custom_Headers "Custom Headers")
* [Post Thumbnails](https://codex.wordpress.org/Post_Thumbnails "Post Thumbnails")
* [Post Formats](https://codex.wordpress.org/Post_Formats "Post Formats")
* [Title Tag](https://codex.wordpress.org/Title_Tag "Title Tag")
* [Editor Style](https://codex.wordpress.org/Editor_Style "Editor Style")
* [Theme Markup](https://codex.wordpress.org/Theme_Markup "Theme Markup")
* [Theme Logo](https://codex.wordpress.org/Theme_Logo "Theme Logo")
* [Sidebars](https://codex.wordpress.org/Sidebars "Sidebars")
* [Navigation Menus](https://codex.wordpress.org/Navigation_Menus "Navigation Menus")

[Liste des fonctionnalités](https://codex.wordpress.org/Theme_Features)

---

### Title tag

Permet de laissez la gestion du `title` à WordPress

#### Déclaration
```php
add_theme_support( 'title-tag' );
```

#### Usage

Si cette fonctionnalité est activée il faut retirer la meta `title` du header. WordPress se chargera de l'ajouter via `wp_head()`

---

### Menus de navigation

[Codex Navigation](https://codex.wordpress.org/Navigation_Menus)

Les menus de navigation déclaré dans l'interface d'administration ont besoin d'un emplacement pour être affiché.

#### Déclaration

La déclaration est automatiquement réalisée lorsqu'un menu est enregistré via `register_nav_menus()` qui se chargera d'executer :

```php
add_theme_support( 'menus' );
```

#### Enregistrement d'un menu

[`register_nav_menus()`](https://developer.wordpress.org/reference/functions/register_nav_menus/)

```php
register_nav_menus([
	'main' => 'Principal',
	'social' => 'Social'
]);
```

#### Utilisation d'un menu

[`wp_nav_menu()`](https://developer.wordpress.org/reference/functions/wp_nav_menu/)

```php
wp_nav_menu([
	'theme_location' => 'main'
]);
```

#### Astuces
##### Classes CSS personnalisées

Il est possible d'ajouter des classes personnalisées aux items d'un menu.

Dans l'interface d'admin `Apparence > Menus > Options d'écran > [x] Classes CSS`

##### container

Par défaut, WordPress ajoute une balise englobante autour de la `nav`.

En précisant `'container' => false` dans les arguments, WordPress supprimera cette balise (Seulement si le menu est alloué dans l'admin)

##### before/after

Via les arguments `before` et `after`, il est possible d'ajouter un contenu supplémentaire avant et après le texte des liens

---

### Post Thumbnails ou Image à la Une

Autorise l'ajout d'image à la une dans les différents posts

#### Déclaration

```php
add_theme_support( 'post-thumbnails' );
```

Il est conseillé, mais pas nécessaire, de créer une taille spécifique aux post-thumbnail. La taille est en fonction des besoins

```php
// 250 pixels de large sur 250 pixels de haut, resize
set_post_thumbnail_size( 250, 250 ); 
// 250 pixels de large sur 250 pixels de haut, crop
set_post_thumbnail_size( 250, 250, true );
// Crop depuis le coin haut / gauche
set_post_thumbnail_size( 250, 250, ['top', 'left'] );
// Crop depuis le centre
set_post_thumbnail_size( 250, 250, ['center', 'center'] );
```

Il est possible de définir de nouvelles tailles

```php
// 500 pixels de large (hauteur illimitée)
add_image_size( 'project-thumb', 500, 9999 ); 
```

#### Usage

Lorsqu'une image à la une est définie, il est possible de l'appelée depuis la boucle via 

[`the_post_thumbnail()`](https://developer.wordpress.org/reference/functions/the_post_thumbnail/)

```php
the_post_thumbnail();
```

Il est possible de préciser la taille du médias

```php
// Sans paramètre ->; Thumbnail
the_post_thumbnail(); 
// Thumbnail (par défaut 150px x 150px max)
the_post_thumbnail( 'thumbnail' ); 
// Medium (par défaut 300px x 300px max)
the_post_thumbnail( 'medium' ); 
// Medium-large (par défaut 768px x pas de hauteur max)
the_post_thumbnail( 'medium_large' ); 
// Large (par défaut 640px x 640px max)
the_post_thumbnail( 'large' );
// Taille d'origine 
the_post_thumbnail( 'full' ); 
// Autres résolutions (largeur, hauteur)
the_post_thumbnail( [ 100, 100 ] ); 
```

Des attributs HTML peuvent être ajoutés

```php
the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive responsive-full', 'title' => 'Feature image']);
```


#### Astuces
##### Récupérer l'URL d'une image à la une

Css-tricks propose une astuce très utile - [featured image URL](https://css-tricks.com/snippets/wordpress/get-featured-image-url/)

```php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];
```

En réutilisant ce code, il est possible d'écrire une fonction utilitaire

```php
function getThumbnailURL($postID, $size) {
	$thumb_id = get_post_thumbnail_id($postID);
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, $size);
	return $thumb_url_array[0];
}
```

Avec un appel au sein d'une boucle WordPress

```php
<?php echo getThumbnailURL($id,'medium_large') ?>
```
---

### Widget Area ou Sidebar

Définition d'une zone de widgets

#### Déclaration

Via `register_sidebar()` il est possible de définir une zone de widgets (sidebar)

[`register_sidebar()`](https://developer.wordpress.org/reference/functions/register_sidebar/)

```php
function themeexemple_register_sidebars() {
    /* 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'class'   => 'sidebar-class',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'themeexemple_register_sidebars' );
```

#### Usage

Via appel dynamique depuis n'importe quel template

```php
<div id="sidebar-primary" class="sidebar">
    <?php dynamic_sidebar( 'primary' ); ?>
</div>
```

Appel via un fichier de template `sidebat-$slug.php`

```php
<?php get_sidebar( 'primary' ); ?>
```

---

### Formats de post

Plusieurs formats de post peuvent être définis

[Formats de post](https://codex.wordpress.org/Post_Formats)

#### Déclaration

```php
add_theme_support( 'post-formats', [
   'aside',
   'image',
   'video',
   'quote',
   'link',
   'gallery',
   'audio',
]);
```
#### Usage

Dans un template

```php
if ( has_post_format( 'quote' )) {
  echo 'Format Quote';
  // ...
}
```
---

