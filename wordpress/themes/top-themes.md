# Les thèmes à connaître

* [Twenty Seventeen](https://fr.wordpress.org/themes/twentyseventeen/)
* [Twenty Sixteen](https://fr.wordpress.org/themes/twentysixteen/)
* [Twenty Fifteen](https://fr.wordpress.org/themes/twentyfifteen/)
* [Twenty Fourteen](https://wordpress.org/themes/twentyfourteen/)
* [Twenty Thirteen](https://wordpress.org/themes/twentythirteen/)
* [Twenty Twelve](https://fr.wordpress.org/themes/twentytwelve/)

---

* [Bento](http://satoristudio.net/bento/)
* [Hueman](https://wordpress.org/themes/hueman/)

---

* [_s](http://underscores.me)
* [components _s](http://components.underscores.me)
* [Beans](http://www.getbeans.io)
* [Divi](https://www.elegantthemes.com/gallery/divi/)
* [Thesis](http://diythemes.com/)
* [Genesis](https://my.studiopress.com/themes/genesis/)

---

* [Sydney](https://fr.wordpress.org/themes/sydney/)
* [Zerif Lite](https://fr.wordpress.org/themes/zerif-lite/)
