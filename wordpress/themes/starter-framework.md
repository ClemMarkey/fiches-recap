# Starter themes et Theme frameworks

https://www.wpmayor.com/23-best-wordpress-starter-themes-frameworks/

## Starter themes

Les principaux fichiers, quelques hooks, des fonctionnalités de thème activées et une structure de dossiers et de fichiers

Ce ne sont pas de "vrai" thèmes mais ils fournissent une base solide pour démarrer la conception d'un thème ambitieux.

**En résumé** Ce sont d'excellentes bases pour les développeurs ou pour construire un futur framework.

* [_s ou underscores](http://underscores.me)
* [components](http://components.underscores.me)
* [Sage](https://roots.io/sage)
* [understrap](https://understrap.com)

## Theme Frameworks

### Explication

Basés sur des starter themes, ils proposent de multiples fonctionnalités et des options avancées. Il ne reste plus qu'à les configurer et à les styliser.

La customisation est grande mais il est très compliqué d'y ajouter nos propres fonctionnements.

**En résumé** Ce sont de formidables outils lorsque le temps pour coder est très limité ou pour des utilisateurs non développeur.

### Exemples

* [Gantry](http://gantry.org)
* [Thesis](http://diythemes.com/)
* [Divi](https://www.elegantthemes.com/gallery/divi/)
* [Ultimatum](https://ultimatumtheme.com/)
* [Beans](http://www.getbeans.io)
* [PageLines](https://www.pagelines.com/)
* [Genesis](https://my.studiopress.com/themes/genesis/)
* [Jump Start](https://wpjumpstart.com/)
* [Unyson](http://unyson.io) 

### 3 approches 

* Beans : Theme vierge avec api
* Helium + Gantry : Theme unique + plugin offrant toute les options de configuration
* Divi + Divi builder + ... : Theme hyper customisable + builder + plugins


