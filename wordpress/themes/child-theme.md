# Thèmes enfant

Le thème enfant permet d'étendre les fonctionnalités ou de modifier l'apparence d'un thème sans risquer de voir ses modifications écrasées lors du travail

[Child Themes](https://codex.wordpress.org/Child_Themes)

* style.css *
* functions.php

## style.css

`Template` permet de définir le thème parent

```css
/*
 Theme Name:   Twenty Sixteen Child
 Theme URI:    http://example.com/twenty-sixteen-child/
 Description:  Twenty Sixteen Child Theme
 Author:       John Doe
 Author URI:   http://example.com
 Template:     twentyfifteen
 Version:      1.0.0
 License:      GNU General Public License v2 or later
 License URI:  http://www.gnu.org/licenses/gpl-2.0.html
 Tags:         light, dark, two-columns, right-sidebar, responsive-layout, accessibility-ready
 Text Domain:  twenty-sixteen-child
*/
```


## functions.php

```php
<?php
function sixteen_child_styles(){
  // identifiant du style du thème parent
  $parent_theme_style = 'parent_theme_style';

  //Styles du theme parent
  wp_enqueue_style(
    $parent_theme_style, 
    get_parent_theme_file_uri('style.css')
  );
  //Styles du theme enfant et dependance avec le theme parent
  wp_enqueue_style(
    'child_theme_style', 
    get_theme_file_uri('style.css'), 
    [$parent_theme_style], 
    '0.0.1'
  );
}

add_action('wp_enqueue_scripts', 'sixteen_child_styles');
```

Lors de la déclaration de fonctions, il est important de vérifier si un conflit peut exister :

```php
if ( ! function_exists('ma_fonction_de_theme')) {
  function ma_fonction_de_theme(){
    //----
  }
}
```