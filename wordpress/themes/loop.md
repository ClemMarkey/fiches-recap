# The Loop

## Qu'est ce que The Loop ou La boucle

The Loop est le système pour afficher les posts dans le système de template, en d'autres termes **La pièce maîtresse** de WordPress.

[The Loop - Codex](https://codex.wordpress.org/The_Loop) - [La Boucle - Codex fr](https://codex.wordpress.org/fr:La_Boucle)

## Main Loop

La boucle principale de WordPress est initialisée au chargement d'une page. Elle se base sur la **Main Query**

## Structure type

```php
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
		...
    <?php endwhile; ?>
<?php else: ?>
	...
<?php endif; ?>
```

L'usage classique dans un template 

```php
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    Contenu avec plusieurs template tags (the_title(), the_content(), ...)
<?php endwhile; endif; ?>
```

[Loop in action](https://codex.wordpress.org/The_Loop_in_Action) - [Mise en pratique de la boucle](https://codex.wordpress.org/fr:La_Boucle_En_Action)

Au sein de la boucle, il est évidemment possible d'utiliser des template tags :

* [`next_post_link()`](https://developer.wordpress.org/reference/functions/next_post_link/)
* [`previous_post_link()`](https://developer.wordpress.org/reference/functions/previous_post_link/)
* [`the_category()`](https://developer.wordpress.org/reference/functions/the_category/)
* [`the_author()`](https://developer.wordpress.org/reference/functions/the_author/)
* [`the_content()`](https://developer.wordpress.org/reference/functions/the_content/)
* [`the_excerpt()`](https://developer.wordpress.org/reference/functions/the_excerpt/)
* [`the_ID()`](https://developer.wordpress.org/reference/functions/the_id/)
* [`the_meta()`](https://developer.wordpress.org/reference/functions/the_meta/)
* [`the_shortlink()`](https://developer.wordpress.org/reference/functions/the_shortlink/)
* [`the_tags()`](https://developer.wordpress.org/reference/functions/the_tags/)
* [`the_title()`](https://developer.wordpress.org/reference/functions/the_title/)
* [`the_time()`](https://developer.wordpress.org/reference/functions/the_time/)
* ...

## Plusieurs boucles

### Avec le même contenu

On peut envisager de répéter la même boucle plusieurs fois grâce à [`rewind_posts()`](https://developer.wordpress.org/reference/functions/rewind_posts/) par exemple pour séparer le traitement de l'affichage des titres et des contenus

```php
<?php
if ( have_posts() ) : 
	// 1ere boucle
    while ( have_posts() ) : the_post();
        the_title();
    endwhile;
 
    // On remet la boucle principale à 0
    rewind_posts();
 
	// 2eme boucle avec le même contenu
    while ( have_posts() ) : the_post();
        the_content();
    endwhile;
endif;
?>
```

### Avec des contenus différents

#### Choisir la bonne méthode : query_posts() vs WP_Query() vs get_posts()

Il est très fréquent de faire plusieurs boucles pour afficher une même page.

Par exemple, des boucles pour :

* La page courante
* Les 3 derniers articles avec leurs commentaires
* Les 2 premiers articles de 2 catégories importantes
* Un Custom post pour la partie équipe de la page

Plusieurs choix sont possible pour réaliser cette opération, le tout est de choisir la bonne méthode

![wp query](../medias/query_functions.png)

Conclusion pour faire simple : [**WP_Query**](https://codex.wordpress.org/Class_Reference/WP_Query)

#### WP_Query !

* Elle ne change pas la valeur de `$wp_query` (référence à la requête principale et à la page courante... Bcp de choses en découle)
* Séparation des appels extrêmement claire
* Reset de postdata et non de la main query
* Bien plus souple que `get_posts()`

[Un excellent article sur WP_Query](https://www.smashingmagazine.com/2013/01/using-wp_query-wordpress/) 

##### 2 boucles basées sur des requêtes différentes

```php
<?php

// Requête 1
$query1 = new WP_Query( $args );

if ( $query1->have_posts() ) {
	// The Loop
	while ( $query1->have_posts() ) {
		$query1->the_post();
		echo '<li>' . get_the_title() . '</li>';
	}
	
	// Pas de wp_reset_query() ! 
	// juste wp_reset_postdata() suffit
	wp_reset_postdata();
}

// Requête 2
$query2 = new WP_Query( $args2 );

if ( $query2->have_posts() ) {
	// The Loop n2
	while ( $query2->have_posts() ) {
		$query2->the_post();
		echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
	}

	// Restaure les post data orginales
	wp_reset_postdata();
}
?>
```

##### Arguments

Bon... il va falloir être courageux... petite Classe PHP de plus de 4000 lignes 

[Code Reference : `WP_Query()`](https://developer.wordpress.org/reference/classes/wp_query/)

###### Exemples

Une page par son slug

```php
$args = [
	'pagename' => 'contact'
];
$query = new WP_Query( $args );
```

Tous les posts sur la même page

```php
$args = [
	'posts_per_page' => -1
];
$query = new WP_Query( $args );
```

Un post aléatoire

```php
$args = [
    'orderby'        => 'rand',
    'posts_per_page' => '1', 
];
$query = new WP_Query( $args );
```

---

