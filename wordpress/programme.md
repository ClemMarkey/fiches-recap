# Programme

## S1

> Souvent sous-estimé par les développeurs, WordPress est un formidable outil de création. On le décortique en profondeur pour comprendre comment ce CMS peut nous simplifier la vie pour créer un site administrable lorsque l'on est développeur.

On s’attaque aussi cette semaine à NPM, Parcel et SASS des outils pour nous simplifier l’intégration cross-browser et mobile.

- Installation de WordPress avec Composer et WP-CLI
- NPM, Parcel, SASS
- Interactions JS
- Thème avancé
- Customizer

---

## S2

> WordPress offre de nombreuses fonctionnalités natives et moult extensions qui permettent d’étendre encore davantage tout ce que fait le CMS. On va découvrir plus en détail, les actions, les filtres, la création de plugins et tout ce qu'il nous faut pour personnaliser notre site, mais aussi le back-office.

L’occasion nous est donnée de nous expérimenter aux tests automatisés pour s'assurer de la qualité de notre code.

- Création de plugins
- Types de contenus et taxonomies personnalisés
- Rôles et capacités
- Tables custom
- Tests automatisés

---

## S3

> WordPress, c'est aussi une formidable API REST. On élargit encore le champ des possibles et on améliore l'UX en utilisant WordPress comme une application Headless avec un front en Vue.js. On découvre des librairies comme Axios.

- REST API
- Vue.js
- Logique de composants
- Axios

---

## S4

> Notre application n'est pas terminée, il nous manque une authentification. On en profite pour faire un point sur la sécurité de notre site  et son déploiement. On aborde aussi la création de blocs Gutenberg pour une édition des contenus aux petits oignons.

- Authentification avec JWT
- REST API avancée
- Blocs Gutenberg
- Sécurité et déploiement

---
