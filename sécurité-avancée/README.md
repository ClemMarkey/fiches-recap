# Sécurité des applications web

## Notions générales de sécurité

- Objectif de la sécurité informatique : empêcher les accès & comportements non-autorisés.
- Il faut définir les comportements autorisés, et partir d'une base où tous les accès sont fermés, pour n'autoriser ensuite au cas-par-cas que les comportements autorisés (principe de la whitelist). Ne surtout pas travailler dans le sens inverse (tous accès ouverts par défaut et bloquer au cas-par-cas — principe de la blacklist).
  - Exemple : bloquer l'accès à toutes les pages d'un site, sauf le formulaire de login, puis autoriser l'accès si login OK. Éventuellement laisser l'accès libre à certaines pages spécifiques et bien identifiées.
- Un système sécurisé à 100% ça n'existe pas ! Partir du principe qu'il y aura toujours des [vulnérabilités](https://fr.wikipedia.org/wiki/Vuln%C3%A9rabilit%C3%A9_(informatique))/failles — c'est ce qu'on constate depuis qu'on s'intéresse au sujet et ça n'a pas de raisons de changer.
- La sécurité, ce n'est pas que la lutte contre les intrusions, le vol ou la destruction de données. Un affichage malencontreux d'une donnée sensible peut-être désastreux (ex. affichage du mot de passe de ses clients « par hasard » dans une page mal conçue, ou rendue publique par erreur ; affichage du mot de passe, en clair, dans le mail de confirmation d'inscription ; etc.)
- La sécurité doit être une activité multi-factorielle / multi-niveaux. Il y a des failles aussi bien sur la couche physique (ex. l'accès aux machines) que sur la partie serveur (vulnérabilités, bugs…), la partie cliente (navigateurs mal protégés, cybercafé, WiFi…), la couche « sociale » (menaces, extorsion, manipulation…) etc.

## Déroulé-type d'une attaque

En résumé :

- l'attaquant se connecte sur un système auquel il n'est pas censé avoir accès
- l'attaquant réalise sur ce système des opérations délictueuses : vol de données, destruction de données, etc.

<details>
<summary>Plus en détails</summary>

- L'attaquant sélectionne une faille repérée sur le système ciblé.
- 1ère charge – connexion : mise en place d'un canal de communication, adapté pour l'attaque, avec le système ciblé (par exemple, requête HTTP pour le web). Peut nécessiter de la part de l'attaquant de réaliser au préalable une [élévation de privilège](https://fr.wikipedia.org/wiki/%C3%89l%C3%A9vation_des_privil%C3%A8ges) grâce à une autre faille repérée en amont.
- 2ème charge — intrusion (souvent, pour de l'observation, du vol de données…) : exploitation du canal de communication pour faire une mise en place de code malicieux : malware, shellcode, RPC de commandes whitelistées… Possibilité de mise en place d'un proxy (programme relais spécifique, anodin en lui-même mais support du code malicieux, qui va permettre de brouiller les pistes).
- 3ème charge – destruction (parfois, mais pas si souvent que ça car ne passe pas inaperçu !) : action destructive / prise de contrôle.

La majorité des attaques s'arrêtent à la seconde charge, et continuent sur ce mode-là tant que l'attaque n'est pas repérée et se justifie.
</details>

<details>
<summary>Exemple</summary>

Un attaquant repère une faille XSS sur un site web (sélection), injecte un script JS avec mise en place d'un canal de communication AJAX voire WebSocket (1ère charge) ; le script est exécuté par le client d'une victime et transmet des informations sensibles (2ème charge).
</details>

## Principes de base de sécurité

> **C'est une très bonne idée de commencer par là**, tout simplement parce que faire compliqué ne servira à rien si ces fondamentaux ne sont pas couverts :ok_hand:

### La théorie

- Retenir la règle simple pour estimer un risque : `niveau de risque = gravité x probabilité`.
- Diviser pour mieux régner : quand une application monolithique est compromise, elle l'est à 100% ! Une architecture distribuée en services est mécaniquement moins vulnérable, dans la mesure où un service compromis ne va affecter les autres, tout du moins sur le plan de la sécurité. Le facteur `gravité` dans le calcul du niveau de risque diminue donc. Toutefois, une architecture distribuée _peut_ parfois augmenter le facteur `probabilité` dans le calcul du niveau risque, car les multiples services sont autant de points de contact avec les attaquants.
- Pour minimiser la `probabilité` d'une attaque réussie, il faut mettre en place plusieurs couches de sécurité, complémentaires et/ou redondantes, notamment sur les parties où on a identifié que le facteur `gravité` est grand.
- L'anticipation du risque n'est pas suffisante. Il faut aussi se préparer à réagir à une attaque : que faire si un système est compromis ? Prévoir des scénarios d'entraînement.
- Certains risques ne peuvent pas être anticipés, on ne peut donc pas, par définition, s'y préparer. Le mieux à faire dans ce cas est d'avoir des routines de réaction génériques et ultra-rapides, du style _shutdown_ général. Mieux vaut une interruption de service, qu'une attaque qui fait de gros dégâts.

### La pratique

- Commencer par procéder à une analyse des risques, qui dépend de l'application et aussi du moment où elle est réalisée (prévoir d'en faire régulièrement pour réviser ses choix).
- Couvrir les risques fondamentaux (cf. référentiels de sécurité ci-après) le plus tôt possible dans la phase de conception-développement, faute de quoi ce ne sera pas fait au final. Les frameworks Web par exemple intègrent tous des mécanismes de sécurité pour les vulnérabilités classiques : configurer tout ça dès les premiers jours.
- Mettre en place les mesures pour les autres risques identifiés dans l'analyse de risques sans attendre. Le travail sur la sécurité fait partie du projet, au même titre que le dev de fonctionnalités, les tests, etc.
- Prévoir un « système d'intégrité » càd. un mécanisme de sauvegarde & restauration, automatique si possible. Par exemple, export SQL quotidien pour la base de données.
- Prévoir un « processus de réaction » pour savoir quoi faire si une attaque est repérée. C'est un plan d'action qui doit dire précisement qui fait quoi si une faille/attaque X ou Y survient. Ce plan d'action doit être connu et accessible de tous les intervenants, et régulièrement révisé. Prévoir des scénarios d'entraînement.
- Dans le plan d'action, distinguer la restauration de l'état du système, de la remise en service du système. Ce sont deux phases à traiter différement (ex. bloquer l'attaque et vérifier les dégâts, puis ré-importer les données, puis seulement réouvrir les accès).

## Grands axes de sécurité web

- Éducation de ses utilisateurs (protection contre les attaques par ingénierie sociale / manipulations).
- Données récupérées de sources externes (protection contre les injections).
- Hash de données (protection contre les attaques par dictionnaires).
- ACL (protection contre les attaques par élévation de privilèges).
- SSL/HTTPS (protection contre le vol de données sur le trafic réseau).
- Protection par domaine…
- Limitations de trafic (quota & _tarpit_)…

Ces différents axes sont **complémentaires** ! Par exemple, une gestion des headers CORS (protection par domaine) n'a aucun impact sur les failles de type CSRF (ingénierie sociale / éducation des utilisateurs), il faut donc travailler sur les deux en parallèle.

## Bonnes pratiques

- Audit(s) de sécurité (analyse des risques).
- Revue(s) de sécurité (temps dédié à la fin des sprints, pendant les _code-reviews_…).
- Tests spécifiques (intrusion, DDOS…).
- Scénarios d'entraînements.
- Monitoring & alertes automatiques, pour pouvoir réagir mais aussi faciliter l'analyse pendant et après l'attaque.
- Intégration, tout au long du processus de conception-développement, des intervenants en sécurité (conception croisée, tests) — assument une grande part de la charge mentale de la sécurité pendant le dev du projet (y compris après la mise en production s'il y a de la [TMA](https://fr.wikipedia.org/wiki/Tierce_maintenance_applicative)).
- Principe du [CERT](https://fr.wikipedia.org/wiki/Computer_emergency_response_team) (_Computer Emergency Response Team_) – assument une grande part de la charge mentale de la sécurité une fois le projet en prod

## Référentiels de sécurité

- [OWASP](https://www.owasp.org/)
  - [Top 10 des vulnérabilités](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project)
  - [Fiches-pratique](https://github.com/OWASP/CheatSheetSeries)
  - [Contrôles proactifs](https://www.owasp.org/index.php/OWASP_Proactive_Controls)
  - [Standards de vérification](https://www.owasp.org/index.php/Category:OWASP_Application_Security_Verification_Standard_Project)
- https://www.cert.ssi.gouv.fr/

## Ressources

### Sites de référence

* https://www.hacksplaining.com/lessons

### Lectures additionnelles

Beaucoup, beaucoup, beaucoup d'informations disponibles sur internet. En plus des ressources ci-dessus, indispensables et déjà très fournies, voici une sélection très succinte et sujette à modification :

- [_A quick introduction to web security_](https://medium.freecodecamp.org/a-quick-introduction-to-web-security-f90beaf4dd41)
- [Série _How Browsers Work_](https://medium.freecodecamp.org/web-application-security-understanding-the-browser-5305ed2f1dac)
- [Ingénierie sociale : _The Life of Death_](https://textslashplain.com/2017/01/14/the-line-of-death/)
