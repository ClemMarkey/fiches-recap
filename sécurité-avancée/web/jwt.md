# Le cas JWT

Un [_JSON Web Token_](https://jwt.io/) (JWT) est avant tout une spécification, qui définit un mécanisme de transport/échange d'information entre un client et un serveur. L'information est signée (donc fiable) et contient en elle-même tout ce qui est nécessaire à sa gestion (contenu, règles d'accès, durée de validité, etc.), de sorte que le serveur qui génère les JWT pour transmettre des informations à des clients ne stocke aucune information une fois les tokens émis. Le format de données est JSON.

Ces tokens sont désormais souvent utilisés comme [token de session](./auth.md) en lieu et place des traditionnels cookies de session, pour transporter un _session ID_ de manière chiffrée. Cette utilisation n'est pas optimale et peut même par certains aspects être considérée comme non-sécurisée.

> Attention : JWT est une forme particulière de token, token qui peut tout à fait être transporté & stocké dans un cookie. On cherche donc ici à savoir si JWT est un bon mécanisme d'implémentation de session, indépendamment du moyen de stockage de cette session.

## Vulnérabilités & faiblesses en tant que mécanisme de session

Quand il est utilisé pour modéliser une session utilisateur, un JWT est un token « autonome » permettant de réaliser une session « coté client » : le token embarque les informations de session, il _est_ la session. Il contient notamment des méta-informations à propos de la gestion de la session, notamment ses propres conditions de révocation (date d'expiration).

Il ne semble pas y avoir de différences entre un cookie de session classique et JWT :

> Attention à ne jamais mettre d’informations sensibles dans la charge utile [du JWT] ! En effet, le jeton est facilement décodable. Ce qui fait sa force, c’est qu’il est signé par le serveur avec une clé secrète que lui seul connait. Et le serveur dans un premier temps ne s’occupera que de valider votre jeton avant de le décoder et d’utiliser les informations utiles contenues dans celui-ci. — [source](https://www.attineos.com/blog/tutotrompe/tutotrompe-video-symfony)

Cette recommandation de sécurité (ne pas stocker d'information sensible dans le token) est la même que pour les cookies, dans lesquels on évite de stocker des infos sensibles tel qu'un mot de passe, et qu'on signe pour éviter une lecture trop aisée et surtout, une modification (_tampering_). Alors, où le problème ?

Il se situe du coté de la révocation de la session.

Dans le cas d'une session coté serveur, c'est le-dit serveur qui crée la session et la mémorise (dans un fichier, une base de données…). Coté client, la session n'existe pas en tant que telle : il y a uniquement un identifiant (stocké dans un cookie, en LocalStorage même si c'est moins sécurisé, etc.), lequel est envoyé à chaque requête et vérifié par le serveur, qui retrouve les informations de session stocké chez lui. Le client ne stocke donc aucune information de la session, et le serveur peut à tout moment décider qu'un identifiant de session n'est plus valide, invalidant _de facto_ la session elle-même.

Avec JWT, la session _est_ le token. Le travail du serveur se limite à l'émission des tokens, puis ceux-ci sont stockés coté client. Un JWT stocké par un client est envoyé par celui-ci à chaque requête vers le serveur, ce dernier se contentant de vérifier qu'il peut le lire (notion de chiffrement), de sorte à en extraire les informations de session. Il ne peut pas révoquer, du moins dans les implémentations populaires du mécanisme, un token. C'est le token lui-même qui s'auto-invalide (par date d'expiration). Par ailleurs, il est tentant d'abuser (souvent par manque de connaissance des bonnes pratiques de sécurité) de JWT pour stocker à l'intérieur des informations sensibles, c'est-à-dire les informations de la session elle-même (mot de passe…).

### Révocation coté serveur

Concrètement, si un site utilisant un mécanisme de session utilisateur basé sur JWT se fait hacker et que des comptes utilisateurs sont compromis, l'implémentation usuelle du mécanisme ne permet pas au serveur de révoquer les tokens de session des utilisateurs compromis. En fait, le problème se pose même dès qu'un utilisateur légitime souhaite se déconnecter ! Le propriétaire d'un JWT ne peut pas non plus révoquer son token : seule sa date d'expiration le désactivera, de façon automatique.

Pour contourner ce problème de révocation, il faut nécessairement mettre en place coté serveur un système (potentiellement complexe) de gestion du cycle de vie des tokens, système qui permettra par exemple, moyennant une interruption de service, de révoquer globalement tous les tokens compromis (c'est-à-dire en pratique tous les tokens, par principe de précaution), ou dans des versions plus compliquées, de révoquer token par token. Ce type de système n'est évidemment pas trivial à concevoir et sécuriser.

JWT favorise donc la rapidité de mise en œuvre sur la sécurité, et son cas d'usage recommandé est celui d'un token d'accès à usage unique ou à expiration rapide, utilisé pour échanger ponctuellement, de manière fiable, de l'information entre deux machines. L'utilisation de JWT pour une session expose à des risques accrus, ou requiert la mise en place d'un système de gestion/révocation des tokens coté serveur, là où l'utilisation de cookies chiffrés est simple, fiable et sécurisée par défaut (moyennant le respect de [quelques règles](./auth.md) en général supporté de base par les librairies d'authentification). Il est donc recommandé, pour les sessions d'authentification, de privilégier par défaut les cookies de session aux JWT, et de n'utiliser JWT que pour des communications _stateless_.

> If you're using JWTs […] you […] NEED to have centralized sessions in some way to manage revocation. JWTs are insecure by design: they cache authentication/authorization data, so in order to work around their speed-vs-security tradeoff you've got to manage a revocation list centrally no matter what: otherwise you end up in situations where revoked permissions/data are being allowed through -- a poor scenario. — [source](https://dev.to/jondubois/comment/373l)

## Avantages de JWT, sécurisation pour les sessions

JWT est, par nature, conçu pour gérer des échanges _stateless_. Quand on l'utilise pour gérer une session utilisateur, sa grande force est de permettre de stocker la session coté client — mais c'est aussi une faiblesse, voire une faille de sécurité dans certaines situations.

Toutefois, JWT peut servir pour d'autres choses que l'authentification (session utilisateur). Typiquement, il est souvent utilisé pour implémenter des stratégies d'autorisations (ex. accès à une API via JWT obtenu suite à une authentification OAuth).

On peut par ailleurs être amené à utiliser JWT pour implémenter une session utilisateur dans certains cas bien précis :

- pas de serveur, ou serveur/API _stateless_
- problème de transport des cookies dans des contextes _cross-domain_, malgré les headers CORS
- problèmes liés à l'utilisation de cookies de cache réseau (_cache busting_), de CDN (restriction par domaine)
- recherche de gains en perfomance en supprimant le stockage de sessions lourdes coté serveur (attention, JWT sécurisé requière quand même une gestion coté serveur du cycle de vie des tokens)
- utilisation de techniques de _tracking_, à des fins marketing par exemple
- utilisation du protocole OpenID Connect (OAuth 2.0) qui impose JWT, ou d'un fournisseur tiers de JWT (ex. serveur dédié à l'authentification, système de SSO, etc.)
- utilisation d'une architecture distribuée (scaling horizontal) sans serveur central d'authentification
- création d'un SSO, nécessité d'implémenter une solution gérant le _roaming_

**Dans tous les cas où JWT sert à implémenter une authentification (session utilisateur), il faut mettre en place coté serveur un sytème de gestion du cycle de vie des tokens, pour être en mesure d'en assurer la révocation, à l'initiative du serveur** (que ce soit pour des raisons de sécurité, ou d'évolution des données). À noter que cela nécessite l'utilisation d'un serveur, en contradiction avec le cas d'usage _stateless_. Dans ce cas, des alternatives à JWT existent (ex. [Paseto](https://github.com/paragonie/paseto)).

## Bonnes pratiques

Coté serveur :

- privilégier l'utilisation d'une [librairie](https://jwt.io/#libraries-io) implémentant une gestion du cycle de vie des tokens, plutôt qu'une implémentation-maison
- signer les tokens
- éventuellement, chiffrer les tokens avec une clé privé (et fournir la clé publique aux clients). Systématiquement chiffrer en cas de données sensibles, même en combinaison avec TLS/HTTPS
- en cas d'utilisation pour une session d'authentification, prévoir un système de révocation

Coté client :

- systématiquement vérifier la signature des tokens
- minimiser la lecture d'information, qui pourrait laisser des traces (historique de navigateur dans un lieu public, etc.) — le cas d'usage standard est que le client ne soit qu'un rebond pour le token, à destination du serveur

## Ressources

- [Site officiel](https://jwt.io/)
- [Jetons JWT et sécurité](https://www.vaadata.com/blog/fr/jetons-jwt-et-securite-principes-et-cas-dutilisation/)
- [_Server-side vs Client-side sessions_](https://stackoverflow.com/questions/43452896/authentication-jwt-usage-vs-session)
- [_JSON Web Tokens vs. Session Cookies_](https://ponyfoo.com/articles/json-web-tokens-vs-session-cookies)
- [_Why JWTs Suck as Session Tokens_](https://developer.okta.com/blog/2017/08/17/why-jwts-suck-as-session-tokens)
- [_Stop using JWT for sessions_](http://cryto.net/~joepie91/blog/2016/06/13/stop-using-jwt-for-sessions/)
- [_Things to use instead of JWT_](https://kevin.burke.dev/kevin/things-to-use-instead-of-jwt/)
- [_How to log out when using JWT?_](https://medium.com/devgorilla/how-to-log-out-when-using-jwt-a8c7823e8a6)
- [_How can I revoke a JWT?_](https://stackoverflow.com/questions/31919067/how-can-i-revoke-a-jwt-token)
- [Auth0](https://auth0.com/), une implémentation sécurisée de session par JWT (spécification OpenID)
- [_Revoking JWTs_](https://fusionauth.io/blog/2019/01/31/revoking-jwts)
