# Cross-Site Request Forgery

* [Fiche CERT-FR](https://www.cert.ssi.gouv.fr/information/CERTA-2008-INF-003/)
* [Fiche OWASP](https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.md)

## Principe de base

Dans de nombreux cas, les clients web (ex. navigateurs) initient des requêtes vers des serveurs en incluant automatiquement les informations de sessions (typiquement un ID de session stocké dans un cookie). Pour un attaquant, il « suffit » alors de forcer la main à un utilisateur pour qu'il réalise, sans le savoir, une action sur un serveur, en étant connecté.

Par exemple, à force d'arguments fallacieux, un utilisateur peut être incité à cliquer sur un lien reçu dans un email, action anodine qui déclencherait en fait une requête pour modifier son mot de passe (par une valeur connue de l'attaquant) sur un site sur lequel il est actuellement connecté. Quand bien même l'attaquant à l'origine de l'email ne voit pas le résulat de la requête (car ce n'est pas lui qui la déclenche, mais sa victime), quand bien même l'attaquant n'est même pas sûr que son attaque va réussir (c'est l'utilisateur qui décide de cliquer ou pas), cet attaquant mise sur la probabilité que dans une masse de tentatives, une personne se laissera berner, et il lui suffit de régulièrement vérifier si une requête malveillante a porté ses fruits, pour ensuite exploiter la brêche ainsi créée (connexion à un compte dont le mot de passe a été modifié).

> À noter : même si de très nombreuses requêtes à effet de bord sont du type POST, et qu'il est _a priori_ difficile pour un attaquant de forcer un client à déclencher autre chose qu'une requête GET, ce n'est en fait pas _si difficile_ que ça. Il suffit par exemple d'automatiser l'envoi d'un formulaire lors de la visite d'une page malicieuse (phishing).

Les failles CSRF se distinguent des autres failles web en ce qu'elles font intervenir de façon prépondérante l'[ingienérie sociale](https://fr.wikipedia.org/wiki/Ing%C3%A9nierie_sociale_(s%C3%A9curit%C3%A9_de_l%27information)) : des techniques de manipulations à des fins d'escroqueries. Ces failles sont particulièrement efficaces car elles exploitent avant tout la vulnérabilité humaine et non technique ! Il est par conséquent relativement difficile d'en prémunir les applications web en tant que tel : si un utilisateur se laisse berner, c'est le début de la fin…

Enfin, il faut retenir que l'attaque repose avant tout sur la notion que l'identité d'une victime est en fait celle de son navigateur. L'attaquant « prend le contrôle » du navigateur de sa victime (en l'incitant à réaliser une action), et de ce fait se fait passer pour elle.

## Exemples

- Cf. fiches CERT-FR & OWASP.
- https://security.stackexchange.com/a/72569

## Remédiations

### Ingénierie sociale

* Ne pas cliquer sur des liens randoms… (facile à dire).
* Systématiquement se déconnecter des sites et services critiques (ex. banque, réseaux sociaux), voire de tous les sites, une fois visités (ie. naviguer le plus possible en mode anonyme).

### Conception-développement

* Pour les actions standards, mettre en place un token de synchronisation CSRF coté serveur, en particulier sur les formulaires en POST et les requêtes AJAX, ce qui permet de casser l'authentification automatique sur la base d'un simple cookie.
  * Éventuellement automatiser l'ajout du token au niveau du serveur web pour éviter les oublis. Vérifier le support et la configuration en cas d'utilisation d'un framework web proposant une protection CSRF.
  * Garder en tête qu'une faille [XSS](./xss.md) peut annuler cette sécurité, le script injecté pouvant lire le token CSRF et l'intégrer à ses requêtes malveillantes.
* Pour les actions critiques, doubler le token CSRF avec une ré-authentification ou une double-authentification (Captcha ou mieux, SMS/action sur terminal physiquement détenu par l'utilisateur, par exemple), ce qui permet de casser la notion d'identité liée au navigateur en forçant une action explicite et unique de la part de l'utilisateur (ne peut être généralisé à toutes les actions car beaucoup trop intrusif / anti-UX).
