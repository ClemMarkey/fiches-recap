# Authentification sécurisée

Comment s'assurer qu'un mécanisme d'authentification est sécurisé sur le web ?

- [OWASP Session Management Cheatsheet](https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Session_Management_Cheat_Sheet.md)

## La session

> Note : cet article traite des sessions dans le contexte client/serveur avec un serveur _stateful_. Le cas des applications mobilisant des API _stateless_ est différent.

### Principe

Sur le web moderne, la notion de session est essentielle. Le web fonctionne en effet sur la base d'un protocole _stateless_, HTTP : il n'y a, par défaut, aucune notion d'utilisateur identifié. C'est seulement à travers un accord protocolaire entre client et serveur qu'une mémoire et donc la notion d'identité peuvent être créées.

Une session « représente » un utilisateur (souvent un navigateur web). À une session donnée correspond une date et un évènement de création, et une date et un évènement d'expiration. Entre ces deux moments privilégiés (création et destruction de la session), l'utilisateur propriétaire de la session et le service vérifiant la session utilisateur peuvent se reconnaître mutuellement au travers des cycles de requêtes/réponses successifs, grâce à l'échange d'un _token de session_. La session peut éventuellement « contenir » des données tangibles.

Le mécanisme de session rend possible le mécanisme d'authentification (paradigme [orienté-connexion](https://fr.wikipedia.org/wiki/Orient%C3%A9_connexion)). Une session peut en effet être utilisée dans le but d'authentifier un utilisateur d'un service (en fait son navigateur), de manière persistante. Dans ce cas, des données seront associées à la session et modéliseront l'utilisateur : a minima, un token agissant comme le _session ID_ sera généré et stocké coté serveur, transmis au client, lequel le renverra systématiquement au serveur à chaque requête, ce dernier en déduisant l'identité de l'utilisateur (en fait de son navigateur).

### Implémentations

#### Cycle de vie de la session

Le mécanisme _exact_ d'implémentation de la session peut grandement varier, mais son principe reste toujours le même. Il se base sur un échange constant entre client et serveur d'une information stable dans le temps, et contrôlée par eux et eux seuls :

- un client anonyme réalise une action d'authentification auprès du serveur
- le serveur vérifie la légitimité des identifiants proposés
- en cas de succès, le serveur répond au client avec un token (quelle que soit sa forme, il s'agit d'un élément virtuel associé de façon unique au client qui vient de s'authentifier)
- le client mémorise/stocke ce token et le retransmettra à chaque future requête au serveur, pour réaliser les requêtes « en son nom »
- le serveur vérifiera la légitimité du token reçu et en déduira l'identité du client lui ayant fait une requête
- le serveur peut à tout moment révoquer le token, et le client détruit sa copie locale, redevenant anonyme au yeux du serveur pour les prochaines requêtes

#### Formes du token

Exemples d'implémentations concrètes coté client :

- token sous la forme d'une chaîne de caractère utilisée comme valeur d'un attribut d'un champ de formulaire, renvoyée au serveur en paramètre d'une requête POST
- token sous la forme d'une chaîne de caractère utilisée comme valeur d'un attribut de balises `meta`, renvoyée au serveur en _query parameter_ (URL)
- token sous la forme d'un cookie de session, renvoyé au serveur par le header `Cookies`
- token sous la forme d'un enregistrement LocalStorage, renvoyé au serveur par un header _custom_ du type `X-API-Token`

Exemples d'implémentations concrètes coté serveur :

- token stocké en mémoire (volatile)
- token stocké dans un fichier (statique)
- token stocké en base de données (statique)
- token stocké en cache (semi-statique)
- token transmis au client comme _body_ d'une réponse
- token transmis au client via `Set-Cookie`
- token transmis au client comme header _custom_

Le stockage coté serveur doit évidemment être lui-même sécurisé.

## Sécurité de la session

Dans le cas d'une session utilisée pour gérer l'authentification des clients, un vol de token est critique car l'attaquant peut alors se faire passer pour une personne tierce, ce qui lui donne en général accès à des informations sensibles, voire à des privilèges sur le serveur (donc potentiellement à toutes les identités stockées).

### Principes

Il est garantie qu'une session est sécurisée si elle respecte quelques principes simples incontournables, qui sont pour partie les mêmes que pour toute [signature numérique](https://fr.wikipedia.org/wiki/Signature_num%C3%A9rique). Un token de session sécurisé est :

- authentique : le token échangé a une validité restreinte à un couple client/serveur spécifique (restriction par domaine en général, mais pas toujours suffisant), de sorte que l'identité du propriétaire de la session doit pouvoir être retrouvée de manière certaine et fiable ;
- infalsifiable : le token échangé, d'une [longueur suffisante](https://www.hacksplaining.com/exercises/weak-session), est généré aléatoirement et chiffré coté serveur (avec la condition corrolaire que le serveur soit lui-même sécurisé), de sorte qu'il ne puisse être falsifié ou deviné ;
- non-final : le token échangé en clair n'est pas le _session ID_, mais une version chiffrée, interprétable uniquement par le code serveur qui seul saura retrouver les éventuelles informations de session stockées coté serveur ;
- révocable : le stockage du token coté client se fait sous le contrôle d'une date d'expiration, laquelle est définie par le serveur et doit être respectée par le client.

En général, le caractère authentique et infalsifiable d'un token de session est tenu pour acquis une fois le serveur sécurisé. Le caractère non-final est facile à implémenter, toujours coté serveur. Le caractère révocable se base sur le respect des règles par le client, ce qui ne peut en général être vérifié ou forcé par le serveur.

> Compte-tenu de ces principes :
>
> - un simple cookie « en clair » n'est pas une implémentation sécurisée d'une session, car il est possible de le modifier librement coté client (non-infalsifiable, ce qui implique le contournement de toutes les autres caractéristiques) ;
> - un token JWT n'est pas une implémentation sécurisée d'une session, car il transporte sa propre information de révocation (qu'un client peut d'ailleurs ne pas respecter, rendant le token « invalidement ») sans possibilité de révocation de la part du serveur qui l'a initialement émis (cf. [fiche dédiée](./jwt.md)).

Par ailleurs, il est fortement recommandé de transmettre le token de session sur une connexion sécurisé par TLS (transport en HTTPS), par redondance avec le principe de non-finalité.

### En pratique

On se place dans le cas particulier d'une session implémentée avec un token échangé entre client et serveur sous la forme d'un cookie de session, c'est-à-dire un cookie stockant de manière chiffrée un ID arbitraire identifiant le client propriétaire de ce cookie.

Pour pleinement sécuriser la session, il faut :

- restreindre la validité du cookie à l'application web, en utilisant les options `Domain` et `Path` du cookie. Attention, en cas de domaine partagé (ex. hébergement mutualisé), la restriction ne doit pas se faire par domaine uniquement, mais bien par process applicatif, en ajustant `Path` et/ou `Domain` selon la configuration de mutualisation (par sous-dossiers ou par sous-domaines) ;
- restreindre la validité du cookie au protocole TLS (anciennement SSL), si utilisé, avec l'option `Secure` ;
- restreindre, si possible, l'accès au cookie à HTTP seulement (pas d'accès en JavaScript) pour contribuer à contrecarrer les failles XSS, avec l'option `HttpOnly`.

Exemple en PHP :

``` php
setcookie(
  '_session_',              // nom du cookie de session (Name)
  'a42xfc54...y8bb',        // valeur - token de session (Value)
  0,                        // règle d'expiration (Expire)
  '/',                      // chemin (Path)
  'www.example.com',        // domaine (Domain)
  isset($_SERVER["HTTPS"]), // TLS-only (Secure)
  true                      // pas d'accès en JS (HttpOnly)
)
```

> Attention, toutes les options du header `Set-Cookie` ne sont pas supportées par la fonction PHP `setcookie`, notamment certaines très intéressantes pour la sécurité telle que `SameSite`). Une implémentation custom peut être préférable.

Dans cet exemple, le contenu de la session n'est pas accessible coté client, qui ne stocke qu'une information limitée et chiffrée (le token de session). Un attaquant désirant voler la session en est empêché car :

- il peut tenter de deviner un token valide (_brute force_), mais ceux-ci sont aléatoires et chiffrés coté serveur par une clé privé qu'il ne possède pas, ce qui rend cette approche virtuellement impossible ;
- il peut tenter de forger un token (_CSRF_), toutefois le token n'est pas la session elle-même, mais une version chiffrée, chiffrement qu'il ne peut reproduire en l'absence de la clé privé ;
- il peut tenter de voler le token (_Man-In-The-Middle_), mais l'échange d'information est lui-même chiffré par TLS.

> Pour renforcer la sécurité, on peut limiter la validité de la session à l'IP active et/ou au _UserAgent_ actif lors de sa création. En pratique, ce n'est pas recommandé, car de nombreux utilisateurs ont une connexion en IP dynamique et/ou utilisent plusieurs clients différents, ce qui les obligerait à se reconnecter régulièrement « sans raison ». Par ailleurs, un attaquant peut se placer sur le même réseau que sa victime si elle utilise un routeur DHCP (très courant), la restriction par IP est donc souvent une configuration générant un faux sentiment de sécurité.

Au niveau du mode d'échange, il faut absolument éviter la transmission par paramètres GET ou payload POST (risque élevé de phishing par [_session fixation_](https://www.hacksplaining.com/prevention/session-fixation) ; augmente la surface d'attaque car le token peut se retrouver dans des logs & historiques publiquement accessibles). Privilégier le stockage en cookie chiffré (ou équivalent, toujours chiffré : localStorage, sessionStorage, IndexedDB…).

En terme de bonnes pratiques :

- quand l'utilisateur se connecte, invalider tout token de session précédement actif pour cet utilisateur, et regénérer un token de session ;
- pour les actions critiques, exiger de l'utilisateur connecté qu'il redonne ses identifiants (double authentification) ;
- quand l'utilisateur se déconnecte, immédiatement invalider le token de session courant, d'une manière prédictible et fiable ;
- examiner le Referrer et éventuellement exiger une double authentification s'il a changé depuis la dernière visite ;
- [le cas JWT](./jwt.md)

## Utiliser une libraire d'authentification

Pour éviter d'implémenter imparfaitement une session sécurisée, il est recommandée d'utiliser une _auth library_ toute faite, dont on aura vérifié la légitimité. La plupart des frameworks populaires intègrent une telle librairie directement.

## Ressources

- Options importantes de `Set-Cookie` : [HttpOnly & Secure](https://blog.dareboost.com/fr/2016/12/securisez-cookies-instructions-secure-httponly/) - [SameSite](https://blog.dareboost.com/fr/2017/06/securisation-cookies-attribut-samesite/)