# Cross-Site Scripting

* [Wikipédia](https://fr.wikipedia.org/wiki/Cross-site_scripting)
* [Excess XSS](https://excess-xss.com/)
* [Google Application Security](https://www.google.com/intl/bg_dm/about/appsecurity/learning/xss/)
* [Fiche OWASP](https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.md)

## Principe de base

Un attaquant injecte du code (typiquement JavaScript) dans un serveur web, via un formulaire, un paramètre d'URL, etc. et le serveur web intègre ensuite ce code dans une de ses prochaines réponses, ce qui en déclenche l'exécution dans un contexte différent, permettant à l'attaquant de récupérer des informations, une identité (session), ou encore un accès au serveur.

Dit autrement, un script est injecté dans un site web depuis un client malveillant, puis un autre client en déclenche ultérieurement l'exécution, sans le vouloir, parce que le serveur web réutilise la donnée utilisateur sans contrôle. L'attaquant exploite dans ce cas une faille du serveur web, mais ne cible pas directement un client particulier. Il gagne par contre l'accès complet à l'expérience utilisateur de sa victime !

``` php
// Serveur PHP

echo "Contenu : $content"
```

Si un client est capable d'envoyer `<script>...</script>` comme contenu au serveur et que ce dernier le réutilise tel quel pour construire une réponse, alors il y a une faille XSS.

* Si la réutilisation du code injecté est immédiate, on parle de _Reflected XSS_ (XSS en miroir). Concerne par exemple les moteurs de recherche. Typiquement, l'attaquant doit préparer un lien à usage unique avec une injection par l'URL.
* Si la réutilisation du code injecté est retardée, on parle de _Stored XSS_ (XSS stocké). Peut concerner tout type d'application. Typiquement, l'attaquant prépare une charge en injectant lui-même un malware qui sera exécuté par une victime lors de son utilisation usuelle de l'application.
* Certaines exploitations XSS se situent coté client, on parle de _DOM XSS_.

> Attention au nom « Cross-Site Scripting » : la vulnérabilité n'est pas toujours lié à plusieurs sites (_cross-site_), et ne fait pas toujours intervenir un script dynamique (_scripting_) en tant que tel ! Il peut s'agir d'une simple injection de code statique sur un seul site.

## Exploitations

Quelques exemples parmi une vaste gamme d'exploits possibles…

### Vol de cookie

En exploitant XSS, on peut accéder à `document.cookies` qui renvoie les cookies pour le domaine vulnérable. Le script peut ensuite envoyer ces informations à l'attaquant.

### Vol d'information sensible

En exploitant XSS, on peut écouter la frappe clavier et récupérer des informations sensibles (identifiants, messages…).

### Phishing

En exploitant XSS, on peut modifier une page (typiquement y ajouter un formulaire) pour inciter le client à entrer des informations sensibles.

## Exemples

### Exemple 1 : récupération de cookies

* Un site de réseau social affiche un fil de commentaires. Chaque utilisateur du site peut poster des commentaires.
* Un attaquant poste un commentaire contenant un `<script>...<script>`, et notamment un code malicieux référençant `document.cookies` pour les envoyer vers son propre serveur evilhacker.com au moyen d'une requête AJAX.
* Le serveur web n'effectue aucun contrôle sur les commentaires.
* Un client (visiteur du site web) affiche le fil de commentaires. Ses cookies sont envoyés à l'attaquant.

### Exemple 2 : transferts bancaires massifs

* Le site d'une banque incorpore sans contrôle des informations fournies par le client sur sa page de transfert de fonds.
* Un attaquant exploite cette faille XSS pour inclure dans la page un script générant un `<form method="POST">` (non-visible) dont le paramètre `action` pointe vers son site `evilhacker.com/transfer`. Le script est également configuré pour déclencher la validation du formulaire malveillant en même temps que la validation du formulaire légitime, avec un transfert limité à 1 centime, dans le but de passer (le plus) inaperçu (possible).
* Les millions de clients de la banque réalise des centaines de milliers de transferts quotidiennement, l'attaquant récupère une grosse somme d'argent même si son script est détecté dans la journée voire dans les heures de sa mise en place.
* _À noter que dans cet exemple, la mécanique de sécurité CORS est totalement contournée, puisque le script malveillant appartient au domaine légitime (le site de la banque)._

## Remédiations

* Coté serveur, toujours valider les données en provenance des clients.
  * A minima, valider au moment de la réutilisation dans une réponse (template / service de rendu).
  * Mieux, réaliser une double validation en contrôlant également au moment de la réception des données (contrôleur / service de validation).
* Coté client, toujours valider les données provenant de l'utilisateur, mais aussi du serveur (ex. réponses aux requêtes AJAX pour une page SPA). Même principe de double-validation que coté serveur.
* Valider veut dire :
  * encoder (désactiver le code en le transformant, tout ou partie, en données brutes _via_ les [entités HTML](https://fr.wikipedia.org/wiki/Liste_des_entit%C3%A9s_caract%C3%A8re_de_XML_et_HTML)) — ex. `&` devient `&amp;` ou encore `<` devient `&lt;` ;
  * _et éventuellement_ filtrer (toujours procéder par whitelist) – ex. n'autoriser que certaines balises HTML dans les messages d'un forum.
* Si possible, utiliser CSP.
* Si possible, utiliser une sécurisation des formulaires et méthodes d'écriture de données par token de synchronisation (aléatoire & à usage unique), dont l'accès est protégé par CORS.
* _À noter, la présence d'une faille XSS annule tout les gains d'une gestion des failles [CSRF](./csrf.md)._