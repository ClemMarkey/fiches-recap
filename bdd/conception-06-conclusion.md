# Conclusion

Nous disposons d'une méthode nous permettant de décrire puis de créer **un système d'information** cohérent (une base de donnée) à partir d'un énoncé de départ.

Cette méthode de conception nous permet d'utiliser **une norme partagée par les différents acteurs du projet**, de **se poser les bonnes questions** sur les données et sur le fonctionnement du projet en lui-même. Sur le long terme c'est **un gain de temps et d'efficacité**.

Résumons-en les étapes :

1. Expression du besoin.
2. Ecriture d'un dictionnaire de données
   - Suppression des redondances.
   - Homogénéisation des données.
   - Typage des données.
   - Identification des entités.
   - Identification des relations.
3. Création d'un Modèle Conceptuel de Données
    - Dessin des entités.
    - Répartition des attributs, dont la clé primaire.
    - Création des relations.
    - Définition des cardinalités.
    - Ajout d'attributs dans les relations le cas échéant.   
4. Déduction du modèle Logique de Données
    - Règles de conversion entité vers table, relation vers table et apparition de la notion de clé étrangère.
5. Implémentation du Modèle Physique de Données
    - Création des tables dans la base en langage SQL
    - Il est déduit directement du MLD.

# Ressources

Les ressources suivantes ont servi de base à l'élaboration de cette fiche récap'. N'hésitez pas à les parcourir afin d'appronfondir le sujet.

Du plus simple au plus complet/complexe :

- [Tutos et cours sur Merise](http://www.sousdoues.com/formations/merise/)
- [Petit cours de Modélisation - Introduction à Merise avec exercices et corrigés](http://pise.info/modelisation/index.htm)
- [Autre cours sur le schéma Entité/Association, synthétique](http://www.crescenzo.nom.fr/CMBasesDeDonnees/002-ModeleEA.html)
- [Cours complet (bien fait, Laurent Audibert est une référence du domaine)](http://laurent-audibert.developpez.com/Cours-BD/?page=conception-des-bases-de-donnees-modele-a)
- [En savoir plus sur les formes normales (Laurent Audibert)](http://laurent-audibert.developpez.com/Cours-BD/?page=bases-de-donnees-relationnelles)

# Logiciels

Il est assez difficile de trouver un logiciel (libre) qui propose la création de MCD au sens du modèle Entité/Association façon Merise. En voici une liste à titre indicatif.

- [JMerise](http://www.jfreesoft.com/JMerise/) => très (trop ?) complet.
- [Draw.io, avec Template Software => Entity Relation](https://www.draw.io/)
- [AnalyseSI](http://www.analysesi.com/) => Plutôt cool, mais ne gère pas la réflexivité.
    - *Les schémas de cette fiche ont été fait avec AnalyseSI*.
- [MoCoDo](http://mocodo.wingi.net/) => Original, en ligne, syntaxe maison.
- [Open ModelSphere](http://www.modelsphere.com/org/) => buggué sous Linux, à voir version Windows
- [MySQL Workbench (MSB)](https://www.mysql.com/fr/products/workbench/) => Pour les MCD en UML

---

> [Retour au sommaire](./README.md)