# Adminer

- [Adminer](#adminer)
  - [Créer une base de données](#cr%c3%a9er-une-base-de-donn%c3%a9es)
  - [Créer un user](#cr%c3%a9er-un-user)
  - [Créer une table et ses champs](#cr%c3%a9er-une-table-et-ses-champs)

## Créer une base de données

---

On privilégiera [la création d'une base de données et d'un utilisateur du même nom](#créer-un-user). Cet utilisateur aura toutes les autorisations sur cette base de données.  
Ainsi, l'utilisateur n'aura aucun accès aux autres bases de données => si notre site se fait pirater, le pirate n'accède qu'à notre base de données, pas aux autres.  
Si malgré tout, vous souhaitez uniquement créer une nouvelle base de données, voici la procédure.

---

On peut commencer la création de la base de données :arrow_down:

1. Cliquer sur le lien affichant le formulaire d'ajout

![](./img/adminer-create-database1.png)

2. Saisir le nom de la base de données et sélectionner l'interclassement (_Collation_) `utf8_general_ci` <small>(ou encore mieux : `utf8mb4_general_ci`)</small>

![](./img/adminer-create-database2.png)

3. Valider :tada:

## Créer un user

1. Revenir sur la page d'accueil d'Adminer

![](./img/adminer-create-user1.png)

2. Aller à la section _Privileges_ pour visualiser les comptes utilisateurs

![](./img/adminer-create-user2.png)

3. Puis, on clique sur _Create user_ <small>(ça parait évident, mais on précise quand même...)</small>

![](./img/adminer-create-user3.png)

4. Et voici le formulaire

![](./img/adminer-create-user4.png)

5. Renseigner le nom de l'utilisateur

![](./img/adminer-create-user5.png)

6. Renseigner le mot de passe choisi (**NE PAS cocher** la checkbox _Hashed_)

![](./img/adminer-create-user6.png)

---

<details><summary>Si, par souci de sécurité, on souhaite créer un utilisateur associé uniquement à la base de données et portant le même nom que lui, voici l'étape supplémentaire à effectuer :</summary>

Alors cocher la checkbox _Grant all privileges to a database with same name_

![](./img/adminer-create-user7.png)

L'input en dessous (dans l'entête verte) est alors automatiquement modifié, et les 2 premiers checkbox en dessous sont automatiquement cochés (voir screenshot).

C'est tout, il n'y a rien d'autre à faire.

</details>

---

7. Valider :tada:

![](./img/adminer-create-user8.png)

## Créer une table et ses champs

1. Sélectionner la base de données dans laquelle on veut créer une table

![](./img/adminer-create-table1.png)

2. Cliquer sur le lien permettant d'afficher le formulaire d'ajout d'une table

![](./img/adminer-create-table2.png)

3. Le formulaire d'ajout s'affiche

![](./img/adminer-create-table3.png)

4. Renseigner le nom de la table, l'engine `InnoDB` et la collation `utf8_general_ci` <small>(ou encore mieux : `utf8mb4_general_ci`)</small>

![](./img/adminer-create-table4.png)

5. Renseigner le premier champ (souvent la clé primaire auto-incrémentée)
   - le nom, `id` par exemple
   - le type
   - la longueur maximum du type (obligatoire pour les `VARCHAR`)
   - l'option (par exemple `UNSIGNED`)
   - ne pas cocher "NULL" pour la clé primaire
   - cocher "AI" (Auto Incrémenté)

![](./img/adminer-create-table4.png)

6. Renseigner les autres champs
   - le nom
   - le type
   - la longueur maximum du type (obligatoire pour les `VARCHAR`)
   - l'option (par exemple `UNSIGNED` pour les `INT`)
   - cocher "NULL" **si** on souhaite autoriser la "valeur" `NULL`
   - ne pas cocher "AI" (1 seul champ peut-être auto-incrémenté)
   - cocher "Default Values" en bas afin d'avoir la colonne pour donner des valeurs par défaut

![](./img/adminer-create-table5.png)

7. Valider :tada: