# MPD

Le modèle physique est l'implémentation directe du MLD. Après quelques ajustements de nommage et la définition des types de champs issue du dictionnaire de données, on obtient ce schéma dans MySQL :

![](./img/MPD-biblio.png)

Vous pouvez [télécharger le fichier .sql](./img/biblio.sql)

Si vous avez utilisé [MySQL Workbench (MSB)](https://www.mysql.com/fr/products/workbench/)** pour créer votre modèle modèle logique, le fichier .sql peut être généré directement depuis le schéma !

---

> [Retour au sommaire](./README.md)