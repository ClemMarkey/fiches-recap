# PHPMyAdmin

- [PHPMyAdmin](#phpmyadmin)
  - [Créer une base de données](#cr%C3%A9er-une-base-de-donn%C3%A9es)
  - [Créer un user](#cr%C3%A9er-un-user)
  - [Créer une table et ses champs](#cr%C3%A9er-une-table-et-ses-champs)

## Créer une base de données

On priviligiera [la création de la base de données au même moment que l'utilisateur](#créer-un-user) ayant tous les accès sur cette base de données uniquement.  
Si malgré tout, vous souhaitez uniquement créer une nouvelle base de données, voici la procédure.

1. Cliquer sur le lien affichant de le formulaire d'ajout

![](./img/pma-create-database1.png)

2. Saisir le nom de la base de données et sélectionner l'interclassement (_Collation_) `utf8_general_ci`

![](./img/pma-create-database2.png)

3. Valider :tada:

## Créer un user

1. Cliquer sur le lien affichant la page de gestion des utilisateurs

![](./img/pma-create-user1.png)

2. En dessous de la liste des utilisateurs, cliquer sur le lien d'ajout

![](./img/pma-create-user2.png)

3. Si je souhaite créer une base de données et l'utilisateur qui aura tous les accès sur cette base de données

   1. alors je renseigne le nom de la base de données comme nom d'utilisateur
   2. je détermine que les accès à la base de données se feront depuis la même machine
   3. je définie un mot de passe, pour se simplifier le développement en local, on choisira le nom de la base de données comme mot de passe
   4. je coche **Créer une base portant son nom et donner à cet utilisateur tous les privilèges sur cette base**
   5. je ne coche **pas** les "Privilèges globaux"

![](./img/pma-create-user3a.png)

4. Si je ne souhaite pas créer la base de données du même nom

    1. je renseigne le nom pour l'utilisateur
    2. je détermine que les accès à la base de données se feront depuis la même machine
    3. je clique sur le bouton "Générer un mot de passe" afin d'avoir un mot de passe fortement sécurisé
    4. je ne coche **pas** "Créer une base portant son nom et donner à cet utilisateur tous les privilèges sur cette base"
    5. pour les "Privilèges globaux" (s'appliquant à toutes les bases de données), il est conseillé de ne pas les fournir. Au pire des cas, limiter les privilèges globaux aux données.

![](./img/pma-create-user3b.png)

5. Valider :tada:

## Créer une table et ses champs

1. Sélectionner la base de données dans laquelle créer une table

![](./img/pma-create-table1.png)

2. Cliquer sur le lien permettant d'afficher le formulaire d'ajout d'une table

![](./img/pma-create-table2.png)

3. Le formulaire d'ajout s'affiche

![](./img/pma-create-table3.png)

4. Renseigner le nom de la table (ne pas cliquer sur _Exécuter_)

![](./img/pma-create-table4.png)

5. Renseigner le premier champ, souvent le champ **ID** de la table, la _Primary Key_ (clé primaire). Souvent, la valeur de ce champ est calculée automatiquement par MySQL => **A_I** = Auto Increment

![](./img/pma-create-table5a.png)

6. Au moment de sélectionner l'index **PRIMARY**, un popin apparaît, cliquer sur _Exécuter_

![](./img/pma-create-table5b.png)

7. Champs de la table, 1 à 1

    1. **Nom** : le nom du champ dans la table
    2. **Type** : le type du champ (INT, VARCHAR, TEXT, DATETIME, etc.)
    3. **Taille/Valeurs** : optionnel, mais, par exemple, pour le type VARCHAR, on doit obligatoirement déterminer le longueur du type => dépend du type choisi
    4. Interclassement (_Collation_) : ne pas renseigner, laisser l'interclassement par défaut de la table
    5. **Atributs** : des options selon le type du champ. Par exemple, il peut être utile d'avoir des INT non signés => _UNSIGNED_. Les autres attributs sont rarement utiles
    6. **Null** : autoriser ou non les valeurs _null_ dans ce champ
    7. **Index** : _PRIMARY_ est _primordial_, _UNIQUE_ et _INDEX_ sont assez utiles
    8. **A_I** : les valeurs sont calculées automatiquement par MySQL, mais réservé au champ _PRIMARY KEY_ (voir _Primary Key_ plus haut)
    9. **Commentaires** : commentaire libre pour décrire le but du champ

8. Configuration de la table

    1. Commentaire de table : commentaire libre pour décrire le but de la table
    2. **Interclassement** (_Collation_) : laisser vide pour utiliser l'interclassement par défaut de la base de données
    3. **Moteur de stockage** : sélectionner InnoDB
    4. Ne pas créer de _Partition_

![](./img/pma-create-table6.png)

9. Cliquer sur le bouton _Enregistrer_ et :tada:

## Problèmes techniques connus

Il peux exister un conflit entre PhpMyAdmin et la version 7.2 de PHP: Cela génére des erreurs *non bloquantes* à l'affichage du détail d'une table et à l'import/export d'un script SQL.

Pour fixer ces erreurs, il suffit de copier coller les deux commandes suivantes dans le terminal :

- A l'affichage d'une table : `sudo sed -i "s/|\s*\((count(\$analyzed_sql_results\['select_expr'\]\)/| (\1)/g" /usr/share/phpmyadmin/libraries/sql.lib.php`

- A l'import/export d'un script : `sudo sed -i "532s|count(|&(array)|" /usr/share/phpmyadmin/libraries/plugin_interface.lib.php`

[Détail de l'erreur](https://stackoverflow.com/questions/52307790/phpmyadmin-export-issue-count-parameter-must-be-an-array-or-an-object-that-i)
