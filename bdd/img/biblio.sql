DROP TABLE IF EXISTS livre ;
CREATE TABLE livre (ISBN INT AUTO_INCREMENT NOT NULL,
titre VARCHAR(50),
annee SMALLINT(4),
personne_id INT,
PRIMARY KEY (ISBN)) ENGINE=InnoDB;

DROP TABLE IF EXISTS genre ;
CREATE TABLE genre (genre_id INT AUTO_INCREMENT NOT NULL,
nom VARCHAR(50),
PRIMARY KEY (genre_id)) ENGINE=InnoDB;

DROP TABLE IF EXISTS personne ;
CREATE TABLE personne (personne_id INT AUTO_INCREMENT NOT NULL,
nom VARCHAR(50),
prenom VARCHAR(50),
PRIMARY KEY (personne_id)) ENGINE=InnoDB;

DROP TABLE IF EXISTS lieu ;
CREATE TABLE lieu (lieu_id INT AUTO_INCREMENT NOT NULL,
nom VARCHAR,
commune INT,
contact INT,
PRIMARY KEY (lieu_id)) ENGINE=InnoDB;

DROP TABLE IF EXISTS livre_genre ;
CREATE TABLE livre_genre (ISBN INT NOT NULL,
genre_id INT NOT NULL,
PRIMARY KEY (ISBN,
 genre_id)) ENGINE=InnoDB;

DROP TABLE IF EXISTS emprunter ;
CREATE TABLE emprunter (personne_id INT NOT NULL,
ISBN INT NOT NULL,
lieu_id INT NOT NULL,
date_emprunt_emprunter DATE,
PRIMARY KEY (personne_id,
 ISBN,
 lieu_id)) ENGINE=InnoDB;

ALTER TABLE livre ADD CONSTRAINT FK_livre_personne_id FOREIGN KEY (personne_id) REFERENCES personne (personne_id);

ALTER TABLE livre_genre ADD CONSTRAINT FK_livre_genre_ISBN FOREIGN KEY (ISBN) REFERENCES livre (ISBN);
ALTER TABLE livre_genre ADD CONSTRAINT FK_livre_genre_genre_id FOREIGN KEY (genre_id) REFERENCES genre (genre_id);
ALTER TABLE emprunter ADD CONSTRAINT FK_emprunter_personne_id FOREIGN KEY (personne_id) REFERENCES personne (personne_id);
ALTER TABLE emprunter ADD CONSTRAINT FK_emprunter_ISBN FOREIGN KEY (ISBN) REFERENCES livre (ISBN);
ALTER TABLE emprunter ADD CONSTRAINT FK_emprunter_lieu_id FOREIGN KEY (lieu_id) REFERENCES lieu (lieu_id);
