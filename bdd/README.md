# Bases de Données

## Conception de bases de données

> Cette rubrique vise à synthétiser l'approche de conception Merise en insistant sur les notions importantes. Vous trouverez des références plus avancées en conclusion.

- [Introduction/Méthodologie](./conception-01-intro.md)
- [Dictionnaire de données](./conception-02-dictionnaire.md)
- [Modèle Conceptuel de Données (MCD) / Modèle Entité-Association](./conception-03-mcd.md)
- [Modèle Logique de Données (MLD)](./conception-04-mld.md)
- [Modèle Physique de Données (MPD)](./conception-05-mpd.md)
- [Conclusion, Ressources, Logiciels](./conception-06-conclusion.md)

## Système de gestion (MySQL etc.)

- [Introduction](intro.md)
- [Système de Gestion de Bases de données relationnelles](rdbms.md)
- [SQL](sql.md)
- [PDO](pdo.md)
- [BDD partagée](bdd-partagee.md)
