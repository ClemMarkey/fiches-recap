# Git remotes

Un projet, plusieurs dépots.

Il est très fortement recommandé d'avoir lu [la fiche récap sur les bases de Git](./git.md) avant d'attaquer la lecture de celle ci.

## Rappel du concept

cf [ce paragraphe](./git.md#concepts-fondamentaux-de-Git).

Dans un setup "classique", le repo local est lié à un repo distant ("remote") qui, par défaut, s'appelle `origin`.

Lorsqu'on crée un repo vide sur Github, ce dernier nous propose d'ailleurs une ligne de commande qui déclare cette _remote_ : 

`git remote add origin git@github.com:monOrga/monRepo.git`

(Lorsqu'on clone un repo avec la commande `git clone`, cette opération est executée par défaut.)


## Déclarer une autre remote

Dans un certain nombre de cas, il est interessant de déclarer un autre repo distant. Par exemple : 
- le repo du formateur, pour récupérer la correction d'un challenge.
- le serveur de production, pour y push directement le code (si si, c'est possible !).
- dans le cas d'un fork, récupérer les changements du repo original.
- ...

Pour déclarer une autre remote, on utilise la commande : 

`git remote add <nomDeLaRemote> <adresseDuRepoDistant>`

On peut nommer les remotes comme on veut, tant que le nom est en minuscule et contient uniquement des lettres.

Pour limiter les problèmes de config, les remotes devraient toutes utiliser le même protocole (`git@` ou `http://`)

## Interagir avec les remotes

#### Lister les remote

`git remote -v` permet de lister les remotes qui sont actuellement liées à un repo local.

#### Pull depuis une remote

`git pull <nomDeLaRemote> <nomDeLaBranche>`

La première fois que cette opération est réalisée, il se peut que git renvoie une erreur `refusing to merge unrelated histories`.

Pour passer outre cette erreur, il suffit de rajouter l'option `--allow-unrelated-histories`.

Attention, comme dans toute opération "pull", il se peut que des conflits apparaissent !

#### Push sur une remote précise

La commande `git push -u origin master` déclare la branche `master` de la remote `origin` comme étant "l'upstream", c'est à dire la destination _par défaut_ des push. 

Par la suite, la commande `git push` (tout court) va donc push sur la branche `master` de la remote `origin`.

Mais on peut toujours spécifier la remote et la branche que l'on vise, simplement en les nommant dans la ligne de commande : `git push <remote> <branche>`

## Gestion des conflits

De manière générale, Git est capable de fusionner différentes versions d'un fichier, sans qu'on ait besoin d'intervenir.

Mais un *conflit* apparait quand une différence trop importante existe entre la version locale et la version distante d'un fichier. À ce moment là, il faut définir une stratégie de gestion des conflits.

Gérer les conflits à la main est une bonne habitude à prendre, car elle nous force à relire les différentes versions du code et à choisir la meilleure _en toute connaissance de cause_.

Mais on peut aussi choisir de définir une stratégie de fusion systématique, grace à l'option `-X <strategy>` : 
- `git pull -X theirs <remote> <branch>` va systématiquement conserver la version _distante_ du code (et donc potentiellement détruire la version locale)
- `git pull -X ours <remote> <branch>` va systématiquement conserver la version _locae_ du code (et donc potentiellement ignorer totalement la version distante)

## Mise en pratique : un exemple.

Je suis un étudiant O'clock. J'ai généré un repo pour un challenge (via un lien classroom par exemple), et je l'ai cloné sur ma machine.

Disons que l'adresse de ce repo est : `git@github.com:oclock/repoEtudiant.git`

Par ailleurs, le formateur a lui aussi généré un repo pour le challenge, et son adresse est : `git@github.com:oclock/repoProf.git`


Après la correction en cockpit, je souhaite récupérer le code de mon formateur. Je vais donc :

1. Déclarer le repo du formateur comme une nouvelle remote, que je nomme "correction" : `git remote add correction git@github.com:oclock/repoProf.git`
2. Récupérer le code du formateur : `git pull --allow-unrelated-histories correction master`
3. Gérer les conflits à la main dans mon éditeur de code
4. Commit la fusion: `git add .` puis `git commit -m "merge from correction"`
5. Push mon nouveau code sur MON repo : `git push origin master` (ou `git push` tout court)
6. Profiter de la vie.


Le lendemain, on a continué à coder sur ce même projet. Ce coup ci, je ne souhaite pas garder mon code, que je trouve pas terrible. Je veux conserver uniquement la version du prof : 
1. Je commit mon travail quand même (garder une trace de tout, c'est bien !): `git add .` puis `git commit -m "un commentaire"` et enfin `git push`
2. Récupérer le code du formateur "en mode bourrin" : `git pull --force -X theirs correction master`
3. Commit la correction: `git add .` puis `git commit -m "correction récupérée"`, et enfin `git push`
4. Profiter de la vie.

Une petite image pour résumer la situation : 

![multi_remote](./img/multi_remote.png)