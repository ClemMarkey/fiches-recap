# Solution de secours lorsque GitHub Classroom ne fonctionne pas

## Etape 1

(Si le repo à été créé par Classroom, le supprimer)
Cloner le repository d'origine en local.
- `git clone ....`

## Etape 2

Supprimer le dossier `.git` du dossier cloné (afin que le dossier ne soit plus relié au répo d'origine)
- `sudo rm -R .git/`

## Etape 3

Versionner de nouveau le dossier cloné
- `git init`
- `git add .`
- `git commit -m "First Commit`

## Etape 4

Créer le nouveau repo sur GitHub
- Dans l'orga de la promo, cliquer le bouton vert `New`
- Renseigner le nom selon la logique suivante: `nomrepoorigine-nomutilisateur`. Ex: `S04-E02-exo-integration-christopheOclock`
- Cocher `Private`
- :warning: Ne pas cocher `Initialize this repository with a README` :warning:
- Valider

## Etape 5

Le nouveau repo créé, GitHub nous donne des lignes de commandes pour relier notre repo local au repo présent sur GitHub.
- Repérer le bloc `…or push an existing repository from the command line`
- Exécuter les 2 lignes dans notre dossier en local.
