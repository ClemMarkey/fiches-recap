# Procédure de mise en place d'un domaine local

Le but est de pouvoir accéder à un projet hébergé sur votre Apache local, mais sans utiliser "localhost" ou "127.0.0.1" dans l'URL du navigateur.

Par exemple, pour le projet oKanban, l'URL serait : `okanban.local`.  
Cette procédure fonctionne pour n'importe quel domaine local. Il faudra juste remplacer tous les `okanban.local` par le nom de domaine souhaité.


## Téléporteur

### Ajouter un domaine local

On va dire à notre ordinateur que le domaine `okanban.local` est hébergé sur le serveur dont l'IP est _127.0.0.1_, c'est-à-dire notre ordinateur.

- Ouvrir un terminal
- taper la commande : `sudo nano /etc/hosts.custom`
- Ajouter la ligne `127.0.0.1 okanban.local`
- CTRL + o puis Entrée
- CTRL + x
- redémarrer le Téléporteur

Maintenant, si vous tapez dans votre navigateur `http://okanban.local/` vous devez avoir la même page que `http://localhost/`.  
Si ce n'est pas le cas, refaire la procédure 1 fois. Si ce n'est toujours pas bon, passer à la suite, et demain de l'aide à un formateur après le cours.

### Définir le VirtualHost

Un VirtualHost sur Apache, c'est un système qui permet d'avoir plusieurs sites internet avec plusieurs noms de domaines sur un seul et même serveur Apache. Chaque site dispose donc de son VirtualHost, c'est-à-dire la configuration du site en question.

Dans notre cas, on doit dire à Apache quel répertoire contient les sources pour le site correspondant au domaine `okanban.local`.

- Ouvrir un terminal
- taper la commande : `sudo nano /etc/apache2/sites-available/okanban.local.conf`
- écrire les directives Apache suivantes dans le fichier :
```apache
<VirtualHost *:80>
  ServerAdmin webmaster@okanban.local
  DocumentRoot "/var/www/html/okanban/public/"
  ServerName okanban.local
  ServerAlias www.okanban.local
</VirtualHost>
```
- il faut personnaliser les directives :
  + _DocumentRoot_ => mettre le chemin absolu vers la source de votre projet (jusqu'au dossier _public_ s'il existe)
  + _DocumentRoot_ => ne pas oublier le `/` à la fin
- CTRL + o puis Entrée
- CTRL + x
- taper `sudo a2ensite okanban.local`
- taper `sudo service apache2 reload`

Maintenant, si vous tapez dans votre navigateur `http://okanban.local/` vous devez avoir la page de votre projet (dont les sources "pubbliques" sont dans _/var/www/html/okanban/public/_)

:bulb: **Tip**  
Si ça ne fonctionne pas, il se peut qu'il y ait des erreurs dans les fichiers de conf créés. On peut tester cela avec la commande : `sudo apachectl configtest`


## Cas du Téléporteur en VM "Serveur"

Si vous accédez à votre Apache (du Téléporteur) depuis votre machine (pas la VM), alors il faut aussi modifier le fichier "hosts" (domaine local) sur cette machine également, mais au lieu de `127.0.0.1`, il faudra mettre l'**adresse IP de la VM** (192.168.xx.xx).

### MacOS

Chemin du fichier **hosts** : `/etc/hosts`

### Windows

Chemin du fichier **hosts** à ouvrir en tant qu'administrateur : `C:\WINDOWS\system32\drivers\etc\hosts`
