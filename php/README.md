# PHP

## Fiches récap

### Les bases

- [La syntaxe](syntaxe.md)
- [Les conditions](conditions.md)
- [Les formulaires](forms.md)
- [Les boucles](boucles.md)
- [Les fonctions](fonctions.md)
- [Les fonctions utiles](fonctions-utiles.md)
- [Les sessions](sessions.md)

### Techniques intermédiaires

- [Programmation Orientée Objet (POO)](programmation-objet.md)
- [Bufferisation de sortie (output buffering)](output-buffering.md)

### Techniques avancées

- [Authentification, sécurité](securite.md)

### Côté serveur

- [Fichiers de configuration, activation des erreurs](config.md)
- [Modifier sa version de PHP](config-migration.md)
- [Composer](composer.md)
- [Problématique serveur et cache côté client](server.md)