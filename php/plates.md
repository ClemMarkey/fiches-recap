# Plates

La librairie *Plates* (http://platesphp.com/) nous facilite l'utilisation des templates. Elle nous permet de séparer notre HTML en plusieurs fichiers et d'inclure ces fichiers dans notre projet avec une facilité déconcertante.

## Installation

En suivant le [guide d'installation](http://platesphp.com/v3/installation/) on voit qu'on peut installer Plates au travers de Composer avec la commande : `composer require league/plates`.

Une fois installer avec Composer, on peut instancier la classe de Plates grâce au namespace `\LeaguePlates\Engine` :


## Utilisation basique

Le but premier de Plates est d'insérer des templates, pour ça, il faut déjà lui indiquer où ces templates se trouvent dans notre arborescence de fichiers :

```php
// Create new Plates instance
$templates = new League\Plates\Engine('/path/to/templates');
```

Si, par exemple, nos templates se trouvent dans le répertoire `Views`, on l'indique de la sorte :

```php
// Create new Plates instance
$templates = new League\Plates\Engine(__DIR__ . '/src/Views');
```

Une fois la classe de Plates instancier, il suffit de l'utiliser lorsqu'on souhaite insérer un template.

### Afficher un template

La principale fonctionnalité du moteur de templates est d'afficher un template en particulier, pour faire ça, il suffit d'utiliser la méthode `render` de Plates :

```php
// Pour afficher le template "home.php"
echo $templates->render('home');
```

#### Utiliser un layout

Lorsque notre site utilise toujours la même entête (logo, menu, accroche, etc...), il peut être bénéfique d'insérer nos templates dans la même structure de code (layout). Pour cela, il faut utiliser la syntaxe suivante dans nos templates :

```php
<?php $this->layout('monLayoutPerso') ?>

<h1>Le titre de mon template...</h1>
```

Cette syntaxe va aller placer le code de notre template directement dans le fichier `monLayoutPerso.php` à l'endroit où se trouve l'instruction suivante :

```php
<?=$this->section('content')?>
```

#### Transmettre de l'information entre templates

Il peut être utile de transmettre des variables à nos templates. Par exemple, pour afficher un article de blog, je peux le récupérer d'une BDD et le transmettre à mon template de la façon suivante :

```php
// On instancie Plates
$templates = new League\Plates\Engine(__DIR__ . '/src/Views');

// On récupère l'article 1
$monArticle = ArticleModel::find( 1 );

// On affiche le template de l'article
$templates->render('article', [ 'article' => $monArticle ]);
```

On utilise le deuxième argument de la méthode `render` en passant un tableau associatif. Les clés de ce tableau associatif seront transformées en variables une fois qu'on sera dans le template :

```php
<h1><?=$article->titre?></h1>
```

> On voit bien que le `'article' => $monArticle` est devenu `$article`.

De la même façon, on peut transférer des informations entre les templates (et/ou les layouts) :

```php
<?php $this->layout('monLayoutPerso', [ 'title' => 'Le title de la page' ]) ?>
```

## Insérer des templates dans des templates

Au delà de la méthode `render` qu'on utilise dans les controllers, il peut être utile d'insérer des fragments de HTML directement à partir d'un template. Pour ça, on utilise la méthode `insert` :

```php
<?php $this->insert('articles/header') ?>
```

> Comme pour les méthodes `render` et `layout`, on peut transférer de l'informations en utilisant le deuxième paramètre de la méthode `insert`.

## Données communes aux templates

Il peut être pratique de définir des informations communes à l'ensemble de nos templates. Ceci est possible via la méthode `addData` de Plates :

```php
$templates->addData([ 'baseUrl' => '/S07/MealOclock' ]);
```

Cette information sera automatiquement transformée en variables dans nos templates et sera utilisable ainsi :

```php
<img src="<?=$baseUrl?>/images/logo.png" alt="le jolie logo" />
```
