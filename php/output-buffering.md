# La bufferisation de sortie

Aussi appelée temporisation de sortie (ou mémoire tampon de l'anglais "buffer").

## Exemple de récupération d'un fichier inclus

Soit la page "mapage.php" :

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
  </head>
  <body>
    <?php echo $body ?>
  </body>
</html>
```

Considérons le script suivant :
```
<?php

// Variables nécessaires à "mapage.php"
$title = "Mon titre de page";
$body = "Mon contenu";

// Démarre la mise en tampon du flux de sortie
ob_start();

include "mapage.php";

// Affiche et purge le tampon
ob_end_flush();
```
**Cela a peu d'intérêt car nous ne faisons rien du buffer.**

**Récupérons le contenu de la page sous forme de variable avec ob_get_contents() :**
```
<?php

$title = "Mon titre de page";
$body = "Mon contenu";

// Démarre la mise en tampon du flux de sortie
ob_start();

include "mapage.php";

// Récupère le flux de sortie
$html = ob_get_contents();
// Vide le flux de sortie
ob_end_clean();

// Affiche le contenu
echo $html;
```

**Ou de manière encore plus simple avec ob_get_clean() :**
```
<?php

$title = "Mon titre de page";
$body = "Mon contenu";

// Démarre la mise en tampon du flux de sortie
ob_start();

include "mapage.php";

// Récupère le flux de sortie et le purge
$html = ob_get_clean();

// Affiche le contenu
echo $html;
```

Ici nous affichons simplement le code HTML généré, mais nous pourrions également modifier ce code généré.

**Cela peut servir par exemple, a créer un gabarit de mail HTML qui sera envoyé ensuite.**
