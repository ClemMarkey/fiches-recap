# XDebug

Xdebug est une extension pour PHP, très pratique pour debugger, comme son nom ne l'indique pas du tout.

## Installer PECL

PECL (PHP Extension Community Library), est un outil pour installer et configurer des extensions PHP. Nous allons nous en servir pour installer XDebug. Il est disponible dans la suite d'outils PEAR (PHP Extension and Application Repository).

Il suffit donc d'installer php-pear pour profiter de PECL :

`sudo apt install php-pear`


## Installer les packages de dev de PHP

Pour fonctionner, XDebug a besoin de la bibliothèque de dévelopement de PHP.

- Vérifiez votre version de PHP avec `php -v`. Nous utiliserons `X.x` pour la suite, que vous devrez remplacer par la version qui vous concerne.

- Installer les packages de dev :  `sudo apt install phpX.x-dev`

- Vérifiez l'installation correcte de pecl avec `pecl version`

## Installer XDebug

`sudo pecl install xdebug`

Repérez la dernière ligne : `You should add "zend_extension=/usr/lib/php/20170718/xdebug.so" to php.ini`

Copiez le contenu des doubles quotes (ici : `zend_extension=/usr/lib/php/20170718/xdebug.so`)

## Activer l'extension dans PHP

Ouvrez le fichier suivant (créez le si nécessaire): `/etc/php/X.x/apache2/conf.d/xdebug.ini`

Dans ce fichier, collez la ligne que vous avez copié, suivi des instructions suivantes : 
```ini
zend_extension=/usr/lib/php/20170718/xdebug.so

[XDebug]
xdebug.remote_enable = 1
xdebug.remote_autostart = 1
```


Relancer Apache : `sudo service apache2 restart`

## Pour vérifier
Créez un fichier php, copiez-y ce contenu : 
```php
<?php
phpinfo();
php>
```
Accèdez à ce fichier via un navigateur, et cherchez "xdebug" dans la page. Vous devriez trouver un tableau intitulé `xdebug` et résumant les différents paramètres de l'extension.

Si ce n'est pas le cas, vérifiez que le fichier `/etc/php/X.x/apache2/conf.d/xdebug.ini` est bien chargé : son nom doit être présent dans la ligne "Additional .ini files parsed" du premier tableau de la page.

Si le fichier `/etc/php/X.x/apache2/conf.d/xdebug.ini` n'est jamais chargé malgré le redémarrage d'apache, vous devez ajouter la config de l'étape précédente directement dans le fichier php.ini (dont le chemin est renseigné dans la page phpinfo à la ligne "Loaded Configuration File")

## Step By Step debugger

Une fois XDebug installé et activé, on peut s'en servir pour débugger une application PHP "pas à pas", c'est à dire en mettant le serveur en pause à chaque instruction !

Dans VSCode : 

- Installer l'extension : PHPDebug (by Felix Becker)
- Aller dans Debug (Ctrl+Shift+D), cliquer sur l'engrenage en haut à gauche, sauver et fermer le fichier launch.json
- Ouvrir un fichier php.
- Ajouter un breakpoint. (Cliquer dans la barre horizontale à gauche des numéros de lignes. Un point rouge devrait apparaitre). Le serveur se mettra en pause à chaque breakpoint.
- Cliquer sur le bouton Play. Une petite barre de raccourcis apparait en haut de la fenêtre. 
- Accéder au fichier via un navigateur... :tada: Normalement, VSCode va revenir au premier plan, et le serveur est en pause. On peut alors vérifier le contenu des variables présentes, et passer d'une instruction à l'autre en utilisant la barre d'outil !