# Titre Professionnel "Concepteur Développeur d'Applications" niv. 6

Le [titre professionnel](https://travail-emploi.gouv.fr/formation-professionnelle/evoluer-professionnellement/titres-professionnels-373014) _Concepteur développeur d'application_ (TP CDA, [RNCP 31678](http://www.rncp.cncp.gouv.fr/grand-public/visualisationFiche?format=fr&fiche=31678)) de niveau 6 (anciennement dit « de niveau II ») requière de présenter au moins un projet (== _situation professionnelle_) devant un jury. Ce titre est accessible après la formation en alternance qui a été arrêté pour le moment.

**Si vous envisagez de passer ce titre, la lecture de cette fiche-récap et des documents de références indiqués en bas est indispensable.**

<details>
<summary>Résumé des objectifs professionnels visés</summary>

Le concepteur développeur d'applications conçoit et développe des **services numériques** à destination des utilisateurs en respectant les **normes et standards** reconnus par la profession et en suivant l'état de l'art de la **sécurité informatique** à toutes les étapes. Il prend en compte les **contraintes **économiques**, en termes de coûts et de délais, les **exigences de sécurité** propres à son domaine d'intervention.

* Pour concevoir et développer les **interfaces utilisateur de type desktop ou web**, il élabore une maquette avec les enchaînements d'écrans, qu'il fait valider à l'utilisateur. Il code les formulaires de saisie et de résultats, ainsi que les états, en programmant de manière sécurisée les événements utilisateur et en accédant aux données stockées dans une base.
* Pour concevoir et mettre en œuvre la **persistance des données**, il analyse un cahier des charges fonctionnel ou une demande utilisateur afin de modéliser et de créer une base de données de type relationnel ou NoSQL (Not only SQL) ou d'adapter une base existante en l'optimisant ou en ajoutant des éléments et en veillant à ne pas introduire de vulnérabilité dans le système d'informations.
* Pour concevoir et développer une **application multicouche répartie**, il analyse la demande en s'appuyant sur une démarche permettant de construire les services numériques en plusieurs couches correspondant aux **couches présentation, métier et persistance**.

Il agit avec **autonomie** […] [Les] projets font suite à des demandes formulées directement par un client, par une maîtrise d'ouvrage ou par l'intermédiaire d'un chef de projet. Il peut travailler **en tant que salarié d'une entreprise, pour un client de la société de services qui l'emploie ou en tant qu'indépendant** directement pour un client. Ses activités diffèrent selon la taille et l'organisation du projet. […] Il applique les **recommandations de sécurité** émises par l'Agence nationale de la sécurité des systèmes d'information (**ANSSI**).
</details>

## 🔨 Activités et compétences

Le TP CDA permet de valider que l'étudiant maîtrise, ou est en bonne voie de maîtriser, les objectifs professionnels listés ci-avant. Pour ce faire, le TP découpe le champ professionnel en activités, et chaque activité en compétences pro.

Les activités types et compétences professionnelles du métier de concepteur développeur d'applications sont décrites dans le :books: _**REAC** (référentiel emploi/activités/compétences_). Il s'agit d'une sorte de fiche de poste très détaillée du développeur web. Votre travail va être évalué relativement à cette « fiche de poste. »

_Il n'est pas attendu de vous que vous maîtrisiez parfaitement toutes les compétences — plutôt que vous ayez acquis et mis en œuvre **suffisamment** de compétences, connaissances et aptitudes pour occuper un poste Junior et être en bonne posture pour ensuite poursuivre votre progression de carrière._

![résumé activités et compétences du titre professionnel](img/TP-CDA-CCP.png)

En résumé :

- l'activité 1 (composants d'interface) regroupe les compétences concernant l’aspect visuel et les traitements côté « client » (aussi bien pour des applications web que non web) ;
- l'activité 2 (persistance des données) regroupe les compétences concernant les données, leur stockage, leur transformation, leur validation ;
- l'activité 3 (application multicouche répartie) regroupe les compétences architecturales.

Le tout constitue un stack technique moderne typique d'une application professionnelle.

## Modalités d'évaluation

Pour atteindre les objectifs professionnels visés par le TP, l'étudiant en alternance doit travailler sur un (ou plusieurs) projet(s) et faire la démonstration auprès du jury d'évaluation qu'un ensemble précis de compétences professionnelles clés sont maîtrisées.

L'évaluation par le jury est réalisé _via_ « une présentation d’un projet réalisé en amont de la session, éventuellement complétée par d’autres modalités d’évaluation : entretien technique, questionnaire professionnel, questionnement à partir de production(s). »

Peuvent également être consultés et mis à profits par le jury :

* le dossier professionnel + annexes éventuelles
* les résultats des évaluations en cours de formation (ECF) transmises par l'école
* un entretien supplémentaire avec le jury

La présentation du projet a le plus gros impact sur l'obtention du TP.

Le TP étant composé de plusieurs compétences professionnelles distinctes, chacun fait l'objet d'un Certificat de compétence professionnelle (CCP). Un étudiant peut obtenir une validation partielle du TP en ne validant que certains CCP, auquel cas il peut compléter le TP ultérieurement.

## 🔧 Le projet

### Quel projet puis-je présenter ?

Les jurys de titre professionnels doivent évaluer les candidats sur la base d'une **mise en situation professionnelle**. Comme on ne peut pas vous demander de coder une application complète sous l'oeil du jury :smirk: il vous est demandé de **présenter, dans un dossier puis à l'oral, un projet** sur lequel vous aurez travaillé *en amont* de la présentation, et dans lequel vous aurez mis en oeuvre *personnellement* les compétences demandées.

### Compétences obligatoires

Le projet présenté doit **obligatoirement** couvrir certaines compétences parmi toutes celles listées par le TP CDA.

<details>
<summary>Liste des compétences obligatoires</summary>

Pour l’activité type 1 (**Concevoir et développer des composants d'interface utilisateur en intégrant les recommandations de sécurité**) :

- Maquetter une application
- Développer des composants d’accès aux données
- Développer la partie front-end d’une interface utilisateur web
- Développer la partie back-end d’une interface utilisateur web

Pour l’activité type 2 (**Concevoir et développer la persistance des données en intégrant les recommandations de sécurité**) :

- Mettre en place une base de données

Pour l’activité type 3 (**Concevoir et développer une application multicouche répartie en intégrant les recommandations de sécurité**) :

- Concevoir une application
- Développer des composants métier
- Construire une application organisée en couches
</details>

> Dans le cas où un seul projet ne permet pas au candidat de faire valoir l’ensemble des compétences devant être couvertes obligatoirement, il peut présenter plusieurs projets. Dans ce cas, il motive ses choix, veille à limiter le nombre de projets présentés et fournit des éléments de contexte correspondant à chacun des projets.

L'étudiant peut présenter un projet réalisé dans son entreprise _si et seulement si_ le projet en question répond aux critères ci-dessus (compétences obligatoires). Dans le cas contraire, l'étudiant doit présenter un projet personnel, mais réalisé dans des conditions de production équivalentes à un projet proposé par l'entreprise.

### Autres compétences

Les compétences prévues dans les trois activités du TP, mais non-obligatoires, n'ont pas nécessairement à être couvertes par le projet présenté lors de la session. Toutefois :

- le cas idéal est évidemment celui où le projet couvre ces compétences ;
- dans le cas contraire, il est _fortement_ recommandé d'avoir à disposition d'autres supports pour démontrer que ces compétences ont été mises en œuvre.

Il est ainsi possible :

- soit de présenter non pas un, mais deux projets pendant les 40 mn de présentation ;
- soit de présenter un seul projet mais d'avoir en complément des « productions » (bouts de code, etc.) à propos desquelles le jury peut poser des questions complémentaires à l'entretien technique (auquel cas, signaler l'existence de ces productions en amont de la présentation).

### Comment préparer au mieux mon dossier _pendant le projet_ ?

Lors de la session de certification, l'étudiant doit présenter un travail **personnel**, quand bien même le projet a été réalisé en équipe. Le but n'est pas de faire un démo du projet ! Mais plutôt d'expliquer en quoi l'étudiant a contribué à le mener à bien, à travers la mise en œuvre des compétences obligatoires (et plus si possibles).

La préparation de l'examen n'est ainsi pas censée changer radicalement l'attitude des intervenants sur le projet pendant son développement. Pour l'étudiant qui prépare un dossier et une présentation à propos du projet en question, il lui suffit d'être consciencieux et régulier sur ces quelques points :

- lire les documents REAC et RC pour savoir précisément ce qui est attendu par le jury ;
- documenter au fur et à mesure tout le travail d'analyse et de conception ;
- écrire chaque jour dans un journal de bord les actions concrètes réalisées, problèmes rencontrés, références importantes rencontrées… pour pouvoir facilement y revenir au moment de la rédaction du dossier ;
- bien noter les évènements et réalisations phares du projet, qui feront l'objet d'un _focus_ dans le dossier ;
- s'impliquer dans la démarche de gestion de projet et jouer le jeu (du professionnalisme) au maximum ;
- écrire des messages de commit explicites, documenter la base de code !

## 📑 Le Dossier

Pour préparer votre évaluation, vous allez rédiger **en français** 3 documents :

- le **dossier de projet**, à propos de *votre travail* sur le projet en situation professionnelle que vous souhaitez présenter au cours des 45 premières minutes de la session ;
- le **support de présentation** venant en appui du dossier de projet, utilisé pendant les 45 première minutes de la session ;
- le **dossier professionnel** qui devra contenir, pour chacune des compétences du titre, la description d'activités liées, réalisées en cours de formation.

Par ailleurs, le livret de résultats des évaluations en cours de formation (ECF), complété par vos formateurs, viendra appuyer ces éléments et sera fourni au jury le jour de l'épreuve — ils s'y référeront si besoin, en cas de doute sur certaines compétences.

<details>  
<summary>Le dossier de projet respecte ce plan type : (source: RC)</summary>  

1. Liste des compétences du référentiel qui sont couvertes par le projet
2. Résumé du projet en anglais d’une longueur d’environ 20 lignes soit 200 à 250 mots, ou environ 1200 caractères espaces non compris
3. Cahier des charges ou expression des besoins du projet
4. Gestion de projet (planning et suivi, environnement humain et technique, objectifs de qualité)
5. Spécifications fonctionnelles du projet
6. Spécifications techniques du projet, élaborées par le candidat, y compris pour la sécurité
7. Réalisations du candidat comportant les extraits de code les plus significatifs et en les argumentant, y compris pour la sécurité
8. Présentation du jeu d’essai élaboré par le candidat de la fonctionnalité la plus représentative (données en entrée, données attendues, données
obtenues)
9. Description de la veille, effectuée par le candidat durant le projet, sur les vulnérabilités de sécurité
10. Description d’une situation de travail ayant nécessité une recherche et effectuée par le candidat durant le projet
</details>

## 🎓 Épreuve de certification

Aussi appelée _session d'évaluation_, tous les détails de son déroulement sont disponibles dans le RC (référentiel de certification) à la section 3.1 — vous y retrouvez également la grille d'évaluation (section 3.2) utilisée par les jurys pendant la session d'évaluation !

La session dure 1h45.

### Présentation orale (40 mn)

<details>
<summary>Déroulé de la présentation du projet</summary>
  
Le candidat présente son projet à l’aide d’un support de présentation réalisé en amont de la session d’examen, et selon ce canevas :

- Présentation de l’entreprise et/ou du service et contexte du projet (cahier des charges, environnement humain et technique)
- Conception et codage des composants front-end et des composants back-end
- Présentation des éléments les plus significatifs de l'interface de l’application
- Présentation du jeu d'essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
- Présentation d’un exemple de recherche effectuée à partir de site anglophone
- Synthèse et conclusion (satisfactions et difficultés rencontrées)

Dans les 40 mn, prévoir 10 mn de démo, à n'importe quel moment (peut être en lien avec la présentation des éléments les plus significatifs, du jeu d'essai, etc.)
</details>

### Entretien technique (45 mn)

Le jury approfondit sa compréhension des éléments présentés pendant les 40 premières minutes avec des questions essentiellement d'ordre techniques. Il cherche à valider la maîtrise des compétences obligatoires, et à explorer les autres compétences inscrites au TP.

### Entretien final (20 mn)

Après la présentation et l'entretien technique associé, le jury pose des questions à l'étudiant sur son parcours personnel et son dossier professionnel.

## Questions diverses

**Je n'ai pas la possiblité de faire un projet en entreprise qui couvre les attentes du TP, que faire ?** Il faut trouver une idée de projet perso, qui sera réalisé en binôme ou trinôme.

**Je n'ai pas d'idée de projet perso et/ou je ne suis pas sûr que mon (mes) idée(s) soi(en)t valide(s)**. Dans tous les cas, il faudra, à un moment précis au cours de la formation, présenter votre idée. C'est à cette période que les idées de projets, aussi bien pros que persos, seront validées par l'équipe pédagogique — qui pourra éventuellement fournir des idées de projets.

**Je souhaite faire un projet perso, puis-je travailler en solo ?** Non, les projets persos doivent être réalisés par des équipes de deux à trois étudiants (pas moins, pas plus). Chaque étudiant investi sur le projet présentera au jury _son travail personnel_, ainsi que le(s) collaborateur(s) et la répartition du travail entre tous les intervenants.

## :books: Documents de référence

[Pour consulter les documents officiels du Titre Professionnel "Concepteur Développeur d'Applications", c'est par ici.](https://www.banque.di.afpa.fr/EspaceEmployeursCandidatsActeurs/EGPResultat.aspx?ct=01281m03&cd=&cr=&type=t)

Vous y retrouverez les documents suivants, **à lire pour préparer votre candidature** :

- fiche communication : résumé de la profession, décrit ce qui sera attendu de vous en tant que professionnel  
- REAC : référentiel emploi/activités/compétences (décrit précisément les compétences)
- RC : référentiel de certification (décrit les conditions de l'évaluation)
