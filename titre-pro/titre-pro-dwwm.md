# Titre Professionnel "Développeur Web et Web Mobile" niveau 5

Le [titre professionnel](https://travail-emploi.gouv.fr/formation-professionnelle/evoluer-professionnellement/titres-professionnels-373014) _Développeur Web et Web Mobile_ (TP DWWM, [RNCP 31114](https://certificationprofessionnelle.fr/recherche/rncp/31114)) de niveau 5 (anciennement dit « de niveau III ») requière de présenter au moins un projet (== _situation professionnelle_) devant un jury. Ce titre est accessible après la [formation développeur web](https://oclock.io/formations/developpeur-web), la [formation fullstack JS](https://oclock.io/formations/developpeur-web-fullstack-javascript) et sous certaines conditions après la professionnalisation (spécialisation + apothéose).

## 🔨 Activités et compétences

Les activités types et compétences professionnelles du métier de développeur web et web mobile sont décrites dans le :books: _**REAC** référentiel emploi/activités/compétences_.  
Il s'agit d'une sorte de fiche de poste très détaillée du développeur web.   
Votre travail va être évalué relativement à cette "fiche de poste".  

![résumé activités et compétences du titre professionnel](img/TP-DWWM-CCP.png)

_Il n'est pas attendu de vous de maîtriser parfaitement toutes les compétences, plutôt que vous ayez acquis et mis en oeuvre **suffisamment** de compétences, connaissances et aptitudes pour occuper un poste junior et poursuivre votre progression._


## 🔧 Le Projet

### Quel projet puis je présenter ?

Les jurys de titre professionnels doivent évaluer les candidats en **mise en situation professionnelle**. Comme on ne peut pas vous demander de coder une application complète sous l'oeil du jury :smirk: , il vous est demandé de présenter (dans un dossier puis à l'oral) un projet sur lequel vous aurez travaillé *en amont*, et dans lequel vous aurez mis en oeuvre personnellement les compétences demandées.

<details>

<summary>Le projet couvre **obligatoirement** les compétences suivantes :</summary>  

Pour l’activité type 1 « **Développer la partie front-end** d’une application web ou web mobile en intégrant les recommandations de sécurité »  
- Maquetter une application  
- Réaliser une interface utilisateur web statique et adaptable  
- Développer une interface utilisateur web dynamique  
**OU**  
- Maquetter une application  
- Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce  

Pour l’activité type 2 « **Développer la partie back-end** d’une application web ou web mobile en intégrant les recommandations de sécurité »  
- Développer les composants d’accès aux données  
- Développer la partie back-end d’une application web ou web mobile  
**OU**  
- Développer les composants d’accès aux données  
- Elaborer et mettre en œuvre des composants dans une application de gestion de contenu ou e-commerce  

</details>  


Si votre projet réalisé pendant l'apothéose (projet de fin de formation) remplit cette condition, vous pouvez le présenter pour le Titre Professionnel.  
Si ce n'est pas le cas, plusieurs options possibles :  
- poursuivre le travail de développement sur ce projet
- présenter un autre projet, développé après la formation, en stage ou en emploi.

_Dans le cas où un seul projet ne permet pas au candidat de faire valoir l’ensemble des compétences devant être couvertes obligatoirement, il peut présenter plusieurs projets. Dans ce cas, il motive ses choix, veille à limiter le nombre de projets présentés et fournit des éléments de contexte correspondant à chacun des projets._


### Comment préparer mon dossier pendant le projet ?

Lors de la session de certification, vous devrez présenter **votre** travail personnel, sur un projet en équipe. Votre but est de montrer que vous savez mettre en oeuvre des compétences techniques, ET avoir un comportement professionnel dans le travail en équipe.   
Finalement, c'est exactement ce que vous ferez déjà en tant que jeune professionnel...  ;)   

La préparation de l'examen n'est pas censée changer radicalement votre attitude pendant le développement du projet, il vous suffira d'être consciencieux et régulier sur ces quelques points :

- lisez les documents REAC et RC pour savoir précisément ce qui est attendu de vous,
- écrivez chaque jour en quelques mots minimum, dans votre journal, les actions réalisées, problèmes rencontrés, références importantes rencontrées...
- impliquez vous dans la démarche de gestion de projet et jouez le jeu (du professionnalisme) au maximum avec votre équipe,
- documentez au fur et à mesure tout le travail d'analyse et de conception que vous ferez (en équipe et individuellement)
- écrivez des messages de commit explicites !


## 📑  Le Dossier

Pour préparer votre évaluation vous allez rédiger **en français** 2 documents :

- Le **Dossier de Projet** est un dossier de présentation de **votre travail sur le projet en situation professionnelle** que vous souhaitez présenter.  

- Le **Dossier Professionnel** devra contenir, pour chacune des compétences clé du Titre, la description d'activités liées, réalisées en cours de formation.
Tout savoir sur le dossier professionnel, que doit il contenir, comment le remplir... : http://www.dossierprofessionnel.fr/

- Le livret de résultats des évaluations en cours de formation, complété par vos formateurs, complètera ces éléments et sera fourni au jury le jour de l'épreuve pour s'y référer si besoin.

<details>  
<summary>Le dossier de projet respecte ce plan type : (source: RC)</summary>  

1. Liste des compétences du référentiel qui sont couvertes par le projet
2. Résumé du projet en français d’une longueur d’environ 20 lignes soit 200 à 250 mots, ou environ 1200 caractères espaces non compris
3. Cahier des charges, expression des besoins, ou spécifications fonctionnelles du projet
4. Spécifications techniques du projet, élaborées par le candidat, y compris pour la sécurité et le web mobile
5. Réalisations du candidat comportant les extraits de code les plus significatifs et en les argumentant, y compris pour la sécurité et le web mobile
6. Présentation du jeu d’essai élaboré par le candidat de la fonctionnalité la plus représentative (données en entrée, données attendues, données
obtenues)
7. Description de la veille, effectuée par le candidat durant le projet, sur les vulnérabilités de sécurité
8. Description d’une situation de travail ayant nécessité une recherche, effectuée par le candidat durant le projet, à partir de site anglophone
9. Extrait du site anglophone, utilisé dans le cadre de la recherche décrite précédemment, accompagné de la traduction en français effectuée par le
candidat sans traducteur automatique (environ 750 signes).  
</details>



## 🎓 Épreuve de certification

Tous les détails du déroulement de l'épreuve peuvent être retrouvés dans le _**RC** référentiel de certification_.  
Vous y retrouvez également la **grille d'évaluation** utilisée par les jurys pendant la session.

- présentation du projet *35 min*  

<details>
<summary>Le candidat présente son projet à l’aide d’un support de présentation réalisé en amont de la session d’examen, et selon ce canevas :</summary>
- Présentation de l’entreprise et/ou du service et contexte du projet (cahier des charges, environnement humain et technique)
- Conception et codage des composants front-end et des composants back-end
- Présentation des éléments les plus significatifs de l’interface de l’application
- Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
- Présentation d’un exemple de recherche effectuée à partir de site anglophone
- Synthèse et conclusion (satisfactions et difficultés rencontrées)
</details>

- questions (du jury) sur le projet *40 min*

- questions (du jury) sur le parcours personnel et dossier professionnel *15 min*


## :books: Documents de référence

[Pour consulter les documents officiels du Titre Professionnel "Développeur Web et Web Mobile", c'est par ici.](https://www.banque.di.afpa.fr/espaceemployeurscandidatsacteurs/EGPResultat.aspx?ct=01280m03&cd=&cr=&type=t)

Vous retrouverez les documents suivants, **à lire pour préparer votre candidature** :
- fiche communication: résumé de la profession, décrit ce qui sera attendu de vous en tant que professionnel  
- REAC: référentiel emploi/activités/compétences (décrit précisément les compétences)
- RC: référentiel de certification (décrit les conditions de l'évaluation)
