# Titre Professionnel


## Qu'est ce que c'est ?

> Un titre professionnel est une certification professionnelle délivrée, au nom de l’État, par le ministère du travail. Il atteste que *son titulaire maîtrise les compétences, aptitudes et connaissances permettant l’exercice d’un métier*.

Pour obtenir cette certification, vous devrez rédiger un dossier décrivant votre mise en situation professionnelle (projet) et votre parcours, et le présenter devant un jury expérimenté.

Les Titres Professionnels **"Développeur Web et Web Mobile"** et **Concepteur Développeur d'Applications** se composent de plusieurs *activités types*.
Chaque *activité type* fait appel à plusieurs *compétences professionnelles*.

Le jury décide de l'attribution ou non du titre, il peut également valider l'obtention d'un "certificat de compétences profesionnelles", portant sur une des activités types composant le titre.

## Développeur Web et Web Mobile 

<details><summary>Le candidat présente son projet à l’aide d’un support de présentation réalisé en amont de la session d’examen, et selon ce canevas :</summary>
  
- Présentation de l’entreprise et/ou du service et contexte du projet (cahier des charges, environnement humain et technique)
- Conception et codage des composants front-end et des composants back-end
- Présentation des éléments les plus significatifs de l’interface de l’application
- Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
- Présentation d’un exemple de recherche effectuée à partir de site anglophone
- Synthèse et conclusion (satisfactions et difficultés rencontrées)

</details>

Vous pouvez passer cette certification à l'issue de votre formation **Développeur Web** dans l'école.
- après la formation 6 mois (Socle + spé + Apothéose), en présentant le projet développé pendant l'Apothéose
- après la formation 6 mois, et un stage, en présentant un projet développé en stage
- après la formation 3 mois (Socle) complété par un stage de minimum 3 mois, en présentant un projet développé en stage


Qu'est ce qu'il faut savoir ? Comment se préparer ? 
La réponse est là : le [Titre Pro Développeur Web et Web Mobile de A à Z](titre-pro-dwwm.md)


## Concepteur-Développeur d'Applications

Qu'est ce qu'il faut savoir ? Comment se préparer ? 
La réponse est là :  le [Titre Pro Concepteur-Développeur d'Applications de A à Z](titre-pro-cda.md)
