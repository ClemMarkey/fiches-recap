# JavaScript

## Fondamentaux

- [La syntaxe de base](syntaxe.md)
- [Notion de « fonction »](fonctions.md)
- [Des fonctions utiles](fonctions-js.md)
- [Les boucles](boucles.md)
- [Les conditions](conditions.md)
- [Atteindre un objectif avec JS](objectifs.md)

## JS pour le Web

- [Le DOM](dom.md)
- [Les Événements](evenements.md)
- [Requêtes AJAX](ajax.md)

## Concepts avancés

- [`this`](this.md)
- [Programmation fonctionnelle](programmation-fonctionnelle.md)
- [Approche modulaire](modules.md)

## Outils, compléments

- [ESLint](eslint.md)
- [jQuery](jquery.md)
