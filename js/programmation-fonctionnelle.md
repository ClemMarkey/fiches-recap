# Programmation fonctionnelle

JavaScript permet à de nombreux égards d'utiliser les paradigmes de la **programmation fonctionnelle**.

Il s'agit avant tout de multiplier le recours aux fonctions pour mieux compartimenter son code. Cette approche produit plusieurs effets intéressants :

- les fonctions, notamment quand elles sont bien nommées, forment des blocs déclaratifs facilement réutilisables ;
- comme chaque fonction JavaScript crée une nouvelle portée de variable, on évite plus facilement les effets de bords indésirables (bugs) ;
- la composition de fonctions pousse naturellement à séparer données et *traitement* de ces données.

Tous ces points peuvent paraître obscurs. Examinons-les au travers de cas pratiques.

## Quelques méthodes utiles

### `.forEach()`

La méthode `.forEach()` permet de parcourir un tableau et d'exécuter, pour chaque élément du tableau, une fonction donnée en argument. On peut ainsi, dans la plupart des cas, se passer d’une [boucle _oldSchool_ du type `for`](../js/boucles.md).

Pour exemple :

```js
var letters = ['a', 'b', 'c'];

// version 1 :
letters.forEach(function(letter) {
  console.log(letter);
});
```

qu'on peut simplifier en :

``` js
// version 2 :
letters.forEach(console.log);
```

> Bien noter que la fonction passée en argument est simplement, soit définie sur le moment, soit référencée. **Elle n'est pas exécutée** (ie. on n'appelle pas la fonction avec la syntaxe `nomDeLaFonction()`). On passe donc simplement en argument à `forEach` une *référence de fonction*, on l'occurence `console.log`. C'est `forEach` qui s'occupera d'appeler cette fonction référéncée au bon moment, en lui passant automatiquement un paramètre différent à chaque tour de boucle. Il s'agit d'une approche déclarative, car on déclare / décrit un comportement futur (afficher chaque élément avec `console.log`) mais on n'écrit plus explicitement le code qui gère la mécanique de boucle.

Une référence de fonction passée en argument de cette manière est souvent appelée de façon générique *handler*, ou *callback*. On dit qu'en JavaScript, les fonctions sont des « citoyens de première classe » (*first-class citizens*) car il est possible de les référencer, notamment en argument d'autres fonctions — ce qui n'est pas le cas dans tous les langages de programmation !

Ce code peut-être « modularisé » en transformant la fonction anonyme en fonction nommée. Le but est de pouvoir la réutiliser ailleurs dans le code :

``` js
var displayLetter = function(letter) {
  console.log(letter);
};

letters.forEach(displayLetter);
// mais on peut aussi réutiliser displayLetter ailleurs…
```

[Lien MDN pour .forEach()]( https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/forEach)

> **À savoir** : La boucle `for` reste parfois plus pertinente qu'un `.forEach()`. Si par exemple on récupère des éléments à l’aide de `document.getElementsByClassName`, on obtient un objet de type `HTMLCollection`, qui ne propose pas de méthode `.forEach()`. Il est tout de même possible d'aller chercher un `.forEach()` en convertissant cet objet `HTMLCollection` en `Array`, grâce à la méthode `Array.from()` (_intégrée à JavaScript depuis ES6_)
[Lien MDN pour Array.from()]( https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/from)


### `.map()`

La méthode `.map()`, qui prend en argument une fonction de traitement, permet de créer un *nouveau* tableau possédant la même structure que le tableau initial, mais dont les éléments auront été transformés en appliquant la fonction de traitement. `map` est donc une méthode de traitement des données sans effet de bord, car le tableau initial n'est pas modifié.

Pour exemple :

```js
var numbers = [1, 2, 3];

// Retourne : [2, 4, 6]
numbers.map(function(number) {
  return number * 2;
});
```

[Lien MDN pour .map()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/map)

On pourrait répliquer le fonctionnement de la méthode `.map()` avec une boucle classique (cf. ci-après « Prototype ») :
```js
Array.prototype.map = function(func) {
  var newArray = [];
  this.forEach(function(item) {
    newArray.push(func(item));
  });
  return newArray;
}
```

> Cf. fiche-récap [`this`](../js/this.md).

### `.filter()`

La méthode `.filter()` crée et retourne un nouveau tableau contenant seulement certains éléments du tableau d'origine. Le filtrage s'effectue au moyen d'une fonction de traitement, qui est appelée une fois par élément. Si cette fonction, qui reçoit l'élément courant en argument, retourne `true`, l'élément est conservé dans le tableau de sortie ; sinon, il est écarté du résultat.

Pour exemple :

```js
const inventors = [
  {
    first: 'Albert',
    last: 'Einstein',
    year: 1879,
  },
  {
    first: 'Isaac',
    last: 'Newton',
    year: 1643,
  },
  {
    first: 'Marie',
    last: 'Curie',
    year: 1867,
  },
];

// Retourne : [{ first: 'Isaac', last: 'Newton', year: 1643 }]
inventors.filter(function(inventor) {
  return inventor.year <= 1800;
});
```

[Lien MDN]( https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter)


### `.reduce()`

Contrairement à `map` et `filter`, la méthode `.reduce()` ne retourne pas un tableau de valeurs, mais *une seule* valeur. Appelée sur un tableau initial, elle va appliquer une fonction de traitement qui est un « accumulateur » et qui traite chaque élément de la liste afin de la réduire à une seule valeur.

Concrètement, `reduce` accepte deux arguments :

- une fonction de traitement, dont le rôle est de définir la nouvelle valeur de l'accumulateur ;
- une valeur initiale pour l'accumulateur.

La fonction de traitement est donc appelée une fois pour chaque élément du tableau initial, et elle reçoit en argument l'accumulateur ainsi que l'élément courant.

Pour exemple :

```js
var numbers = [0, 1, 2, 3];

// Retourne : 6
numbers.reduce(function(total, number) {
  return total + number;
}, 0);
```

qu'il est toujours possible d'écrire sous la forme suivante :

``` js
var numbers = [0, 1, 2, 3];
var sumReducer = function(total, number) {
  return total + number;
};
numbers.reduce(sumReducer, 0);
```

Autre exemple :

```js
const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk', 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck'];

// Retourne un objet avec le décompte d'items distincts :
// {
//   car: 5,
//   truck: 3,
//   bike: 2,
//   walk: 2,
//   van: 2
// }
data.reduce(function (obj, item) {
  if (!obj[item]) {
    obj[item] = 0;
  }
  obj[item]++;
  return obj;
}, {});
```

[Lien MDN]( https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/reduce)
[Quelques exemples d'utilisations de `reduce`](http://putaindecode.io/fr/articles/js/array-reduce/)


## Prototype

La programmation fonctionnelle étant basée sur les fonctions, il est intéressant de savoir qu'on peut également rajouter de nouvelles méthodes aux types existants. Il suffit de stocker ces méthodes dans le prototype d'un type.

```js
// Rend la méthode sum disponible sur tous les tableaux :
Array.prototype.sum = function() {
  return this.reduce((cumul, item) => cumul + item, 0);
};

// Retourne : 6
[1, 2, 3].sum();
```

```js
Number.prototype.even = function() {
  return this % 2 === 0;
};

// Retourne : true
// Les parenthèses sont obligatoires, pour ne pas confondre avec
// un nombre à virgule qui serait noté, par exemple, 120.5
(120).even();
```

## Closures

En programmation orientée objet (POO), un objet stocke souvent à la fois des données, et des méthodes de traitement de ces données.

En programmation fonctionnelle, on a tendance à travailler différement : on essaye de séparer les données de leur traitement ; mais il arrive qu'il soit utile (voire indispensable !) d'adopter ponctuellement une « approche » POO dans un code fonctionnel.

Prenons comme exemple le fait de faire un cadeau à quelqu'un. Il y a deux grandes étapes à suivre :

1. Emballer le cadeau (en indiquant sur une étiquette, qui fait ce cadeau, à qui) ;
2. Déballer et découvrir le cadeau (avec l'étiquette à disposition, pour comprendre le contexte du cadeau).

Voici une version code de ce scénario en deux étapes :

``` js
// wrapGift : phase 1 - emballage du cadeau, mise en place de l'étiquette
function wrapGift() {
  var from = "Philippe";
  var to = "Maxime";
  
  // unwrap : phase 2 - déballage du cadeau, prise en compte de l'étiquette
  var unwrap = function() {
    return `A mechanical kitten, offered by ${from} to ${to}!`
  };
  
  return unwrap;
}

// Utilisation des fonctions :
var newGiftToUnwrap = wrapGift(); // phase 1 - emballage du cadeau => le retour est la fonction unwrap
newGiftToUnwrap() // phase 2 - déballage du cadeau => affiche "A mechanical kitten, offered by Philippe to Maxime!
```

Normalement, une fonction n'a pas de mémoire. Mais il existe un cas particulier, illustré par l'exemple ci-dessus : lorsqu'on une fonction définit des variables _et_ retourne une nouvelle fonction _qui utilise_ ces mêmes variables (comme c'est le cas pour `wrapGift` et `unwrap`), on parle de "closure" (ou « fermeture », « clôture »). La closure permet de mémoriser un contexte, qui sera disponible ultérieurement pour la fonction retournée.

> Techniquement, la closure désigne le duo « fonction interne » (ex. `unwrap` dans notre exemple) + contexte (l'ensemble des variables mémorisées pour le bon fonctionnement ultérieur de la fonction interne, ex. `from` et `to` dans notre exemple). On peut aussi désigner par closure le mécanisme de fonctions imbriquées, en général, avec ou sans variables mémoriées.

Le mécanisme de closure permet de limiter l'accès à des informations, tout en les persistant pour éviter de perdre du contexte. On peut ainsi définir des « usines à fonctions » :

```js
function createSum(a) {
  // La fonction renvoyée utilise la variable a, bien que a ne soit pas
  // définie *dans* cette fonction interne. Elle est mise à disposition
  // par le mécanisme de closure :
  return function(b) {
    return a + b;
  };
}

var addFour = createSum(4);
addFour(8); // Renvoie 12
```

Un autre exemple :

``` js
function wrapGift(from, to) {
  return function(gift) {
    console.log(`${gift} offered by ${from} to ${to}!`)
  }
}
wrapGift("Philippe", "Maxime")("A mechanical kitten")
wrapGift("Philippe", "Maxime")("A pen")
wrapGift("Anatole", "Béatrice")("Some dirt")
```

## Programmation déclarative

La [programmation déclarative](https://fr.wikipedia.org/wiki/Programmation_d%C3%A9clarative) est un [paradigme de programmation](https://fr.wikipedia.org/wiki/Paradigme_%28programmation%29) qui vise à séparer strictement données et traitement de ces données, tout en mettant l'accent sur le résulat des transformations appliquées aux données, au lieu des transformations elles-mêmes.

Ce paradigme consiste à créer des applications sur la base de composants logiciels indépendants du contexte et ne comportant aucun état interne. Autrement dit, l'appel d'un de ces composants avec les mêmes arguments produit exactement le même résultat, quel que soit le moment et le contexte de l'appel. On évite ainsi les [les effets de bord](https://fr.wikipedia.org/wiki/Effet_de_bord_%28informatique%29).
