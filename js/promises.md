



# Callbacks, Promises, et Async/Await

Les opérations **synchrones** en JavaScript impliquent que chaque étape d'une opération attend l'exécution complète de l'étape précédente. Cela signifie que peu importe la durée du processus précédent, le processus subséquent ne démarre pas tant que le premier n'est pas terminé.

Les opérations **asynchrones**, en revanche, reportent les opérations. Tout processus qui prend beaucoup de temps à traiter est généralement exécuté en parallèle d'autres opérations synchrones et se termine dans l'avenir.

Cette fiche récap s'appuie sur les concepts fondamentaux sur lesquels JavaScript repose pour gérer les opérations asynchrones. 

Ces concepts incluent: 



*   Les fonctions de **callback**
*   Les **promises** et l'utilisation **d'Async** et **Await** pour gérer les opérations différées en JavaScript.

Beaucoup de lib l’utilisent comme fs, readline, sequelize et tellement d’autres ...

Ils sont très utiles lors de la gestion des événements et des demandes d'API ( et tout type de ressources en général )


# Les opérations asynchrones


```javascript
console.log("Planter du maïs");
console.log("Arroser les plantes");
console.log("Ajouter du fertilisant");
```


Si nous exécutons le code ci-dessus, les éléments suivants seront affichés dans la console :


```
Planter du maïs
Arroser les plantes
Ajouter du fertilisant
```




Maintenant, ajustons un peu pour que l’irrigation de la ferme prenne plus de temps que la plantation et la fertilisation:


```javascript
console.log("Planter du maïs");

setTimeout(function() {
  console.log("Arroser les plantes")
},3000);

console.log("Ajouter du fertilisant");
```
Si nous exécutons le code ci-dessus, les éléments suivants seront affichés dans la console :
```
Planter du maïs
Ajouter du fertilisant
Arroser les plantes
```


Ah, super, c’est les 2 derniers sont inversés … pourquoi ? La fonction `setTimeout` rend l'opération asynchrone en différant l'arrosage de la plante.

Elle lui dit de se produire après 3 secondes. Cela dit, l'ensemble de l'opération ne s'arrête pas pendant 3 secondes, il s’affiche donc «Arroser les plantes». Au contraire, le système va de l'avant pour appliquer des engrais, puis arroser la plante après 3 secondes. On peut donc dire que l’arrosage se fera de façon asynchrone.

En gros Michel plante du maïs, et José doit arroser les plantes juste après, sauf qu’il est à la bourre, il a 3 secondes de retard, du coups Michel continue son travail qui consiste à ajouter du fertilisant, et quand José arrivera, il pourra enfin Arroser les plantes ( cette grosse feignasse )


# 


# Les fonctions de callback

Lorsqu'une fonction accepte simplement une autre fonction comme argument, cette fonction contenue est appelée **callback**.

L'utilisation des fonctions de **callback** est un concept de programmation fonctionnel de base, et vous pouvez les trouver dans la plupart des codes JavaScript; soit dans des fonctions simples comme `setInterval`, l'écoute d'événements ou lors des appels d'API.

Prenons par exemple la méthode `setInterval`, qui admet une fonction de **callback** comme premier argument et également un intervalle de temps en second argument. 


```javascript
setInterval(function() { // Ici on passe une fonction de callback anonyme
  console.log('hello!');
}, 1000);
```
Un autre exemple utilisant `.map()`;
```javascript
const list    = ['chaton', 'chiot', 'poulette']

// créé un nouveau tableau
// boucle à travers le tableau et mappe les données à un nouveau contenu
const newList = list.map(function(val) {
  return "Animal mignon " + val;
});

// newList = ['Animal mignon chaton', 'Animal mignon chiot', 'Animal mignon poulette']
```

# Fonctions de callback nommées

Les fonctions de **callback** peuvent être des fonctions nommées ou anonymes. Dans nos premiers exemples, on a utilisé des fonctions de **callback** anonyme. Voyons une fonction de **callback** nommée:


```javascript
function disCoucou(name) {
  console.log(`Coucou ${name}, bienvenu chez O'clock!`);
}
```
La fonction ci-dessus se voit attribuer un nom “disCoucou” et un argument “name”. Nous utilisons également une syntaxe ES6. Utilisons cette fonction comme fonction de callback :
```javascript
function introduction(prenom, nom, callback) {
  const nomComplet = `${prenom} ${nom}`;

  callback(nomComplet);
}

introduction('John','Doe', disCoucou); // Coucou John Doe, bienvenu chez O'clock!
```
Notez l'utilisation du **callback** ! Les parenthèses suivant la fonction ne sont pas utilisés lors du passage de la fonction en tant que paramètre, c’est ce qu’on appelle un passage par **référence**.

Remarque: La fonction de **callback** n'est exécutée que si elle est appelée par la fonction qui la contient. D’où le terme `fonction de callback`

Plusieurs fonctions peuvent être créées indépendamment et utilisées comme fonctions de **callback**. Celles-ci créent des fonctions à plusieurs niveaux. Lorsque cet arbre de fonctions créé devient trop volumineux, le code devient parfois incompréhensible et n'est pas facile à factoriser. Ceci est connu sous le nom de **callback hell**. 

Voyons un exemple:

```javascript
// un tas de fonctions est défini, chacune prenant une fonction de callback en 1er argument qui retourne son résultat en argument du callback

// Utilisons nos fonctions dans un callback hell
function setInfo(name) {
  address((myAddress) => {
    officeAddress((myOfficeAddress) => {
      telephoneNumber((myTelephoneNumber) => {
        nextOfKin((myNextOfKin) => {
          console.log('Ouf, terminé !'); // Allez, c'est partit pour fermer tous les callbacks
        });
      });
    });
  });
}
```
On suppose que ces fonctions ont déjà été définies ailleurs. 

Vous pouvez voir à quel point il est déroutant de passer chaque fonction en tant que **callback**. Les fonctions de **callback** sont utiles pour les opérations asynchrones courtes. Lorsque vous travaillez avec de grands ensembles, cela n'est pas considéré comme la meilleure pratique. 

Un autre exemple de cascade de la mort plus parlant : 

![Callback Hell](https://miro.medium.com/max/1400/1*Co0gr64Uo5kSg89ukFD2dw.jpeg)


En raison de ce problème, des promesses ont été introduites pour simplifier les activités différées.


# Les promesses

Je **promets** d'essayer de le faire. Je te tiens au jus quand j'aurais **terminé**. Et si je rencontre un **problème**, je te le dis **aussitôt** et j'arrête tout

Ceci est une simple illustration des promesses JavaScript. Ça ressemble à une déclaration IF non ? On va bientôt voir une énorme différence.

Une promesse est utilisée pour gérer le résultat asynchrone d'une opération.

JavaScript est conçu pour ne pas attendre qu'un bloc de code asynchrone s'exécute complètement avant que d'autres parties synchrones du code puissent s'exécuter.

Par exemple, lorsque vous faites des demandes d'API aux serveurs, on a aucune idée de si ces serveurs sont hors ligne ou en ligne, ni combien de temps il faut pour traiter la demande du serveur.

Avec `Promises`, nous pouvons différer l'exécution d'un bloc de code jusqu'à ce qu'une requête **asynchrone** soit terminée. De cette façon, d'autres opérations peuvent continuer à fonctionner sans interruption.

Les promesses ont trois états:

*   `Pending` ( En attente ) : il s'agit de l'état initial de la promesse avant le début d'une opération
*   `Resolved` ( Réalisé ): ça signifie que l'opération spécifiée a été terminée
*   `Rejected` ( Rejeté ): l'opération ne s'est pas terminée; une erreur est généralement levée


# Ça marche comment une promesse ?

Vachement bien ! Plutôt que des mots, voyons comment ça se passe avec un diagramme de séquence

![Diagrame de séquence](img/diagramme-sequence-promise.png)

# Utilisation des promesses

Utiliser une promesse qui a été créée est relativement simple; nous enchaînons .then () et .catch () à notre promesse comme ceci:

```javascript
date
  .then(function(done) {
    // sera déclenché par l'appel du callback resolve()
  })
  .catch(function(error) {
    // sera déclenché par l'appel du callback reject()
  });
```


En utilisant la `Promise` que nous avons créée ci-dessus, allons plus loin:


```javascript
const myDate = function() {
  date
    .then(function(done) {
      console.log('On a un rendez vous ! Whaouuuu')
      console.log(done)
    })
    .catch(function(error) {
        console.log(error.message)
    })
}

myDate();
```

`.then()` reçoit une fonction avec un argument qui est la valeur de résolution de notre promesse.

`.catch()` renvoie la valeur de rejet de notre promesse.

Remarque: les promesses sont asynchrones, les fonctions qui contiennent des promesses sont placées dans une file d'attente de micro-tâches et exécutées lorsque les autres opérations synchrones sont terminées.

_En gros la promesse n’existe pas tant que l’on a pas appeler la fonction synchrone myDate_


# Créer une promesse

L'objet Promise est créé à l'aide du mot clé `new` et contient la promesse; il s'agit d'une fonction exécuteur de **callback**, en l'occurrence `resolve` et `reject`. Comme leurs noms l'indiquent, chacun de ces **callback** renvoie une valeur, le `reject` renvoyant un objet `Error`.


```javascript
const promise = new Promise(function(resolve, reject) {
  // Description de la promesse
})
```

Bon vous l’attendiez tous … On va en voir une ! Créons une `Promise`!

```javascript
const date    = new Promise(function(resolve, reject) {
   //Définissons une variable de test permettant de passer soit dans le resolve soit dans le reject
  const weather = true
  if (weather) {
    const dateDetails = {
      name:     'Restaurant à la fourchette',
      location: 'Chez mémé',
      table:    5
    };

    resolve(dateDetails)
  } else {
    reject(new Error('Il fera pas beau, du coups pas de rendez-vous !'))
  }
});
```


Si `weather` vaut `true`, la promesse sera résolue et le retour des données représentera `dateDetails`,  sinon on retourne un objet `Error` avec pour message : 
> Il fera pas beau, du coups pas de rendez-vous !”


Puisque la valeur `weather` est `true`, on appelle `myDate()` et on verra écrit dans la console : 


```
On a un rendez vous ! Whaouuuu
{
  name:     'Restaurant à la fourchette',
  location: 'Chez mémé',
  table:    5
}
```

# Chaîner les promesses

Parfois, nous pouvons avoir besoin d'exécuter deux ou plusieurs opérations asynchrones en fonction du résultat des promesses précédentes. Dans ce cas, les promesses sont enchaînées. Toujours en utilisant notre promesse créée, commandons un Uber si nous allons à une date.

Nous créons donc une autre `Promise`:

```javascript
const orderUber = function(dateDetails) {
  return new Promise(function(resolve, reject) {
    const message = `Envoie un Uber ASAP à ${dateDetails.location}, on a un rendez-vous !`;

    resolve(message)
  });
}
```

Cette `Promise` peut être raccourcie comme ceci :

```javascript
const orderUber = function(dateDetails) {
  const message = `Envoie un Uber ASAP à ${dateDetails.location}, on a un rendez-vous !`;
  return Promise.resolve(message)
} 
```

On enchaîne cette promesse à notre fonction date() écrite précédemment: 

```javascript
const myDate = function() {
  date
    .then(orderUber)
    .then(function(done) {
      console.log(done);
    })
    .catch(function(error) {
      console.log(error.message)
    })
}

myDate();
```


Et comme `weather` vaut toujours `true`, on obtient : 


```
Envoie un Uber ASAP à Chez mémé, on a un rendez-vous !
```


Une fois que la promesse `orderUber` est enchaînée avec `.then()`, le `.then()` suivant utilise les données de la précédente et ainsi de suite.


# Async and Await

Une fonction asynchrone est une modification de la syntaxe utilisée dans l'écriture de promesses. On peut l'appeler `syntactic sugar over promises`, littéralement un sucre syntaxique sur les promesses. Cela ne fait que faciliter la rédaction des promesses.

Une fonction asynchrone renvoie une promesse - si la fonction renvoie une valeur, la promesse sera résolue avec la valeur, mais si la fonction asynchrone renvoie une erreur, la promesse est rejetée avec cette valeur. 

Gné ?

Voyons une fonction asynchrone:


```javascript
async function maMoto() {
  return 'Honda CBR 125R';
}
```

et une fonction différente qui fait la même chose mais au format Promise :

```javascript
function taMoto() {
  return Promise.resolve('Honda CBR 125R');
}
```


D'après les déclarations ci-dessus, `maMoto()` et `taMoto()` sont équivalent et se résoudront tous deux en `Honda CBR 125R`. De plus, lorsqu'une promesse est rejetée, une fonction asynchrone est représentée comme ceci:


```javascript
function foo() {
  return Promise.reject(25)
}

// est équivalent à 
async function() {
  throw 25; // Ici on jette une exception, https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/throw
}
```

# Await

`await` n'est utilisé qu'avec une fonction asynchrone. 

Le mot-clé `await` est utilisé dans une fonction asynchrone pour garantir que toutes les promesses retournées dans la fonction asynchrone sont synchronisées, c'est-à-dire que la fonction qu’on `await`, va attendre toutes les fonctions asynchrone qu’elle contient

`await` élimine l'utilisation des **callbacks** dans `.then()` et `.catch()`.

En utilisant`async` et `await`, `async` est ajouté au début lors du retour d'une promesse, `await` est ajouté au début lors de l'appel d'une promesse. `try` et `catch` sont également utilisés pour obtenir la valeur de rejet d'une fonction asynchrone. 

Voyons cela avec notre exemple de date:

```javascript
async function myDate() {
  try {

    let dateDetails = await date;
    let message     = await orderUber(dateDetails);
    console.log(message);

  } catch(error) {
    console.log(error.message);
  }
}
```


Ici si on `reject`, dans `date` ou dans `orderUber`, on va forcément atterrir dans le `.catch()` et on va logger le message correspondant à l’erreur.

Enfin, nous appelons notre fonction asynchrone:

```javascript
(async () => { 
  await myDate();
})();
```


Notez la syntaxe ES6 de la fonction, fonction à flèche


# Conclusion

Comprendre les concepts de callback, de promesses et d'async / await peut être assez déroutant, mais c’est quand même super pratique et tellement puissant ! 

:link: [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)

Pour aller plus loin : 
:link: [https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise/finally](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise/finally)