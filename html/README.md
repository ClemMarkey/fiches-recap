# HTML

## Fiches récap

- [La syntaxe](syntaxe.md)
- [Textes](textes.md)
- [Médias](medias.md)
- [Tableaux](tableaux.md)
- [Formulaires](formulaires.md)
- [Structure et sémantique](structure.md)
- [Template HTML](template-html.md)

## Ressources

- http://htmlreference.io/ : une ressource inestimable ! Chaque balise HTML, avec son type, ses attributs, ainsi qu'un exemple concret pour l'utiliser.
