Fonctionnalités avancées de VS Code
===================================

Bien connaître son éditeur de texte est crucial pour développer efficacement (et être heureux). Voici quelques fonctionnalités avancées proposées par VS Code.

## Navigation entre les fichiers

Nativement :

- `Ctrl+Tab` (maintenu puis relâché) permet de parcourir la liste des fichiers ouverts et de se déplacer arbitrairement.
- `Ctrl+P` permet d'ouvrir ou d'afficher (si déjà ouvert) un fichier du projet, en faisant une recherche du type `fuzzy-find` (tapez certains caractères seulement du chemin/nom de fichier, dans l'ordre, pour filtrer la liste des fichiers)

Si le langage le permet (nativement ou _via_ une extension) :

- `F12` permet de se rendre à la _définition_ de l'entité sous le curseur
- `Ctrl+F12` permet de se rendre à l'_implémentation_ de l'entité sous le curseur
- `Maj+F12` permet d'avoir un aperçu de toutes les occurences de l'entité sous le curseur
- `Ctrl+Maj+F10` permet d'avoir un aperçu de la _définition_ de l'entité sous le curseur

## Modification

- `F2` permet de renommer l'entité sous le curseur, dans le fichier voire dans tout le projet
- `Ctrl+.` permet d'activer les icônes « ampoule » qui automatisent certaines tâches de _refactoring_
