# La mise à jour de vscode

Dans le cadre du téléporteur les mises à jour sont bloquées afin qu’un maximum d’étudiants ai exactement la même configuration.
Cela évite l’apparition de bug isolé lors des cours.

Les mises à jour de vscode font également parties des mises à jour bloquées.

## Quand faire la mise à jour

Généralement quand le prof en fait la demande.

Cela va correspondre, la plupart du temps, à des soucis répétitifs avec le plugin Liveshare.

En effet Microsoft réalise régulièrement des mises à jour de son côté qui viennent “casser” la compatibilité avec les anciennes version de vscode.

## Comment faire la mise à jour

Ouvrir un terminal sur le téléporteur.

Executer les commandes suivantes, une par une en attendant bien que la commande précédente aie terminée :

- `sudo oclock-apt-enable`
- `sudo apt-get update`
- `sudo apt-get install code`
- `sudo oclock-apt-disable`
