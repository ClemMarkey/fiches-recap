Visual Studio Code
==================

_Visual Studio Code_, aussi appelé VS Code, VSCode ou vscode, est un éditeur de texte open-source développé sous l'initiative de Microsoft. Relativement léger comparé à un [IDE](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement), il est bien adapté au développement web.

- [Site officiel](https://code.visualstudio.com/)
- [Fiche Wikipédia](https://fr.wikipedia.org/wiki/Visual_Studio_Code)

Points forts :

- _cross-platform_
- activement développé et maintenu
- relativement léger (attentions aux plugins, comme toujours)
- plugin [_Live Share_](https://visualstudio.microsoft.com/fr/services/live-share/) pour collaborer en direct sur du code

## Deux versions possibles

VS Code peut être installé dans deux versions :

- VS Code « normal »
- VS Code « [_Insiders_](https://code.visualstudio.com/blogs/2016/05/23/evolution-of-insiders) »

La version _Insiders_ correspond tout simplement à une _Daily release_, c'est-à-dire que chaque jour, une nouvelle version de VS Code est mise à disposition, avec les dernières fonctionnalités, corrections de bugs… et nouveaux bugs non encore découverts !

Cette version _Insiders_, pas 100% fiable par nature, peut toutefois résoudre certains problèmes d'installation sur des machines récalcitrantes.

Attention, au niveau des commandes du terminal :

- VS Code « normal » => `code`
- VS Code « _Insiders_ » => `code-insiders` (possibilité de se faire un `alias`)
