# Custom Templates for PHP Getters & Setters

## Install PHP Getters & Setters

https://marketplace.visualstudio.com/items?itemName=phproberto.vscode-php-getters-setters#overview

## Create Folder

Créer un dossier où seront stocker les custom templates. N'importe où, le principal c'est de le retrouver facilement.

## Create templates

- Créer un fichier **getter.js** selon ce sample :

```Code à Copier
module.exports = (property) => `
	/**
	 * ${property.getterDescription()}
	 * @return  ${property.getType() ? property.getType() : 'mixed'}
	 */
	public function ${property.getterName()}() : ${property.getType()}
	{
		return $this->${property.getName()};
	}
`
```

- Créer un fichier **setter.js** selon ce sample :

```Code à Copier
module.exports = (property) => `
	/**
	 * ${property.setterDescription()}
	 *
	 * @param   ${property.getType() ? property.getType() : 'mixed'}  \$${property.getName()}  ${property.getDescription() ? property.getDescription() : ''}
	 *
	 * @return  self
	 */
	public function ${property.setterName()}(${property.getTypeHint() ? property.getTypeHint() + ' ' : ''}\$${property.getName()})
	{
		if (!empty(\$${property.getName()})) {
		$this->${property.getName()} = \$${property.getName()};
		}
		return $this;
	}
`
```

## Setup the path to custom templates

Dans File => Preferences => Settings => Default User Settings => PHP Getter & Setter Configuration :

**Editer la ligne :**

>// Folder where custom templates are stored
"phpGettersSetters.templatesDir": null

**La remplacer dans Users Settings par :**

>"phpGettersSetters.templatesDir": "chemin/vers/le/fichier/contenant/les/getteretsetter.js",

**Relancer Vs Code.**

## Prepare Block

Préparer les blocks de tag pour facilité la lecture du code et le bon fonctionnement des getter.js et setter.js (https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker).

Exemples de blocks de tag :

```Code à Copier
GETTER :

	/**
	* Get the value of id
	* @return  int
	*/

SETTER :

	/**
	* Set the value of id
	*
	* @param   int  $id  
	*
	* @return  self
	*/
```
