Configuration de VS Code
========================

_Trucs & astuces pour configurer son éditeur._

## _Live Share_

Il est possible de passer de 5 personnes max par session, à 30 personnes, avec la config suivante :

``` json
"liveshare.features": "experimental"
```

## React

Le support de la syntaxe JSX, souvent utilisée avec React, est intégré à l'éditeur _via_ Emmet, mais doit être activé manuellement :

``` json
"emmet.includeLanguages": {
  "javascript": "javascriptreact",
}
```
