# Layout

`Layout` signifie littéralement `Disposition`. On parle donc ici des différentes méthodes pour "placer" des éléments dans une page HTML.

## Box model

> Un élément HTML, c'est comme un oignon, ça a des couches.

On distingue 4 espaces principaux dans un élément HTML : 

![border-box](./img/border-box.png)

## Box sizing

Un élément HTML possède 2 tailles : une largeur (`width`), et une hauteur (`height`). Ces tailles peuvent être fixées à la main, ou calculées par le navigateur. 

Mais on peut aussi définir quelle est la zone qui sera prise en compte pour appliquer ou calculer ces tailles, en utilisant la propriété [box-sizing](https://developer.mozilla.org/fr/docs/Web/CSS/box-sizing)

![content-box](./img/content-box.png)


## Aller plus loin

Ressources pour comprendre la mise en page en CSS

- [MDN : Introduction au positionnement CSS](https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/La_disposition)
- [LearnLayout : Apprendre les mises en page CSS](http://fr.learnlayout.com/)
- [MDN : Le modèle de boite](https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_mod%C3%A8le_de_bo%C3%AEte)
- [CSS Reference.io : Le modèle de boite](http://cssreference.io/box-model/)
- [CSS Reference.io : Le positionnement](http://cssreference.io/positioning/)
- [MDN : Disposition multi-colonnes](https://developer.mozilla.org/fr/docs/Web/CSS/Colonnes_CSS/Utiliser_une_disposition_multi-colonnes)
- [MDN : Utilisation des flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/Disposition_des_bo%C3%AEtes_flexibles_CSS/Utilisation_des_flexbox_en_CSS)