Langages multi-paradigmatiques
==============================

De nombreux langages, notamment parmi les plus récents, offrent la possibilité d'écrire du code mélangeant plusieurs paradigmes « purs », de façon à adopter le style qui semble le plus naturel pour répondre à tel ou tel problème.

Concrètement, la plupart des langages supportent à la fois le style [impératif](../impératif) et des éléments [déclaratifs](../déclaratif), mais certains sont connus pour être particulièrement multi-paradigmatiques : C++, Lisp, Perl, Mathematica, JavaScript, Python, Scala, Haskell…

## Exemple

``` js
// JavaScript
let counter = 0; // impératif
const countClick = (evt) => { // déclaratif (via objet)
  counter++; // impératif
  console.log(counter); // impératif (via API)
};
document.addEventListener('click', countClick); // fonctionnel (via objet/impératif)
```
