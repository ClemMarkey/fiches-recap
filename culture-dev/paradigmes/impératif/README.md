Paradigme impératif
===================

* Avantages : beaucoup de liberté, un contrôle fin, optimisable
* Inconvénients : fastidieux/verbeux, éloigné de la logique humaine

## Définition

La programmation impérative consiste à communiquer des instructions explicites et précises à la machine, dans le but de réaliser une tâche. Toutes les étapes requises pour la réalisation de la tâche finale sont explicitées, et placées sous le contrôle du développeur (qui doit donc… les penser et les écrire).

> Évidemment, les langages de programmation « haut niveau », comme C, Bash, PHP ou JavaScript, ne sont pas 100% impératifs : s'ils l'étaient, ils ne formeraient qu'un seul et même langage, l'[assembleur](https://fr.wikipedia.org/wiki/Assembleur) ! Un langage impératif est donc toujours, dans une certaine mesure [déclaratif](../déclaratif) — c'est-à-dire rapporté à un certain niveau d'abstraction.

La logique impérative consiste à « fonctionner comme la machine », c'est-à-dire de façon séquentielle et explicite : on se concentre sur le chemin (_comment arriver au résultat_), plutôt que sur le résultat en lui-même. Cette posture mentale se ressent dans la syntaxe des langages impératifs, qui donnent lieux à des séries de micro-instructions (éventuellement arrangées sous la forme de classes et d'objets dans le paradigme [orienté-objet](../orienté-objet)).

## Exemple

``` bash
#!/bin/sh
echo 'number=?'
read x

if [ $x -eq 5 ]
then
	echo "five"
fi
```
