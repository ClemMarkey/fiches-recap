Paradigme fonctionnel
=====================

La programmation fonctionnelle consiste à utiliser/composer des fonctions (qui sont des mini-programmes de style procédural/[impératif](../impératif) !) dans le but de [décrire](../déclaratif) l'état de la machine.

Contrairement à l'approche impératif, on n'assigne pas de valeurs à des variables, dans le but de modifier ultérieurement ces valeurs : on les calcule sur le moment, _en fonction_ des contraintes du moment (les contraintes étant formalisées par les arguments d'une fonction). Les programmes écrits dans un style fonctionnel sont dits « sans état », ce qui résoud naturellement toute une classe de bugs !

On distingue :

- la programmation fonctionnelle « pure », basée sur des fonctions mathématiques, la théorie des groupes, la transparence référentielle, etc. (approche très puissante mais assez compliquée à appréhender) => ex. Haskell ;
- la programmation fonctionnelle « partielle », qui laisse de coté certains aspects formels pour se concentrer sur la notion de _function_ comme outil de travail => ex. JavaScript.

## Exemple

``` python
# Python
squares = map(lambda x: x * x, [0, 1, 2, 3, 4])
```
