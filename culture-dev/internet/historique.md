## Bref historique d'Internet

> En tant que _développeur d'applications_, on peut être amené à créer des applications non connectés (logiciel de bureautique, jeu vidéo single player, logiciel de création, etc.) ou des applications connectées à un réseau local (Intranet), un réseau privé (Extranet, VPN) ou public sur Internet et notamment sur le WWW.

Historiquement Internet se défini par la construction d'un réseau de télécommunication à l'échelle internationale, notamment le réseau ARPANET, USA, 1970 et le réseau Cyclades, France, ~1970 - ainsi que d'autres acteurs américains et européens. Les réseaux sont d'abord accessibles pour des enjeux commerciaux, puis les premières utilisations grand public (adresses e-mail) arrivent en 1989 aux USA (Compuserve).

Alternatives à Internet : [Minitel](https://en.wikipedia.org/wiki/Minitel) (1982-2012), [Usenet](https://en.wikipedia.org/wiki/Usenet) (1980-) rapidement raccordé à Internet.

Jusqu'à cette date de 1989 le réseau Internet fonctionne donc déjà pour échanger des informations mais reste restreint à des domaines spécifiques ou aux geeks de l'époque. A partir de ce moment-là le besoin d'échanger des informations pour le grand public de façon plus riche, à base de documents, donne naissance au WWW.

## Le World Wide Web (WWW)

> Le Web n’est qu’une des applications d’Internet, distincte d’autres applications comme le courrier électronique, la messagerie instantanée et le partage de fichiers en pair à pair. 

> Le Web a été inventé au CERN à Genève par [Sir Tim Berners-Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee) et Robert Cailliau, en 1989.

> A partir de 1990, ils développent les trois principales technologies du Web :
> - les adresses web (URL),
> - l'Hypertext Transfer Protocol (HTTP)
> - l'Hypertext Markup Language (HTML)
> 
> Pour favoriser leur utiisation, il créent également :
> - le premier navigateur web
> - un éditeur HTML
> - un serveur HTTP

### Définitions

> Le World Wide Web (WWW), littéralement la « toile (d’araignée) mondiale », communément appelé le **Web**, et parfois la Toile, est un système **hypertexte** public fonctionnant sur **Internet**. Le Web permet de consulter, avec un **navigateur**, des **pages** accessibles sur des **sites**. L'image de la toile d’araignée vient des **hyperliens** qui lient les pages web entre elles. [Wikipédia](https://fr.wikipedia.org/wiki/World_Wide_Web)

Cela introduit les notions de :
- Hypertexte et hyperlien
- Client/Serveur (Navigateur/Site)
- Contenu (page, document, **ressource**)

#### Hypertexte

Un hypertexte est un document ou un ensemble de documents contenant des informations liées entre elles par des hyperliens. Ce système permet à l'utilisateur d'aller directement à ce qui l'intéresse, à son gré, d'une façon non linéaire.

Hypertexte = "Texte augmenté" (dépassement de la contrainte du texte écrit). Si le document est une vidéo ou de l'audio, on parle d'hypermédia.

- 1910 : Paul Otlet, Bibliophoto (microfilms consultables à distance, concept)
- 1945 : Projet Memex (concept seulement)
- 1965 : Projet Xanadu (prototype) : 1965

> Quid des pigeons voyageurs, du télégramme, de la CB, du FAX ? :trollface:

L'hypertexte est utilisé dans des locigiels depuis les années 1960 par les institutions diverses, et fera son entrée chez le grand public notamment grâce au logiciel HyperCard (1987) sur Apple. Voyez ça comme un site web en local. Le jeu vidéo _Myst_ a été conçu via cette plateforme.

Il est également utilisé sur CD-ROM pour le besoin des encyclopédies notamment (_Universalis_, etc.).

Puis bien sûr, pour ce qui nous concerne, sur le web à partir de 1992, via le HTML => _HyperText Markup Language_. Le HTML permet la mise en forme des **documents** sur le Web.

#### Hyperlien

Un hyperlien ou **lien hypertexte** ou lien web ou simplement **lien**, est une référence dans un système hypertexte permettant de passer d'un document consulté à un document lié.

### Vers l'application client/serveur

Dans ce contexte il ne reste plus qu'à donner la possibilité à des _clients_ d'être mis en relation à des _serveurs_ afin d'obtenir la page, la ressource demandée. On parle plus communément de navigateur et de site web.

> [Fiche récap' Client/Serveur](https://github.com/O-clock-Alumni/fiches-recap/tree/master/culture-dev/client-serveur)

### Règles de communication

Pour communiquer, il nous faut un protocole définissant certaines règles. Les inventeurs du WWW créent donc le protocole HyperText Transfer Protocol (HTTP) afin de pouvoir mettre en application leur système. Ce protocole fonctionnera avec : 

- Client
- Requête
- Adresse IP
- Serveur
- Réponse

> [Fiche récap' Protocole](https://github.com/O-clock-Alumni/fiches-recap/tree/master/culture-dev/internet/protocole.md)
