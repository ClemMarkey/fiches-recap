## Qu'est-ce qu'Internet ?

Internet signifie _interconnected networks_, des « réseaux interconnectés » : Internet est donc un réseau de réseaux… ce qui nécessite une définition d'un réseau.

![Architecture d'Internet](../img/architecture-internet.png)

### Architecture d'Internet

Un réseau est constitué par plusieurs périphériques informatiques (téléphones, ordinateurs…), distincts les uns des autres, mais qui sont connectés et qui peuvent communiquer. Le moyen de connexion importe peu (un fil de cuivre, des ondes électromagnétiques, une fibre optique, tout ça à la fois…), le plus important va surtout être de faire en sorte que les périphériques se voient, puissent échanger de l'information, et se comprennent. Il faut donc concevoir :

- un moyen de transport de l'information (pour concrétiser le réseau) ;
- un moyen d'étiquettage des périphériques (pour savoir qui parle à qui) ;
- un langage commun (pour comprendre ce qui est raconté dans les échanges sur le réseau).

Un tel réseau commence avec deux machines. On peut créer un réseau dit local : pour un appartement, un immeuble, une entreprise. Ou bien un réseau dit de méso-échelle (meso == moyen) : pour une région ou un pays par exemple. Mais si on veut passer à l'échelle supérieure, à l'échelle mondiale en fait, c'est-à-dire autoriser _n'importe quelle machine du monde à se connecter à n'importe quelle autre_, le plus simple consiste à considérer chacuns de ces réseaux locaux/meso comme un « périphérique », et à mettre en réseau tous ces périphériques. Internet est très exactement cela : la mise en réseau de sous-réseaux.

Ainsi, quand on dit qu'on se connecte à Internet avec son ordinateur ou son téléphone, en réalité, notre périphérique n'est pas _directement_ sur Internet : il fait d'abord parti d'un « petit » réseau, celui du Fournisseur d'accès à Internet (FAI ou _ISP_ en anglais : Orange, SFR, Free, etc.) Le réseau des clients du FAI est lui connecté à Internet, c'est-à-dire connecté aux autres FAI du monde dans le réseau des réseaux.

Cette interconnexion à grande échelle mobilise des infrastructures de télécommunication bien réelles, qui sont des [enjeux stratégiques majeurs](https://www.youtube.com/watch?v=Cb7ibgRivwU) pour les pays qui les détiennent et pour la communauté internationale qui les utilise. Cette couche physique, pour ainsi dire le « vrai » réseau, est soumis à des législations complexes et des tensions régulatrices constantes.

Au-dessus, il y a une couche de transport, dite « [protocolaire](./protocole.md) », qui définit les règles d'usage d'Internet. Comment une machine doit-elle parler à une autre ? Qui prend l'initiative de démarrer une communication, de la stopper ? Comment trouver la bonne machine à contacter ? Etc. Autant de questions qui nécessite que tous les acteurs du réseau soient d'accord sur le fonctionnement d'Internet. Cette ensemble de règles est également un enjeu stratégique important, puisque sans lui, Internet ne serait qu'un énorme investissement structurel mais sans aucun usage concret.

Enfin, dernière couche d'Internet, celle des contenus et services. Les périphériques sont connectés au réseau des réseaux, savent se parler, mais que se disent-ils ? La couche applicative est la plus proche des utilisateurs finaux, et pas la moins régulée (des tensions importantes existent sur des questions aussi diverses que la propriété intellectuelle, la protection des enfants, la cybercriminalité, la liberté de contenu, l'équité de la bande passante, etc.)

### Technologies d'Internet

Internet est rendu possible par la mise en œuvre de plusieurs technologies, collectivement désignées par l'accronyme TCP/IP. Sans rentrer dans les détails techniques (cf. fiche [protocoles](./protocole) pour cela), il y a trois grands principes à connaître.

#### Principe de commutation de paquets

Pour des questions d'efficacité et de résilience, une information devant circuler sur le réseau n'est pas envoyée telle quelle. Si elle est trop grande (par rapport à une taille optimale, qui dépend de l'infrastructure de transport), elle sera découpée en plusieurs petits « paquets » ; si elle est trop petite, elle sera regroupée avec d'autres paquets. Chaque paquet de données peut prendre un chemin différent des autres pour arriver à destination — en cas de découpage de l'information, celle-ci est reconstitutée à l'arrivée. L'information est donc transformée (elle mute) pour circuler sur Internet (_packet switching_).

> En simplifiant quelque peu, on peut dire qu'un paquet est toujours constitué d'information « brute » (payload) et de méta-information (headers). L'adresse IP du périphérique émissaire (host) ainsi que l'IP du périphérique destinataire (client) sont indiqués dans les headers.

#### Principe de bout en bout

Autrement dit, principe de neutralité du réseau, dont le fonctionnement ne doit pas dépendre du type de communication qui s'effectue sur lui. Concrètement, Internet ne cherche pas à contrôler ou à s'appuyer techniquement sur les contenus et services échangés, il ne fournit que la partie transport. C'est ce principe qui permet à Internet de supporter des services très variés : pages Web, e-mail, VoIP, etc.

#### Principe de robustesse

Également appelé loi de Postel du nom de son créateur, il se résumé par la formule : « _soyez conservateur dans ce que vous faites, soyez libéral dans ce que vous acceptez des autres_ » Concrètement, Internet impose des normes techniques / protocolaires strictes pour envoyer des paquets, mais ces contraintes sont fortement relâchées coté réception des données : les utilisateurs du réseau font « ce qu'ils veulent » avec les données reçues. Ce principe est important pour assurer, notamment, une bonne interopérabilité des services entre eux, mais pose certains problèmes d'innovation et de gouvernance, ce qui amène régulièrement à des efforts (conflits ?) de [régulation protocolaire](https://tools.ietf.org/id/draft-thomson-postel-was-wrong-03.html) (par exemple, évolution et standardisation du comportement des navigateurs Web par rapport aux formats de données, aux données sécurisées, aux DRM, etc.)

### Services d'Internet

#### World Wide Web

Le Web, ou `www`, est souvent confondu avec Internet. Il ne s'agit pourtant que d'un service parmi d'autres. Il est focalisé sur l'échange d'information sous forme de texte structuré (hypertexte). Il s'agit d'un service généraliste, ce qui lui permet de transporter une grande variété d'information : texte brut bien sûr, mais aussi pages web, images, vidéos… tous ces contenus étant encodés « sous forme texte ». Les clients du réseau doivent donc savoir décoder ces contenus, sans se tromper. Ils se basent pour cela sur des standards : XML, HTML, CSS, JSON, des formats d'images comme PNG ou JPEG, etc. La correspondance entre l'hypertexte échangé et son interprétation concrète est contrôlée par les _headers_.

#### E-mail

#### VoIP

#### …

---

Pour en savoir plus, _An Introduction to Internet Governance: 7th edition_, Dr Jovan Kurbalija.
