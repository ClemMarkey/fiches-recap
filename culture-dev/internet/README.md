## Internet

On parle de « développement web », et pas de « développement internet » : pourquoi ?

Cette section explique la différence entre internet et le web, et traite d'autres questions fondamentales à propos d'internet.

* [Élements historiques](./historique.md)
* [Internet vs le Web](./internet.md)
* [Protocoles de communication](./protocole.md)