Éléments de base
================

## Un programmeur, c'est quoi ?

Un programmeur est un concepteur d'interfaces.

## Une interface, c'est quoi ?

Une interface, c'est un moyen bien spécifique d'accéder à des données qu'on ne possède pas (encore).

Une interface s'_intercale_ entre deux univers initialement séparés.

Par exemple :
- une interface graphique permet à un être humain d'accéder et/ou manipuler des données appartenant à un ordinateur ;
- une interface de commande vocale du type « OK Google ! » permet à tout système capable d'émettre du son, d'accéder et/ou manipuler des données stockées sur internet ;
- une fonction dans un programme est une interface _de programmation_ (API) entre l'endroit du programme où la fonction est utilisée, et les données auxquelles la fonction seule a accès (notamment, sa valeur de retour) ;
- une API Web est une interface entre un client (navigateur Web) et un serveur, les deux communiquant en général en HTTP ;
- etc.

La notion d'interface (API) est partout en programmation !

## Le rôle et le fonctionnement d'un programme

### Rôle

Un programme sert à anticiper et/ou automatiser le traitement de données. Si ces données sont de nature informatique, on parle de programme informatique. Mais une recette de cuisine dans un livre est un programme tout aussi valide !
 
Un programme informatique consiste en une liste d'ordres (instructions) indiquant à une machine, _quoi faire_, le plus souvent en lui précisant explicitement _comment_. La machine ne se préoccupant pas de _pourquoi_ on fait des choses !
 
Schématiquement :

```
Entrées (instructions) --- Traitement (travail de la machine) ---> Sortie (résultat visible sur les données)
```
 
### Fonctionnement

Il faut écrire des instructions précises dans un fichier texte, qui sera ensuite lu et interpreté par la machine, dans le but de produire des effets sur ses données.
 
On distingue donc :
- le code source du programme (représentation en mémoire morte : disque dur)
- l'exécution du programme (représentation en mémoire vive : RAM, _stackframe_, _at runtime_)
- la plateforme d'exécution (la machine, ses logiciels, son matériel)

### Comment un programme est-il exécuté ?

Un programme est, dans tous les cas, lu et interprêté par la machine qui souhaite l'éxécuter. Par contre, un programme peut être :
- compilé : les phases de lecture et d'interprétation sont distinctes, un code source « compilé », visible sur le disque dur, est produit à l'issue de la première phase pour optimiser la seconde. On peut compiler le programme sans chercher à l'exécuter ensuite ;
- interprété : les phases d'interprétation et d'exécution sont fusionnées ou quasi-fusionnées, il n'y a pas de code source « compilé » à proprement parler (rien sur le disque dur), et l'exécution est toujours déclenchée immédiatement à la suite de l'interprétation.

## Le rôle et la démarche d'un programeur

### Rôle

Le rôle du programmeur (aussi appelé développeur) est d'écrire les programmes informatiques. Par _écrire_, on entend :
- imaginer (concevoir théoriquement)
- coder (implémenter avec un langage de programmation)
- maintenir (résoudre les bugs, améliorer, etc.)
 
Selon qu'un programmeur met l'accent sur l'une ou l'autre de ces activités, on dira que son métier est développeur, concepteur-développeur, architecte, etc.

### Démarche

Analyser et décomposer le problème à résoudre _via_ un programme, souvent en le décomposant en plusieurs sous-problèmes plus simples (ex. étapes d'une recette de cuisine complexe).

> On distingue les étapes simples (_fais ci_, _fais ça_), les étapes conditionnelles (_fais ci seulement si truc est égal à machin_), les étapes répétitives (_fais ci tant que truc est égal à machin_).

Concevoir des algorithmes, pour organiser les étapes de résolution d'un problème de manière à produire le résultat final le plus rapidement possible, et/ou avec le plus de fiabilité possible, et/ou d'une manière la plus élégante possible, etc.

> L'algorithmique est indépendante de l'implémentation (ie. du langage de programmation utilisé).
