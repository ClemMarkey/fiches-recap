# Être développeur, késako ?

En tant que dev, je fais l'interface entre un besoin (domaine humain) et une solution technique (domaine machine). Concrètement, je crée des interfaces (_data proxy_) : des moyens d'accès à des données/comportements que je ne possède pas initiallement (ex. un navigateur internet permet d'accéder à mes informations de compte utilisateur grâce à un formulaire ; une télécommande à bouton permet de contrôler un robot-démineur grâce à des ondes radio ; etc.)

Les interfaces humain <-> données que je crée reflètent des « visions du monde » : pour une interface donnée à créer (ex. un site de e-commerce), j'en choisis la structure/modélisation (ex. MVC) et une implémentation particulière (ex. implémenter MVC en JavaScript, PHP, Python, C++, etc.) Ce faisant, je détermine le bon niveau d'abstraction (= cacher la complexité d'une communication derrière une interface agréable et efficace) pour mon problème. Je dois donc effectuer un travail de modélisation (simplification du réel), dit de conception.

Le trio _choix modélisation_ + _choix abstraction_ + _choix implémentation_ me permet de piocher, parmi tous ceux existants ou que je pourrais imaginer, les « bons » outils (adaptés à ce que je dois faire). Outils que j'apprends à maîtriser, pour faire « vite et bien » sous des contraintes temps / budget / qualité de service.

## Des notions transverses

* [Client/serveur](./client-serveur) : modèle fractal, importance du rôle (point de vue, qui dépend du moment / lieu / objectif)
* Méta-données : par exemple, du texte HTML est dit _self-documenting_ car porteur d'informations à propos de l'information (contenu du `<body>`), dans le but d'aider le consommateur du contenu en question à l'interpréter correctement
* Des systèmes qui doivent interagir s'appuient sur une interface de communication, un protocole de communication et une couche de transport de la communication (ex. respectivement API Web, protocole HTTP et réseau internet ; oreille humaine + cerveau, langue française et ondes sonores)
* etc.

Ces notions sont transverses, car on les retrouve dans tous les domaines de l'informatique, aussi bien coté développement que coté gestion de projet. Ce sont des briques cognitives qu'une personne faisant de la conception-développement utilise au quotidien pour travailler efficacement sur des projets de natures très variées.

## Gestion de projet

Une [méthodologie de gestion de projet](../gestion-projet/gestion-projet.md) permet de relier un besoin (humain) à un objectif (technique). La méthodologie suivie a des impacts au-delà du simple code : relations humaines (avec le client, en interne dans l'équipe, etc.), gestion des risques (budgétaires, temporels, RH), etc.

## Stack

![stack](../img/stack-tech.jpg)
