# Apprendre à apprendre

> _«The only person who is educated is the one who has learned how to learn and change.»_
>
> **Carl Rogers**

Il est difficile pour certains de concevoir qu'on puisse apprendre par soi-même. C'est une expression dont le sens est cependant exagéré. Apprendre par soi-même serait finalement le fait d'apprendre de ses erreurs et de ne se baser que sur la documentation. C'est aussi le choix de prendre un livre ou choisir un tutoriel, un article ou un cours en ligne. Mais dès lors qu'on apprend à partir des explications d'autres personnes, a-t-on vraiment appris seul ? Vous avez quatre heures !
Plus sérieusement, il faut reconnaître qu'on a toujours quelque chose à apprendre des autres comme on a toujours quelque chose à leur apprendre. L'humilité qu'on vous demande d'avoir en donnant cet argument est accentué par le fait qu'on n'apprend pas tous de la même manière et qu'il faut parfois, même aux meilleurs, se faire expliquer trois fois les choses de maniêres différentes pour bien intégrer une notion.

Il faut ensuite accepter que le métier évolue. On pourrait dire, pour plaisanter, qu'il change toutes les heures. Ça ne veut pas dire que ce que vous apprenez aujourd'hui ne sera plus valable dans quelques années. Ça veut seulement dire qu'il faut vous tenir à jour car les techniques s'améliorent et les solutions évolues. Il y a longtemps on reprochait beaucoup de choses à PHP, disant que ce n'était pas un langage assez robuste pour répondre aux besoins des entreprises. On observe bien l'inverse aujourd'hui !
Voici donc quelques conseils pour continuer d'apprendre en tant que développeur.

```
Martine est développeuse junior, elle sort de l'ècole O'clock. Elle n'est pas allé à la plage pendant 5 mois pour se former de chez elle. Après quelques mois à consolider ses connaissances pendant sa première expérience en entreprise, Martine aimerait bien apprendre à coder en python.
```


## Accepter qu'on ne comprend pas tout même si ça marche

On apprend parfois par soi-même, on arrive d'ailleurs à ses fins sans nécessairement tout comprendre, sans tout savoir. On comprend parfois ce que ça fait sans comprendre comment la machine exécute exactement les choses, c'est l'effet _boîte noire magique_. On peut s'en contenter dans un premier temps mais il faut rester curieux. On peut apprendre à utiliser un framework sans comprendre les détails, ça sert justement à ça. Cependant il sera intéressant d'aller lire code pour tentere de comprendre dans quel ordre les choses s'exécutent. Cette connaissance peut vous éviter la perte de cheveux, la crise de nerfs, la perte de temps et la baisse de confiance en soi. Adoptez-la, elle est gratuite !

## Avoir le bon état d'esprit

On ne le répète jamais assez, l'humilité est la clé : on ne sait pas tout donc on est prêt à apprendre et se remettre en question !
Il faut donc trouver l'équilibre entre savoir qu'on connait et remettre en question ses connaissances.

## Trouver des ressources

On ne pourrait se contenter d'une seule source d'information pour se tenir à jour, en voici donc quelques-unes.

```
Martine avait l'embarras du choix...
```

###  Google et ses différents résultats

Évitez un maximum de laisser une question en suspens. Une petite recherche sur Google peut vous aider à mieux comprendre. Parmi tous les résultats, privilégiez les documentations ou les sites bien connus. Vous les connaissez déjà sans qu'on ne leur fasse de promotion dans ce document. C'est ensuite à vous, à travers votre expérience et les conseils de vos confréres, d'ajouter certains blogs ou certains forums à votre liste de confiance. La recherche a cependant ses limites, il vous faudra aussi continuer d'apprendre avec d'autres sources.

```
Peut-être que les tutos en ligne ne suffisaient pas. Martine a compris à quoi ressemble python, ce qu'on peut en faire et qui sont ses développeurs. Tiens en plus c'est utilisé par de grandes entreprises, comme PHP. On y trouverait aussi de nombreuses librairies pour faire du machine learning. Martine avait hâte d'approfondir le sujet !
```

### Les livres

On nous demande parfois de conseiller des livres, chaque membre de l'école peut avoir ses préférences. De nouveaux livres techniques sortent tous les mois et ce sera à vous de les départager. Mais d'abord, pourquoi lire un livre technique ?

Il y a plusieurs types de livres techniques. Certains abordent un langage de programmation, d'autres l'approfondissent, d'autres encore explorent un framework précise. On trouve aussi des livres qui parle plus généralement de sécurité, de référencement ou de bonnes pratiques de développement. De plus, chaque livre est dédié à un public différent. Ils s'adressent parfois à de fins connaisseurs qui veulent savoir comment apprivoiser un aspect technique ou un langage. Ils sont également parfois dédiés à un lectorat de purs débutants que vous n'êtes plus vraiment.

*Pour les choisir*, la solution la plus simple reste de lire la table des matières. C'est un conseil basique mais les éditeurs travaillent pour que cette partie indique clairement si on tient le livre qu'on cherche ou non. Pour compléter, il faut également lire le paragraphe ou le chapitre qui s'appelle souvent "À qui s'adresse ce livre ?" Ensuite, à vous de faire confiance à l'éditeur ou à l'auteur. Un paragraphe est souvent déidé à l'auteur au début du livre, bien que cette information ne guidera probablement pas votre choix. Les éditeurs les plus répandus en France sont Eyrolles, Dunod, ENI, First Editions et O'reilly. Certains éditeurs s'adressent surtout à des initiés, comme O'reilly, là où d'autres ont pour vocation de rendre la plupart de leurs collection accessibles à tous les moldus, comme Eyrolles. Certains livres n'existent qu'en anglais et mettent longtemps avant d'être traduits, Là aussi un bon niveau d'anglais techniques sera un avantage.

*Pour les trouver* en librairie, il est possible de les commander en magasin. Un livre qui n'est pas présent en magasin reste accessible sur commande auprès du fournisseur de votre libraire. Les grandes surfaces (FNAC, Cultura, Gibert Joseph,…) peuvent obtenir tout ce qui est distribué en France car ils ont des contrats avec tous les distributeurs. Les petites librairies ne peuvent pas tout commander, il leur arrive de ne jamais travailler avec certains distributeurs, mais il sera toujours intéressant de leur demander. Sachez qu'en France, depuis a loi Lang, le prix du livre est unique et déterminé par l'éditeur. Les libraires n'ont pas le droit de solder un livre. Le rabais maximum possible est de 5%, donc le prix sera le même, qu'importe le commerçant. Le prix unique du livre s'applique également pour Amazon, sauf pour les livres importés.

*Pour les consulter* gratuitement ou presque, il est possible de les emprunter à une bibliothèque. Oubliez la médiathèque, elle s'adresse au grand public et ne propose pas de livres destinés à des professionnels. Cependant, les bibliothèques universitaires (BU) sont ouvertes à tous. Ce service public porte bien son nom, ses portes sont ouvertes aux curieux autant qu'aux étudiants et aux chercheurs. Il est possible d'y consulter tous les livres que vous souhaitez, dans la mesure où ils sont disponibles. Pour ce qui est de les emprunter, il faut voir le détail auprès de chaque BU. Il est régulier qu'elles proposent une inscription aux personnes externes à l'université, payante ou non. Cette inscription est souvent payante pour la plupart des gens et gratuite ou à prix réduit pour les personnes en formation continue et les demandeurs d'emploi. Ce n'est pas le cas partout mais renseignez-vous, une source de savoir inépuisable et régulièrement renouvelée vous y attend ! Le site de la BU la plus proche devrait vous permettre de rechercher gratuitement parmi son catalogue.

```
Pour choisir un bon livre et enfin apprendre à faire du python, Martine avait un choix bien immense. Elle ne savait pas exactement ce qu'elle voulait apprendre, juste qu'elle voulait comprendre le python pour ajouter une corde à son arc. Elle avait cependant conscience qu'il lui faudrait pratiquer pour arriver à ses fins, avec un projet personnel ou un nouveau projet dans son entreprise.

Il lui fallait donc regarder ce que chaque livre intéressant semblait proposer. L'un d'eux l'intéressait beaucoup mais elle avait le sentiment qu'après l'avoir étudié, ce livre ne ferait que prendre la poussière et devenir rapidement obsolète. Elle se décida à l'emprunter à la BU du coin. Après tout elle voulait un guide pour se mettre à python. Elle se demande encore comment manipuler un tableau et afficher du HTML grâce à ce langage.
```

## Suivre l'actualité

Non seulement les langages informatiques que vous utilisez évoluent mais les outils comme Boostrap, jQuery, et les technologies dont les noms désignent les spécialités chez O'clock évoluent aussi. En plus, de nouveaux langages, de nouveaux frameworks et de nouvelles librairies apparaissent régulièrement. Pour suivre tout ça, il faut faire sa [veille technologique](veille-technologique.md).

```
Après la lecture (à la plage !) et quelques heures d'expériences avec python, Martine comprend un peu mieux pourquoi PHP est tout aussi bien et elle sait enfin manipuler Django, le framework python qui ressemble à Symfony pour PHP. C'est assez rudimentaire mais elle a pris quelques repères avec ce framework. Avec toutes ces informations dans sa tềte, difficile d'être sûre de suivre le fil ! Et si une nouvelle propriété CSS était à la mode mais qu'elle était encore la dernière à le savoir ? Et si PHP7.4 allait changer la face du web ou faire fuir les développeurs ?
```

## Développer pour soi, en dehors du travail

Ce n'est pas une obligation mais on vous encourage grandement à consacrer un peu de votre temps libre à de petits projets, ou même de plus gros. Il n'est pas nécessaire d'arriver au bout d'une V1.0 complète, il s'agit surtout d'avoir un cadre sans contrainte dans lequel vous pouvez tester une nouvelle technique, mettre en pratique ce que vous connaissez déjà et vous lancer un défi. Eh oui, on vous le répête encore, le meilleur moyen d'apprendre est de pratiquer !

La première idée que vous aurez sera probablement de faire un blog ou un portfolio que vous metterez en production. C'est une belle occasion de revoir les bases.
Cependant ce n'est pas suffisant pour mettre en pratique des concepts plus complexe. Inspirez-vous d'applications qui existent déjà ou de ce que vous souhaiteriez automatiser. Si vous cherchez une idée, vous pouvez aussi vous inspirer des projets qui ont été discuté lors de l'apothéose, voire peut-être d'un projet qui a été réalisé par un autre groupe mais que vous referez à votre façon.

Attention, il n'y a aucune pression ici. L'objectif est de se faire plaisir. Lorsqu'on réalise un projet pour soi, si on y prend goût, on y reviendra à son rythme. Si vous abandonnez votre projet, c'est peut-être que vous avez exploré ce que vous souhaitiez. Il n'est pas toujours nécessaire d'amener un projet à son terme. Faire des pauses dans un projet personnel vous permet aussi d'avoir du recul sur ce vous souhaitez en faire.

## Meetups et associations locales

Selon où vous vivez, il pourrait exister des groupes de réflexion et de partage des connaissances. Ces gens se rencontrent en vrai pour discuter de toutes sortes de sujets comme Symfony, le PHP, la sécurité, Docker et tant d'autres… On en trouve sur [Meetup](https://www.meetup.com/fr-FR/) mais aussi auprès d'associations comme les antennes locales de l'[AFUP](https://afup.org/home). De manière plus générale vous pouvez aussi vous pencher sur les événements organisés dans les lieux des coworkings et les tiers lieux. Rencontrer des gens sur votre territoire sera d'ailleurs un bon moyen d'agrandir votre réseau.

## Savoir aussi ne pas apprendre pendant quelques temps

Tous ces conseils sont bien beaux mais on n'est pas obligés d'apprendre tout le temps. Ce fameux syndrome de l'imposteur pourrait vous faire culpabiliser de ne pas avoir été assidu dans votre veille ou dans le suivi d'un petit projet. L'énergie physique et psychologique qu'on peut utiliser dans une journée est limitée et il faut l'accepter. N'allez surtout pas vous mettre la pression inutile de ne pas avoir _«eu le temps»_ d'avoir lu tout le web trois fois et de ne pas avoir réalisés les huit projets que vous trottent dans la tête depuis un moment.

Il faut faire avec, le développement peut s'étudier à l'infini mais il faut faire des choix et, parfois, faire une pause dans son évolution sera une bonne chose pour se concentrer sur sa vie privée et sur son travail.

```
Martine avait bien mal à la tête aprês avoir lu cette fiche, ça faisait beaucoup d'information. Au moins elle savait à peu près comment se mettre à Python toute seule.

La route à prendre n'était pas tracée d'avance. Elle se décida donc à créer la sienne. Et pour être plus efficace, elle s'entoura de dévelopeurs qui auraient pu la conseiller. Pour Martine, le python ne faisait que commencer !
```
