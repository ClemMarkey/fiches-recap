# Comptes Twitter à suivre

Note : ceci est un document collaboratif. Si vous voyez un compte à ajouter, ou si vous voulez proposer votre propre compte dans la seconde liste, n'hésitez pas, faites [une pull request](https://github.com/O-clock-Alumni/fiches-recap/blob/master/MAJ-fiche.md).

## Quelques comptes à suivre sur le dev, ou le web en général

- https://twitter.com/alsacreations
- https://twitter.com/umaar
- https://twitter.com/frenchweb
- https://twitter.com/css
- https://twitter.com/github
- https://twitter.com/mozilla
- https://twitter.com/FrontEndDaily
- https://twitter.com/CodeWisdom
- https://twitter.com/opquast
- https://twitter.com/commitStrip_fr
- https://twitter.com/lesjoiesducode
- https://twitter.com/grafikart_fr
- https://twitter.com/MissionSoNum
- https://twitter.com/Souvir

## Quelques comptes personnels O'clockiens :)

- https://twitter.com/Oclock_io
- https://twitter.com/webdif
- https://twitter.com/dariospagnolo
- https://twitter.com/CelineBe_33
- https://twitter.com/blackrhumbar
- https://twitter.com/khammileon
- https://twitter.com/CyrielMartin
- https://twitter.com/HaCoBa_Laure
- https://twitter.com/dakkeyras
- https://twitter.com/AdrienSergent2
- https://twitter.com/PrescilliaDSD
- https://twitter.com/KatKoding
- https://twitter.com/karine_dev
- https://twitter.com/Scttpr
- https://twitter.com/Gorski_anthony
