# Comment faire de la "veille" ?

Suivre l'actualité s'appelle aussi «faire sa veille». Il s'agit de se tenir au courant des dernières nouveautés. Ça ne veut pas dire qu'il faut maitriser tous les sujets qui vous sont proposés, juste qu'il faut vous donner les moyens de voir au moins les titres des dernières informations. Vous ne pourrez pas tout suivre et ce n'est pas grave.

## Flux RSS

### Défintion

Ça vous dit quelque chose le *RSS (Really Simple Syndication)* ? Par extension on parle souvent du flux RSS pour parler du fichier sur le site d'un média qui contient, au format XML, les informations sur les dernières publications d'un site ou d'un magazine. Ce n'est pas à proprement parler un flux, c'est à dire que ce n'est pas un «stream» de données obtenues en temps réel. On réactualise régulièrement le flux en rechargeant ce fichier RSS.
C'est un format standard pour transmettre des actualités. Le principe est de «s'abonner» à un flux en l'ajoutant à un logiciel qui ira voir régulièrement les nouveautés dans ce flux.

*Ça sert à* importer facilement, dans un logiciel qui comprend le format RSS, les dernières publications liées à ce flux. Ce logiciel va donc nous permettre de lister les articles non lus. Comme ça, inutile d'allez voir 67 sites tous les matins, on reçoit toutes les nouveautés disponibles au même endroit. Si on n'a pas eu le temps de tout lire, les articles non lus restent visibles pour une prochaine fois.

*Exemples* de flux RSS (utiles ou non) :

 - Liste des derniers articles parus sur Numerama : https://www.numerama.com/rss/news.rss
 - Liste des modifications faites sur la page d'accueil de Wikipedia France : https://fr.wikipedia.org/w/index.php?title=Wikip%C3%A9dia:Accueil_principal&feed=atom&action=history
 - Liste des versions de Mastodon sur son dépôt Github, pour savoir quand faire ses mises à jour : https://github.com/tootsuite/mastodon/releases.atom

### Lecteurs RSS

Vous vous souvenez que vous devrez vous faire votre liste de sites de confiance ? On y vient, on mettra cette liste dans une app dédiée !
Bien sûr en cherchant «rss reader» sur votre moteur de recherche préféré vous trouverez plein de résultats possibles. En voici quand même deux bien connus : [Feedly](https://feedly.com) et [The Old Reader](https://theoldreader.com/). Les deux ont une offre gratuite qui devrait être suffisante. Ils ont également une application mobile qui vous permettra de faire votre veille sur n'importe quel appareil. Pour s'abonner on peut coller le lien d'un flux RSS, qu'on trouvera directement dans le code source HTML d'une page. On peut également le trouver grâce à ce [logo](https://fontawesome.com/icons/rss?style=solid) ou [celui-ci](https://fontawesome.com/icons/rss-square?style=solid), il est souvent orange.

### Quels flux choisir ?

C'est à vous de voir. Mettez-y tous les blogs que vous voulez mais trop en mettre serait contre productif. Choisissez les blogs qui vous semblent de qualité. Faites bien la différence entre un blog de dev et un site d'information générique autour du numérique. Les deux sont intéressants et n'avoir que l'un ou que l'autre pourrait nuire à votre culture générale de développeur.

Il existe aussi des flux RSS qui référencent les dernières parutions d'un podcast. L'application [Podcast Addict](https://play.google.com/store/apps/details?id=com.bambuna.podcastaddict&hl=fr) exploite bien ces flux. Elle propose même un catalogue de podcasts auxquels vous abonner.

Il est aussi possible d'ajouter des chaines YouTube dans votre lecteur RSS préféré mais la manœuvre est devenu un peu plus complexe de nos jours.

## Mettre des liens de côté

```
Trop d'infos à suivre, Martine n'avait pas le temps tous les jours. Elle recevait aussi des liens très intéressants de ses collègues mais elle avait toujorus 67 onglets constamment ouverts pour «les lire plus tard» et elle n'avait jamais le temps de les lire ! Martine se sentait souvent submergée…
```

Il existe des applications qui permettent de stocker des liens, un peu comme des favoris. Le but est ici de mettre un lien en lecture pour plus tard. Ces applications ont une version web et une application. Elles vont, dans la mesure du possible, conserver localement la balise `<article>` pour vous proposer une lecture un peu plus zen de ces pages que vous avez mises de côté.
L'avantage avec cet outil c'est qu'avec un peu de recul, des mois plus tard, vous ferez certainement le tri et abandonnerez la lecture de certains article. Et ce n'est pas grave, c'est même sain !

L'application la plus connue est [Pocket](https://getpocket.com/), il s'agit d'un service privée. Son penchant libre est [Wallabag](https://wallabag.org/fr). En plus d'être un logiciel libre, Wallabag est basé sur Symfony. Vous pouvez l'installer sur n'importe quel serveur qui vous appartient, ou vous limitez à l'utiliser localement.

## Les réseaux sociaux

Avec les flux RSS on garde souvent les même sources, le même genre d'articles publiés par les même sites. Utiliser les réseaux sociaux vous permettra de profiter de ce que d'autres développeurs partagent. Lorsqu'ils font leur veille, ils partagent les articles intéressants. Lors de la S00, on vous a parlé de Twitter. C'est un bon moyen de compléter votre veille, en plus d'agrandir votre réseau professionnel.

Fouillez les réseaux, il y en a d'autres en ligne. Il semblerait qu'on trouve de nombreux développeurs sur Twitter et sur Mastodon. On a d'ailleurs une [liste collaborative de comptes à suivre](https://github.com/O-clock-Alumni/fiches-recap/blob/master/culture-dev/etre-developpeur/comptes-twitter.md) dans ces fiches récap. Il n'y a rien de mieux à faire que de construire votre réseau vous même, choisir qui vous suivez en fonction de l'intérêt que vous portez à leurs publications. Peut-être que vos contacts sur Facebook ou LinkedIn partagent aussi des articles intéressants, à vous d'en juger.

## Les magazines en papier

On vous a parlé de toutes ces sources d'information mais les vieilles méthodes non numériques fonctionnent aussi ! On ne saurait vous conseiller un titre plutôt qu'un autre mais on tenait à vous rappeler que ça existe.
