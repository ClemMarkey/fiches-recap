Modèle client/serveur
=====================

L'informatique consiste essentiellement à faire circuler de l'information, entre différentes entités séparées spatialement et/ou temporellement.

Pour ce faire, il faut initier des requêtes (_j'ai besoin de telle info, [tout de suite|dans 5m|dès que possible]_), et attendre la réponse du système qui possède l'information recherchée. Ce dialogue de question/réponse est à la base du modèle de programmation client/serveur.

Prenons un exemple : quand on ouvre son navigateur internet et qu'on tape l'adresse d'un site web ou qu'on clique sur un lien, on débute une communication avec un serveur web. Un serveur web est responsable, dans le dialogue qui s'établit entre un client et lui, de fournir une réponse à une requête initiée par le client. Cette réponse, toujours du texte, peut représenter une page web (.html), une image (.jpg), une vidéo (.mp4), des données brutes (.txt), etc. La communication peut être cryptée ou pas, rapide ou pas, directe ou pas, couronnée de succès ou pas ; mais dans tous les cas, les rôles respectifs du client et du serveur sont fixés à l'avance :

- le client initie une demande (_request_)
- le serveur répond à cette demande (_response_)

Ces responsabilités n'ont rien à voir avec le web : le modèle client/serveur s'applique à tous types de programmes.

- [Cas d'un serveur web](./serveur-web.md)
- [Exemples d'utilisation](./exemples.md)
- [Avantages & inconvénient](./bilan.md)
