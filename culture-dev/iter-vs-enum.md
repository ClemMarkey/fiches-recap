# Énumérable versus Itérable

Deux gros mots qui cachent des notions simples mais surtout fondamentales, qu'on retrouve partout sans même s'en rendre compte.

Les termes _énumérable_ et _itérable_ désignent 2 caractéristiques applicables aux _collections_.

### Et qu'est-ce qu'une collection ?

C'est un ensemble d'informations.
- En JavaScript, un Array ou un Object est une collection.
- En PHP, un tableau, qu'il soit associatif ou numérique, est une collection.
- En Python, un dictionnaire est une collection.
- Un fichier HTML est une collection de balises.
- Un fichier CSS est une collection de styles.
- Une table de BDD est une collection d'enregistrements.
- Cette liste d'exemples est une collection :smirk:

Et on peut continuer encore très longtemps comme ça :grin: Mais on va plutôt se pencher sur ce qui fait qu'une collection est énumérable

## Énumérable

Une collection est énumérable si on peut... énumérer ses éléments.

- un tableau associatif PHP est énumérable.
- un Object en JS/JSON est énumérable.
- l'ensemble des **attributs d'un élément HTML** est énumérable.

Si on fait la liste des personnages principaux de Kaamelott (ceux qui apparaissent dans plus de 100 épisodes), ça donne :

> Arthur, Perceval, Karadoc, Léodagan, Lancelot, Bohort, le Père Blaise et Guenièvre. 

C'est une collection énumérable.

Mais cette même liste de personnages principaux n'est pas _itérable_. Pourquoi ? Car si on l'énumère comme ceci :

> Karadoc, Léodagan, Guenièvre, Lancelot, Arthur, Perceval, Bohort et le Père Blaise.

On a exactement la même liste, l'ordre n'a aucune importance.

## Itérable

Vous l'aurez compris, une collection itérable est une collection énumérable dont **l'ordre d'énumération est important**. (pas juste important d'ailleurs, mais unique, fondamental, primordial, pensez au précieux de Gollum).

Si on liste le podium du 100m femmes aux JO de 2016 à Rio, ça donne :

> Elaine Thompson, Tori Bowie et Shelly-Ann Fraser-Pryce

Sans même vérifier, vous avez déduit qu'Elaine Thompson a gagné la médaille d'or. Car l'ordre est important. Ici, il est implicite : quand on parle de _podium_, on s'attend à avoir le trio de tête d'une épreuve en partant du 1er.

Dans le code, l'ordre d'une collection itérable est explicite : c'est parce qu'on place les éléments dans cet ordre, qu'on devra les énumérer dans ce même ordre.

- un tableau numérique PHP est itérable.
- un Array en JS/JSON est itérable.
- l'ensemble des **éléments HTML d'un document** est itérable.

Pour ces trois exemples, modifier **l'ordre des éléments** de la collection modifierait **le sens** de la collection.

### Coup de pouce mnémotechnique

Prenons cet exemple :

```html
<a href="http://republiquedesmangues.fr/" id="callout" class="important">Mobilisez-vous !</a>
<p class="description">La mangue a besoin de vous ! Et de vos proches, n'hésitez pas à faire circuler l'information !</p>
```

Les attributs du lien sont énumérables. On peut aussi les écrire comme ça :

```html
<a class="important" id="callout" href="http://republiquedesmangues.fr/">Mobilisez-vous !</a>
<!-- OU -->
<a id="callout" href="http://republiquedesmangues.fr/" class="important">Mobilisez-vous !</a>
<!-- OU -->
<a id="callout" class="important" href="http://republiquedesmangues.fr/">Mobilisez-vous !</a>
```

Ça ne change absolument rien.

Par contre, les éléments eux-mêmes forment une collection itérable, car écrit comme ceci :

```html
<p class="description">La mangue a besoin de vous ! Et de vos proches, n'hésitez pas à faire circuler l'information !</p>
<a href="http://republiquedesmangues.fr/" id="callout" class="important">Mobilisez-vous !</a>
```

Le document n'a plus le même sens :wink: Et même si, à grands renforts de CSS, vous inversez les deux éléments pour faire quand même apparaître le lien en premier, vous n'aurez modifié que **la présentation** du document, pas son sens (mais c'est un autre débat).

## Conclusion

### L'un n'empêche pas l'autre

Tout ce qui est _itérable_ est _énumérable_, car la seule caractéristique d'un _énumérable_ est finalement de pouvoir l'énumérer :man_shrugging: L'_importance ou non_ de l'ordre vient du _caractère itérable ou non_ de la collection.

Et par contre, tout ce qui est _énumérable_ n'est pas forcément _itérable_.

### Et à quoi ça sert de savoir ça ?

Ce n'est pas dans le dossier culture dév pour rien. C'est de la culture, ce n'est jamais inutile :wink: Exemple concret en JS :

```javascript
var elaine = {
    fullname: 'Elaine Thompson',
    birthdate: '1992/06/28',
    weight: 57,
    height: 167,
    discipline : 'sprint'
}
```

Déclarez dans la console de votre navigateur l'objet littéral suivant. Puis écrivez simplement `elaine` pour que la console retrouve l'objet.

![affichage en console](./img/console-display.png)

La console réstitue d'abord l'objet tel qu'il a été déclaré : c'est juste pour vous aider à identifier que c'est bien celui que vous cherchiez. Mais en l'explorant (d'un clic sur la petite flèche à gauche), vous découvrez que la console n'a pas retenu son ordre, qui n'a aucune importance ici : elle liste ses propriétés dans l'ordre alphabétique (parce qu'il faut bien en choisir un :man_shrugging:).

Tentez maintenant d'énumérer les propriétés de l'objet avec la boucle `for...of`, réservée aux _itérables_.

![for...of en console](./img/console-forof.png)

Ici, vous demandez explicitement à boucler sur l'objet `elaine` dans son ordre "logique" : la console vous indique qu'il n'y en a pas. Vous avez là la confirmation qu'un Object est _énumérable_ mais pas _itérable_ :nerd_face: