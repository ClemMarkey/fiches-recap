# Liste des différents types de _case_

Un _case_ est une convention d'écriture, utilisée en informatique pour nommer les variables, fonctions, classes, etc. Un _case_ permet de rapidement identifier un type de variable, simplement en fonction de son écriture (c'est un indice visuel qui apporte du confort à la lecture d'un programme).

> Cette fiche récap n'est en aucun cas exhaustive, ni dans les types de _case_, ni dans les exemples d'utilisations !

## Le plus connu: le _camel case_

Le principe du _camel case_ (souvent écrit CamelCase ou camelCase) est de supprimer les espaces entre les mots et de mettre une majuscule sur chacun.

La dénommination _camel case_ provient d'une analogie avec la forme des bosses du chameau (_camel_ en anglais) :  

![](img/camel-case.png)
© wikipedia

Le terme regroupe en fait 2 types de _case_ différents:
* [lowerCamelCase](#le-lower-camel-case)
* [UpperCamelCase](#le-upper-camel-case-ou-pascal-case)

## Le _lower camel case_

Le _lower camel case_ (souvent écrit lowerCamelCase) est un type de _camel case_ dont la première lettre est toujours en minuscule.

### Exemples
* mon super nom de variable :arrow_right: `monSuperNomDeVariable`
* user list :arrow_right: `userList`

### Utilisations
* en **PHP**, pour les variables contenant des objets (instances de classe) :
  * `$productModel = new Product();`
* pour les fonctions :
  * **JS** :arrow_right: `getPost: function(evt) { … }`
  * **PHP** :arrow_right: `public function getAll()`

## Le _upper camel case_, aussi appelé _pascal case_

Le _upper camel case_ (souvent écrit UpperCamelCase), également appelé _pascal case_ (souvent écrit PascalCase), est un type de _camel case_ dont la première lettre est toujours en majuscule.

### Exemples
* mon super nom de variable :arrow_right: `MonSuperNomDeVariable`
* user list :arrow_right: `UserList`

### Utilisation
* en **PHP** comme en **JS** pour la déclaration des classes :
  * `class MainController`
* en **React** pour les noms de composants personnalisés :
  * `<TaskList />`

## Le _snake case_

Le _snake case_ consiste à tout écrire en minuscule et remplacer les espaces avec des _underscores_ : \_.

### Exemples
* mon super nom de variable :arrow_right: `mon_super_nom_de_variable`
* user list :arrow_right: `user_list`

### Utilisations
* utilisé en **PHP** comme en **JS** pour nommer les variables qui contiennent tout, sauf des objets :
  * en **PHP** :arrow_right: `$array_users = array();`
  * en **JS** :arrow_right: `var array_users = ['ben', 'jc', 'lucie'];`

## Le _screaming snake case_

Le _screaming snake case_ est un dérivé du _snake case_. Le principe est le même, sauf que l'intégralité des caractères sont en majuscule (_caps lock_).

### Exemples
* mon super nom de variable :arrow_right: `MON_SUPER_NOM_DE_VARIABLE`
* user list :arrow_right: `USER_LIST`

### Utilisation
* en général utilisé pour le nommage des constantes :
  * `define('BASE_URL', 'http://localhost/oclock/');`
