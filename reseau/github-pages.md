# GitHub Pages

GitHub Pages vous permet de stocker des pages _statiques_ (HTML, CSS, JS) directement depuis un dépôt GitHub. Modifiez, pushez, vos changements sont pris en compte.

Cette doc est un condensé en français de [GitHub Pages](https://pages.github.com/).

> :bulb: **Pour déployer une application React** sur Github Pages _sans index.html_ [suivez ce lien](https://github.com/gitname/react-gh-pages/tree/master).

## Comment ça marche ?

Vous disposez d'un unique site associé à votre compte GitHub et d'un nombre illimité de sites par dépôt.

Cela se traduit par :
- Un site accessible depuis votre nom d'utilisateur : `https://username.github.io`
- Un nombre illimité de sites accessibles par projet, depuis votre nom d'utilisateur et le nom du dépôt : `http://username.github.io/repository`

Simple non :slightly_smiling_face: ?

### Depuis votre nom d'utilisateur

#### 1. Créez un dépôt (ou renommez un dépôt existant)

Depuis votre compte utilisateur qui se nomme _username.github.io_, où _username_ est votre nom d'utilisateur.

> Attention cela doit correspondre exactement à votre nom d'utilisateur.

Si votre dépôt est vide, il va falloir y ajouter du contenu, sinon vous pouvez tout de suite y accéder via `https://username.github.io`

#### 2. Clonez le dépôt

`git clone https://github.com/username/username.github.io`

#### 3. Ajoutez du contenu

```
cd username.github.io
atom index.html`
```

> Ajoutez du contenu HTML.

#### 4. Pushez le contenu

```
git add --all
git commit -m "initial commit"
git push -u origin master
```

#### 5. C'est tout !

Accédez à votre site via `https://username.github.io`

### Depuis n'importe quel dépôt

GitHub vous propose des thèmes graphiques, si cela vous intéresse [rendez-vous sur le site](https://pages.github.com/) pour voir comment faire.

#### 1. Pour créer un site _from scratch_

Rendez-vous sur un dépôt existant ou créez-en un nouveau depuis votre compte, par exemple `mon-projet`.

#### 2. Créez du contenu

Depuis l'interface GitHub (ou via le terminal comme vu précédemment), créez un fichier index.html. Ajoutez-y du contenu.

#### 3. Commitez le fichier

Ou add/commit/push si vous travaillez depuis le terminal.

#### 4. Définissez GitHub Pages

Depuis les _Settings_ de votre dépôt, allez jusqu'à la section _GitHub Pages_. Dans **Source** sélectionnez _master branch_ et cliquez sur le bouton **Save**.

#### 5. C'est tout !

Accédez à votre site via `http://username.github.io/mon-projet`

## En savoir plus

- [GitHub Pages](https://pages.github.com/)
- [Deploying a React App* to GitHub Pages](https://github.com/gitname/react-gh-pages/tree/master)
