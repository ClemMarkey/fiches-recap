# Le protocole HTTP (HyperText Transfer Protocol )

## HTTP: request/response

HTTP est le protocole de communication entre le client et le serveur. Le client envoie une *requête*, le serveur renvoie une *réponse*. Requête et réponse sont formées d'en-têtes (headers) et d'un contenu (body).

### Requête HTTP

En-têtes de requête :  

- Méthode
- URL
- Version du protocole HTTP
- Domaine de destination (host)
- Cookies
- Paramètres
- Types de réponses acceptés
- ...

```
GET / HTTP/1.1
Host: oclock.io
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Encoding: gzip, deflate, sdch, br
Accept-Language: en-US,en;q=0.8,fr;q=0.6,la;q=0.4
Cookie: _ga=GA1.2.123456789.1234567890; _gat=1
```

### Réponse HTTP

En-têtes de réponse :  

- Version du protocole HTTP
- Statut HTTP
- Cookies à définir si besoin
- Types de contenu envoyé
- ...

Le corps de la réponse se situe en-dessous (un saut de ligne les sépare)

```
HTTP/1.1 200 OK
Date: Sun, 16 Apr 2017 10:35:54 GMT
Server: Apache/2.4.10 (Debian)
Content-Type: text/html; charset=utf-8
Vary: Accept-Encoding
Content-Encoding: gzip
Content-Length: 5125
Keep-Alive: timeout=5, max=87
Connection: Keep-Alive

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
    <p>Ceci est une page :)</p>
	</body>
</html>
```

## Méthodes HTTP et leur sens

Lors de la requête, le client précise la méthode utilisée. Cette méthode précise comment sont envoyées les informations au serveur et ce que le client souhaite faire grâce à cette requête

| Méthode      | Sens                                 |
| ------------ | ------------------------------------ |
| GET          | Lire des données                     |
| POST         | Insérer des données                  |
| PUT or PATCH | Mettre à jour ou insérer des données |
| DELETE       | Supprimer des données                |

En réalité, la plupart des navigateurs actuels ne supportent que POST et GET dans les formulaires HTML. Les autres sont utilisés avec [XMLHttpRequest](https://en.wikipedia.org/wiki/XMLHttpRequest).

## Pour plus d'infos

- Toutes les bases du protocole HTTP sur [Wikipedia](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol)
- Liste de tous les codes de réponse HTTP sur [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
- Liste des en-têtes HTTP sur [MDN](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers)
